//
//  basecamp_ios_sdkTests.m
//  basecamp-ios-sdkTests
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "basecamp_ios_sdkTests.h"
#import "BCAccount.h"
#import "BCProject.h"
#import "BCTodoList.h"
#import "BCTodoListItem.h"
#import "BCMilestone.h"
#import "BCCompany.h"
#import "BCMessage.h"
#import "BCComment.h"
#import "BCWebSession.h"
@implementation basecamp_ios_sdkTests
@synthesize finished,error;
- (void)setUp
{
    finished = NO;
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}
//-(void) testWebSessionAuthentication
//{
//    finished = NO;
//    BCAccount *testAccount = [[BCAccount alloc] initWithAccountURL:TEST_ACCOUNT_URL username:TEST_USER_NAME password:TEST_PASSWORD];
//    BCWebSession *webSession = [[BCWebSession alloc] initWithAccount:testAccount];
//    [webSession loadWebSessionWithCompletionHandler:^(BCWebSession *_webSession,NSError *_error){
//        error = _error;
//        finished = YES;
//    }];
//    while(!finished)
//    {
//        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
//    }
//    if (error != nil) {
//        NSLog(@"Error: %d - %@",[error code],[error localizedDescription]);
//        STFail(@"Unable to List Companies");
//    }
//    [testAccount release];
//    [webSession release];
//}
-(void) testCompanies
{
    finished = NO;
    BCAccount *testAccount = [[BCAccount alloc] initWithAccountURL:TEST_ACCOUNT_URL username:TEST_USER_NAME password:TEST_PASSWORD];
    
    [BCCompany loadCompaniesForAccount:testAccount withCompletionHandler:^(NSArray *companies, NSError *_error) {
        error = _error;
        if(error == nil)
        {
            for(BCCompany *company in companies)
            {
                NSLog(@"Company: %@",company.name);
            }
        }
        finished = YES;
    }];
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    if (error != nil) {
        NSLog(@"Error: %d - %@",[error code],[error localizedDescription]);
        STFail(@"Unable to List Companies");
    }
    [testAccount release];
}

-(void)testComments
{
    finished = NO;
    BCAccount *testAccount = [[BCAccount alloc] initWithAccountURL:TEST_ACCOUNT_URL username:TEST_USER_NAME password:TEST_PASSWORD];
    BCMessage *message = [[BCMessage alloc] initWithAccount:testAccount];
    message.messageID = [NSNumber numberWithInt:TEST_COMMENT_MESSAGE_ID];
    [BCComment loadCommentsForMessage:message withCompletionHandler:^(NSArray *comments, NSString *continuedAt, NSError *_error) {
        error = _error;
        if(error  == nil)
        {
            NSLog(@"Continued At:%@",continuedAt);
            for(BCComment *comment in comments)
            {
                NSLog(@"Comment %d Body: %@ -- %@",[comment.commentID integerValue],comment.body,comment.createdAt);
            }
        }
        finished = YES;
    }];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    if (error != nil) {
        NSLog(@"Error: %d - %@",[error code],[error localizedDescription]);
        STFail(@"Unable to List Companies");
    }
    
    [message release];
    [testAccount release];
}

-(void) testMilestones
{
    finished = NO;
    BCAccount *testAccount = [[BCAccount alloc] initWithAccountURL:TEST_ACCOUNT_URL username:TEST_USER_NAME password:TEST_PASSWORD];
    
    [BCProject loadProject:TEST_PROJECT_ID forAccount:testAccount withCompletionHandler:^(BCProject *_project, NSError *_error) {
        error = _error;
        if(_error == nil)
        {
            [BCMilestone loadMilestonesForProject:_project
             withCompletionHandler:^(NSArray *milestones, NSError *__error) {
                 error = __error;
                 if(__error == nil)
                 {
                     for(BCMilestone *_milestone in milestones)
                     {
                         NSLog(@"Found Project Milestone: %@",_milestone.title);
                     }
                 }
                 finished = YES;
             }];
        }
        else
        {
            finished = YES;
        }
    }];
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    if (error != nil) {
        NSLog(@"Error: %d - %@",[error code],[error localizedDescription]);
        STFail(@"Unable to List Milestones");
    }
    [testAccount release];   
}
-(void) testCreateMilestone
{
    finished = NO;
    BCAccount *testAccount = [[BCAccount alloc] initWithAccountURL:TEST_ACCOUNT_URL username:TEST_USER_NAME password:TEST_PASSWORD];
    
    [BCProject loadProject:TEST_PROJECT_ID forAccount:testAccount withCompletionHandler:^(BCProject *_project, NSError *_error) {
        if(_error == nil)
        {
            BCMilestone *newMilestone = [[[BCMilestone alloc] initWithProject:_project] autorelease];
            newMilestone.title = @"Basecampe SDK Milestone";
            newMilestone.deadline = [NSDate date];
            [newMilestone saveWithCompletionHandler:^(BCMilestone *milestone, NSError *_error) {
                error = _error;
                if(error == nil)
                {
                    NSLog(@"Milestone Created Successfully: %d!",[[newMilestone milestoneID] integerValue]);
                    finished =YES;
                }
                else
                {
                    finished = YES;
                }
                        
            } notify:YES];
        }
    }];
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    if (error != nil) {
        NSLog(@"Error: %d - %@",[error code],[error localizedDescription]);
        STFail(@"Unable to Create Todolist");
    }
    [testAccount release];
}
-(void) testCreateTodoList
{
    finished = NO;
    BCAccount *testAccount = [[BCAccount alloc] initWithAccountURL:TEST_ACCOUNT_URL username:TEST_USER_NAME password:TEST_PASSWORD];

    [BCProject loadProject:TEST_PROJECT_ID forAccount:testAccount withCompletionHandler:^(BCProject *_project, NSError *_error) {
        if(_error == nil)
        {
            NSLog(@"Project Loaded!");
            BCTodoList *newTodoList = [[[BCTodoList alloc] initWithProject:_project] autorelease];
            newTodoList.name = [NSString stringWithFormat:@"Basecamp-ios-test"];
            newTodoList.description = [NSString stringWithFormat:@"Running test scenarios trying to createa  todo list"];
            [newTodoList saveWithCompletionHandler:^(BCTodoList *_todoList, NSError *_error) {
                error = _error;
                if(error == nil)
                {
                    _todoList.description = [NSString stringWithFormat:@"Testing the Update Method of the Basecamp API"];
                    [_todoList saveWithCompletionHandler:^(BCTodoList *__todoList, NSError *_error) {
                        error = _error;
                        if(error == nil)
                        {
                            BCTodoListItem *_newTodoListItem = [[[BCTodoListItem alloc] initWithTodoList:__todoList] autorelease];
                            _newTodoListItem.content = @"Add the ability to create items";
                            [_newTodoListItem saveWithCompletionHandler:^(BCTodoListItem *_todoListItem, NSError *_error) {
                                error = _error;
                                if(error == nil)
                                {
                                        [_todoListItem markCompleteWithCompletionHandler:^(BCTodoListItem *todoListItem, NSError *_error) {
                                            error = _error;
                                            if(error == nil)
                                            {
                                                BCTodoListItem *_deletedTodoItem = [[[BCTodoListItem alloc] initWithTodoList:todoListItem.todoList] autorelease];
                                                _deletedTodoItem.content = @"Please Delete Me!";
                                                [_deletedTodoItem saveWithCompletionHandler:^(BCTodoListItem *__todoListItem, NSError *__error) {
                                                    error = __error;
                                                    if(__error == nil)
                                                    {
                                                        [__todoListItem destroyWithCompletionHandler:^(BCTodoListItem *todoListItem, NSError *__error) {
                                                            error = __error;
                                                            
                                                            finished = YES;
                                                        }];
                                                    }
                                                    else
                                                    {
                                                        
                                                        finished = YES;
                                                    }
                                                } notify:NO];
                                                
                                            }
                                            else {
                                                finished = YES;
                                            }
                                        }];
                                        
                                }
                                else
                                {
                                    finished = YES;
                                }
                            } notify:YES];

                        }
                        else
                        {
                            finished = YES;
                        }

                    }];
                    
                }
                else
                {
                    finished = YES;
                }

            }];

        }
    }];

    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    if (error != nil) {
        NSLog(@"Error: %d - %@",[error code],[error localizedDescription]);
        STFail(@"Unable to Create Todolist");
    }
    [testAccount release];

}
//-(void) testCreateProject
//{
//    finished = NO;
//    BCAccount *testAccount = [[BCAccount alloc] initWithAccountURL:TEST_ACCOUNT_URL username:TEST_USER_NAME password:TEST_PASSWORD];
//    
//    BCProject *newProject = [[BCProject alloc] initWithAccount:testAccount];
//    newProject.name = @"Basecamp IOS SDK";
//    
//    [newProject createWithCompletionHandler:^(BCProject *project,NSError *_error)
//     {
//         if(project != nil && error == nil)
//         {
//            NSLog(@"%d - Project: %@ - %@",[project.projectID integerValue],project.name,project.status);
//         }
//
//        finished = YES;
//     }];
//    while(!finished)
//    {
//        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
//    }
//    if (error != nil) {
//        NSLog(@"Error: %d - %@",[error code],[error localizedDescription]);
//        STFail(@"Unable to perform Connection");
//    }
//    [newProject release];
//        [testAccount release];
//}
- (void)testProjects
{
    finished = NO;
    BCAccount *testAccount = [[BCAccount alloc] initWithAccountURL:TEST_ACCOUNT_URL username:TEST_USER_NAME password:TEST_PASSWORD];

    [BCProject loadProjectsForAccount:testAccount withCompletionHandler:^(NSArray *projects,NSError *_error)
     {
         self.error = _error;
         if(projects != nil)
         {
             for(BCProject *_project in projects)
             {
                 NSLog(@"%d - Project: %@ - %@",[_project.projectID integerValue],_project.name,_project.status);
             }

         }
         finished = YES;
         
//        STAssertTrue(false,@"Unit tests are not implemented yet in basecamp-ios-sdkTests");         
     }];

    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    if (error != nil) {
        NSLog(@"Error: %d - %@",[error code],[error localizedDescription]);
        STFail(@"Unable to perform Connection");
    }
        [testAccount release];
}
-(void)testTodoList
{
    finished = NO;
    BCAccount *testAccount = [[BCAccount alloc] initWithAccountURL:TEST_ACCOUNT_URL username:TEST_USER_NAME password:TEST_PASSWORD];
    
    [BCTodoList loadTodoListsForAccount:testAccount withCompletionHandler:^(NSArray *todoLists,NSError *_error)
     {
         self.error = _error;
         if(todoLists != nil)
         {
             for(BCTodoList *_todoList in todoLists)
             {
                 NSLog(@"-- %@",_todoList.name);
                 if(_todoList.todoListItems != nil)
                 {
                     for(BCTodoListItem *_todoListItem in _todoList.todoListItems)
                     {
                         if([_todoListItem.completed boolValue])
                         {
                             NSLog(@"---* %@ (%d)",_todoListItem.content,[_todoListItem.commentsCount integerValue]);    
                         }
                         else
                         {
                             NSLog(@"---- %@ (%d)",_todoListItem.content,[_todoListItem.commentsCount integerValue]);    
                         }
                         
                     }
                 }
             }
             
         }
         finished = YES;
         
         //        STAssertTrue(false,@"Unit tests are not implemented yet in basecamp-ios-sdkTests");         
     }];
    
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    if (error != nil) {
        NSLog(@"Error: %d - %@",[error code],[error localizedDescription]);
        STFail(@"Unable to perform Connection");
    }
    [testAccount release];
}
-(void)testTodoListItems
{
    finished = NO;
    BCAccount *testAccount = [[BCAccount alloc] initWithAccountURL:TEST_ACCOUNT_URL username:TEST_USER_NAME password:TEST_PASSWORD];
    BCTodoList *_todoList = [[BCTodoList alloc] initWithAccount:testAccount];
    _todoList.todoListID = [NSNumber numberWithInteger:TEST_TODOLIST_ID];
    [BCTodoListItem loadTodoListItemsForTodoList:_todoList withCompletionHandler:^(NSArray *todoListItems, NSError *_error) {
        error = _error;
        if(_error == nil)
        {
            for(BCTodoListItem *todoListItem in todoListItems)
            {
                NSLog(@"==== %@ (%d)",todoListItem.content,[todoListItem.commentsCount integerValue]);    
            }

        }
        finished = YES;
    }];
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    if (error != nil) {
        NSLog(@"Error: %d - %@",[error code],[error localizedDescription]);
        STFail(@"Unable to perform Connection");
    }
    [_todoList release];
    [testAccount release];
}
//-(void)testProject
//{
//        finished = NO;
//    BCAccount *testAccount = [[BCAccount alloc] initWithAccountURL:TEST_ACCOUNT_URL username:TEST_USER_NAME password:TEST_PASSWORD];
//    
//    [BCProject loadProject:TEST_PROJECT_ID forAccount:testAccount withCompletionHandler:^(BCProject *project,NSError *_error)
//     {
//         self.error = _error;
//         if(project != nil)
//         {
//                 NSLog(@"%d - Project: %@ - %@",[project.projectID integerValue],project.name,project.status);
//
//         }
//         finished = YES;
//         //        STAssertTrue(false,@"Unit tests are not implemented yet in basecamp-ios-sdkTests");         
//     }];
//    
//    
//    while(!finished)
//    {
//        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
//    }
//    if (error != nil) {
//        NSLog(@"Error: %d - %@",[error code],[error localizedDescription]);
//        STFail(@"Unable to perform Connection");
//    }
//        [testAccount release];
//}

@end
