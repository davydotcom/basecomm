//
//  basecamp_ios_sdkTests.h
//  basecamp-ios-sdkTests
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#define TEST_ACCOUNT_URL @"redwindsw"
#define TEST_USER_NAME @"davydotcom"
//#define TEST_USER_NAME @"47c06cf6480ad6df55f71f54eaac5ac5f083347"
//#define TEST_PASSWORD @"X"
#define TEST_PASSWORD @"rockstar"
#define TEST_PROJECT_ID 5794443
#define TEST_TODOLIST_ID 13165329
#define TEST_COMMENT_MESSAGE_ID 47215083
@class BCProject;
@class BCTodoList;
@interface basecamp_ios_sdkTests : SenTestCase {
    BOOL finished;
    NSError *error;
    BCTodoList *todoList;
@private
    
}
@property(nonatomic,retain) NSError *error;
@property(nonatomic,assign) BOOL finished;
@end
