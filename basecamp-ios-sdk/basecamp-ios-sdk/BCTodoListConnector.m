//
//  BCTodoListConnector.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/3/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCTodoListConnector.h"
#import "BCProject.h"
#import "BCAccount.h"
#import "BCTodoList.h"
#import "BCTodoListItem.h"
#import "TouchXML.h"
@implementation BCTodoListConnector
@synthesize project;


-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(objectID > 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/todo_lists/%d.xml",[self.account requestURL],objectID];           
    } 
    else if(project != nil)
    {
      requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/todo_lists.xml",[self.account requestURL],[project.projectID integerValue]];     
    }
    else 
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/todo_lists.xml",[self.account requestURL]];   
    }
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:NO];
    
    [request release];
    [connection release];
    [autoreleasepool release];
}

#pragma mark -
#pragma mark TouchXML Methods
-(void) parseXMLResponse
{
    //    NSString *responseString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    //    NSLog(responseString);
    //    [responseString release];
    CXMLDocument *doc = [[CXMLDocument alloc] initWithData:requestData options:0 error:nil];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
    [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSArray *nodes = nil;
    NSError *_error = nil;
    @try
    {
        nodes = [doc nodesForXPath:@"//todo-list" error:&_error];
        if(_error != nil)
        {
            NSLog(@"Error Parsing XML: %d",[_error code]);
            [doc release];
            return;
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception %@",[e name]);
    }
    NSMutableArray *_todoLists = [[NSMutableArray alloc] initWithCapacity:1];
    for(CXMLElement *node in nodes)
    {
        BCTodoList *_todoList = [[BCTodoList alloc] initWithAccount:self.account];
        
        for (CXMLElement *childNode in [node children]) 
        {
            if([[childNode name] isEqualToString:@"id"])
            {
                NSNumber *_todoListID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _todoList.todoListID = _todoListID;
                [_todoListID release];
            }
            if([[childNode name] isEqualToString:@"project-id"])
            {
                NSNumber *_projectID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                if(self.project != nil && [_projectID integerValue] == [self.project.projectID integerValue])
                {
                    _todoList.project = self.project;
                }
                _todoList.projectID = _projectID;
                [_projectID release];
            }
            if([[childNode name] isEqualToString:@"milestone-id"])
            {
                NSNumber *_milestoneID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _todoList.milestoneID = _milestoneID;
                [_milestoneID release];
            }
            else if([[childNode name] isEqualToString:@"name"])
            {
                _todoList.name = [childNode stringValue];
            }

            else if([[childNode name] isEqualToString:@"description"])
            {
                _todoList.description = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"completed-count"])
            {
                NSNumber *completedCount = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _todoList.completedCount = completedCount;
                [completedCount release];
            }
            else if([[childNode name] isEqualToString:@"uncompleted-count"])
            {
                NSNumber *uncompletedCount = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _todoList.uncompletedCount = uncompletedCount;
                [uncompletedCount release];
            }
            if([[childNode name] isEqualToString:@"position"])
            {
                NSNumber *_position =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _todoList.position = _position;
                [_position release];
            }
            else if([[childNode name] isEqualToString:@"private"])
            {
                NSNumber *isPrivate = nil;
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    isPrivate = [[NSNumber alloc] initWithBool:YES];
                }
                else
                {
                    isPrivate = [[NSNumber alloc] initWithBool:NO];
                }
                _todoList.isPrivate = isPrivate;
                [isPrivate release];
            }
            else if([[childNode name] isEqualToString:@"tracked"])
            {
                NSNumber *tracked = nil;
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    tracked = [[NSNumber alloc] initWithBool:YES];
                }
                else
                {
                    tracked = [[NSNumber alloc] initWithBool:NO];
                }
                _todoList.tracked = tracked;
                [tracked release];
            }
            else if([[childNode name] isEqualToString:@"complete"])
            {
                NSNumber *complete = nil;
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    complete = [[NSNumber alloc] initWithBool:YES];
                }
                else
                {
                    complete = [[NSNumber alloc] initWithBool:NO];
                }
                _todoList.complete = complete;
                [complete release];
            }
            
            else if([[childNode name] isEqualToString:@"todo-items"])
            {

                if(_todoList.todoListItems == nil)
                {
                    NSMutableArray *todoListItems = [[NSMutableArray alloc] initWithCapacity:1];
                    _todoList.todoListItems = todoListItems;
                    [todoListItems release];
                }
                for(CXMLNode *itemNode in [childNode children])
                {

                    if([[itemNode name] isEqualToString:@"todo-item"])
                    {

                        BCTodoListItem *todoListItem = [[BCTodoListItem alloc] initWithTodoList:_todoList];
                        for(CXMLNode *itemFieldNode in [itemNode children])
                        {
                            if([[itemFieldNode name] isEqualToString:@"comments-count"])
                            {
                                NSNumber *_commentsCount = [[NSNumber alloc] initWithInt:[[itemFieldNode stringValue] integerValue]];
                                todoListItem.commentsCount = _commentsCount;
                                [_commentsCount release];
                            }
                            else if([[itemFieldNode name] isEqualToString:@"completed"])
                            {
                                NSNumber *complete = nil;
                                if([[itemFieldNode stringValue] isEqualToString:@"true"])
                                {
                                    complete = [[NSNumber alloc] initWithBool:YES];
                                }
                                else
                                {
                                    complete = [[NSNumber alloc] initWithBool:NO];
                                }
                                todoListItem.completed = complete;
                                [complete release];
                            }
                            else if([[itemFieldNode name] isEqualToString:@"content"])
                            {
                                todoListItem.content = [itemFieldNode stringValue];
                            }
                            else if([[itemFieldNode name] isEqualToString:@"creator-id"])
                            {
                                NSNumber *_creatorID = [[NSNumber alloc] initWithInt:[[itemFieldNode stringValue] integerValue]];
                                todoListItem.creatorID = _creatorID;
                                [_creatorID release];
                            }
                            else if([[itemFieldNode name] isEqualToString:@"created-at"])
                            {
                                NSDate *createdAt = [xmlDateTimeFormatter dateFromString:[itemFieldNode stringValue]];
                                todoListItem.createdAt = createdAt;
                            }
                            else if([[itemFieldNode name] isEqualToString:@"due-at"])
                            {
                                NSDate *dueAt = [xmlDateTimeFormatter dateFromString:[itemFieldNode stringValue]];
                                todoListItem.dueAt = dueAt;
                            }
                            else if([[itemFieldNode name] isEqualToString:@"id"])
                            {
                                NSNumber *_todoListItemID = [[NSNumber alloc] initWithInt:[[itemFieldNode stringValue] integerValue]];
                                todoListItem.todoListItemID = _todoListItemID;
                                [_todoListItemID release];
                            }
                            else if([[itemFieldNode name] isEqualToString:@"position"])
                            {
                                NSNumber *_numberValue = [[NSNumber alloc] initWithInt:[[itemFieldNode stringValue] integerValue]];
                                todoListItem.position = _numberValue;
                                [_numberValue release];
                            }
                            else if([[itemFieldNode name] isEqualToString:@"responsible-party-id"])
                            {
                                NSNumber *_numberValue = [[NSNumber alloc] initWithInt:[[itemFieldNode stringValue] integerValue]];
                                todoListItem.responsiblePartyID = _numberValue;
                                [_numberValue release];
                            }
                            else if([[itemFieldNode name] isEqualToString:@"responsible-party-name"])
                            {
                                todoListItem.responsiblePartyName = [itemFieldNode stringValue];
                            }
                            else if([[itemFieldNode name] isEqualToString:@"responsible-party-type"])
                            {
                                todoListItem.responsiblePartyType = [itemFieldNode stringValue];
                            }
                            else if([[itemFieldNode name] isEqualToString:@"todo-list-id"])
                            {
                                NSNumber *_numberValue = [[NSNumber alloc] initWithInt:[[itemFieldNode stringValue] integerValue]];
                                todoListItem.todoListID = _numberValue;
                                todoListItem.todoList = _todoList;
                                [_numberValue release];
                            }
                            else if([[itemFieldNode name] isEqualToString:@"creator-name"])
                            {
                                todoListItem.creatorName = [itemFieldNode stringValue];
                            }
                            
                        }
                        [_todoList.todoListItems addObject:todoListItem];
                        [todoListItem release];
                    }
                    
                }

            }
            
        }
        
        [_todoLists addObject:_todoList];
        [_todoList release];
    }
    objects = _todoLists;
    [doc release];
    [xmlDateFormatter release];
    [xmlDateTimeFormatter release];
}

#pragma mark -
#pragma mark Main Thread Methods

-(void) loadObjectsForProject:(BCProject*)_project
{
    [self retain];
    self.project = _project;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];   
}


#pragma mark -
#pragma mark Memory Management Methods
-(void) dealloc
{
    [project release];
    [super dealloc];
}
@end
