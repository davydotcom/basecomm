//
//  BCTodoListItemConnector.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/4/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCTodoListItemConnector.h"
#import "BCAccount.h"
#import "BCTodoList.h"
#import "BCTodoListItem.h"
#import "BCProject.h"
#import "TouchXML.h"
@implementation BCTodoListItemConnector
@synthesize todoList;

-(id) initWithTodoList:(BCTodoList *)_todoList andCompletionHandler:(void (^)(NSArray *todoListItems,NSError *error)) _completionHandler
{
    if((self = [super initWithAccount:_todoList.account andCompletionHandler:_completionHandler]))
    {
        self.todoList = _todoList;
    }
    return self;
}
-(id) initWithTodoList:(BCTodoList *)_todoList andObjectCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler
{
    if((self = [super initWithAccount:_todoList.account andObjectCompletionHandler:_completionHandler]))
    {
        self.todoList = _todoList;
    }
    return self;
}

-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(self.account == nil)
    {
        self.account = self.todoList.account;
    }
    if(objectID > 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/todo_items/%d.xml",[self.account requestURL],objectID];           
    } 
    else
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/todo_lists/%d/todo_items.xml",[self.account requestURL],[todoList.todoListID integerValue]];     
    }

    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:NO];
    
    [request release];
    [connection release];
    [autoreleasepool release];
}


#pragma mark -
#pragma mark TouchXML Methods
-(void) parseXMLResponse
{
//    NSString *responseString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
//    NSLog(responseString);
//    [responseString release];
    CXMLDocument *doc = [[CXMLDocument alloc] initWithData:requestData options:0 error:nil];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
    [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSArray *nodes = nil;
    NSError *_error = nil;
    @try
    {
        nodes = [doc nodesForXPath:@"//todo-item" error:&_error];
        if(_error != nil)
        {
            NSLog(@"Error Parsing XML: %d",[_error code]);
            [doc release];
            return;
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception %@",[e name]);
    }
    NSMutableArray *_todoListItems = [[NSMutableArray alloc] initWithCapacity:1];
    for(CXMLElement *node in nodes)
    {
        BCTodoListItem *_todoListItem = [[BCTodoListItem alloc] initWithTodoList:todoList];
        
        for (CXMLElement *childNode in [node children]) 
        {
            if([[childNode name] isEqualToString:@"comments-count"])
            {
                NSNumber *_commentsCount = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _todoListItem.commentsCount = _commentsCount;
                [_commentsCount release];
            }
            else if([[childNode name] isEqualToString:@"completed"])
            {
                NSNumber *complete = nil;
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    complete = [[NSNumber alloc] initWithBool:YES];
                }
                else
                {
                    complete = [[NSNumber alloc] initWithBool:NO];   
                }
                _todoListItem.completed = complete;
                [complete release];
            }
            else if([[childNode name] isEqualToString:@"content"])
            {
                _todoListItem.content = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"creator-id"])
            {
                NSNumber *_creatorID = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _todoListItem.creatorID = _creatorID;
                [_creatorID release];
            }
            else if([[childNode name] isEqualToString:@"created-at"])
            {
                NSDate *createdAt = [xmlDateTimeFormatter dateFromString:[childNode stringValue]];
                _todoListItem.createdAt = createdAt;
            }
            else if([[childNode name] isEqualToString:@"due-at"])
            {
                NSDate *dueAt = [xmlDateTimeFormatter dateFromString:[childNode stringValue]];
                _todoListItem.dueAt = dueAt;
            }
            else if([[childNode name] isEqualToString:@"id"])
            {
                NSNumber *_todoListItemID = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _todoListItem.todoListItemID = _todoListItemID;
                [_todoListItemID release];
            }
            else if([[childNode name] isEqualToString:@"position"])
            {
                NSNumber *_numberValue = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _todoListItem.position = _numberValue;
                [_numberValue release];
            }
            else if([[childNode name] isEqualToString:@"responsible-party-id"])
            {
                NSNumber *_numberValue = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _todoListItem.responsiblePartyID = _numberValue;
                [_numberValue release];
            }
            else if([[childNode name] isEqualToString:@"responsible-party-name"])
            {
                _todoListItem.responsiblePartyName = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"responsible-party-type"])
            {
                _todoListItem.responsiblePartyType = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"todo-list-id"])
            {
                NSNumber *_numberValue = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _todoListItem.todoListID = _numberValue;
                [_numberValue release];
            }
            else if([[childNode name] isEqualToString:@"creator-name"])
            {
                _todoListItem.creatorName = [childNode stringValue];
            }
        }
        
        [_todoListItems addObject:_todoListItem];
        [_todoListItem release];
    }
    objects = _todoListItems;
    [doc release];
    [xmlDateFormatter release];
    [xmlDateTimeFormatter release];
}



#pragma mark -
#pragma mark Memory Management Methods
-(void) dealloc
{
    [todoList release];
    [super dealloc];
}

@end
