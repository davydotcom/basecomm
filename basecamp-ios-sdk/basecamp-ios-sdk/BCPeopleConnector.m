//
//  BCPeopleConnector.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCPeopleConnector.h"
#import "BCProject.h"
#import "BCCompany.h"
#import "BCPerson.h"
#import "TouchXML.h"
@implementation BCPeopleConnector
@synthesize project,company,me;

-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(objectID > 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/people/%d.xml",[self.account requestURL],objectID];           
    }
    else if(me == YES)
    {
       requestString = [[NSString alloc] initWithFormat:@"%@/me.xml",[self.account requestURL]];  
    }
    else if(project != nil)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/people.xml",[self.account requestURL],[project.projectID integerValue]];     
    }
    else if(company != nil)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/companies/%d/people.xml",[self.account requestURL],[company.companyID integerValue]];     
    }
    else 
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/people.xml",[self.account requestURL]];   
    }
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:NO];
    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void) loadObjectsForProject:(BCProject*)_project
{
    [self retain];
    self.project = _project;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];   
}
-(void) loadObjectsForCompany:(BCCompany*)_company
{
    [self retain];
    self.company = _company;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];   
}
-(void) loadMe
{
    [self retain];
    sourceThread = [NSThread currentThread];
    me = YES;
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];  
}
#pragma mark -
#pragma mark TouchXML Methods
-(void) parseXMLResponse
{
    CXMLDocument *doc = [[CXMLDocument alloc] initWithData:requestData options:0 error:nil];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
    [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSArray *nodes = nil;
    NSError *_error = nil;
    @try
    {
        nodes = [doc nodesForXPath:@"//person" error:&_error];
        if(_error != nil)
        {
            NSLog(@"Error Parsing XML: %d",[_error code]);
            [doc release];
            return;
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception %@",[e name]);
    }
    NSMutableArray *_objects = [[NSMutableArray alloc] initWithCapacity:1];
    for(CXMLElement *node in nodes)
    {
        BCPerson *_person = [[BCPerson alloc] init];
        _person.account = self.account;
        _person.project = self.project;
        _person.company = self.company;
        for (CXMLElement *childNode in [node children]) 
        {
            if([[childNode name] isEqualToString:@"id"])
            {
                NSNumber *_personID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _person.personID = _personID;
                [_personID release];
            }
            else if([[childNode name] isEqualToString:@"first-name"])
            {
                _person.firstName = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"last-name"])
            {
                _person.lastName = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"title"])
            {
                _person.title = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"email-address"])
            {
                _person.emailAddress = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"im-handle"])
            {
                _person.imHandle = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"im-service"])
            {
                _person.imService = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"avatar-url"])
            {
                _person.avatarURL = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"phone-number-office"])
            {
                _person.phoneNumberOffice = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"phone-number-fax"])
            {
                _person.phoneNumberFax = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"phone-number-office-ext"])
            {
                _person.phoneNumberOfficeExt = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"phone-number-home"])
            {
                _person.phoneNumberHome = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"phone-number-mobile"])
            {
                _person.phoneNumberMobile = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"user-name"])
            {
                _person.userName = [childNode stringValue];
            }
           

            else if([[childNode name] isEqualToString:@"administrator"])
            {
                NSNumber *administrator = nil;
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    administrator = [[NSNumber alloc] initWithBool:YES];
                }
                _person.administrator = administrator;
                [administrator release];
            }
            else if([[childNode name] isEqualToString:@"deleted"])
            {
                NSNumber *deleted = nil;
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    deleted = [[NSNumber alloc] initWithBool:YES];
                }
                _person.deleted = deleted;
                [deleted release];
            }
            else if([[childNode name] isEqualToString:@"has-access-to-new-projects"])
            {
                NSNumber *hasAccess = nil;
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    hasAccess = [[NSNumber alloc] initWithBool:YES];
                }
                _person.hasAccessToNewProjects = hasAccess;
                [hasAccess release];
            }
            else if([[childNode name] isEqualToString:@"company-id"])
            {
                NSNumber *companyID = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _person.companyID = companyID;
                [companyID release];
            }
            else if([[childNode name] isEqualToString:@"client-id"])
            {
                NSNumber *clientID = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _person.clientID = clientID;
                [clientID release];
            }
            else if([[childNode name] isEqualToString:@"last-login"])
            {
                
                NSDate *lastLogin = [xmlDateTimeFormatter dateFromString:[childNode stringValue]];
                _person.lastLogin = lastLogin;
                
            }
            
        }
        
        [_objects addObject:_person];
        [_person release];
    }
    objects = _objects;
    [doc release];
    [xmlDateFormatter release];
    [xmlDateTimeFormatter release];
}
#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    [project release];
    [company release];
    [super dealloc];
}
@end
