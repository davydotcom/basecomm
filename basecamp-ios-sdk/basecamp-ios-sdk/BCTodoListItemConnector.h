//
//  BCTodoListItemConnector.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/4/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCConnector.h"
@class BCAccount;
@class BCTodoList;
@class BCTodoListItem;
@interface BCTodoListItemConnector : BCConnector {

    BCTodoList *todoList;
}

@property(nonatomic,retain) BCTodoList *todoList;


-(id) initWithTodoList:(BCTodoList *)_todoList andCompletionHandler:(void (^)(NSArray *todoListItems,NSError *error)) _completionHandler;
-(id) initWithTodoList:(BCTodoList *)_todoList andObjectCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler;
-(void)runThread;

-(void) parseXMLResponse;
@end
