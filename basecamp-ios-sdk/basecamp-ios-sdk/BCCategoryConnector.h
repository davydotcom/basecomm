//
//  BCCategoryConnector.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCConnector.h"
@class BCProject;
@interface BCCategoryConnector : BCConnector {
    BCProject *project;
    NSString *type;
}
@property(nonatomic,retain) BCProject *project;
@property(nonatomic,retain) NSString *type;
-(void)runThread;
-(void) parseXMLResponse;
-(void) loadObjectsForProject:(BCProject*)_project;
-(void) loadObjectsForProject:(BCProject*)_project withType:(NSString*)_type;
@end
