//
//  BCTimeEntry.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 6/10/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCTimeEntry.h"
#import "BCAccount.h"
#import "BCProject.h"
#import "BCTodoListItem.h"
#import "BCTimeEntryConnector.h"
#import "TouchXML.h"
@implementation BCTimeEntry
@synthesize account,project,todoListItemID,todoListItem,timeEntryID,projectID,personID,date,hours,description,deleted;

+(void)loadTimeEntriesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *timeEntries,NSError *error)) completionHandler
{
    BCTimeEntryConnector *timeEntryConnector = [[BCTimeEntryConnector alloc] initWithAccount:_project.account andCompletionHandler:completionHandler];
    [timeEntryConnector loadObjectsForProject:_project];
    [timeEntryConnector release];
}
+(void)loadTimeEntriesForTodoListItem:(BCTodoListItem *)_todoListItem withCompletionHandler:(void (^)(NSArray *timeEntries,NSError *error)) completionHandler
{
    BCTimeEntryConnector *timeEntryConnector = [[BCTimeEntryConnector alloc] initWithAccount:_todoListItem.account andCompletionHandler:completionHandler];
    [timeEntryConnector loadObjectsForTodoListItem:_todoListItem];
    [timeEntryConnector release];    
}
+(void)loadTimeEntry:(NSInteger)_timeEntryID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCTimeEntry *timeEntry,NSError *error)) completionHandler
{
    BCTimeEntryConnector *timeEntryConnector = [[BCTimeEntryConnector alloc] initWithAccount:_account andObjectCompletionHandler:completionHandler];
    [timeEntryConnector loadObject:_timeEntryID];
    [timeEntryConnector release];        
}


-(void)createWithCompletionHandler:(void (^)(BCTimeEntry *timeEntry,NSError *error))_completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)saveWithCompletionHandler:(void (^)(BCTimeEntry *timeEntry,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)destroyWithCompletionHandler:(void (^)(BCTimeEntry *timeEntry,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    
    [NSThread detachNewThreadSelector:@selector(destroyThread) toTarget:self withObject:nil];
    
}
#pragma mark Threaded Methods
-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(self.todoListItem != nil && [self.todoListItemID integerValue] == 0)
    {
        self.todoListItemID = self.todoListItem.todoListItemID;
    }
    if(self.project != nil && [self.projectID integerValue] == 0)
    {
        self.projectID = self.project.projectID;
    }
        
    if([timeEntryID integerValue] != 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/time_entries/%d.xml",[self.account requestURL],[timeEntryID integerValue]];           
    } 
    else if([self.todoListItemID integerValue] != 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/todo_items/%d/time_entries.xml",[self.account requestURL],[todoListItemID integerValue]];   
        
    }
    else if([self.projectID integerValue] != 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/time_entries.xml",[self.account requestURL],[projectID integerValue]];   
        
    }
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    if([timeEntryID integerValue] != 0)
    {
        [request setHTTPMethod:@"PUT"];
    }
    else
    {    
        [request setHTTPMethod:@"POST"];        
    }
    [request setHTTPBody:[self toRequestXML]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    if(error == nil && self.timeEntryID != nil)
    {
        finished = NO;
        [BCTimeEntry loadTimeEntry:[self.timeEntryID integerValue] forAccount:self.account withCompletionHandler:^(BCTimeEntry *_timeEntry, NSError *_error) {
            if(_timeEntry != nil)
            {
                self.description = _timeEntry.description;
                self.date = _timeEntry.date;
                self.personID = _timeEntry.personID;
                self.todoListItemID = _timeEntry.todoListItemID;
                self.projectID = _timeEntry.projectID;
                self.hours = _timeEntry.hours;
            }
            if(_error != nil)
            {
                error = [_error copy];
            }
            finished = YES;
        }];
    }
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    NSLog(@"Running Thread!");
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void)destroyThread
{
    finished = NO;
    deleting = YES;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    
    
    requestString = [[NSString alloc] initWithFormat:@"%@/time_entries/%d",[self.account requestURL],[timeEntryID integerValue]];           
    
    
    
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    
    [request setHTTPMethod:@"DELETE"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void) performCompletionHandlerOnSourceThread
{
    if(error != nil)
    {
        [error autorelease];
    }
    if(completionHandler != nil)
    {
        void (^_completionHandler)(BCTimeEntry *timeEntry,NSError *error) = completionHandler;
        completionHandler = nil;
        _completionHandler(self,error);
        [_completionHandler release];
    }
    [self release];
}
#pragma mark -
#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
    else if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodHTTPBasic])
    {
        if(authenticationAttempted)
        {
            [[challenge sender] cancelAuthenticationChallenge:challenge];
            return;
        }
        if(account != nil)
        {
            NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:account.username password:account.password persistence:NSURLCredentialPersistenceForSession];
            [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
            [credential release];
            authenticationAttempted = YES;
        }
        
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error
{
    
    error = [_error copy];
    [requestData release];
    finished = YES;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [requestData appendData:data];   
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    statusCode = [urlResponse statusCode];
    NSLog(@"Did Receive Response! %d",statusCode);
    if(statusCode == 201)
    {
        error = nil;
        NSString *location = [[[urlResponse allHeaderFields] valueForKey:@"Location"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *paths = [location componentsSeparatedByString:@"/"];
        NSString *commentIDString = [[paths lastObject] stringByReplacingOccurrencesOfString:@".xml" withString:@""];
        
        NSNumber *_ID = [[NSNumber alloc] initWithInt:[commentIDString integerValue]];
        self.timeEntryID = _ID;
        [_ID release];
    }
    else if (statusCode == 200 && deleting == YES) 
    {
        deleted = YES;
        deleting = NO;
    }
    else if (statusCode != 200)
    {
        error = [[NSError alloc] initWithDomain:@"BCTodoList" code:statusCode userInfo:nil];
    }
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    NSString *requestString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    
    [requestString release];
    [requestData release];
    finished = YES;
}
#pragma mark - XML Methods

-(NSData *)toRequestXML
{
    CXMLDocument *doc = (CXMLDocument *)[CXMLDocument elementWithName: @"time_entry"];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    CXMLElement *personElement = (CXMLElement *)[CXMLNode elementWithName:@"person-id"];
    [personElement setStringValue:[self.personID stringValue]];
    [doc addChild:personElement];

    CXMLElement *dateElement = (CXMLElement *)[CXMLNode elementWithName:@"date"];
    [dateElement setStringValue:[xmlDateFormatter stringFromDate:self.date]];
    [doc addChild:dateElement];
    
    CXMLElement *hoursElement = (CXMLElement *)[CXMLNode elementWithName:@"hours"];
    [hoursElement setStringValue:[self.hours stringValue]];
    [doc addChild:hoursElement];
    CXMLElement *descElement = (CXMLElement *)[CXMLNode elementWithName:@"description"];
    [descElement setStringValue:self.description];
    [doc addChild:descElement];        
    
    if([self.todoListItemID integerValue] != 0)
    {
        CXMLElement *todoItemElement = (CXMLElement *)[CXMLNode elementWithName:@"todo-item-id"];
        [todoItemElement setStringValue:[self.todoListItemID stringValue]];
        [doc addChild:todoItemElement];
    }
    
    NSData *returnData = [[doc XMLString] dataUsingEncoding:NSUTF8StringEncoding];
    
    
    [xmlDateFormatter release];
    return returnData;
    
}

-(void)dealloc 
{
    [account release];
    [project release];
    [todoListItem release];
    [timeEntryID release];
    [personID release];
    [date release];
    [description release];
    [projectID release];
    [todoListItemID release];
    [hours release];
    [super dealloc];
}
@end
