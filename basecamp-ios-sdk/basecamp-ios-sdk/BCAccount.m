//
//  BCAccount.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCAccount.h"
#import "BCSubscription.h"
#import "basecamp.h"
#import "TouchXML.h"
@implementation BCAccount
@synthesize accountURL,username,password,requireSSL;
@synthesize name,accountHolderID,primaryCompanyID,emailNotificationEnabled,timeTrackingEnabled,updatedAt,subscription,defaultPostCategories,defaultAttachmentCategories,accountID;

-(id)initWithAccountURL:(NSString *)_accountURL username:(NSString *)_username password:(NSString *)_password
{
    if((self = [super init]))
    {
        self.password = _password;
        self.username = _username;
        self.accountURL = _accountURL;
        self.requireSSL = YES;
    }
    return self;
}

-(NSString *)requestURL
{
    if(self.requireSSL)
    {
        return [NSString stringWithFormat:@"https://%@.%@",accountURL,BASECAMP_URL];
    }
    return [NSString stringWithFormat:@"http://%@.%@",accountURL,BASECAMP_URL];
}
-(void)loadAccountDetailsWithCompletionHandler:(void (^)(BCAccount *_account,NSError *error)) _completionHandler
{
    completionHandler = [_completionHandler copy];

    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];

}

-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = [[NSString alloc] initWithFormat:@"%@/account.xml",[self requestURL]];   
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:NO];
    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void) performCompletionHandlerOnSourceThread
{
    [error autorelease];
    if(completionHandler != nil)
    {
        void (^_completionHandler)(BCAccount *account,NSError *error) = completionHandler;
        completionHandler = nil;
        _completionHandler(self,error);
        [_completionHandler release];
    }
}
#pragma mark -
#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    if(authenticationAttempted)
    {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }

        NSLog(@"Sending Credentials!");
        NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:self.username password:self.password persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
        [credential release];
        authenticationAttempted = YES;
    
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error
{
    [requestData release];
    [_error retain];
    error = _error;
    finished = YES;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [requestData appendData:data];   
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    statusCode = [urlResponse statusCode];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if(statusCode == 200)
    {
        [self parseXMLResponse];
    }
    else
    {
        NSError *_error = [[NSError alloc] initWithDomain:@"BCAccount" code:statusCode userInfo:nil];
        error = _error;
    }
    finished = YES;
    [requestData release];
}
#pragma mark -
#pragma mark TouchXML Methods
-(void) parseXMLResponse
{
    CXMLDocument *doc = [[CXMLDocument alloc] initWithData:requestData options:0 error:nil];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
    [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSArray *nodes = nil;
    NSError *_error = nil;
    @try
    {
        nodes = [doc nodesForXPath:@"//account" error:&_error];
        if(_error != nil)
        {
            NSLog(@"Error Parsing XML: %d",[_error code]);
            [doc release];
            return;
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception %@",[e name]);
    }

    for(CXMLElement *node in nodes)
    {
        
        for (CXMLElement *childNode in [node children]) 
        {
            if([[childNode name] isEqualToString:@"id"])
            {
                NSNumber *_accountID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                self.accountID = _accountID;
                [_accountID release];
            }
            else if([[childNode name] isEqualToString:@"name"])
            {
                self.name = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"updated-at"])
            {
                
                NSDate *_updatedAt = [xmlDateTimeFormatter dateFromString:[childNode stringValue]];
                self.updatedAt = _updatedAt;
                
            }
            else if([[childNode name] isEqualToString:@"account-holder-id"])
            {
                NSNumber *_number =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                self.accountHolderID = _number;
                [_number release];
            }
            else if([[childNode name] isEqualToString:@"primary-company-id"])
            {
                NSNumber *_number =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                self.primaryCompanyID = _number;
                [_number release];
            }
            else if([[childNode name] isEqualToString:@"email-notification-enabled"])
            {
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    NSNumber *_trueNumber = [[NSNumber alloc] initWithBool:YES];
                    self.emailNotificationEnabled = _trueNumber;
                    [_trueNumber release];
                }
            }
            else if([[childNode name] isEqualToString:@"time-tracking-enabled"])
            {
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    NSNumber *_trueNumber = [[NSNumber alloc] initWithBool:YES];
                    self.timeTrackingEnabled = _trueNumber;
                    [_trueNumber release];
                }
            }
            else if([[childNode name] isEqualToString:@"subscription"])
            {
                BCSubscription *_subscription = [[BCSubscription alloc] init];
                for(CXMLElement *subNode in [childNode children])
                {
                    if([[subNode name] isEqualToString:@"name"])
                    {
                        _subscription.name = [subNode stringValue];
                    }
                    else if([[subNode name] isEqualToString:@"writeboards"])
                    {
                        NSNumber *_number =  [[NSNumber alloc] initWithInt:[[subNode stringValue] integerValue]];
                        _subscription.writeboards = _number;
                        [_number release];
                    }
                    else if([[subNode name] isEqualToString:@"projects"])
                    {
                        NSNumber *_number =  [[NSNumber alloc] initWithInt:[[subNode stringValue] integerValue]];
                        _subscription.projects = _number;
                        [_number release];
                    }
                    else if([[subNode name] isEqualToString:@"storage"])
                    {
                        NSNumber *_number =  [[NSNumber alloc] initWithInt:[[subNode stringValue] integerValue]];
                        _subscription.storage = _number;
                        [_number release]; 
                    }
                    else if([[subNode name] isEqualToString:@"ssl"])
                    {
                        if([[subNode stringValue] isEqualToString:@"true"])
                        {
                            NSNumber *_trueNumber = [[NSNumber alloc] initWithBool:YES];
                            _subscription.ssl = _trueNumber;
                            [_trueNumber release];
                        }
                    }
                    else if([[subNode name] isEqualToString:@"time-tracking"])
                    {
                        if([[subNode stringValue] isEqualToString:@"true"])
                        {
                            NSNumber *_trueNumber = [[NSNumber alloc] initWithBool:YES];
                            _subscription.timeTracking = _trueNumber;
                            [_trueNumber release];
                        }
                    }
                }
                self.subscription = _subscription;
                [_subscription release];
            }
        }
    }
    [doc release];
    [xmlDateFormatter release];
    [xmlDateTimeFormatter release];
}
#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    
    [password release];
    [username release];
    [accountURL release];
    [name release];
    [accountHolderID release];
    [primaryCompanyID release];
    [emailNotificationEnabled release];
    [timeTrackingEnabled release];
    [updatedAt release];
    [subscription release];
    [defaultAttachmentCategories release];
    [defaultPostCategories release];
    [accountID release];
    
    [super dealloc];
}
@end
