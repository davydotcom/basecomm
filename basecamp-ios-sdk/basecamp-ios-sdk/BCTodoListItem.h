//
//  BCTodoListItem.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCAccount;
@class BCTodoList;
@class BCTodoListItem;
@interface BCTodoListItem : NSObject {
    BCAccount *account;
    BCTodoList *todoList;
    NSNumber *todoListID;
    NSNumber *completed;
    NSNumber *commentsCount;
    NSDate *createdAt;
    NSDate *dueAt;
    NSNumber *creatorID;
    NSNumber *todoListItemID;
    NSString *content;
    NSNumber *position;
    NSNumber *responsiblePartyID;
    NSString *responsiblePartyName;
    NSString *responsiblePartyType;
    NSString *creatorName;
    BOOL deleted;
@private
    void (^completionHandler)(BCTodoListItem *todoListItem,NSError *error);
    NSThread *sourceThread;
    NSError *error;
    NSMutableData *requestData;
    NSInteger statusCode;
    BOOL finished;
    BOOL deleting;
    BOOL authenticationAttempted;
    BOOL notify;
}
@property(nonatomic, assign) BOOL deleted;
@property(nonatomic, retain) BCAccount *account;
@property(nonatomic, retain) BCTodoList *todoList;
@property(nonatomic, retain) NSNumber *todoListID;
@property(nonatomic, retain) NSNumber *completed;
@property(nonatomic, retain) NSNumber *commentsCount;
@property(nonatomic, retain) NSDate *createdAt;
@property(nonatomic, retain) NSDate *dueAt;
@property(nonatomic, retain) NSNumber *creatorID;
@property(nonatomic, retain) NSNumber *todoListItemID;
@property(nonatomic, retain) NSString *content;
@property(nonatomic, retain) NSNumber *position;
@property(nonatomic, retain) NSNumber *responsiblePartyID;
@property(nonatomic, retain) NSString *responsiblePartyName;
@property(nonatomic, retain) NSString *responsiblePartyType;
@property(nonatomic, retain) NSString *creatorName;

+(void)loadTodoListItemsForTodoList:(BCTodoList *)_todoList withCompletionHandler:(void (^)(NSArray *todoListItems,NSError *error)) completionHandler;
+(void)loadTodoListItem:(NSInteger)_todoListItemID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler;
-(id)initWithTodoList:(BCTodoList *) _todoList;
-(id)initWithAccount:(BCAccount *) _account;
-(NSData *)toRequestXML;

-(void)saveWithCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler notify:(BOOL)_notify;
-(void)createWithCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler notify:(BOOL)_notify;
-(void)markCompleteWithCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler;
-(void)markIncompleteWithCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler;
-(void)destroyWithCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler;
-(void)completeThread:(NSNumber *)_complete;
-(void)runThread;
@end
