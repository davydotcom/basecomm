//
//  BCTodoListItem.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCTodoListItem.h"

#import "BCAccount.h"
#import "BCTodoList.h"
#import "BCTodoListItemConnector.h"
#import "TouchXML.h"

@implementation BCTodoListItem
@synthesize todoList,account,dueAt,createdAt,creatorID,creatorName,responsiblePartyID,responsiblePartyName,content,completed,position,todoListItemID,todoListID,commentsCount,responsiblePartyType,deleted;
#pragma mark -
#pragma mark Class Methods

+(void)loadTodoListItemsForTodoList:(BCTodoList *)_todoList withCompletionHandler:(void (^)(NSArray *todoListItems,NSError *error)) completionHandler
{
    BCTodoListItemConnector *todoListItemConnector = [[BCTodoListItemConnector alloc] initWithTodoList:_todoList andCompletionHandler:completionHandler];
    NSLog(@"Setting Account Value!");
    todoListItemConnector.account = _todoList.account;
    [todoListItemConnector loadObjects];
    [todoListItemConnector release];
}
+(void)loadTodoListItem:(NSInteger)_todoListItemID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler
{
    BCTodoListItemConnector *todoListItemConnector = [[BCTodoListItemConnector alloc] initWithAccount:_account andObjectCompletionHandler:_completionHandler];
    [todoListItemConnector loadObject:_todoListItemID];
    [todoListItemConnector release];
}
+(void)reorderTodoListItems:(NSArray *)_todoListItems forTodoList:(BCTodoList *)_todoList withCompletionHandler:(void (^)(NSArray *todoListItems,NSError *error)) completionHandler
{
    //TODO: Add Connectors to Reorder the TodoLists
}
#pragma mark -
#pragma mark Instance Methods
-(id)initWithTodoList:(BCTodoList *) _todoList
{
    if((self = [super init]))
    {
        self.todoList = _todoList;
        self.account = _todoList.account;
    }
    return self;
}
-(id)initWithAccount:(BCAccount *) _account
{
    if((self = [super init]))
    {
        self.account = _account;   
    }
    return self;    
}
-(void)saveWithCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler notify:(BOOL)_notify
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    notify = _notify;
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)createWithCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler notify:(BOOL)_notify
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    notify = _notify;
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)markCompleteWithCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    NSNumber *_complete = [[NSNumber alloc] initWithBool:YES];
    [NSThread detachNewThreadSelector:@selector(completeThread:) toTarget:self withObject:_complete];
    [_complete release];
}
-(void)markIncompleteWithCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    NSNumber *_complete = [[NSNumber alloc] initWithBool:NO];
    [NSThread detachNewThreadSelector:@selector(completeThread:) toTarget:self withObject:_complete];
    [_complete release];
}
-(void)destroyWithCompletionHandler:(void (^)(BCTodoListItem *todoListItem,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];

    [NSThread detachNewThreadSelector:@selector(destroyThread) toTarget:self withObject:nil];

}
#pragma mark Threaded Methods
-(void)completeThread:(NSNumber *)_complete
{
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(self.todoList != nil && self.todoListID == nil)
    {
        self.todoListID = self.todoList.todoListID;
    }
    if([_complete boolValue])
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/todo_items/%d/complete.xml",[self.account requestURL],[todoListItemID integerValue]];           
    }
    else
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/todo_items/%d/uncomplete.xml",[self.account requestURL],[todoListItemID integerValue]];                   
    }

    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    

    [request setHTTPMethod:@"PUT"];

    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    if(error == nil && self.todoListID != nil)
    {
        finished = NO;
        [BCTodoListItem loadTodoListItem:[self.todoListItemID integerValue] forAccount:self.account withCompletionHandler:^(BCTodoListItem *_todoListItem, NSError *_error) {
            if(_todoListItem != nil)
            {
                self.completed = _todoListItem.completed;
                self.commentsCount = _todoListItem.commentsCount;
                self.createdAt = _todoListItem.createdAt;
                self.dueAt = _todoListItem.dueAt;
                self.creatorID = _todoListItem.creatorID;
                self.creatorName = _todoListItem.creatorName;
                self.todoListID = _todoListItem.todoListID;
                self.content = _todoListItem.content;
                self.position = _todoListItem.position;
                self.responsiblePartyID = _todoListItem.responsiblePartyID;
                self.responsiblePartyName = _todoListItem.responsiblePartyName;
                self.responsiblePartyType = _todoListItem.responsiblePartyType;
            }
            if(_error != nil)
            {
                error = [_error copy];
            }
            finished = YES;
        }];
    }
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    NSLog(@"Running Thread!");
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void)destroyThread
{
    finished = NO;
    deleting = YES;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(self.todoList != nil && self.todoListID == nil)
    {
        self.todoListID = self.todoList.todoListID;
    }

        requestString = [[NSString alloc] initWithFormat:@"%@/todo_items/%d",[self.account requestURL],[todoListItemID integerValue]];           


    
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    
    [request setHTTPMethod:@"DELETE"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }

    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(self.todoList != nil && self.todoListID == nil)
    {
        self.todoListID = self.todoList.todoListID;
    }
    if([todoListItemID integerValue] != 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/todo_items/%d.xml",[self.account requestURL],[todoListItemID integerValue]];           
    } 
    else 
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/todo_lists/%d/todo_items.xml",[self.account requestURL],[todoListID integerValue]];   
    }
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    if([todoListItemID integerValue] != 0)
    {
        if(deleting)
        {
            [request setHTTPMethod:@"DELETE"];
        }
        else
        {
            [request setHTTPMethod:@"PUT"];
        }
    }
    else
    {    
        [request setHTTPMethod:@"POST"];        
    }
    [request setHTTPBody:[self toRequestXML]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    if(error == nil && self.todoListID != nil)
    {
        finished = NO;
        [BCTodoListItem loadTodoListItem:[self.todoListItemID integerValue] forAccount:self.account withCompletionHandler:^(BCTodoListItem *_todoListItem, NSError *_error) {
            if(_todoListItem != nil)
            {
                self.completed = _todoListItem.completed;
                self.commentsCount = _todoListItem.commentsCount;
                self.createdAt = _todoListItem.createdAt;
                self.dueAt = _todoListItem.dueAt;
                self.creatorID = _todoListItem.creatorID;
                self.creatorName = _todoListItem.creatorName;
                self.todoListID = _todoListItem.todoListID;
                self.content = _todoListItem.content;
                self.position = _todoListItem.position;
                self.responsiblePartyID = _todoListItem.responsiblePartyID;
                self.responsiblePartyName = _todoListItem.responsiblePartyName;
                self.responsiblePartyType = _todoListItem.responsiblePartyType;
            }
            if(_error != nil)
            {
                error = [_error copy];
            }
            finished = YES;
        }];
    }
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    NSLog(@"Running Thread!");
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}

-(void) performCompletionHandlerOnSourceThread
{
    if(error != nil)
    {
        [error autorelease];
    }
    if(completionHandler != nil)
    {
        void (^_completionHandler)(BCTodoListItem *todoListItem,NSError *error) = completionHandler;
        completionHandler = nil;
        _completionHandler(self,error);
        [_completionHandler release];
    }
    [self release];
}
#pragma mark -
#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
    else if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodHTTPBasic])
    {
        if(authenticationAttempted)
        {
            [[challenge sender] cancelAuthenticationChallenge:challenge];
            return;
        }
        if(account != nil)
        {
            NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:account.username password:account.password persistence:NSURLCredentialPersistenceForSession];
            [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
            [credential release];
            authenticationAttempted = YES;
        }
        
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error
{
    
    error = [_error copy];
    [requestData release];
    finished = YES;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [requestData appendData:data];   
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    statusCode = [urlResponse statusCode];
    NSLog(@"Did Receive Response! %d",statusCode);
    if(statusCode == 201)
    {
        error = nil;
        NSString *location = [[[urlResponse allHeaderFields] valueForKey:@"Location"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *paths = [location componentsSeparatedByString:@"/"];
        NSString *todoListIDString = [[paths lastObject] stringByReplacingOccurrencesOfString:@".xml" withString:@""];
        
        NSNumber *_todoListItemID = [[NSNumber alloc] initWithInt:[todoListIDString integerValue]];
        self.todoListItemID = _todoListItemID;
        [_todoListItemID release];
        
    }
    else if (statusCode == 200 && deleting == YES) 
    {
        deleted = YES;
        deleting = NO;
    }
    else if (statusCode != 200)
    {
        deleting = NO;
        error = [[NSError alloc] initWithDomain:@"BCTodoList" code:statusCode userInfo:nil];
    }
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    NSString *requestString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    NSLog(@"Response: %@",requestString);
    [requestString release];
    [requestData release];
    finished = YES;
}

#pragma mark -
#pragma mark XML Methods
-(NSData *)toRequestXML
{
    CXMLDocument *doc = (CXMLDocument *)[CXMLDocument elementWithName: @"todo-item"];
    
    
    CXMLElement *contentElement = (CXMLElement *)[CXMLNode elementWithName:@"content"];
    [contentElement setStringValue:self.content];
    [doc addChild:contentElement];

    CXMLElement *dueAtElement = (CXMLElement *)[CXMLNode elementWithName:@"due-at"];
    if(self.dueAt != nil)
    {
        NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
        [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        [dueAtElement setStringValue:[xmlDateTimeFormatter stringFromDate:self.dueAt]];
        [xmlDateTimeFormatter release];
    }
    [doc addChild:dueAtElement];
    CXMLElement *responsibleElement = (CXMLElement *)[CXMLNode elementWithName:@"responsible_party"];
    
    if([self.responsiblePartyID integerValue] > 0)
    {
        if([self.responsiblePartyType isEqualToString:@"Company"])
        {
            NSString *cID = [[NSString alloc] initWithFormat:@"c%@",[self.responsiblePartyID stringValue]];
            [responsibleElement setStringValue:cID];
            [cID release];
        }
        else
        {
            [responsibleElement setStringValue:[self.responsiblePartyID stringValue]];
        }
        
    }
    [doc addChild:responsibleElement];
    
    
    CXMLElement *notifyElement = (CXMLElement *)[CXMLNode elementWithName:@"notify"];
    if(notify == YES)
    {
        [notifyElement setStringValue:@"true"];
    }
    else
    {
        [notifyElement setStringValue:@"false"];
    }
    [doc addChild:notifyElement];
    
    NSLog(@"REquest XML: %@",[doc XMLString]);
    NSData *returnData = [[doc XMLString] dataUsingEncoding:NSUTF8StringEncoding];
    
    return returnData;
    
}
#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    
    [todoList release];
    [account release];
    [dueAt release];
    [createdAt release];
    [creatorID release];
    [creatorName release];
    [responsiblePartyName release];
    [responsiblePartyID release];
    [content release];
    [completed release];
    [position release];
    [todoListItemID release];
    [todoListID release];
    [commentsCount release];
    [responsiblePartyType release];
    [super dealloc];
}

@end
