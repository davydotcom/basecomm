//
//  BCCommentConnector.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 6/9/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCCommentConnector.h"
#import "BCAccount.h"
#import "TouchXML.h"
#import "BCComment.h"
#import "BCTodoListItem.h"
#import "BCMilestone.h"
#import "BCMessage.h"
@implementation BCCommentConnector
@synthesize account,finished,statusCode,authenticationAttempted,objectID,url,message,milestone,todoListItem;

-(id) initWithAccount:(BCAccount *)_account andCompletionHandler:(void (^)(NSArray *objects,NSString *continuedAt,NSError *error)) _completionHandler
{
    if((self = [super init]))
    {
        objectID = 0;
        objectCompletionHandler = nil;
        completionHandler = [_completionHandler copy];
        self.account = _account;
    }
    return self;
}
-(id) initWithAccount:(BCAccount *)_account andObjectCompletionHandler:(void (^)(id object,NSError *error)) _completionHandler
{
    if((self = [super init]))
    {
        objectID = 0;
        completionHandler = nil;
        objectCompletionHandler = [_completionHandler copy];
        self.account = _account;
    }
    return self;
}
#pragma mark -
#pragma mark Main Thread Methods

-(void) loadObjectsForMilestone:(BCMilestone*)_milestone
{
    [self retain];
    self.milestone = _milestone;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void) loadObjectsForMessage:(BCMessage*)_message
{
    [self retain];
    self.message = _message;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void) loadObjectsForTodoListItem:(BCTodoListItem *)_todoListItem
{
    [self retain];
    self.todoListItem = _todoListItem;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void) loadObjectsWithUrl:(NSString *)_url
{
    [self retain];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
    self.url = _url;
}
-(void) loadObject:(NSInteger)_objectID
{
    self.objectID = _objectID;
    [self retain];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];    
}
-(void) performCompletionHandlerOnSourceThread
{
    if(completionHandler != nil)
    {
        
        completionHandler(objects,nil,error);
        [completionHandler release];
        completionHandler = nil;
    }
    if(objectCompletionHandler != nil)
    {
        
        NSObject *firstObject = nil;
        if(objects != nil && [objects count] > 0)
        {
            firstObject = [[objects objectAtIndex:0] retain];
            [objects removeObject:firstObject];
            
        }
        objectCompletionHandler([firstObject autorelease],error);
        [objectCompletionHandler release];
        objectCompletionHandler = nil;
    }
    
    [error autorelease];
    [objects autorelease];
    [self release];
}
-(void) parseXMLResponse
{
    CXMLDocument *doc = [[CXMLDocument alloc] initWithData:requestData options:0 error:nil];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
    [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSArray *nodes = nil;
    NSError *_error = nil;
    @try
    {
        self.url = [[[doc rootElement] attributeForName:@"continued-at"] stringValue];
        nodes = [doc nodesForXPath:@"//comment" error:&_error];
        if(_error != nil)
        {
            NSLog(@"Error Parsing XML: %d",[_error code]);
            [doc release];
            return;
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception %@",[e name]);
    }
    NSMutableArray *_objects = [[NSMutableArray alloc] initWithCapacity:1];
    for(CXMLElement *node in nodes)
    {
        BCComment *_comment = [[BCComment alloc] init];
        _comment.account = self.account;


        for (CXMLElement *childNode in [node children]) 
        {
            if([[childNode name] isEqualToString:@"id"])
            {
                NSNumber *_ID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _comment.commentID = _ID;
                [_ID release];
            }
            else if([[childNode name] isEqualToString:@"body"])
            {
                _comment.body = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"author-id"])
            {
                NSNumber *_ID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _comment.authorID = _ID;
                [_ID release];
            }
            else if([[childNode name] isEqualToString:@"author-name"])
            {
                _comment.authorName = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"commentable-type"])
            {
                _comment.commentableType = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"emailed-from"])
            {
                if([childNode attributeForName:@"nil"] != nil && ![[[childNode attributeForName:@"nil"] stringValue] isEqualToString:@"true"])
                {
                    _comment.emailedFrom = [childNode stringValue];
                }
            }
            else if([[childNode name] isEqualToString:@"commentable-id"])
            {
                NSNumber *_ID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _comment.commentableID = _ID;
                [_ID release];
            }
            else if([[childNode name] isEqualToString:@"created-at"])
            {
                _comment.createdAt = [xmlDateTimeFormatter dateFromString:[childNode stringValue]];
            }
            else if([[childNode name] isEqualToString:@"attachments-count"])
            {
                NSNumber *_Count =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _comment.attachmentsCount = _Count;
                [_Count release];
            }
            
            
        }
        
        [_objects addObject:_comment];
        [_comment release];
    }
    objects = _objects;
    [doc release];
    [xmlDateFormatter release];
    [xmlDateTimeFormatter release];
    /*<comment>
     <id type="integer">#{id}</id>
     <author-id type="integer">#{author_id}</author-id>
     <author-name>#{author_name}</author-name>
     <commentable-id type="integer">#{commentable_id}</commentable-id>
     <commentable-type>#{commentable_type}</commentable-type>
     <body>#{body}</body>
     <emailed-from nil="true">#{emailed_from}</emailed-from>
     <created-at type="datetime">#{created_at}</created-at>
     
     <attachments-count type="integer">#{attachments_count}</attachments-count>
     <attachments type="array">
     <attachment>
     ...
     </attachment>
     ...
     </attachments>
     </comment>*/
}
-(void)runThread
{
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(self.url != nil)
    {
        requestString = url;
    }
    else if(self.objectID != 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/comments/%d.xml",[self.account requestURL],self.objectID];             
    }
    else if(self.todoListItem != nil)
    {
        
        requestString = [[NSString alloc] initWithFormat:@"%@/todo_items/%d/comments.xml",[self.account requestURL],[self.todoListItem.todoListItemID integerValue]];             
    }
    else if(self.milestone != nil)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/milestones/%d/comments.xml",[self.account requestURL],[self.milestone.milestoneID integerValue]];             
    }
    else if(self.message != nil)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/posts/%d/comments.xml",[self.account requestURL],[self.message.messageID integerValue]];             
    }
    else 
    {
        NSError *_error = [[NSError alloc] initWithDomain:@"BCCommentConnector" code:404 userInfo:nil];
        error = _error;
        [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];
        return;
    }
    NSURL *_url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:_url];
    self.url = nil;
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:NO];
    
    [request release];
    [connection release];
    [autoreleasepool release];
}
#pragma mark -
#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    if(authenticationAttempted)
    {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
    if(account != nil)
    {
        NSLog(@"Sending Credentials!");
        NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:account.username password:account.password persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
        [credential release];
        authenticationAttempted = YES;
    }
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error
{
    [requestData release];
    [_error retain];
    error = _error;
    finished = YES;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [requestData appendData:data];   
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    self.statusCode = [urlResponse statusCode];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if(self.statusCode == 200)
    {
        [self parseXMLResponse];
    }
    else
    {
        NSError *_error = [[NSError alloc] initWithDomain:@"BCConnector" code:self.statusCode userInfo:nil];
        error = _error;
    }
    finished = YES;
    [requestData release];
}

-(void)dealloc
{
    [account release];
    [milestone release];
    [todoListItem release];
    [message release];
    [url release];
    [super dealloc];
    
}
@end
