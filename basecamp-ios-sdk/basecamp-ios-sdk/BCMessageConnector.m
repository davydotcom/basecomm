//
//  BCMessageConnector.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 4/10/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCMessageConnector.h"
#import "TouchXML.h"
#import "BCAccount.h"
#import "BCProject.h"
#import "BCMessage.h"
#import "BCCategory.h"

@implementation BCMessageConnector
@synthesize project,category;


-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(objectID > 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/posts/%d.xml",[self.account requestURL],objectID];           
    } 
    else if(category != nil)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/cat/%d/posts.xml",[self.account requestURL],[project.projectID integerValue],[category.categoryID integerValue]];     
    }
    else
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/posts.xml",[self.account requestURL],[project.projectID integerValue]];     
    }

    
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:NO];
    
    [request release];
    [connection release];
    [autoreleasepool release];
}

#pragma mark -
#pragma mark TouchXML Methods
-(void) parseXMLResponse
{
    //    NSString *responseString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    //    NSLog(responseString);
    //    [responseString release];
    CXMLDocument *doc = [[CXMLDocument alloc] initWithData:requestData options:0 error:nil];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
    [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSArray *nodes = nil;
    NSError *_error = nil;
    @try
    {
        nodes = [doc nodesForXPath:@"//post" error:&_error];
        if(_error != nil)
        {
            NSLog(@"Error Parsing XML: %d",[_error code]);
            [doc release];
            return;
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception %@",[e name]);
    }
    NSMutableArray *_messages = [[NSMutableArray alloc] initWithCapacity:1];
    for(CXMLElement *node in nodes)
    {
        BCMessage *_message = [[BCMessage alloc] initWithProject:self.project];
        _message.project = self.project;
        for (CXMLElement *childNode in [node children]) 
        {
            if([[childNode name] isEqualToString:@"id"])
            {
                NSNumber *_messageID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _message.messageID = _messageID;
                [_messageID release];
            }
            else if([[childNode name] isEqualToString:@"title"])
            {
                _message.title = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"body"])
            {
                _message.body = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"display-body"])
            {
                _message.displayBody = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"author-name"])
            {
                _message.authorName = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"attachments-count"])
            {
                NSNumber *_attachmentCount =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _message.attachmentsCount = _attachmentCount;
                [_attachmentCount release];
            }
            else if([[childNode name] isEqualToString:@"project-id"])
            {
                NSNumber *_idNumber =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _message.projectID = _idNumber;
                [_idNumber release];
            }
            else if([[childNode name] isEqualToString:@"author-id"])
            {
                NSNumber *_idNumber =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _message.authorID = _idNumber;
                [_idNumber release];
            }
            else if([[childNode name] isEqualToString:@"category-id"])
            {
                NSNumber *_idNumber =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _message.categoryID = _idNumber;
                [_idNumber release];
            }
            else if([[childNode name] isEqualToString:@"milestone-id"])
            {
                NSNumber *_idNumber =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _message.milestoneID = _idNumber;
                [_idNumber release];
            }
            else if([[childNode name] isEqualToString:@"use-textile"])
            {
                if ([[childNode stringValue] isEqualToString:@"true"]) {
                    NSNumber *_idNumber =  [[NSNumber alloc] initWithInt:1];
                    _message.useTextile = _idNumber;
                    [_idNumber release];
                }
                else
                {
                    NSNumber *_idNumber =  [[NSNumber alloc] initWithInt:0];
                    _message.useTextile = _idNumber;
                    [_idNumber release];                    
                }
            }
            else if([[childNode name] isEqualToString:@"private"])
            {
                if ([[childNode stringValue] isEqualToString:@"true"]) {
                    NSNumber *_idNumber =  [[NSNumber alloc] initWithInt:1];
                    _message.isPrivate = _idNumber;
                    [_idNumber release];
                }
                else
                {
                    NSNumber *_idNumber =  [[NSNumber alloc] initWithInt:0];
                    _message.isPrivate = _idNumber;
                    [_idNumber release];                    
                }
            }
            else if([[childNode name] isEqualToString:@"comments-count"])
            {
                NSNumber *_commentsCount =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _message.commentsCount = _commentsCount;
                [_commentsCount release];
            }
            else if([[childNode name] isEqualToString:@"posted-on"])
            {
                NSDate *_postedOn = [xmlDateTimeFormatter dateFromString:[childNode stringValue]];
                _message.postedOn = _postedOn;
            }
            else if([[childNode name] isEqualToString:@"commented-at"])
            {
                NSDate *_commentedAt = [xmlDateTimeFormatter dateFromString:[childNode stringValue]];
                _message.commentedAt = _commentedAt;
            }
            else if([[childNode name] isEqualToString:@"category"])
            {
                BCCategory *_category = [[BCCategory alloc] initWithProject:self.project];
                NSString *categoryType = [[NSString alloc] initWithFormat:@"post"];
                _category.type = categoryType;
                [categoryType release];
                for(CXMLElement *catNode in [childNode children])
                {
                    if([[catNode name] isEqualToString:@"id"])
                    {
                        NSNumber *_catID = [[NSNumber alloc] initWithInt:[[catNode stringValue] integerValue]];
                        _category.categoryID = _catID;
                        [_catID release];
                    }
                    else if([[catNode name] isEqualToString:@"name"])
                    {
                        _category.name = [catNode stringValue];
                    }
                }
                _message.category = _category;
                [_category release];
            }
            
            
        }
        
        [_messages addObject:_message];
        [_message release];
    }
    objects = _messages;
    [doc release];
    [xmlDateFormatter release];
    [xmlDateTimeFormatter release];
}

#pragma mark -
#pragma mark Main Thread Methods

-(void) loadObjectsForProject:(BCProject*)_project
{
    [self retain];
    self.project = _project;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];   
}
-(void) loadObjectsForCategory:(BCCategory*)_category inProject:(BCProject*)_project
{
    [self retain];
    self.category = _category;
    self.project = _project;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];   
}

#pragma mark -
#pragma mark Memory Management Methods
-(void) dealloc
{
    [project release];
    [category release];
    [super dealloc];
}
@end
