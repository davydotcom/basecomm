//
//  BCProject.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCProject.h"
#import "BCAccount.h"
#import "BCProjectConnector.h"
#import "TouchXML.h"
#import "BCTodoList.h"
@implementation BCProject
@synthesize account,projectID,name,status,createdOn,lastChangedOn,announcement,startPage,showWriteboards,showAnnouncement,companies,todoLists;


+(void)loadProjectsForAccount:(BCAccount *)_account withCompletionHandler:(void (^)(NSArray *projects,NSError *error)) completionHandler
{
    BCProjectConnector *projectConnector = [[BCProjectConnector alloc] initWithAccount:_account andCompletionHandler:completionHandler];
    [projectConnector loadObjects];
    [projectConnector release];
}

+(void)loadProject:(NSInteger)_projectID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCProject *project,NSError *error)) completionHandler
{
    BCProjectConnector *projectConnector = [[BCProjectConnector alloc] initWithAccount:_account andObjectCompletionHandler:completionHandler];
    [projectConnector loadObject:_projectID];
    [projectConnector release];
}

-(id)initWithAccount:(BCAccount *)_account
{
    if((self = [super init]))
    {
        self.account = _account;
    }
    return self;
}
-(void)createWithCompletionHandler:(void (^)(BCProject *project,NSError *error))_completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)saveWithCompletionHandler:(void (^)(BCProject *project,NSError *error)) _completionHandler
{
    completionHandler = [_completionHandler copy];
}

-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if([projectID integerValue] != 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d.xml",[self.account requestURL],projectID];           
    } 
    else 
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects.xml",[self.account requestURL]];   
    }

    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    if([projectID integerValue] != 0)
    {
        [request setHTTPMethod:@"PUT"];
    }
    else
    {    
        [request setHTTPMethod:@"POST"];        
        [request setHTTPBody:[self toRequestXML]];
    }

    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];

    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }

    if(error == nil && self.projectID != nil)
    {
        finished = NO;
        [BCProject loadProject:[self.projectID integerValue] forAccount:self.account withCompletionHandler:^(BCProject *_project, NSError *_error) {
            if(_project != nil)
            {
                self.status = _project.status;
                self.createdOn = _project.createdOn;
                self.lastChangedOn = _project.lastChangedOn;
                self.announcement = _project.announcement;
                self.showWriteboards = _project.showWriteboards;
                self.showAnnouncement = _project.showAnnouncement;
                self.companies = _project.companies;
                self.startPage = _project.startPage;
            
            }
            if(_error != nil)
            {
                error = [_error copy];
            }
            finished = YES;
        }];
    }
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
        [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}

-(void) performCompletionHandlerOnSourceThread
{
    [error autorelease];
    if(completionHandler != nil)
    {
        void (^_completionHandler)(BCProject *project,NSError *error) = completionHandler;
        completionHandler = nil;
        _completionHandler(self,error);
        [_completionHandler release];
    }
    [self release];
}
-(void) performReorderCompletionHandlerOnSourceThread:(NSArray *)_todoLists
{
    if(error != nil)
    {
        [error autorelease];
    }
    if(completionHandler != nil)
    {
        void (^_reorderCompletionHandler)(NSArray *todoLists,NSError *error) = reorderCompletionHandler;
        
        reorderCompletionHandler = nil;
        _reorderCompletionHandler(_todoLists,error);
        [_reorderCompletionHandler release];
    }
    [self release];
}
#pragma mark -
#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
    else if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodHTTPBasic])
    {
        if(authenticationAttempted)
        {
            [[challenge sender] cancelAuthenticationChallenge:challenge];
            return;
        }
        if(account != nil)
        {
            NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:account.username password:account.password persistence:NSURLCredentialPersistenceForSession];
            [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
            [credential release];
            authenticationAttempted = YES;
        }
        
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error
{

    error = [_error copy];
    finished = YES;
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    statusCode = [urlResponse statusCode];

    if(statusCode == 201)
    {
        error = nil;
        NSString *location = [[[urlResponse allHeaderFields] valueForKey:@"Location"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *paths = [location componentsSeparatedByString:@"/"];
        NSString *projectIDString = [[paths lastObject] stringByReplacingOccurrencesOfString:@".xml" withString:@""];
        
        NSNumber *_projectID = [[NSNumber alloc] initWithInt:[projectIDString integerValue]];
        self.projectID = _projectID;
        [_projectID release];
        NSLog(@"Project Created Successfully with ID: %d",[self.projectID integerValue]);
    }
    else
    {
        error = [[NSError alloc] initWithDomain:@"BCProject" code:statusCode userInfo:nil];
    }
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    finished = YES;
}
#pragma mark - reorder Methods
-(void)reorderTodoLists:(NSArray *)_todoLists withCompletionHandler:(void (^)(NSArray *todoLists,NSError *error)) _completionHandler
{
    [self retain];
    reorderCompletionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    
    [NSThread detachNewThreadSelector:@selector(reorderThread:) toTarget:self withObject:_todoLists];
}
-(void)reorderThread:(NSArray *)_todoLists
{
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    
    requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/todo_lists/reorder.xml",[self.account requestURL],[self.projectID integerValue]];   
    
    
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    [request setHTTPMethod:@"POST"];        
    
    [request setHTTPBody:[self listOrderXML:_todoLists]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    NSInteger _position = 0;
    for(BCTodoList *todoList in _todoLists)
    {
        NSNumber *currentPosition = [[NSNumber alloc] initWithInt:_position];
        todoList.position = currentPosition;
        [currentPosition release];
        _position++;
    }
    if(error == nil && self.projectID != nil)
    {
        finished = NO;
    }
    
    [self performSelector:@selector(performReorderCompletionHandlerOnSourceThread:) onThread:sourceThread withObject:_todoLists waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
#pragma mark -
#pragma mark XML Methods
-(NSData *)listOrderXML:(NSArray *)_todoLists
{
    
    CXMLDocument *doc = (CXMLDocument *)[CXMLDocument elementWithName: @"todo-lists"];
    
    for(BCTodoList *todoList in _todoLists)
    {
        CXMLElement *todoItemElement = (CXMLElement *)[CXMLNode elementWithName:@"todo-list"];
        CXMLElement *idElement = (CXMLElement *)[CXMLNode elementWithName:@"id"];
        [idElement setStringValue:[todoList.todoListID stringValue]];
        [todoItemElement addChild:idElement];
        [doc addChild:todoItemElement];
    }

    NSData *returnData = [[[doc XMLString] stringByReplacingOccurrencesOfString:@"<todo-lists>" withString:@"<todo-lists type=\"array\">"] dataUsingEncoding:NSUTF8StringEncoding];
    return returnData;
}
-(NSData *)toRequestXML
{
    CXMLDocument *doc = (CXMLDocument *)[CXMLDocument elementWithName: @"request"];
    CXMLElement *projectElement = (CXMLElement *)[CXMLNode elementWithName:@"project"];
    CXMLElement *projectNameElement = (CXMLElement *)[CXMLNode elementWithName:@"name"];
    [projectNameElement setStringValue:self.name];
    
    [doc addChild:projectElement];
    [projectElement addChild:projectNameElement];
    NSData *returnData = [[doc XMLString] dataUsingEncoding:NSUTF8StringEncoding];
    ;
    
    return returnData;
    
}

#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    [todoLists release];
    [announcement release];
    [showWriteboards release];
    [showAnnouncement release];
    [startPage release];
    [companies release];
    [account release];
    [projectID release];
    [name release];
    [createdOn release];
    [lastChangedOn release];
    [status release];
    [super dealloc];
}
@end
