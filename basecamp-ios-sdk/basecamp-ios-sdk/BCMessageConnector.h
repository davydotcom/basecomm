//
//  BCMessageConnector.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 4/10/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCConnector.h"
@class BCProject;
@class BCCategory;
@interface BCMessageConnector : BCConnector {
    BCProject *project;
    BCCategory *category;
}
@property(nonatomic,retain) BCProject *project;
@property(nonatomic,retain) BCCategory *category;

-(void)runThread;
-(void) loadObjectsForProject:(BCProject*)_project;
-(void) loadObjectsForCategory:(BCCategory*)_category inProject:(BCProject*)_project;
-(void) parseXMLResponse;
@end
