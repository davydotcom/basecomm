//
//  BCConnector.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BCAccount.h"
@interface BCConnector : NSObject {
    BCAccount *account;
    BOOL authenticationAttempted;
    void (^completionHandler)(NSArray *objects,NSError *error);
    void (^objectCompletionHandler)(id object,NSError *error);
    BOOL finished;
    NSInteger objectID;
    NSInteger statusCode;
    NSMutableArray *objects;
    NSMutableData *requestData;
    NSError *error;
    NSThread *sourceThread;  
}
@property(nonatomic,assign) NSInteger objectID;
@property(nonatomic,assign) BOOL authenticationAttempted;
@property(nonatomic,assign) NSInteger statusCode;
@property(nonatomic,assign) BOOL finished;
@property(nonatomic,retain) BCAccount *account;


-(id) initWithAccount:(BCAccount *)_account andCompletionHandler:(void (^)(NSArray *projects,NSError *error)) _completionHandler;
-(id) initWithAccount:(BCAccount *)_account andObjectCompletionHandler:(void (^)(id object,NSError *error)) _completionHandler;
-(void)runThread;
-(void) loadObjects;
-(void) loadObject:(NSInteger)_objectID;
-(void) performCompletionHandlerOnSourceThread;
-(void) parseXMLResponse;
@end
