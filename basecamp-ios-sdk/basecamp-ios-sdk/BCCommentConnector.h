//
//  BCCommentConnector.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 6/9/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BCAccount.h"
@class BCMilestone,BCTodoListItem,BCMessage;
@interface BCCommentConnector : NSObject {
    BCAccount *account;
    BCMilestone *milestone;
    BCTodoListItem *todoListItem;
    BCMessage *message;
    BOOL authenticationAttempted;
    void (^completionHandler)(NSArray *objects,NSString *continuedAt,NSError *error);
    void (^objectCompletionHandler)(id object,NSError *error);
    BOOL finished;
    NSInteger objectID;
    NSInteger statusCode;
    NSMutableArray *objects;
    NSMutableData *requestData;
    NSError *error;
    NSString *url;
    NSThread *sourceThread;  
}
@property(nonatomic,assign) NSInteger objectID;
@property(nonatomic,assign) BOOL authenticationAttempted;
@property(nonatomic,assign) NSInteger statusCode;
@property(nonatomic,assign) BOOL finished;
@property(nonatomic,retain) BCAccount *account;
@property(nonatomic,retain) NSString *url;
@property(nonatomic, retain) BCMilestone *milestone;
@property(nonatomic, retain) BCTodoListItem *todoListItem;
@property(nonatomic, retain) BCMessage *message;
-(id) initWithAccount:(BCAccount *)_account andCompletionHandler:(void (^)(NSArray *projects,NSString *continuedAt,NSError *error)) _completionHandler;
-(id) initWithAccount:(BCAccount *)_account andObjectCompletionHandler:(void (^)(id object,NSError *error)) _completionHandler;
-(void)runThread;

-(void) loadObject:(NSInteger)_objectID;
-(void) loadObjectsWithUrl:(NSString *)_url;
-(void) performCompletionHandlerOnSourceThread;
-(void) parseXMLResponse;
-(void) loadObjectsForMessage:(BCMessage*)_message;
-(void) loadObjectsForTodoListItem:(BCTodoListItem *)_todoListItem;
-(void) loadObjectsForMilestone:(BCMilestone*)_milestone;
-(void) loadObjectsWithUrl:(NSString *)_url;
@end
