//
//  BCPeopleConnector.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCConnector.h"

@class BCProject;
@class BCCompany;
@interface BCPeopleConnector : BCConnector {
    BCProject *project;
    BCCompany *company;
    BOOL me;
}

@property(nonatomic, retain) BCProject *project;
@property(nonatomic, retain) BCCompany *company;
@property(nonatomic, assign) BOOL me;
-(void)runThread;
-(void) parseXMLResponse;
-(void) loadObjectsForProject:(BCProject*)_project;
-(void) loadObjectsForCompany:(BCCompany*)_company;
-(void) loadMe;
@end
