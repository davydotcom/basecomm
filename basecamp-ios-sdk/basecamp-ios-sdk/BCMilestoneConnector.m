//
//  BCMilestoneConnector.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/3/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCMilestoneConnector.h"
#import "BCAccount.h"
#import "BCProject.h"
#import "BCMilestone.h"
#import "TouchXML.h"
@implementation BCMilestoneConnector
@synthesize project;
-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(objectID > 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/milestones/%d.xml",[self.account requestURL],objectID];           
    } 
    else if(project != nil)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/milestones/list.xml",[self.account requestURL],[project.projectID integerValue]];     
    }
    else 
    {
        NSError *_error = [[NSError alloc] initWithDomain:@"BCMilestoneConnector" code:404 userInfo:nil];
        error = _error;
        [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];
        return;
    }
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];
    
    [request release];
    [connection release];
    [autoreleasepool release];
}

#pragma mark -
#pragma mark TouchXML Methods
-(void) parseXMLResponse
{
    //    NSString *responseString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    //    NSLog(responseString);
    //    [responseString release];
    CXMLDocument *doc = [[CXMLDocument alloc] initWithData:requestData options:0 error:nil];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
    [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSArray *nodes = nil;
    NSError *_error = nil;
    @try
    {
        nodes = [doc nodesForXPath:@"//milestone" error:&_error];
        if(_error != nil)
        {
            NSLog(@"Error Parsing XML: %d",[_error code]);
            [doc release];
            return;
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception %@",[e name]);
    }
    NSMutableArray *_milestones = [[NSMutableArray alloc] initWithCapacity:1];
    for(CXMLElement *node in nodes)
    {
        BCMilestone *_milestone = [[BCMilestone alloc] initWithAccount:self.account];
        
        for (CXMLElement *childNode in [node children]) 
        {
            if([[childNode name] isEqualToString:@"id"])
            {
                NSNumber *_milestoneID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _milestone.milestoneID = _milestoneID;
                [_milestoneID release];
            }
            if([[childNode name] isEqualToString:@"project-id"])
            {
                NSNumber *_projectID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                if(self.project != nil && [_projectID integerValue] == [self.project.projectID integerValue])
                {
                    _milestone.project = self.project;
                }
                _milestone.projectID = _projectID;
                [_projectID release];
            }
           
            else if([[childNode name] isEqualToString:@"title"])
            {
                _milestone.title = [childNode stringValue];
            }
            
            
            else if([[childNode name] isEqualToString:@"comments-count"])
            {
                NSNumber *commentsCount = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _milestone.commentsCount = commentsCount;
                [commentsCount release];
            }
            else if([[childNode name] isEqualToString:@"creator-id"])
            {
                NSNumber *creatorID = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _milestone.creatorID = creatorID;
                [creatorID release];
            }
            else if([[childNode name] isEqualToString:@"creator-name"])
            {
                _milestone.creatorName = [childNode stringValue];
            }
            if([[childNode name] isEqualToString:@"responsible-party-id"])
            {
                NSNumber *_responsiblePartyID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _milestone.responsiblePartyID = _responsiblePartyID;
                [_responsiblePartyID release];
            }
            else if([[childNode name] isEqualToString:@"responsible-party-type"])
            {
                _milestone.responsiblePartyType = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"responsible-party-name"])
            {
                _milestone.responsiblePartyName = [childNode stringValue];
            }
            
            else if([[childNode name] isEqualToString:@"completed"])
            {
                NSNumber *complete = nil;
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    complete = [[NSNumber alloc] initWithBool:YES];
                }
                else
                {
                    complete = [[NSNumber alloc] initWithBool:NO];
                }
                _milestone.completed = complete;
                [complete release];
            }
            else if([[childNode name] isEqualToString:@"deadline"])
            {
                
                NSDate *deadline = [xmlDateFormatter dateFromString:[childNode stringValue]];
                _milestone.deadline = deadline;
                
            }
            else if([[childNode name] isEqualToString:@"created-on"])
            {
                
                NSDate *createdOn = [xmlDateTimeFormatter dateFromString:[childNode stringValue]];
                _milestone.createdOn = createdOn;
                
            }
            else if([[childNode name] isEqualToString:@"completed-on"])
            {
                
                NSDate *completedOn = [xmlDateTimeFormatter dateFromString:[childNode stringValue]];
                _milestone.completedOn = completedOn;
                
            }
            
            
        }
        
        [_milestones addObject:_milestone];
        [_milestone release];
    }
    objects = _milestones;
    [doc release];
    [xmlDateFormatter release];
    [xmlDateTimeFormatter release];
}

#pragma mark -
#pragma mark Main Thread Methods

-(void) loadObjectsForProject:(BCProject*)_project
{
    [self retain];
    self.project = _project;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];   
}

#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    [project release];
    [super dealloc];
}
@end
