//
//  BCMessage.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCMessage.h"
#import "BCProject.h"
#import "BCCategory.h"
#import "BCAccount.h"
#import "BCPerson.h"
#import "BCMessageConnector.h"
#import "TouchXML.h"

@implementation BCMessage
@synthesize messageID,title,authorID,authorName,commentedAt,postedOn,attachmentsCount,category,project,deleted,account,projectID,body,isPrivate,displayBody,categoryID,commentsCount,milestoneID,useTextile,notifyPeople;
#pragma mark - Static Methods
+(void)loadMessagesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *messages,NSError *error)) completionHandler
{
    BCMessageConnector *messageConnector = [[BCMessageConnector alloc] initWithAccount:_project.account andCompletionHandler:completionHandler];
    [messageConnector loadObjectsForProject:_project];
    [messageConnector release];
}
+(void)loadMessagesForProject:(BCProject *)_project inCategory:(BCCategory *)_category withCompletionHandler:(void (^)(NSArray *messages,NSError *error)) completionHandler
{
    BCMessageConnector *messageConnector = [[BCMessageConnector alloc] initWithAccount:_project.account andCompletionHandler:completionHandler];
    [messageConnector loadObjectsForCategory:_category inProject:_project];
    [messageConnector release];
}
+(void)loadMessage:(NSInteger)_messageID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCMessage *message,NSError *error)) _completionHandler
{
    BCMessageConnector *messageConnector = [[BCMessageConnector alloc] initWithAccount:_account andObjectCompletionHandler:_completionHandler];
    [messageConnector loadObject:_messageID];
    [messageConnector release];
}
#pragma mark -
#pragma mark Instance Methods
-(id)initWithProject:(BCProject *) _project
{
    if((self = [super init]))
    {
        self.project = _project;
        self.account = _project.account;
    }
    return self;
}
-(id)initWithAccount:(BCAccount *) _account
{
    if((self = [super init]))
    {
        self.account = _account;   
    }
    return self;    
}

-(void)createWithCompletionHandler:(void (^)(BCMessage *message,NSError *error))_completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)saveWithCompletionHandler:(void (^)(BCMessage *message,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)destroyWithCompletionHandler:(void (^)(BCMessage *message,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    
    [NSThread detachNewThreadSelector:@selector(destroyThread) toTarget:self withObject:nil];
    
}
#pragma mark Threaded Methods
-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(self.project != nil && self.projectID == nil)
    {
        self.projectID = self.project.projectID;
    }
    if([messageID integerValue] != 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/posts/%d.xml",[self.account requestURL],[messageID integerValue]];           
    } 
    else 
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/posts.xml",[self.account requestURL],[projectID integerValue]];   
    }
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    if([messageID integerValue] != 0)
    {
        [request setHTTPMethod:@"PUT"];
    }
    else
    {    
        [request setHTTPMethod:@"POST"];        
    }
    [request setHTTPBody:[self toRequestXML]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    if(error == nil && self.messageID != nil)
    {
        finished = NO;
        [BCMessage loadMessage:[self.messageID integerValue] forAccount:self.account withCompletionHandler:^(BCMessage *_message, NSError *_error) {
            if(_message != nil)
            {
                self.title = _message.title;
                self.body = _message.body;
                self.displayBody = _message.displayBody;
                self.postedOn = _message.postedOn;
                self.attachmentsCount = _message.attachmentsCount;
                self.commentsCount = _message.commentsCount;
                self.commentedAt = _message.commentedAt;
                self.useTextile = _message.useTextile;
                self.isPrivate = _message.isPrivate;
                self.authorID = _message.authorID;
                self.authorName = _message.authorName;
                self.milestoneID = _message.milestoneID;
                self.categoryID = _message.categoryID;
                self.projectID = _message.projectID;
                
            }
            if(_error != nil)
            {
                error = [_error copy];
            }
            finished = YES;
        }];
    }
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    NSLog(@"Running Thread!");
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void)destroyThread
{
    finished = NO;
    deleting = YES;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    
    
    requestString = [[NSString alloc] initWithFormat:@"%@/posts/%d",[self.account requestURL],[messageID integerValue]];           
    
    
    
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    
    [request setHTTPMethod:@"DELETE"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void) performCompletionHandlerOnSourceThread
{
    if(error != nil)
    {
        [error autorelease];
    }
    if(completionHandler != nil)
    {
        void (^_completionHandler)(BCMessage *message,NSError *error) = completionHandler;
        completionHandler = nil;
        _completionHandler(self,error);
        [_completionHandler release];
    }
    [self release];
}

#pragma mark -
#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
    else if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodHTTPBasic])
    {
        if(authenticationAttempted)
        {
            [[challenge sender] cancelAuthenticationChallenge:challenge];
            return;
        }
        if(account != nil)
        {
            NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:account.username password:account.password persistence:NSURLCredentialPersistenceForSession];
            [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
            [credential release];
            authenticationAttempted = YES;
        }
        
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error
{
    
    error = [_error copy];
    [requestData release];
    finished = YES;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [requestData appendData:data];   
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    statusCode = [urlResponse statusCode];
    NSLog(@"Did Receive Response! %d",statusCode);
    if(statusCode == 201)
    {
        error = nil;
        NSString *location = [[[urlResponse allHeaderFields] valueForKey:@"Location"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *paths = [location componentsSeparatedByString:@"/"];
        NSString *messageIDString = [[paths lastObject] stringByReplacingOccurrencesOfString:@".xml" withString:@""];
        
        NSNumber *_messageID = [[NSNumber alloc] initWithInt:[messageIDString integerValue]];
        self.messageID = _messageID;
        [_messageID release];
        
    }
    else if (statusCode == 200 && deleting == YES) 
    {
        deleted = YES;
        deleting = NO;
    }
    else if (statusCode != 200)
    {
        error = [[NSError alloc] initWithDomain:@"BCMessage" code:statusCode userInfo:nil];
    }
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    NSString *requestString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    NSLog(@"Response: %@",requestString);
    [requestString release];
    [requestData release];
    finished = YES;
}
#pragma mark - XML Methods

-(NSData *)toRequestXML
{
    CXMLDocument *doc = (CXMLDocument *)[CXMLDocument elementWithName: @"request"];
    
    CXMLElement *postElement = (CXMLElement *)[CXMLNode elementWithName:@"post"];
    [doc addChild:postElement];
                                               
    
    CXMLElement *categoryIDElement = (CXMLElement *)[CXMLNode elementWithName:@"category-id"];
    [categoryIDElement setStringValue:[self.categoryID stringValue]];
    [postElement addChild:categoryIDElement];    
    
    CXMLElement *bodyElement = (CXMLElement *)[CXMLNode elementWithName:@"body"];
    [bodyElement setStringValue:self.body];
    [postElement addChild:bodyElement];
    
    CXMLElement *titleElement = (CXMLElement *)[CXMLNode elementWithName:@"title"];
    [titleElement setStringValue:self.title];
    [postElement addChild:titleElement];
    
    
    CXMLElement *isPrivateElement = (CXMLElement *)[CXMLNode elementWithName:@"private"];
    if([self.isPrivate integerValue] > 0)
    {
        [isPrivateElement setStringValue:@"true"];
    }
    else
    {
        [isPrivateElement setStringValue:@"false"];
    }
    [postElement addChild:isPrivateElement];
    
    
    if(self.notifyPeople != nil)
    {
        for(BCPerson *person in notifyPeople)
        {
            CXMLElement *notifyElement = (CXMLElement *)[CXMLNode elementWithName:@"notify"];
            [notifyElement setStringValue:[person.personID stringValue]];
            [doc addChild:notifyElement];
        }
    }
    
    
    NSLog(@"REquest XML: %@",[doc XMLString]);
    NSData *returnData = [[doc XMLString] dataUsingEncoding:NSUTF8StringEncoding];
    
    return returnData;
    
}
#pragma mark - Memory Management Methods
-(void)dealloc
{
    [messageID release];
    [title release];
    [authorName release];
    [commentedAt release];
    [postedOn release];
    [attachmentsCount release];
    [category release];
    [categoryID release];
    [project release];
    [account release];
    [projectID release];
    [milestoneID release];
    [authorID release];
    [commentsCount release];
    [useTextile release];
    [displayBody release];
    [notifyPeople release];
    [body release];

    [super dealloc];
}
@end
