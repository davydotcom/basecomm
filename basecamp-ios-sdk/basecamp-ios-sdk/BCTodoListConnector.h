//
//  BCTodoListConnector.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/3/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCConnector.h"
@class BCProject;
@interface BCTodoListConnector : BCConnector {

    BCProject *project;

}

@property(nonatomic,retain) BCProject *project;

-(void)runThread;
-(void) loadObjectsForProject:(BCProject*)_project;
-(void) parseXMLResponse;
@end
