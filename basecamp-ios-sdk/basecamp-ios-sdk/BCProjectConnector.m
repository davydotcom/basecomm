//
//  BCProjectConnector.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCProjectConnector.h"
#import "BCAccount.h"
#import "BCProject.h"

#import "TouchXML.h"


@implementation BCProjectConnector

-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(objectID > 0)
    {
     requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d.xml",[self.account requestURL],objectID];           
    } 
    else 
    {
     requestString = [[NSString alloc] initWithFormat:@"%@/projects.xml",[self.account requestURL]];   
    }
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];

    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:NO];

    [request release];
    [connection release];
    [autoreleasepool release];
}

#pragma mark -
#pragma mark TouchXML Methods
-(void) parseXMLResponse
{
    CXMLDocument *doc = [[CXMLDocument alloc] initWithData:requestData options:0 error:nil];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
    [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSArray *nodes = nil;
    NSError *_error = nil;
    @try
    {
        nodes = [doc nodesForXPath:@"//project" error:&_error];
        if(_error != nil)
        {
            NSLog(@"Error Parsing XML: %d",[_error code]);
            [doc release];
            return;
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception %@",[e name]);
    }
    NSMutableArray *_projects = [[NSMutableArray alloc] initWithCapacity:1];
    for(CXMLElement *node in nodes)
    {
        BCProject *_project = [[BCProject alloc] init];
        _project.account = self.account;
        
        for (CXMLElement *childNode in [node children]) 
        {
            if([[childNode name] isEqualToString:@"id"])
            {
                NSNumber *_projectID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _project.projectID = _projectID;
                [_projectID release];
            }
            else if([[childNode name] isEqualToString:@"name"])
            {
                _project.name = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"created-on"])
            {

                NSDate *createdOn = [xmlDateFormatter dateFromString:[childNode stringValue]];
                _project.createdOn = createdOn;
             
            }
            else if([[childNode name] isEqualToString:@"last-changed-on"])
            {

                NSDate *lastChangedOn = [xmlDateTimeFormatter dateFromString:[childNode stringValue]];
                _project.lastChangedOn = lastChangedOn;
             
            }
            else if([[childNode name] isEqualToString:@"status"])
            {
                _project.status = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"start-page"])
            {
                _project.startPage = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"show-announcement"])
            {
                NSNumber *showAnnouncement = nil;
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    showAnnouncement = [[NSNumber alloc] initWithBool:YES];
                }
                else
                {
                    showAnnouncement = [[NSNumber alloc] initWithBool:NO];   
                }
                _project.showAnnouncement = showAnnouncement;
                [showAnnouncement release];
            }
            else if([[childNode name] isEqualToString:@"show-writeboards"])
            {
                NSNumber *showWriteboards = nil;
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    showWriteboards = [[NSNumber alloc] initWithBool:YES];
                }
                else
                {
                    showWriteboards = [[NSNumber alloc] initWithBool:NO];
                }
                _project.showWriteboards = showWriteboards;
                [showWriteboards release];
            }
            
            else if([[childNode name] isEqualToString:@"company"])
            {
                if(_project.companies == nil)
                {
                    NSMutableArray *companies = [[NSMutableArray alloc] initWithCapacity:1];
                    _project.companies = companies;
                    [companies release];
                }
                NSMutableDictionary *company = [[NSMutableDictionary alloc] initWithCapacity:2];
                for(CXMLNode *companyNode in [childNode children])
                {
                    if([[companyNode name] isEqualToString:@"id"])
                    {
                        NSNumber *_companyID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                        [company setValue:_companyID forKey:@"companyID"];
                        [_companyID release];
                    }
                    else if([[companyNode name] isEqualToString:@"name"])
                    {
                        [company setValue:[companyNode stringValue] forKey:@"name"];
                    }
                }
                [_project.companies addObject:company];
                [company release];
            }

        }
        
        [_projects addObject:_project];
        [_project release];
    }
    objects = _projects;
    [doc release];
    [xmlDateFormatter release];
    [xmlDateTimeFormatter release];
}



@end
