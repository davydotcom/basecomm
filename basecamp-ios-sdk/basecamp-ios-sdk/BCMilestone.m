//
//  BCMilestone.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCMilestone.h"
#import "BCAccount.h"
#import "BCProject.h"
#import "BCMilestoneConnector.h"
#import "TouchXML.h"
@implementation BCMilestone
@synthesize project,account,milestoneID,title,deadline,completed,projectID,createdOn,creatorID,creatorName,responsiblePartyID,responsiblePartyType,responsiblePartyName,commentsCount,completedOn,completerID,completerName,deleted;
#pragma mark -
#pragma mark Class Methods

+(void)loadMilestonesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *milestones,NSError *error)) completionHandler
{
    BCMilestoneConnector *milestoneConnector = [[BCMilestoneConnector alloc] initWithAccount:_project.account andCompletionHandler:completionHandler];
    [milestoneConnector loadObjectsForProject:_project];
    [milestoneConnector release];
}
//+(void)loadMilestone:(NSInteger)_milestoneID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCTodoList *todoList,NSError *error)) _completionHandler
//{
//    BCMilestoneConnector *milestoneConnector = [[BCMilestoneConnector alloc] initWithAccount:_account andObjectCompletionHandler:_completionHandler];
//    [milestoneConnector loadObject:_todoListID];
//    [milestoneConnector release];
//}
#pragma mark -
#pragma mark Instance Methods
-(id)initWithProject:(BCProject *) _project
{
    if((self = [super init]))
    {
        self.project = _project;
        self.account = _project.account;
    }
    return self;
}
-(id)initWithAccount:(BCAccount *) _account
{
    if((self = [super init]))
    {
        self.account = _account;   
    }
    return self;    
}
-(void)saveWithCompletionHandler:(void (^)(BCMilestone *milestone,NSError *error)) _completionHandler notify:(BOOL)_notify
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    notify = _notify;
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)createWithCompletionHandler:(void (^)(BCMilestone *milestone,NSError *error)) _completionHandler notify:(BOOL)_notify
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    notify = _notify;
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)markCompleteWithCompletionHandler:(void (^)(BCMilestone *milestone,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    NSNumber *_complete = [[NSNumber alloc] initWithBool:YES];
    [NSThread detachNewThreadSelector:@selector(completeThread:) toTarget:self withObject:_complete];
    [_complete release];
}
-(void)markIncompleteWithCompletionHandler:(void (^)(BCMilestone *milestone,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    NSNumber *_complete = [[NSNumber alloc] initWithBool:NO];
    [NSThread detachNewThreadSelector:@selector(completeThread:) toTarget:self withObject:_complete];
    [_complete release];
}
-(void)destroyWithCompletionHandler:(void (^)(BCMilestone *milestone,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    
    [NSThread detachNewThreadSelector:@selector(destroyThread) toTarget:self withObject:nil];
    
}
#pragma mark Threaded Methods
-(void)completeThread:(NSNumber *)_complete
{
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(self.project != nil && self.projectID == nil)
    {
        self.projectID = self.project.projectID;
    }
    if([_complete boolValue])
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/milestones/complete/%d",[self.account requestURL],[milestoneID integerValue]];           
    }
    else
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/milestones/uncomplete/%d",[self.account requestURL],[milestoneID integerValue]];                   
    }
    
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    
    [request setHTTPMethod:@"PUT"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    
    
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void)destroyThread
{
    finished = NO;
    deleting = YES;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    
    
    requestString = [[NSString alloc] initWithFormat:@"%@/milestones/delete/%d",[self.account requestURL],[milestoneID integerValue]];           
    
    
    
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    
    [request setHTTPMethod:@"POST"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(self.project != nil && self.projectID == nil)
    {
        self.projectID = self.project.projectID;
    }
    if([milestoneID integerValue] != 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/milestones/update/%d.xml",[self.account requestURL],[milestoneID integerValue]];           
    } 
    else 
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/milestones/create",[self.account requestURL],[projectID integerValue]];   
    }
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
 
    [request setHTTPMethod:@"POST"];        

    [request setHTTPBody:[self toRequestXML]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    

    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}

-(void) performCompletionHandlerOnSourceThread
{
    if(error != nil)
    {
        [error autorelease];
    }
    if(completionHandler != nil)
    {
        void (^_completionHandler)(BCMilestone *milestone,NSError *error) = completionHandler;
        completionHandler = nil;
        _completionHandler(self,error);
        [_completionHandler release];
    }
    [self release];
}
#pragma mark -
#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
    else if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodHTTPBasic])
    {
        if(authenticationAttempted)
        {
            [[challenge sender] cancelAuthenticationChallenge:challenge];
            return;
        }
        if(account != nil)
        {
            NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:account.username password:account.password persistence:NSURLCredentialPersistenceForSession];
            [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
            [credential release];
            authenticationAttempted = YES;
        }
        
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error
{
    
    error = [_error copy];
    [requestData release];
    finished = YES;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [requestData appendData:data];   
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    statusCode = [urlResponse statusCode];
    NSLog(@"Did Receive Response! %d",statusCode);
    if(statusCode == 201)
    {
        error = nil;
        
    }
    else if (statusCode == 200 && deleting == YES) 
    {
        deleted = YES;
        deleting = NO;
    }
    else if (statusCode != 200)
    {
        deleting = NO;
        error = [[NSError alloc] initWithDomain:@"BCTodoList" code:statusCode userInfo:nil];
    }
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    [self parseXMLResponse];
    [requestData release];
    finished = YES;
}

#pragma mark -
#pragma mark XML Methods
-(NSData *)toRequestXML
{
    CXMLDocument *doc = (CXMLDocument *)[CXMLDocument elementWithName: @"request"];
    
    CXMLElement *milestoneElement = (CXMLElement *)[CXMLNode elementWithName:@"milestone"];
    [doc addChild:milestoneElement];
    CXMLElement *titleElement = (CXMLElement *)[CXMLNode elementWithName:@"title"];
    [titleElement setStringValue:self.title];
    [milestoneElement addChild:titleElement];
    
    CXMLElement *dueAtElement = (CXMLElement *)[CXMLNode elementWithName:@"deadline"];
    if(self.deadline != nil)
    {
        NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
        [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        [dueAtElement setStringValue:[xmlDateTimeFormatter stringFromDate:self.deadline]];
        [xmlDateTimeFormatter release];
    }
    [milestoneElement addChild:dueAtElement];
    CXMLElement *responsibleElement = (CXMLElement *)[CXMLNode elementWithName:@"responsible-party"];
    
    if([self.responsiblePartyID integerValue] > 0)
    {
        if(self.responsiblePartyType == @"Company")
        {
            NSString *cID = [[NSString alloc] initWithFormat:@"c%@",[self.responsiblePartyID stringValue]];
            [responsibleElement setStringValue:cID];
            [cID release];
        }
        else
        {
            [responsibleElement setStringValue:[self.responsiblePartyID stringValue]];
        }
        
    }
    [milestoneElement addChild:responsibleElement];
    
    
    CXMLElement *notifyElement = (CXMLElement *)[CXMLNode elementWithName:@"notify"];
    if(notify == YES)
    {
        [notifyElement setStringValue:@"true"];
    }
    else
    {
        [notifyElement setStringValue:@"false"];
    }
    [milestoneElement addChild:notifyElement];
    
    NSLog(@"REquest XML: %@",[doc XMLString]);
    NSData *returnData = [[doc XMLString] dataUsingEncoding:NSUTF8StringEncoding];
    
    return returnData;
    
}
-(void) parseXMLResponse
{
    //    NSString *responseString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    //    NSLog(responseString);
    //    [responseString release];
    CXMLDocument *doc = [[CXMLDocument alloc] initWithData:requestData options:0 error:nil];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
    [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSArray *nodes = nil;
    NSError *_error = nil;
    @try
    {
        nodes = [doc nodesForXPath:@"//milestone" error:&_error];
        if(_error != nil)
        {
            NSLog(@"Error Parsing XML: %d",[_error code]);
            [doc release];
            return;
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception %@",[e name]);
    }

    for(CXMLElement *node in nodes)
    {
        
        for (CXMLElement *childNode in [node children]) 
        {
            if([[childNode name] isEqualToString:@"id"])
            {
                NSNumber *_milestoneID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                self.milestoneID = _milestoneID;
                [_milestoneID release];
            }
            if([[childNode name] isEqualToString:@"project-id"])
            {
                NSNumber *_projectID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                
                self.projectID = _projectID;
                [_projectID release];
            }
            
            else if([[childNode name] isEqualToString:@"title"])
            {
                self.title = [childNode stringValue];
            }
            
            
            else if([[childNode name] isEqualToString:@"comments-count"])
            {
                NSNumber *_commentsCount = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                self.commentsCount = _commentsCount;
                [_commentsCount release];
            }
            else if([[childNode name] isEqualToString:@"creator-id"])
            {
                NSNumber *_creatorID = [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                self.creatorID = _creatorID;
                [_creatorID release];
            }
            else if([[childNode name] isEqualToString:@"creator-name"])
            {
                self.creatorName = [childNode stringValue];
            }
            if([[childNode name] isEqualToString:@"responsible-party-id"])
            {
                NSNumber *_responsiblePartyID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                self.responsiblePartyID = _responsiblePartyID;
                [_responsiblePartyID release];
            }
            else if([[childNode name] isEqualToString:@"responsible-party-type"])
            {
                self.responsiblePartyType = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"responsible-party-name"])
            {
                self.responsiblePartyName = [childNode stringValue];
            }
            
            else if([[childNode name] isEqualToString:@"completed"])
            {
                NSNumber *complete = nil;
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    complete = [[NSNumber alloc] initWithBool:YES];
                }
                self.completed = complete;
                [complete release];
            }
            else if([[childNode name] isEqualToString:@"deadline"])
            {
                
                NSDate *_deadline = [xmlDateFormatter dateFromString:[childNode stringValue]];
                self.deadline = _deadline;
                
            }
            else if([[childNode name] isEqualToString:@"created-on"])
            {
                
                NSDate *_createdOn = [xmlDateTimeFormatter dateFromString:[childNode stringValue]];
                self.createdOn = _createdOn;
                
            }
            else if([[childNode name] isEqualToString:@"completed-on"])
            {
                
                NSDate *_completedOn = [xmlDateTimeFormatter dateFromString:[childNode stringValue]];
                self.completedOn = _completedOn;
                
            }
            
            
        }
        
    }

    [doc release];
    [xmlDateFormatter release];
    [xmlDateTimeFormatter release];
}

#pragma mark -
#pragma Memory Management Methods
-(void)dealloc
{
    [project release];
    [account release];
    [milestoneID release];
    [title release];
    [deadline release];
    [completed release];
    [projectID release];
    [createdOn release];
    [creatorID release];
    [creatorName release];
    [responsiblePartyID release];
    [responsiblePartyName release];
    [responsiblePartyType release];
    [commentsCount release];
    [completedOn release];
    [completerID release];
    [completerName release];
    [super dealloc];
}
@end
