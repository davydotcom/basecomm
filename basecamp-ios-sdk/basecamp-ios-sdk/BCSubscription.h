//
//  BCSubscription.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/6/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BCSubscription : NSObject {
    NSString *name;
    NSNumber *writeboards;
    NSNumber *projects;
    NSNumber *storage;
    NSNumber *ssl;
    NSNumber *timeTracking;
}
@property(nonatomic, retain) NSString *name;
@property(nonatomic, retain) NSNumber *writeboards;
@property(nonatomic, retain) NSNumber *projects;
@property(nonatomic, retain) NSNumber *storage;
@property(nonatomic, retain) NSNumber *ssl;
@property(nonatomic, retain) NSNumber *timeTracking;
@end
