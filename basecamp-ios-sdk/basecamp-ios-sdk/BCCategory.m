//
//  BCCategory.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCCategory.h"

#import "BCProject.h"
#import "BCCategoryConnector.h"
#import "TouchXML.h"
@implementation BCCategory
@synthesize account,project,projectID,type,name,elementsCount,categoryID,deleted;
+(void)loadCategoriesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *categories,NSError *error)) completionHandler
{
    BCCategoryConnector *categoryConnector = [[BCCategoryConnector alloc] initWithAccount:_project.account andCompletionHandler:completionHandler];
    [categoryConnector loadObjectsForProject:_project];
    [categoryConnector release];
}
+(void)loadPostCategoriesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *categories,NSError *error)) completionHandler
{
    BCCategoryConnector *categoryConnector = [[BCCategoryConnector alloc] initWithAccount:_project.account andCompletionHandler:completionHandler];
    [categoryConnector loadObjectsForProject:_project withType:@"post"];
    [categoryConnector release];
}
+(void)loadAttachmentCategoriesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *categories,NSError *error)) completionHandler
{
    BCCategoryConnector *categoryConnector = [[BCCategoryConnector alloc] initWithAccount:_project.account andCompletionHandler:completionHandler];
    [categoryConnector loadObjectsForProject:_project withType:@"attachment"];
    [categoryConnector release];
}
+(void)loadCategory:(NSInteger)_categoryID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCCategory *category,NSError *error)) completionHandler
{
    BCCategoryConnector *categoryConnector = [[BCCategoryConnector alloc] initWithAccount:_account andObjectCompletionHandler:completionHandler];
    [categoryConnector loadObject:_categoryID];
    [categoryConnector release];
}
-(id)initWithProject:(BCProject *)_project
{
    self = [super init];
    if(self)
    {
        self.project = _project;
        self.account = _project.account;
    }
    return self;
}
-(void)createWithCompletionHandler:(void (^)(BCCategory *category,NSError *error))_completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)saveWithCompletionHandler:(void (^)(BCCategory *category,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)destroyWithCompletionHandler:(void (^)(BCCategory *category,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];

    [NSThread detachNewThreadSelector:@selector(destroyThread) toTarget:self withObject:nil];

}
#pragma mark Threaded Methods
-(void)runThread
{
    
    finished = NO;
    deleting = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(self.project != nil && self.projectID == nil)
    {
        self.projectID = self.project.projectID;
    }
    if([categoryID integerValue] != 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/categories/%d.xml",[self.account requestURL],[categoryID integerValue]];           
    } 
    else 
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/categories.xml",[self.account requestURL],[projectID integerValue]];   
    }
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    if([categoryID integerValue] != 0)
    {
        [request setHTTPMethod:@"PUT"];
    }
    else
    {    
        [request setHTTPMethod:@"POST"];        
    }
    [request setHTTPBody:[self toRequestXML]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    if(error == nil && self.categoryID != nil)
    {
        finished = NO;
        [BCCategory loadCategory:[self.categoryID integerValue] forAccount:self.account withCompletionHandler:^(BCCategory *_category, NSError *_error) {
            if(_category != nil)
            {
                self.name = _category.name;
                self.projectID = _category.projectID;
                self.elementsCount = _category.elementsCount;
                self.type = _category.type;
            }
            if(_error != nil)
            {
                error = [_error copy];
            }
            finished = YES;
        }];
    }
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    NSLog(@"Running Thread!");
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void)destroyThread
{
    finished = NO;
    deleting = YES;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(self.project != nil && self.projectID == nil)
    {
        self.projectID = self.project.projectID;
    }
    
    requestString = [[NSString alloc] initWithFormat:@"%@/categories/%d",[self.account requestURL],[categoryID integerValue]];           
    
    
    
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    
    [request setHTTPMethod:@"DELETE"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}

-(void) performCompletionHandlerOnSourceThread
{
    if(error != nil)
    {
        [error autorelease];
    }
    if(completionHandler != nil)
    {
        void (^_completionHandler)(BCCategory *category,NSError *error) = completionHandler;
        completionHandler = nil;
        _completionHandler(self,error);
        [_completionHandler release];
    }
    [self release];
}
#pragma mark -
#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
    else if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodHTTPBasic])
    {
        if(authenticationAttempted)
        {
            [[challenge sender] cancelAuthenticationChallenge:challenge];
            return;
        }
        if(account != nil)
        {
            NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:account.username password:account.password persistence:NSURLCredentialPersistenceForSession];
            [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
            [credential release];
            authenticationAttempted = YES;
        }
        
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error
{
    
    error = [_error copy];
    [requestData release];
    finished = YES;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [requestData appendData:data];   
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    statusCode = [urlResponse statusCode];
    NSLog(@"Did Receive Response! %d",statusCode);
    if(statusCode == 201)
    {
        error = nil;
        NSString *location = [[[urlResponse allHeaderFields] valueForKey:@"Location"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *paths = [location componentsSeparatedByString:@"/"];
        NSString *categoryIDString = [[paths lastObject] stringByReplacingOccurrencesOfString:@".xml" withString:@""];
        
        NSNumber *_categoryID = [[NSNumber alloc] initWithInt:[categoryIDString integerValue]];
        self.categoryID = _categoryID;
        [_categoryID release];
        
    }
    else if (statusCode == 200 && deleting == YES) 
    {
        deleted = YES;
        deleting = NO;
    }
    else if (statusCode != 200)
    {
        error = [[NSError alloc] initWithDomain:@"BCCategory" code:statusCode userInfo:nil];
    }
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    NSString *requestString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    NSLog(@"Response: %@",requestString);
    [requestString release];
    [requestData release];
    finished = YES;
}

#pragma mark -
#pragma mark XML Methods
-(NSData *)toRequestXML
{
    CXMLDocument *doc = (CXMLDocument *)[CXMLDocument elementWithName: @"category"];
    
    
    
    
    
    CXMLElement *typeElement = (CXMLElement *)[CXMLNode elementWithName:@"type"];
    [typeElement setStringValue:self.type];
    [doc addChild:typeElement];
    
    CXMLElement *nameElement = (CXMLElement *)[CXMLNode elementWithName:@"name"];
    [nameElement setStringValue:self.name];
    [doc addChild:nameElement];
    
    
    NSData *returnData = [[doc XMLString] dataUsingEncoding:NSUTF8StringEncoding];
    
    return returnData;
    
}
#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc {
    [account release];
    [project release];
    [projectID release];
    [type release];
    [name release];
    [elementsCount release];
    [categoryID release];
    [super dealloc];
}
@end
