//
//  BCCategoryConnector.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCCategoryConnector.h"
#import "BCProject.h"
#import "BCCategory.h"
#import "TouchXML.h"

@implementation BCCategoryConnector
@synthesize project,type;
-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(objectID > 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/categories/%d.xml",[self.account requestURL],objectID];           
    } 
    else if(project != nil)
    {
        if(type != nil)
        {
            requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/categories.xml?type=%@",[self.account requestURL],[project.projectID integerValue],type];                 
        }
        else
        {
            requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/categories.xml",[self.account requestURL],[project.projectID integerValue]];     
        }
    }
    else 
    {
        NSError *_error = [[NSError alloc] initWithDomain:@"BCCategoryConnector" code:404 userInfo:nil];
        error = _error;
        [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];
        return;
    }
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:NO];
    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void) loadObjectsForProject:(BCProject*)_project
{
    [self retain];
    self.project = _project;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];   
}
-(void) loadObjectsForProject:(BCProject*)_project withType:(NSString*)_type
{
    [self retain];
    self.project = _project;
    self.type = _type;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];   
}
#pragma mark -
#pragma mark TouchXML Methods
-(void) parseXMLResponse
{
    CXMLDocument *doc = [[CXMLDocument alloc] initWithData:requestData options:0 error:nil];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
    [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSArray *nodes = nil;
    NSError *_error = nil;
    @try
    {
        nodes = [doc nodesForXPath:@"//category" error:&_error];
        if(_error != nil)
        {
            NSLog(@"Error Parsing XML: %d",[_error code]);
            [doc release];
            return;
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception %@",[e name]);
    }
    NSMutableArray *_objects = [[NSMutableArray alloc] initWithCapacity:1];
    for(CXMLElement *node in nodes)
    {
        BCCategory *_category = [[BCCategory alloc] init];
        _category.account = self.account;
        _category.project = self.project;
        for (CXMLElement *childNode in [node children]) 
        {
            if([[childNode name] isEqualToString:@"id"])
            {
                NSNumber *_categoryID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _category.categoryID = _categoryID;
                [_categoryID release];
            }
            else if([[childNode name] isEqualToString:@"name"])
            {
                _category.name = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"type"])
            {
                _category.type = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"project-id"])
            {
                NSNumber *_projectID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _category.projectID = _projectID;
                [_projectID release];
            }
            else if([[childNode name] isEqualToString:@"elements-count"])
            {
                NSNumber *_elementsCount =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _category.elementsCount = _elementsCount;
                [_elementsCount release];
            }
            
            
        }
        
        [_objects addObject:_category];
        [_category release];
    }
    objects = _objects;
    [doc release];
    [xmlDateFormatter release];
    [xmlDateTimeFormatter release];
}
#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    [project release];
    [type release];
    [super dealloc];
}
@end
