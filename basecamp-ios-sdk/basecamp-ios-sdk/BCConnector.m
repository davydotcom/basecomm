//
//  BCConnector.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCConnector.h"
#import "BCAccount.h"

@implementation BCConnector
@synthesize account,finished,statusCode,authenticationAttempted,objectID;

-(id) initWithAccount:(BCAccount *)_account andCompletionHandler:(void (^)(NSArray *objects,NSError *error)) _completionHandler
{
    if((self = [super init]))
    {
        objectID = 0;
        objectCompletionHandler = nil;
        completionHandler = [_completionHandler copy];
        self.account = _account;
    }
    return self;
}
-(id) initWithAccount:(BCAccount *)_account andObjectCompletionHandler:(void (^)(id object,NSError *error)) _completionHandler
{
    if((self = [super init]))
    {
        objectID = 0;
        completionHandler = nil;
        objectCompletionHandler = [_completionHandler copy];
        self.account = _account;
    }
    return self;
}
#pragma mark -
#pragma mark Main Thread Methods
-(void) loadObjects
{
    [self retain];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void) loadObject:(NSInteger)_objectID
{
    self.objectID = _objectID;
    [self retain];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];    
}
-(void) performCompletionHandlerOnSourceThread
{
    if(completionHandler != nil)
    {
        
        completionHandler(objects,error);
        [completionHandler release];
        completionHandler = nil;
    }
    if(objectCompletionHandler != nil)
    {
        
        NSObject *firstObject = nil;
        if(objects != nil && [objects count] > 0)
        {
            firstObject = [[objects objectAtIndex:0] retain];
            [objects removeObject:firstObject];
            
        }
        objectCompletionHandler([firstObject autorelease],error);
        [objectCompletionHandler release];
        objectCompletionHandler = nil;
    }
    
    [error autorelease];
    [objects autorelease];
    [self release];
}
-(void) parseXMLResponse
{
    
}
-(void)runThread
{
    
}
#pragma mark -
#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    if(authenticationAttempted)
    {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
    if(account != nil)
    {
        NSLog(@"Sending Credentials!");
        NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:account.username password:account.password persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
        [credential release];
        authenticationAttempted = YES;
    }
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error
{
    [requestData release];
    [_error retain];
    error = _error;
    finished = YES;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [requestData appendData:data];   
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    self.statusCode = [urlResponse statusCode];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if(self.statusCode == 200)
    {
        [self parseXMLResponse];
    }
    else
    {
        NSError *_error = [[NSError alloc] initWithDomain:@"BCConnector" code:self.statusCode userInfo:nil];
        error = _error;
    }
    finished = YES;
    [requestData release];
}

-(void)dealloc
{
    [account release];
    [super dealloc];
}
@end
