//
//  BCTimeEntry.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 6/10/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BCAccount, BCProject,BCTodoListItem,BCTimeEntry;
@interface BCTimeEntry : NSObject {
    BCAccount *account;
    BCProject *project;
    BCTodoListItem *todoListItem;
    NSNumber *timeEntryID;
    NSNumber *projectID;
    NSNumber *personID;
    NSDate *date;
    NSNumber *hours;
    NSString *description;
    NSNumber *todoListItemID;
    BOOL deleted;
@private
    void (^completionHandler)(BCTimeEntry *timeEntry,NSError *error);
    NSThread *sourceThread;
    NSError *error;
    NSMutableData *requestData;
    NSInteger statusCode;
    BOOL finished;
    BOOL deleting;
    BOOL authenticationAttempted;
}
@property(nonatomic, assign) BOOL deleted;
@property(nonatomic, retain) BCAccount *account;
@property(nonatomic, retain) BCProject *project;
@property(nonatomic, retain) BCTodoListItem *todoListItem;
@property(nonatomic, retain) NSNumber *timeEntryID;
@property(nonatomic, retain) NSNumber *projectID;
@property(nonatomic, retain) NSNumber *personID;
@property(nonatomic, retain) NSDate *date;
@property(nonatomic, retain) NSNumber *hours;
@property(nonatomic, retain) NSString *description;
@property(nonatomic, retain) NSNumber *todoListItemID;

//Static Methods
+(void)loadTimeEntriesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *timeEntries,NSError *error)) completionHandler;
+(void)loadTimeEntriesForTodoListItem:(BCTodoListItem *)_todoListItem withCompletionHandler:(void (^)(NSArray *timeEntries,NSError *error)) completionHandler;
+(void)loadTimeEntry:(NSInteger)_timeEntryID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCTimeEntry *timeEntry,NSError *error)) completionHandler;

-(void)createWithCompletionHandler:(void (^)(BCTimeEntry *timeEntry,NSError *error))_completionHandler;
-(void)saveWithCompletionHandler:(void (^)(BCTimeEntry *timeEntry,NSError *error)) _completionHandler;
-(void)destroyWithCompletionHandler:(void (^)(BCTimeEntry *timeEntry,NSError *error)) _completionHandler;
-(NSData *)toRequestXML;
-(void)runThread;
@end


/*<time-entry>
<id type="integer">#{id}</id>
<project-id type="integer">#{project-id}</project-id>
<person-id type="integer">#{person-id}</person-id>
<date type="date">#{date}</date>
<hours>#{hours}</hours>
<description>#{description}</description>
<todo-item-id type="integer">#{todo-item-id}</todo-item-id>
</time-entry>*/