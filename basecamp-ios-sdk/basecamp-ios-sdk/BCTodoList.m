//
//  BCTodoList.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCTodoList.h"
#import "BCProject.h"
#import "BCAccount.h"
#import "BCTodoListItem.h"
#import "BCTodoListConnector.h"
#import "TouchXML.h"
@implementation BCTodoList
@synthesize account,project,todoListID,complete,completedCount,uncompletedCount,projectID,name,description,milestoneID,isPrivate,position,tracked,todoListItems,deleted;

#pragma mark -
#pragma mark Class Methods
+(void)loadTodoListsForAccount:(BCAccount *)_account withCompletionHandler:(void (^)(NSArray *todoLists,NSError *error)) completionHandler
{
    BCTodoListConnector *todoListConnector = [[BCTodoListConnector alloc] initWithAccount:_account andCompletionHandler:completionHandler];
    [todoListConnector loadObjects];
    [todoListConnector release];
}
+(void)loadTodoListsForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *todoLists,NSError *error)) completionHandler
{
    BCTodoListConnector *todoListConnector = [[BCTodoListConnector alloc] initWithAccount:_project.account andCompletionHandler:completionHandler];
    [todoListConnector loadObjectsForProject:_project];
    [todoListConnector release];
}
+(void)loadTodoList:(NSInteger)_todoListID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCTodoList *todoList,NSError *error)) _completionHandler
{
    BCTodoListConnector *todoListConnector = [[BCTodoListConnector alloc] initWithAccount:_account andObjectCompletionHandler:_completionHandler];
    [todoListConnector loadObject:_todoListID];
    [todoListConnector release];
}

#pragma mark -
#pragma mark Instance Methods
-(id)initWithProject:(BCProject *) _project
{
    if((self = [super init]))
    {
        self.project = _project;
        self.account = _project.account;
    }
    return self;
}
-(id)initWithAccount:(BCAccount *) _account
{
    if((self = [super init]))
    {
        self.account = _account;   
    }
    return self;    
}

-(void)createWithCompletionHandler:(void (^)(BCTodoList *todoList,NSError *error))_completionHandler
{
   [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)saveWithCompletionHandler:(void (^)(BCTodoList *todoList,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)destroyWithCompletionHandler:(void (^)(BCTodoList *todoList,NSError *error)) _completionHandler
{
     [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    
    [NSThread detachNewThreadSelector:@selector(destroyThread) toTarget:self withObject:nil];
    
}
#pragma mark Threaded Methods
-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(self.project != nil && self.projectID == nil)
    {
        self.projectID = self.project.projectID;
    }
    if([todoListID integerValue] != 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/todo_lists/%d.xml",[self.account requestURL],[projectID integerValue],[todoListID integerValue]];           
    } 
    else 
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/todo_lists.xml",[self.account requestURL],[projectID integerValue],[todoListID integerValue]];   
    }
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    if([todoListID integerValue] != 0)
    {
        [request setHTTPMethod:@"PUT"];
    }
    else
    {    
        [request setHTTPMethod:@"POST"];        
    }
    [request setHTTPBody:[self toRequestXML]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    if(error == nil && self.todoListID != nil)
    {
        finished = NO;
        [BCTodoList loadTodoList:[self.todoListID integerValue] forAccount:self.account withCompletionHandler:^(BCTodoList *_todoList, NSError *_error) {
            if(_todoList != nil)
            {
                self.todoListItems = _todoList.todoListItems;
                self.description = _todoList.description;
                self.name = _todoList.name;
                self.complete = _todoList.complete;
                self.isPrivate = _todoList.isPrivate;
                self.completedCount = _todoList.completedCount;
                self.uncompletedCount = _todoList.uncompletedCount;
                self.tracked = _todoList.tracked;
                self.milestoneID = _todoList.milestoneID;
                self.position = _todoList.position;
            }
            if(_error != nil)
            {
                error = [_error copy];
            }
            finished = YES;
        }];
    }
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    NSLog(@"Running Thread!");
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void)destroyThread
{
    finished = NO;
    deleting = YES;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;

    
    requestString = [[NSString alloc] initWithFormat:@"%@/todo_lists/%d",[self.account requestURL],[todoListID integerValue]];           
    
    
    
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    
    [request setHTTPMethod:@"DELETE"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void) performCompletionHandlerOnSourceThread
{
    if(error != nil)
    {
        [error autorelease];
    }
    if(completionHandler != nil)
    {
        void (^_completionHandler)(BCTodoList *todoList,NSError *error) = completionHandler;
        completionHandler = nil;
        _completionHandler(self,error);
        [_completionHandler release];
    }
    [self release];
}
-(void) performReorderCompletionHandlerOnSourceThread:(NSArray *)_todoListItems
{
    if(error != nil)
    {
        [error autorelease];
    }
    if(completionHandler != nil)
    {
        void (^_reorderCompletionHandler)(NSArray *todoListItems,NSError *error) = reorderCompletionHandler;

        reorderCompletionHandler = nil;
        _reorderCompletionHandler(_todoListItems,error);
        [_reorderCompletionHandler release];
    }
    [self release];
}
#pragma mark -
#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
    else if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodHTTPBasic])
    {
        if(authenticationAttempted)
        {
            [[challenge sender] cancelAuthenticationChallenge:challenge];
            return;
        }
        if(account != nil)
        {
            NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:account.username password:account.password persistence:NSURLCredentialPersistenceForSession];
            [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
            [credential release];
            authenticationAttempted = YES;
        }
        
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error
{
    
    error = [_error copy];
    [requestData release];
    finished = YES;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [requestData appendData:data];   
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    statusCode = [urlResponse statusCode];
    NSLog(@"Did Receive Response! %d",statusCode);
    if(statusCode == 201)
    {
        error = nil;
        NSString *location = [[[urlResponse allHeaderFields] valueForKey:@"Location"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *paths = [location componentsSeparatedByString:@"/"];
        NSString *todoListIDString = [[paths lastObject] stringByReplacingOccurrencesOfString:@".xml" withString:@""];
        
        NSNumber *_todoListID = [[NSNumber alloc] initWithInt:[todoListIDString integerValue]];
        self.todoListID = _todoListID;
        [_todoListID release];
     
    }
    else if (statusCode == 200 && deleting == YES) 
    {
        deleted = YES;
        deleting = NO;
    }
    else if (statusCode != 200)
    {
        error = [[NSError alloc] initWithDomain:@"BCTodoList" code:statusCode userInfo:nil];
    }
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{

    
    NSString *requestString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    NSLog(@"Response: %@",requestString);
    [requestString release];
    [requestData release];
    finished = YES;
}
#pragma mark - reorder Methods
-(void)reorderTodoListItems:(NSArray *)_todoListItems withCompletionHandler:(void (^)(NSArray *todoListItems,NSError *error)) _completionHandler
{
    [self retain];
    reorderCompletionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    
    [NSThread detachNewThreadSelector:@selector(reorderThread:) toTarget:self withObject:_todoListItems];
}
-(void)reorderThread:(NSArray *)_todoListItems
{
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;

    requestString = [[NSString alloc] initWithFormat:@"%@/todo_lists/%d/todo_items/reorder.xml",[self.account requestURL],[self.todoListID integerValue]];   

    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    [request setHTTPMethod:@"POST"];        
    
    [request setHTTPBody:[self itemOrderXML:_todoListItems]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    NSInteger _position = 0;
    for(BCTodoListItem *todoListItem in _todoListItems)
    {
        NSNumber *currentPosition = [[NSNumber alloc] initWithInt:_position];
        todoListItem.position = currentPosition;
        [currentPosition release];
        _position++;
    }
    if(error == nil && self.todoListID != nil)
    {
        finished = NO;
    }

    [self performSelector:@selector(performReorderCompletionHandlerOnSourceThread:) onThread:sourceThread withObject:_todoListItems waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
#pragma mark -
#pragma mark XML Methods
-(NSData *)itemOrderXML:(NSArray *)_todoListItems
{
    
        CXMLDocument *doc = (CXMLDocument *)[CXMLDocument elementWithName: @"todo-items"];

        for(BCTodoListItem *todoListItem in _todoListItems)
        {
            CXMLElement *todoItemElement = (CXMLElement *)[CXMLNode elementWithName:@"todo-item"];
            CXMLElement *idElement = (CXMLElement *)[CXMLNode elementWithName:@"id"];
            [idElement setStringValue:[todoListItem.todoListItemID stringValue]];
            [todoItemElement addChild:idElement];
            [doc addChild:todoItemElement];
        }
        NSLog(@"Request XML: %@",[doc XMLString]);
        NSData *returnData = [[[doc XMLString] stringByReplacingOccurrencesOfString:@"<todo-items>" withString:@"<todo-items type=\"array\">"] dataUsingEncoding:NSUTF8StringEncoding];
    return returnData;
}
-(NSData *)toRequestXML
{
    CXMLDocument *doc = (CXMLDocument *)[CXMLDocument elementWithName: @"todo-list"];




    if(self.description != nil)
    {
        CXMLElement *descriptionElement = (CXMLElement *)[CXMLNode elementWithName:@"description"];
        [descriptionElement setStringValue:self.description];
        [doc addChild:descriptionElement];
    }
    
    CXMLElement *nameElement = (CXMLElement *)[CXMLNode elementWithName:@"name"];
    [nameElement setStringValue:self.name];
    [doc addChild:nameElement];

    CXMLElement *milestoneIDElement = (CXMLElement *)[CXMLNode elementWithName:@"milestone-id"];
    if([self.milestoneID integerValue] > 0)
    {
        [milestoneIDElement setStringValue:[self.milestoneID stringValue]];
    }
    else
    {
//        [milestoneIDElement attributeForName:]
    }
    [doc addChild:milestoneIDElement];
    
    CXMLElement *isPrivateElement = (CXMLElement *)[CXMLNode elementWithName:@"private"];
    if([self.isPrivate integerValue] > 0)
    {
        [isPrivateElement setStringValue:@"true"];
    }
    else
    {
        [isPrivateElement setStringValue:@"false"];
    }
    [doc addChild:isPrivateElement];
    
    CXMLElement *trackedElement = (CXMLElement *)[CXMLNode elementWithName:@"tracked"];
    if([self.tracked integerValue] > 0)
    {
        [trackedElement setStringValue:@"true"];
    }
    else
    {
        [trackedElement setStringValue:@"false"];
    }
    [doc addChild:trackedElement];
    NSLog(@"REquest XML: %@",[doc XMLString]);
    NSData *returnData = [[doc XMLString] dataUsingEncoding:NSUTF8StringEncoding];
    
    return returnData;
    
}
#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    [account release];
    [project release];
    [todoListID release];
    [projectID   release];
    [name release];
    [description release];
    [complete release];
    [completedCount release];
    [uncompletedCount release];
    [tracked release];
    [position release];
    [milestoneID release];
    [isPrivate release];
    [todoListItems release];
    [super dealloc];
}

@end
