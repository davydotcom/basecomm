//
//  BCPerson.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCPerson.h"
#import "BCAccount.h"
#import "BCPeopleConnector.h"
#import "BCProject.h"
#import "BCCompany.h"
@implementation BCPerson
@synthesize account,personID,firstName,lastName,title,emailAddress,imHandle,imService,phoneNumberOffice,phoneNumberOfficeExt,phoneNumberMobile,phoneNumberHome,phoneNumberFax,lastLogin,companyID,clientID,avatarURL,userName,administrator,deleted,hasAccessToNewProjects,project,company;
#pragma mark -
#pragma mark Class Methods
+(void)loadPeopleForAccount:(BCAccount *)_account withCompletionHandler:(void (^)(NSArray *people,NSError *error)) _completionHandler
{
    BCPeopleConnector *peopleConnector = [[BCPeopleConnector alloc] initWithAccount:_account andCompletionHandler:_completionHandler];
    [peopleConnector loadObjects];
    [peopleConnector release];
}
+(void)loadPeopleForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *people,NSError *error)) _completionHandler
{
    BCPeopleConnector *peopleConnector = [[BCPeopleConnector alloc] initWithAccount:_project.account andCompletionHandler:_completionHandler];
    [peopleConnector loadObjectsForProject:_project];
    [peopleConnector release];
}
+(void)loadPeopleForCompany:(BCCompany *)_company withCompletionHandler:(void (^)(NSArray *people,NSError *error)) _completionHandler
{
    BCPeopleConnector *peopleConnector = [[BCPeopleConnector alloc] initWithAccount:_company.account andCompletionHandler:_completionHandler];
    [peopleConnector loadObjectsForCompany:_company];
    [peopleConnector release];
}

+(void)loadPerson:(NSInteger)_personID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCPerson *person,NSError *error)) _completionHandler
{
    BCPeopleConnector *peopleConnector = [[BCPeopleConnector alloc] initWithAccount:_account andObjectCompletionHandler:_completionHandler];
    [peopleConnector loadObject:_personID];
    [peopleConnector release];
}
+(void)loadCurrentPersonforAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCPerson *person,NSError *error)) _completionHandler
{
    BCPeopleConnector *peopleConnector = [[BCPeopleConnector alloc] initWithAccount:_account andObjectCompletionHandler:_completionHandler];
    [peopleConnector loadMe];
    [peopleConnector release];
}
#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    [account release];
    [project release];
    [company release];
    [personID release];
    [firstName release];
    [lastName release];
    [title release];
    [emailAddress release];
    [imHandle release];
    [imService release];
    [phoneNumberOffice release];
    [phoneNumberOfficeExt release];
    [phoneNumberMobile release];
    [phoneNumberHome release];
    [phoneNumberFax release];
    [lastLogin release];
    [companyID release];
    [clientID release];
    [avatarURL release];
    [userName release];
    [administrator release];
    [deleted release];
    [hasAccessToNewProjects release];
    [super dealloc];
}

@end
