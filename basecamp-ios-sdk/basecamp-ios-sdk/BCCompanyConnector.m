//
//  BCCompanyConnector.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCCompanyConnector.h"
#import "BCProject.h"
#import "BCCompany.h"
#import "BCAccount.h"
#import "TouchXML.h"
@implementation BCCompanyConnector
@synthesize project;
-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(objectID > 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/companies/%d.xml",[self.account requestURL],objectID];           
    } 
    else if(project != nil)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/companies.xml",[self.account requestURL],[project.projectID integerValue]];     
    }
    else 
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/companies.xml",[self.account requestURL]];   
    }
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:NO];
    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void) loadObjectsForProject:(BCProject*)_project
{
    [self retain];
    self.project = _project;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];   
}
#pragma mark -
#pragma mark TouchXML Methods
-(void) parseXMLResponse
{
    CXMLDocument *doc = [[CXMLDocument alloc] initWithData:requestData options:0 error:nil];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
    [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSArray *nodes = nil;
    NSError *_error = nil;
    @try
    {
        nodes = [doc nodesForXPath:@"//company" error:&_error];
        if(_error != nil)
        {
            NSLog(@"Error Parsing XML: %d",[_error code]);
            [doc release];
            return;
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception %@",[e name]);
    }
    NSMutableArray *_objects = [[NSMutableArray alloc] initWithCapacity:1];
    for(CXMLElement *node in nodes)
    {
        BCCompany *_company = [[BCCompany alloc] init];
        _company.account = self.account;
        
        for (CXMLElement *childNode in [node children]) 
        {
            if([[childNode name] isEqualToString:@"id"])
            {
                NSNumber *_companyID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _company.companyID = _companyID;
                [_companyID release];
            }
            else if([[childNode name] isEqualToString:@"name"])
            {
                _company.name = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"address-one"])
            {
                _company.addressOne = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"address-two"])
            {
                _company.addressTwo = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"city"])
            {
                _company.city = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"state"])
            {
                _company.state = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"zip"])
            {
                _company.zip = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"country"])
            {
                _company.country = [childNode stringValue];
            }
            
            else if([[childNode name] isEqualToString:@"web-address"])
            {
                _company.webAddress = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"phone-number-office"])
            {
                _company.phoneNumberOffice = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"phone-number-fax"])
            {
                _company.phoneNumberFax = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"time-zone-id"])
            {
                _company.timeZoneID = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"can-see-private"])
            {
                NSNumber *canSeePrivate = nil;
                if([[childNode stringValue] isEqualToString:@"true"])
                {
                    canSeePrivate = [[NSNumber alloc] initWithBool:YES];
                }
                _company.canSeePrivate = canSeePrivate;
                [canSeePrivate release];
            }
            else if([[childNode name] isEqualToString:@"url-name"])
            {
                _company.urlName = [childNode stringValue];
            }
                        
        }
        
        [_objects addObject:_company];
        [_company release];
    }
    objects = _objects;
    [doc release];
    [xmlDateFormatter release];
    [xmlDateTimeFormatter release];
}

-(void)dealloc
{
    [project release];
    [super dealloc];
}

@end
