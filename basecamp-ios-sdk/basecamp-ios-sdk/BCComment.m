//
//  BCComment.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 6/8/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCComment.h"
#import "BCAccount.h"
#import "BCMilestone.h"
#import "BCTodoListItem.h"
#import "BCMessage.h"
#import "BCCommentConnector.h"
#import "TouchXML.h"
@implementation BCComment
@synthesize account,commentID,commentableID,commentableType,commentableObject,authorID,authorName,attachments,attachmentsCount,emailedFrom,createdAt,body,deleted;

+(void)loadCommentsForMessage:(BCMessage *)_message withCompletionHandler:(void (^)(NSArray *comments,NSString *continuedAt,NSError *error)) completionHandler
{
    BCCommentConnector *commentConnector = [[BCCommentConnector alloc] initWithAccount:_message.account andCompletionHandler:completionHandler];
    [commentConnector loadObjectsForMessage:_message];
    [commentConnector release];
  
}
+(void)loadCommentsForMilestone:(BCMilestone *)_milestone withCompletionHandler:(void (^)(NSArray *comments,NSString *continuedAt,NSError *error)) completionHandler
{
    BCCommentConnector *commentConnector = [[BCCommentConnector alloc] initWithAccount:_milestone.account andCompletionHandler:completionHandler];
    [commentConnector loadObjectsForMilestone:_milestone];
    [commentConnector release];
}
+(void)loadCommentsForTodoListItem:(BCTodoListItem *)_todoListItem withCompletionHandler:(void (^)(NSArray *comments,NSString *continuedAt,NSError *error)) completionHandler
{
    BCCommentConnector *commentConnector = [[BCCommentConnector alloc] initWithAccount:_todoListItem.account andCompletionHandler:completionHandler];
    [commentConnector loadObjectsForTodoListItem:_todoListItem];
    [commentConnector release];
}
+(void)loadCommentsForURLString:(NSString *)_urlString andAccount:(BCAccount *)_account withCompletionHandler:(void (^)(NSArray *comments,NSString *continuedAt,NSError *error)) completionHandler
{
    BCCommentConnector *commentConnector = [[BCCommentConnector alloc] initWithAccount:_account andCompletionHandler:completionHandler];
    [commentConnector loadObjectsWithUrl:_urlString];
    [commentConnector release];    
}
+(void)loadComment:(NSInteger)_commentID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCComment *comment,NSError *error)) completionHandler
{
    BCCommentConnector *commentConnector = [[BCCommentConnector alloc] initWithAccount:_account andObjectCompletionHandler:completionHandler];
    [commentConnector loadObject:_commentID];
    [commentConnector release];
}


-(void)createWithCompletionHandler:(void (^)(BCComment *comment,NSError *error))_completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)saveWithCompletionHandler:(void (^)(BCComment *comment,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
}
-(void)destroyWithCompletionHandler:(void (^)(BCComment *comment,NSError *error)) _completionHandler
{
    [self retain];
    completionHandler = [_completionHandler copy];
    sourceThread = [NSThread currentThread];
    
    [NSThread detachNewThreadSelector:@selector(destroyThread) toTarget:self withObject:nil];
    
}
#pragma mark Threaded Methods
-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;

    if([commentID integerValue] != 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/comments/%d.xml",[self.account requestURL],[commentID integerValue]];           
    } 
    else 
    {
        if([self.commentableType isEqualToString:@"Milestone"])
        {
            requestString = [[NSString alloc] initWithFormat:@"%@/milestones/%d/comments.xml",[self.account requestURL],[commentableID integerValue]];   
        }
        else if([self.commentableType isEqualToString:@"Post"])
        {
            requestString = [[NSString alloc] initWithFormat:@"%@/posts/%d/comments.xml",[self.account requestURL],[commentableID integerValue]];   
        }
        else
        {
            requestString = [[NSString alloc] initWithFormat:@"%@/todo_items/%d/comments.xml",[self.account requestURL],[commentableID integerValue]];   
        }

    }
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    if([commentID integerValue] != 0)
    {
        [request setHTTPMethod:@"PUT"];
        [request setHTTPBody:[self toRequestXML]];
    }
    else
    {    
        [request setHTTPMethod:@"POST"];        
        [request setHTTPBody:[self toCreateXML]];
    }

    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    if(error == nil && self.commentID != nil)
    {
        finished = NO;
        [BCComment loadComment:[self.commentID integerValue] forAccount:self.account withCompletionHandler:^(BCComment *_comment, NSError *_error) {
            if(_comment != nil)
            {
                self.body = _comment.body;
                self.commentableID = _comment.commentableID;
                self.commentableType = _comment.commentableType;
                self.authorID = _comment.authorID;
                self.authorName = _comment.authorName;
                self.createdAt = _comment.createdAt;
                self.emailedFrom = _comment.emailedFrom;
            }
            if(_error != nil)
            {
                error = [_error copy];
            }
            finished = YES;
        }];
    }
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    NSLog(@"Running Thread!");
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void)destroyThread
{
    finished = NO;
    deleting = YES;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    
    
    requestString = [[NSString alloc] initWithFormat:@"%@/comments/%d",[self.account requestURL],[commentID integerValue]];           
    
    
    
    NSLog(@"Requesting: %@",requestString);
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];    
    
    [request setHTTPMethod:@"DELETE"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void) performCompletionHandlerOnSourceThread
{
    if(error != nil)
    {
        [error autorelease];
    }
    if(completionHandler != nil)
    {
        void (^_completionHandler)(BCComment *comment,NSError *error) = completionHandler;
        completionHandler = nil;
        _completionHandler(self,error);
        [_completionHandler release];
    }
    [self release];
}
#pragma mark -
#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
    else if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodHTTPBasic])
    {
        if(authenticationAttempted)
        {
            [[challenge sender] cancelAuthenticationChallenge:challenge];
            return;
        }
        if(account != nil)
        {
            NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:account.username password:account.password persistence:NSURLCredentialPersistenceForSession];
            [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
            [credential release];
            authenticationAttempted = YES;
        }
        
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error
{
    
    error = [_error copy];
    [requestData release];
    finished = YES;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [requestData appendData:data];   
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    statusCode = [urlResponse statusCode];
    NSLog(@"Did Receive Response! %d",statusCode);
    if(statusCode == 201)
    {
        error = nil;
        NSString *location = [[[urlResponse allHeaderFields] valueForKey:@"Location"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *paths = [location componentsSeparatedByString:@"/"];
        NSString *commentIDString = [[paths lastObject] stringByReplacingOccurrencesOfString:@".xml" withString:@""];
        
        NSNumber *_commentID = [[NSNumber alloc] initWithInt:[commentIDString integerValue]];
        self.commentID = _commentID;
        [_commentID release];
    }
    else if (statusCode == 200 && deleting == YES) 
    {
        deleted = YES;
        deleting = NO;
    }
    else if (statusCode != 200)
    {
        error = [[NSError alloc] initWithDomain:@"BCTodoList" code:statusCode userInfo:nil];
    }
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    NSString *requestString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    
    [requestString release];
    [requestData release];
    finished = YES;
}
#pragma mark - XML Methods

-(NSData *)toRequestXML
{
    CXMLDocument *doc = (CXMLDocument *)[CXMLDocument elementWithName: @"request"];
    
    CXMLElement *commentElement = (CXMLElement *)[CXMLNode elementWithName:@"comment"];
    [doc addChild:commentElement];
    
    
    CXMLElement *bodyElement = (CXMLElement *)[CXMLNode elementWithName:@"body"];
    [bodyElement setStringValue:self.body];
    [commentElement addChild:bodyElement];        
    
    NSData *returnData = [[doc XMLString] dataUsingEncoding:NSUTF8StringEncoding];
    
    return returnData;
    
}
-(NSData *)toCreateXML
{
    CXMLDocument *doc = (CXMLDocument *)[CXMLDocument elementWithName: @"comment"];
    

    
    
    CXMLElement *bodyElement = (CXMLElement *)[CXMLNode elementWithName:@"body"];
    [bodyElement setStringValue:self.body];
    [doc addChild:bodyElement];        
    
    NSData *returnData = [[doc XMLString] dataUsingEncoding:NSUTF8StringEncoding];
    
    return returnData;
    
}
-(void)dealloc
{
    [account release];
    [commentableObject release];
    [commentableID release];
    [commentableType release];
    [commentID release];
    [authorID release];
    [authorName release];
    [body release];
    [emailedFrom release];
    [createdAt release];
    [attachmentsCount release];
    [attachments release];
    [super dealloc];
}
@end
