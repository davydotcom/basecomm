//
//  BCWebSession.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/11/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCWebSession.h"
#import "BCAccount.h"

@implementation BCWebSession
@synthesize account,headerFields,cookies;
-(id)initWithAccount:(BCAccount *)_account
{
    self = [super init];
    if(self)
    {
        self.account = _account;
        NSMutableArray *_cookies = [[NSMutableArray alloc] initWithCapacity:1];
        
        self.cookies = _cookies;
        [_cookies release];
    }
    return self;
}

-(void)loadWebSessionWithCompletionHandler:(void (^)(BCWebSession *_webSession,NSError *error)) _completionHandler
{
    completionHandler = [_completionHandler copy];
    
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];
    
}

-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = [[NSString alloc] initWithFormat:@"https://launchpad.37signals.com/authenticate"];   
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    NSString *postString = [[NSString alloc] initWithFormat:@"&product=basecamp&subdomain=%@&username=%@&password=%@&commit=Sign%%20in",[self.account.accountURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[self.account.username stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[self.account.password stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSData *_requestData = [NSData dataWithBytes:[postString UTF8String] length:[postString length]];
    NSLog(@"Request String: %@",requestString);
    NSLog(@"Posting: %@",postString);
    NSString *cookiesString = [[NSString alloc] initWithFormat:@"return_to=%@",[[self.account requestURL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [request setValue:cookiesString forHTTPHeaderField:@"Cookie"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:_requestData];
    [postString release];
    NSString *referer = [[NSString alloc] initWithFormat:@"%@/login",[account requestURL]];
    [request setValue:referer forHTTPHeaderField:@"Referer"];
    [referer release];
    

    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:NO];
    
    [request release];
    [connection release];
    [autoreleasepool release];
}

-(void) performCompletionHandlerOnSourceThread
{
    [error autorelease];
    if(completionHandler != nil)
    {
        void (^_completionHandler)(BCWebSession *_webSession,NSError *error) = completionHandler;
        completionHandler = nil;
        _completionHandler(self,error);
        [_completionHandler release];
    }
}
#pragma mark -
#pragma mark NSURLConnectionDelegate Methods
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    if(authenticationAttempted)
    {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
    
    NSLog(@"Sending Credentials!");
//    NSURLCredential *credential = [[NSURLCredential alloc] initWithUser:self.username password:self.password persistence:NSURLCredentialPersistenceForSession];
//    [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
//    [credential release];
    authenticationAttempted = YES;
    
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error
{
    [requestData release];
    [_error retain];
    error = _error;
    finished = YES;
}
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)redirectResponse;
    statusCode = [urlResponse statusCode];
    NSLog(@"Response URL: %@",[[urlResponse URL] absoluteString]);
    NSDictionary *_headerFields = [urlResponse allHeaderFields];
    NSArray *keys = [_headerFields allKeys];
    for(NSString *key in keys)
    {
        NSLog(@"Parsing key: %@ : %@",key,[_headerFields valueForKey:key]);
        if([key isEqualToString:@"Set-Cookie"])
        {
            NSArray *cookieComponents = [[_headerFields valueForKey:key] componentsSeparatedByString:@";"];
            NSArray *cookieNameValue = [[cookieComponents objectAtIndex:0] componentsSeparatedByString:@"="];
            [self setCookie:[cookieNameValue objectAtIndex:0] withValue:[cookieNameValue objectAtIndex:1]];
            
        }
    }
    NSDictionary *requestFields = [request allHTTPHeaderFields];
    for(NSString *_key in [requestFields allKeys])
    {
        NSLog(@"Redirecting with Field: %@ = %@",_key,[requestFields valueForKey:_key]);
    }
//    NSMutableURLRequest *responseRequest = [NSMutableURLRequest requestWithURL:[request URL]];
//    [responseRequest setHTTPShouldHandleCookies:<#(BOOL)#>
//    [responseRequest setValue:[self cookiesString] forHTTPHeaderField:@"Cookie"]
//    [responseRequest setHTTPBody:[request HTTPBody]];
    [request retain];
    
    return request;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [requestData appendData:data];   
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    statusCode = [urlResponse statusCode];
    NSLog(@"Response URL: %@",[[urlResponse URL] absoluteString]);
    NSDictionary *_headerFields = [urlResponse allHeaderFields];
    NSArray *keys = [_headerFields allKeys];
    for(NSString *key in keys)
    {
        NSLog(@"Parsing key: %@ : %@",key,[_headerFields valueForKey:key]);
        if([key isEqualToString:@"Set-Cookie"])
        {
            NSArray *cookieComponents = [[_headerFields valueForKey:key] componentsSeparatedByString:@";"];
            NSArray *cookieNameValue = [[cookieComponents objectAtIndex:0] componentsSeparatedByString:@"="];
            [self setCookie:[cookieNameValue objectAtIndex:0] withValue:[cookieNameValue objectAtIndex:1]];
//          //  NSDictionary *cookie = nil;
//            if([cookieComponents count] > 1)
//            {
////                cookie = [[NSDictionary alloc] initWithObjectsAndKeys:[cookieComponents objectAtIndex:0],@"cookie",[cookieComponents objectAtIndex:1],@"expires", nil];
//            }
//            else
//            {
////                cookie = [[NSDictionary alloc] initWithObjectsAndKeys:[cookieComponents objectAtIndex:0],@"cookie",@"",@"expires", nil];
//            }
//            
//            //[cookies addObject:cookie];
//            [cookie release];
            
        }
    }
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"Status: %d",statusCode);
    if(statusCode == 200)
    {
        
    }
    else
    {
        NSLog(@"Not successful1");
        
    }
    NSString *requestString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    NSLog(@"Received: %@",requestString);
    
    for(NSDictionary *cookie in cookies)
    {
        NSLog(@"Cookie: %@ ; %@",[cookie valueForKey:@"cookie"],[cookie valueForKey:@"expires"]);
    }
    finished = YES;
    [requestData release];
}
-(void)setCookie:(NSString *)_cookieName withValue:(NSString *)_value
{
    for(NSDictionary *cookie in cookies)
    {
        if([[cookie valueForKey:@"name"] isEqualToString:_cookieName])
        {   
            [cookies removeObject:cookie];
            break;
        }
    }
    NSDictionary *newCookie = [[NSDictionary alloc] initWithObjectsAndKeys:_cookieName,@"name",_value,@"value",@"",@"expires", nil];
    [cookies addObject:newCookie];
    [newCookie release];
}
-(NSString *) cookiesString
{
    NSMutableString *cookieString = [NSMutableString stringWithCapacity:25];
    for(NSDictionary *cookie in cookies)
    {
        [cookieString appendString:[cookie valueForKey:@"cookie"]];
        if(cookie == [cookies lastObject])
        {
            [cookieString appendString:@";"];
        }
    }
    return cookieString;
}
#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    [account release];
    [headerFields release];
    [cookies release];
    [super dealloc];
}
@end
