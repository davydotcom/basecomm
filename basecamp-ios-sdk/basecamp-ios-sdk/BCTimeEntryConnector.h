//
//  BCTimeEntryConnector.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 6/10/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCConnector.h"
@class BCProject,BCTodoListItem;
@interface BCTimeEntryConnector : BCConnector {
    BCProject *project;
    BCTodoListItem *todoListItem;
}
@property(nonatomic,retain) BCProject *project;
@property(nonatomic, retain) BCTodoListItem *todoListItem;
-(void)runThread;
-(void) loadObjectsForProject:(BCProject*)_project;
-(void) loadObjectsForTodoListItem:(BCTodoListItem*)_todoListItem;
-(void) parseXMLResponse;
@end
