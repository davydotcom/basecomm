//
//  BCTimeEntryConnector.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 6/10/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCTimeEntryConnector.h"
#import "BCTimeEntry.h"
#import "BCProject.h"
#import "BCTodoListItem.h"
#import "TouchXML.h"
@implementation BCTimeEntryConnector
@synthesize todoListItem,project;
-(void)runThread
{
    
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    NSString *requestString = nil;
    if(objectID > 0)
    {
        requestString = [[NSString alloc] initWithFormat:@"%@/time_entries/%d.xml",[self.account requestURL],objectID];           
    } 
    else if(project != nil)
    {
            requestString = [[NSString alloc] initWithFormat:@"%@/projects/%d/time_entries.xml",[self.account requestURL],[project.projectID integerValue]];     
    }
    else if(self.todoListItem != nil)
    {
            requestString = [[NSString alloc] initWithFormat:@"%@/todo_items/%d/time_entries.xml",[self.account requestURL],[todoListItem.todoListItemID integerValue]];     
    }
    else 
    {
        NSError *_error = [[NSError alloc] initWithDomain:@"BCTimeEntryConnector" code:404 userInfo:nil];
        error = _error;
        [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:YES];
        return;
    }
    NSURL *url = [NSURL URLWithString:requestString];
    [requestString release];
    requestData = [[NSMutableData alloc] init];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    [self performSelector:@selector(performCompletionHandlerOnSourceThread) onThread:sourceThread withObject:nil waitUntilDone:NO];
    
    [request release];
    [connection release];
    [autoreleasepool release];
}
-(void) loadObjectsForProject:(BCProject*)_project
{
    [self retain];
    self.project = _project;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];   
}
-(void) loadObjectsForTodoListItem:(BCTodoListItem*)_todoListItem;
{
    [self retain];
    self.todoListItem = _todoListItem;
    sourceThread = [NSThread currentThread];
    [NSThread detachNewThreadSelector:@selector(runThread) toTarget:self withObject:nil];   
}
#pragma mark -
#pragma mark TouchXML Methods
-(void) parseXMLResponse
{
    CXMLDocument *doc = [[CXMLDocument alloc] initWithData:requestData options:0 error:nil];
    NSDateFormatter *xmlDateFormatter = [[NSDateFormatter alloc] init];
    [xmlDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *xmlDateTimeFormatter = [[NSDateFormatter alloc] init];
    [xmlDateTimeFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSArray *nodes = nil;
    NSError *_error = nil;
    @try
    {
        nodes = [doc nodesForXPath:@"//time-entry" error:&_error];
        if(_error != nil)
        {
            NSLog(@"Error Parsing XML: %d",[_error code]);
            [doc release];
            return;
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception %@",[e name]);
    }
    NSMutableArray *_objects = [[NSMutableArray alloc] initWithCapacity:1];
    for(CXMLElement *node in nodes)
    {
        BCTimeEntry *_timeEntry = [[BCTimeEntry alloc] init];
        _timeEntry.account = self.account;
        _timeEntry.project = self.project;
        _timeEntry.todoListItem = self.todoListItem;

        for (CXMLElement *childNode in [node children]) 
        {
            if([[childNode name] isEqualToString:@"id"])
            {
                NSNumber *_ID =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _timeEntry.timeEntryID = _ID;
                [_ID release];
            }
            else if([[childNode name] isEqualToString:@"description"])
            {
                _timeEntry.description = [childNode stringValue];
            }
            else if([[childNode name] isEqualToString:@"person-id"])
            {
                NSNumber *_nodeNumber =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _timeEntry.personID = _nodeNumber;
                [_nodeNumber release];
            }
            else if([[childNode name] isEqualToString:@"hours"])
            {
                NSNumber *_nodeNumber =  [[NSNumber alloc] initWithFloat:[[childNode stringValue] floatValue]];
                _timeEntry.hours = _nodeNumber;
                [_nodeNumber release];
            }
            else if([[childNode name] isEqualToString:@"todo-item-id"])
            {
                NSNumber *_nodeNumber =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _timeEntry.todoListItemID = _nodeNumber;
                [_nodeNumber release];
            }
            else if([[childNode name] isEqualToString:@"project-id"])
            {
                NSNumber *_nodeNumber =  [[NSNumber alloc] initWithInt:[[childNode stringValue] integerValue]];
                _timeEntry.projectID = _nodeNumber;
                [_nodeNumber release];
            }
            else if([[childNode name] isEqualToString:@"date"])
            {
                _timeEntry.date = [xmlDateFormatter dateFromString:[childNode stringValue]];
            }
        }   
        
        [_objects addObject:_timeEntry];
        [_timeEntry release];
    }
    objects = _objects;
    [doc release];
    [xmlDateFormatter release];
    [xmlDateTimeFormatter release];
}

-(void)dealloc
{
    [todoListItem release];
    [project release];
    [super dealloc];
}
@end
