//
//  BCSubscription.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/6/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCSubscription.h"


@implementation BCSubscription
@synthesize name,writeboards,projects,storage,ssl,timeTracking;
#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    [name release];
    [writeboards release];
    [projects release];
    [storage release];
    [ssl release];
    [timeTracking release];
    [super dealloc];
}
@end
