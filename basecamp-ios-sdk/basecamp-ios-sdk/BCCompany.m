//
//  BCCompany.m
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCCompany.h"
#import "BCAccount.h"
#import "BCProject.h"
#import "BCCompanyConnector.h"
@implementation BCCompany
@synthesize project,account,companyID,name,addressOne,addressTwo,city,state,zip,country,webAddress,phoneNumberFax,phoneNumberOffice,timeZoneID,canSeePrivate,urlName;


+(void)loadCompaniesForAccount:(BCAccount *)_account withCompletionHandler:(void (^)(NSArray *companies,NSError *error)) completionHandler
{
    BCCompanyConnector *companyConnector = [[BCCompanyConnector alloc] initWithAccount:_account andCompletionHandler:completionHandler];
    [companyConnector loadObjects];
    [companyConnector release];
}
+(void)loadCompaniesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *companies,NSError *error)) completionHandler
{
    BCCompanyConnector *companyConnector = [[BCCompanyConnector alloc] initWithAccount:_project.account andCompletionHandler:completionHandler];
    [companyConnector loadObjectsForProject:_project];
    [companyConnector release];
}

+(void)loadCompany:(NSInteger)_companyID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCCompany *company,NSError *error)) completionHandler
{
    BCCompanyConnector *companyConnector = [[BCCompanyConnector alloc] initWithAccount:_account andObjectCompletionHandler:completionHandler];
    [companyConnector loadObject:_companyID];
    [companyConnector release];
}

#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    [project release];
    [account release];
    [companyID release];
    [name release];
    [addressOne release];
    [addressTwo release];
    [city release];
    [state release];
    [zip release];
    [country release];
    [webAddress release];
    [phoneNumberFax release];
    [phoneNumberOffice release];
    [timeZoneID release];
    [canSeePrivate release];
    [urlName release];

    [super dealloc];
}
@end
