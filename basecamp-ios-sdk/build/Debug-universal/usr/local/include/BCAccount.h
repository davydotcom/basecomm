//
//  BCAccount.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCSubscription;
@class BCAccount;
@interface BCAccount : NSObject {
    NSString *accountURL;
    NSString *username;
    NSString *password;
    BOOL requireSSL;
    NSNumber *accountID;
    NSString *name;
    NSNumber *accountHolderID;
    NSNumber *primaryCompanyID;
    NSNumber *emailNotificationEnabled;
    NSNumber *timeTrackingEnabled;
    NSDate *updatedAt;
    BCSubscription *subscription;
    NSArray *defaultAttachmentCategories;
    NSArray *defaultPostCategories;
@private
    void (^completionHandler)(BCAccount *account,NSError *error);
    NSThread *sourceThread;
    NSError *error;
    NSMutableData *requestData;
    NSInteger statusCode;
    BOOL finished;
    BOOL authenticationAttempted;
}
@property(nonatomic,assign) BOOL requireSSL;
@property(nonatomic,retain) NSString *accountURL;
@property(nonatomic,retain) NSString *username;
@property(nonatomic,retain) NSString *password;
@property(nonatomic, retain) NSNumber *accountID;
@property(nonatomic, retain) NSString *name;
@property(nonatomic, retain) NSNumber *accountHolderID;
@property(nonatomic, retain) NSNumber *primaryCompanyID;
@property(nonatomic, retain) NSNumber *emailNotificationEnabled;
@property(nonatomic, retain) NSNumber *timeTrackingEnabled;
@property(nonatomic, retain) NSDate *updatedAt;
@property(nonatomic, retain) BCSubscription *subscription;
@property(nonatomic, retain) NSArray *defaultAttachmentCategories;
@property(nonatomic, retain) NSArray *defaultPostCategories;

-(id)initWithAccountURL:(NSString *)_accountURL username:(NSString *)_username password:(NSString *)_password;
-(NSString *)requestURL;
-(void) parseXMLResponse;
-(void)loadAccountDetailsWithCompletionHandler:(void (^)(BCAccount *_account,NSError *error)) _completionHandler;
-(void)runThread;
@end
