//
//  BCProjectConnector.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCConnector.h"
@class BCAccount;
@class BCProject;

@interface BCProjectConnector : BCConnector {

}
-(void)runThread;
-(void) parseXMLResponse;
@end
