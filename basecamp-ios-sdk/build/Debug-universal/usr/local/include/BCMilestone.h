//
//  BCMilestone.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCAccount;
@class BCProject;
@class BCMilestone;
@interface BCMilestone : NSObject {
    BCProject *project;
    BCAccount *account;
    
    NSNumber *milestoneID;
    NSString *title;
    NSDate *deadline;
    NSNumber *completed;
    NSNumber *projectID;
    NSDate *createdOn;
    NSNumber *creatorID;
    NSString *creatorName;
    NSNumber *responsiblePartyID;
    NSString *responsiblePartyType;
    NSString *responsiblePartyName;
    NSNumber *commentsCount;
    NSDate *completedOn;
    NSNumber *completerID;
    NSString *completerName;
    BOOL deleted;
@private
    void (^completionHandler)(BCMilestone *milestone,NSError *error);
    NSThread *sourceThread;
    NSError *error;
    NSMutableData *requestData;
    NSInteger statusCode;
    BOOL finished;
    BOOL deleting;
    BOOL authenticationAttempted;
   BOOL notify;
}


@property(nonatomic, retain) BCProject *project;
@property(nonatomic, retain) BCAccount *account;
@property(nonatomic, assign) BOOL deleted;
@property(nonatomic, retain) NSNumber *milestoneID;
@property(nonatomic, retain) NSString *title;
@property(nonatomic, retain) NSDate *deadline;
@property(nonatomic, retain) NSNumber *completed;
@property(nonatomic, retain) NSNumber *projectID;
@property(nonatomic, retain) NSDate *createdOn;
@property(nonatomic, retain) NSNumber *creatorID;
@property(nonatomic, retain) NSString *creatorName;
@property(nonatomic, retain) NSNumber *responsiblePartyID;
@property(nonatomic, retain) NSString *responsiblePartyType;
@property(nonatomic, retain) NSString *responsiblePartyName;
@property(nonatomic, retain) NSNumber *commentsCount;
@property(nonatomic, retain) NSDate *completedOn;
@property(nonatomic, retain) NSNumber *completerID;
@property(nonatomic, retain) NSString *completerName;

+(void)loadMilestonesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *milestones,NSError *error)) completionHandler;
-(void)saveWithCompletionHandler:(void (^)(BCMilestone *milestone,NSError *error)) _completionHandler notify:(BOOL)_notify;
-(void)createWithCompletionHandler:(void (^)(BCMilestone *milestone,NSError *error)) _completionHandler notify:(BOOL)_notify;
-(void)markCompleteWithCompletionHandler:(void (^)(BCMilestone *milestone,NSError *error)) _completionHandler;
-(void)markIncompleteWithCompletionHandler:(void (^)(BCMilestone *milestone,NSError *error)) _completionHandler;
-(void)destroyWithCompletionHandler:(void (^)(BCMilestone *milestone,NSError *error)) _completionHandler;
-(id)initWithProject:(BCProject *) _project;
-(id)initWithAccount:(BCAccount *) _account;
-(void) parseXMLResponse;
-(NSData *)toRequestXML;
@end
