//
//  BCCategory.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCAccount;
@class BCProject;
@class BCCategory;
@interface BCCategory : NSObject {
    BCAccount *account;
    BCProject *project;
    
    NSNumber *categoryID;
    NSString *name;
    NSNumber *projectID;
    NSNumber *elementsCount;
    NSString *type;
    BOOL deleted;
@private
    void (^completionHandler)(BCCategory *category,NSError *error);
    NSThread *sourceThread;
    NSError *error;
    NSMutableData *requestData;
    NSInteger statusCode;
    BOOL finished;
    BOOL deleting;
    BOOL authenticationAttempted;
}
@property(nonatomic,assign) BOOL deleted;
@property(nonatomic,retain) BCAccount *account;
@property(nonatomic,retain) BCProject *project;
@property(nonatomic,retain) NSNumber *categoryID;
@property(nonatomic,retain) NSString *name;
@property(nonatomic,retain) NSNumber *projectID;
@property(nonatomic,retain) NSNumber *elementsCount;
@property(nonatomic,retain) NSString *type;
+(void)loadCategoriesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *categories,NSError *error)) completionHandler;
-(id)initWithProject:(BCProject *)_project;
+(void)loadPostCategoriesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *categories,NSError *error)) completionHandler;
+(void)loadAttachmentCategoriesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *categories,NSError *error)) completionHandler;
+(void)loadCategory:(NSInteger)_categoryID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCCategory *category,NSError *error)) completionHandler;
-(void)createWithCompletionHandler:(void (^)(BCCategory *category,NSError *error))_completionHandler;
-(void)saveWithCompletionHandler:(void (^)(BCCategory *category,NSError *error)) _completionHandler;
-(NSData *)toRequestXML;
-(void)runThread;
@end
