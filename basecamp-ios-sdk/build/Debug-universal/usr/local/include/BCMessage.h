//
//  BCMessage.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCCategory, BCProject, BCAccount, BCMessage;
@interface BCMessage : NSObject {
    NSNumber *messageID;
    NSString *title;
    NSString *displayBody;
    NSString *body;
    NSNumber *authorID;
    NSString *authorName;
    NSDate *postedOn;
    NSDate *commentedAt;
    NSNumber *attachmentsCount;
    NSNumber *commentsCount;
    NSNumber *isPrivate;
    BCCategory *category;
    BCProject *project;
    BCAccount *account;
    NSNumber *projectID;
    NSNumber *categoryID;
    NSNumber *milestoneID;
    NSNumber *useTextile;
    NSArray *notifyPeople;
    BOOL deleted;
@private
    BOOL deleting;
    void (^completionHandler)(BCMessage *message,NSError *error);
    NSThread *sourceThread;
    NSError *error;
    NSMutableData *requestData;
    NSInteger statusCode;
    BOOL finished;
    BOOL authenticationAttempted;
}
@property(nonatomic, assign) BOOL deleted;
@property(nonatomic,retain) NSNumber *messageID;
@property(nonatomic,retain) NSNumber *projectID;
@property(nonatomic,retain) NSNumber *categoryID;
@property(nonatomic,retain) NSNumber *milestoneID;
@property(nonatomic,retain) NSNumber *isPrivate;
@property(nonatomic,retain) NSString *title;
@property(nonatomic,retain) NSString *body;
@property(nonatomic,retain) NSString *displayBody;
@property(nonatomic,retain) NSNumber *authorID;
@property(nonatomic,retain) NSString *authorName;
@property(nonatomic,retain) NSDate *postedOn;
@property(nonatomic,retain) NSDate *commentedAt;
@property(nonatomic,retain) NSNumber *attachmentsCount;
@property(nonatomic,retain) NSNumber *commentsCount;
@property(nonatomic,retain) NSNumber *useTextile;
@property(nonatomic,retain) NSArray *notifyPeople;
@property(nonatomic,retain) BCCategory *category;
@property(nonatomic,retain) BCProject *project;
@property(nonatomic,retain) BCAccount *account;

-(id)initWithProject:(BCProject *) _project;
-(id)initWithAccount:(BCAccount *) _account;
-(void)createWithCompletionHandler:(void (^)(BCMessage *message,NSError *error))_completionHandler;
-(void)saveWithCompletionHandler:(void (^)(BCMessage *message,NSError *error)) _completionHandler;
-(void)destroyWithCompletionHandler:(void (^)(BCMessage *message,NSError *error)) _completionHandler;

+(void)loadMessagesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *messages,NSError *error)) completionHandler;
+(void)loadMessagesForProject:(BCProject *)_project inCategory:(BCCategory *)_category withCompletionHandler:(void (^)(NSArray *messages,NSError *error)) completionHandler;
+(void)loadMessage:(NSInteger)_messageID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCMessage *message,NSError *error)) _completionHandler;
-(NSData *)toRequestXML;
@end
/*
 
 XML FORMAT:
 
 <post>
 <id type="integer">#{id}</id>
 <title>#{title}</title>
 <body>#{body}</body>
 <display-body>#{display_body}</display-body>
 <posted-on type="datetime">#{posted_on}</posted-on>
 <commented-at type="datetime">#{commented_at}</commented-at>
 <project-id type="integer">#{project_id}</project-id>
 <category-id type="integer">#{category_id}</category-id>
 <author-id type="integer">#{author_id}</author-id>
 <author-name>#{author_name}</author-name>
 <milestone-id type="integer">#{milestone_id}</milestone-id>
 <comments-count type="integer">#{comments_count}</comments-count>
 <use-textile type="boolean">#{use_textile}</use-textile>
 <extended-body deprecated="true">#{extended_body}</extended-body>
 <display-extended-body deprecated="true">#{display_extended_body}</display-extended-body>
 
 <attachments-count type="integer">#{attachments_count}</attachments-count>
 <attachments type="array">
 <attachment>
 ...
 </attachment>
 ...
 </attachments>
 
 <!-- if user can see private posts -->
 <private type="boolean">#{private}</private>
 </post>
*/