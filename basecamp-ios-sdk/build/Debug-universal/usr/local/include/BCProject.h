//
//  BCProject.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCAccount;
@class BCProject;
@class BCTodoList;
@class BCMilestone;
@class BCMessage;
@interface BCProject : NSObject {
    BCAccount *account;
    NSNumber *projectID;
    NSString *name;
    NSDate *createdOn;
    NSDate *lastChangedOn;
    NSString *status;
    NSString *announcement;
    NSNumber *showAnnouncement;
    NSNumber *showWriteboards;
    NSString *startPage;
    NSMutableArray *companies;
    
    NSMutableArray *todoLists;
    
@private
    
    void (^completionHandler)(BCProject *project,NSError *error);
    void (^reorderCompletionHandler)(NSArray *todoLists,NSError *error);
    NSThread *sourceThread;
    NSError *error;
    NSInteger statusCode;
    BOOL finished;
    BOOL authenticationAttempted;
    NSMutableData *requestData;
    
}
@property(nonatomic,retain) NSMutableArray *todoLists;
@property(nonatomic,retain) NSString *announcement;
@property(nonatomic,retain) NSNumber *showAnnouncement;
@property(nonatomic,retain) NSNumber *showWriteboards;
@property(nonatomic,retain) NSString *startPage;
@property(nonatomic,retain) NSMutableArray *companies;
@property(nonatomic,retain) BCAccount *account;
@property(nonatomic,retain) NSNumber *projectID;
@property(nonatomic,retain) NSString *name;
@property(nonatomic,retain) NSDate *createdOn;
@property(nonatomic,retain) NSDate *lastChangedOn;
@property(nonatomic,retain) NSString *status;

+(void)loadProjectsForAccount:(BCAccount *)_account withCompletionHandler:(void (^)(NSArray *projects,NSError *error)) completionHandler;
+(void)loadProject:(NSInteger)_projectID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCProject *project,NSError *error)) completionHandler;
-(void)reorderTodoLists:(NSArray *)_todoLists withCompletionHandler:(void (^)(NSArray *todoLists,NSError *error)) _completionHandler;
-(id)initWithAccount:(BCAccount *)_account;
-(void)createWithCompletionHandler:(void (^)(BCProject *project,NSError *error))_completionHandler;
-(void)saveWithCompletionHandler:(void (^)(BCProject *project,NSError *error))_completionHandler;
-(NSData *)listOrderXML:(NSArray *)_todoLists;
-(NSData *)toRequestXML;
@end
