//
//  BCComment.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 6/8/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCAccount,BCMessage,BCTodoListItem,BCMilestone,BCComment;
@interface BCComment : NSObject {
    BCAccount *account;
    NSObject *commentableObject;
    NSNumber *commentID;
    NSNumber *authorID;
    NSString *authorName;
    NSNumber *commentableID;
    NSString *commentableType;
    NSString *body;
    NSString *emailedFrom;
    NSDate *createdAt;    
    NSNumber *attachmentsCount;
    NSArray *attachments;
    BOOL deleted;
@private
    void (^completionHandler)(BCComment *comment,NSError *error);
    NSThread *sourceThread;
    NSError *error;
    NSMutableData *requestData;
    NSInteger statusCode;
    BOOL finished;
    BOOL deleting;
    BOOL authenticationAttempted;
}
@property(nonatomic, assign) BOOL deleted;
@property(nonatomic, retain) BCAccount *account;
@property(nonatomic, retain) NSObject *commentableObject;
@property(nonatomic, retain) NSNumber *commentID;
@property(nonatomic, retain) NSNumber *authorID;
@property(nonatomic, retain) NSString *authorName;
@property(nonatomic, retain) NSNumber *commentableID;
@property(nonatomic, retain) NSString *commentableType;
@property(nonatomic, retain) NSString *body;
@property(nonatomic, retain) NSString *emailedFrom;
@property(nonatomic, retain) NSDate *createdAt;    
@property(nonatomic, retain) NSNumber *attachmentsCount;
@property(nonatomic, retain) NSArray *attachments;

//Static Methods
+(void)loadCommentsForMessage:(BCMessage *)_message withCompletionHandler:(void (^)(NSArray *comments,NSString *continuedAt,NSError *error)) completionHandler;
+(void)loadCommentsForMilestone:(BCMilestone *)_milestone withCompletionHandler:(void (^)(NSArray *comments,NSString *continuedAt,NSError *error)) completionHandler;
+(void)loadCommentsForTodoListItem:(BCTodoListItem *)_todoListItem withCompletionHandler:(void (^)(NSArray *comments,NSString *continuedAt,NSError *error)) completionHandler;
+(void)loadCommentsForURLString:(NSString *)_urlString andAccount:(BCAccount *)_account withCompletionHandler:(void (^)(NSArray *comments,NSString *continuedAt,NSError *error)) completionHandler;
+(void)loadComment:(NSInteger)_commentID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCComment *comment,NSError *error)) completionHandler;

-(void)createWithCompletionHandler:(void (^)(BCComment *comment,NSError *error))_completionHandler;
-(void)saveWithCompletionHandler:(void (^)(BCComment *comment,NSError *error)) _completionHandler;
-(void)destroyWithCompletionHandler:(void (^)(BCComment *comment,NSError *error)) _completionHandler;
-(NSData *)toRequestXML;
-(NSData *)toCreateXML;
-(void)runThread;
@end



/*<comment>
<id type="integer">#{id}</id>
<author-id type="integer">#{author_id}</author-id>
<author-name>#{author_name}</author-name>
<commentable-id type="integer">#{commentable_id}</commentable-id>
<commentable-type>#{commentable_type}</commentable-type>
<body>#{body}</body>
<emailed-from nil="true">#{emailed_from}</emailed-from>
<created-at type="datetime">#{created_at}</created-at>

<attachments-count type="integer">#{attachments_count}</attachments-count>
<attachments type="array">
<attachment>
...
</attachment>
...
</attachments>
</comment>*/