//
//  BCCompany.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCAccount;
@class BCProject;
@interface BCCompany : NSObject {
    BCAccount *account;
    BCProject *project;
    NSNumber *companyID;
    NSString *name;
    NSString *addressOne;
    NSString *addressTwo;
    NSString *city;
    NSString *state;
    NSString *zip;
    NSString *country;
    NSString *webAddress;
    NSString *phoneNumberOffice;
    NSString *phoneNumberFax;
    NSString *timeZoneID;
    NSNumber *canSeePrivate;
    NSString *urlName;
}

@property(nonatomic,retain) BCAccount *account;
@property(nonatomic,retain) BCProject *project;

@property(nonatomic,retain) NSNumber *companyID;
@property(nonatomic,retain) NSString *name;
@property(nonatomic,retain) NSString *addressOne;
@property(nonatomic,retain) NSString *addressTwo;
@property(nonatomic,retain) NSString *city;
@property(nonatomic,retain) NSString *state;
@property(nonatomic,retain) NSString *zip;
@property(nonatomic,retain) NSString *country;
@property(nonatomic,retain) NSString *webAddress;
@property(nonatomic,retain) NSString *phoneNumberOffice;
@property(nonatomic,retain) NSString *phoneNumberFax;
@property(nonatomic,retain) NSString *timeZoneID;
@property(nonatomic,retain) NSNumber *canSeePrivate;
@property(nonatomic,retain) NSString *urlName;

+(void)loadCompaniesForAccount:(BCAccount *)_account withCompletionHandler:(void (^)(NSArray *companies,NSError *error)) completionHandler;

+(void)loadCompany:(NSInteger)_companyID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCCompany *company,NSError *error)) completionHandler;
+(void)loadCompaniesForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *companies,NSError *error)) completionHandler;
@end
