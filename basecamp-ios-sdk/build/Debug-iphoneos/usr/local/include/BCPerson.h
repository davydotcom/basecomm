//
//  BCPerson.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCAccount;
@class BCProject;
@class BCCompany;
@interface BCPerson : NSObject {
    BCAccount *account;
    BCProject *project;
    BCCompany *company;
    NSNumber *personID;
    NSString *firstName;
    NSString *lastName;
    NSString *title;
    NSString *emailAddress;
    NSString *imHandle;
    NSString *imService;
    NSString *phoneNumberOffice;
    NSString *phoneNumberOfficeExt;
    NSString *phoneNumberMobile;
    NSString *phoneNumberHome;
    NSString *phoneNumberFax;
    NSDate *lastLogin;
    NSNumber *companyID;
    NSNumber *clientID;
    NSString *avatarURL;
    NSString *userName;
    NSNumber *administrator;
    NSNumber *deleted;
    NSNumber *hasAccessToNewProjects;
}

@property(nonatomic, retain) BCAccount *account;
@property(nonatomic, retain) BCProject *project;
@property(nonatomic, retain) BCCompany *company;
@property(nonatomic, retain) NSNumber *personID;
@property(nonatomic, retain) NSString *firstName;
@property(nonatomic, retain) NSString *lastName;
@property(nonatomic, retain) NSString *title;
@property(nonatomic, retain) NSString *emailAddress;
@property(nonatomic, retain) NSString *imHandle;
@property(nonatomic, retain) NSString *imService;
@property(nonatomic, retain) NSString *phoneNumberOffice;
@property(nonatomic, retain) NSString *phoneNumberOfficeExt;
@property(nonatomic, retain) NSString *phoneNumberMobile;
@property(nonatomic, retain) NSString *phoneNumberHome;
@property(nonatomic, retain) NSString *phoneNumberFax;
@property(nonatomic, retain) NSDate *lastLogin;
@property(nonatomic, retain) NSNumber *companyID;
@property(nonatomic, retain) NSNumber *clientID;
@property(nonatomic, retain) NSString *avatarURL;
@property(nonatomic, retain) NSString *userName;
@property(nonatomic, retain) NSNumber *administrator;
@property(nonatomic, retain) NSNumber *deleted;
@property(nonatomic, retain) NSNumber *hasAccessToNewProjects;

+(void)loadPeopleForAccount:(BCAccount *)_account withCompletionHandler:(void (^)(NSArray *people,NSError *error)) _completionHandler;
+(void)loadPeopleForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *people,NSError *error)) _completionHandler;
+(void)loadPeopleForCompany:(BCCompany *)_company withCompletionHandler:(void (^)(NSArray *people,NSError *error)) _completionHandler;
+(void)loadPerson:(NSInteger)_personID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCPerson *person,NSError *error)) _completionHandler;
+(void)loadCurrentPersonforAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCPerson *person,NSError *error)) _completionHandler;
@end
