//
//  BCWebSession.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/11/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCAccount;
@class BCWebSession;
@interface BCWebSession : NSObject {
    BCAccount *account;
    NSMutableDictionary *headerFields;
    NSMutableArray *cookies;
@private
    void (^completionHandler)(BCWebSession *_webSession,NSError *error);
    NSThread *sourceThread;
    NSError *error;
    NSMutableData *requestData;
    NSInteger statusCode;

    BOOL finished;
    BOOL authenticationAttempted;
}
@property(nonatomic, retain) NSMutableArray *cookies;
@property(nonatomic, retain) BCAccount *account;
@property(nonatomic, retain) NSMutableDictionary *headerFields;

-(id)initWithAccount:(BCAccount *)_account;
-(void)loadWebSessionWithCompletionHandler:(void (^)(BCWebSession *_webSession,NSError *error)) _completionHandler;
-(void)setCookie:(NSString *)_cookieName withValue:(NSString *)_value;
-(NSString *) cookiesString;
@end
