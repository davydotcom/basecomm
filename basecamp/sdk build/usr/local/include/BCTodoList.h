//
//  BCTodoList.h
//  basecamp-ios-sdk
//
//  Created by David Estes on 3/2/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCAccount;
@class BCProject;
@class BCMilestone;
@class BCTodoListItem;
@class BCTodoList;
@interface BCTodoList : NSObject {
    NSNumber *todoListID;    
    NSNumber *completedCount;
    NSString *description;
    NSNumber *milestoneID;
    NSString *name;
    NSNumber *tracked;
    NSNumber *projectID;
    NSNumber *uncompletedCount;
    NSNumber *complete;
    NSNumber *isPrivate;
    NSNumber *position;
    BCProject *project;
    BCAccount *account;
    NSMutableArray *todoListItems;
    BOOL deleted;
@private
    BOOL deleting;
    void (^completionHandler)(BCTodoList *todoList,NSError *error);
    void (^reorderCompletionHandler)(NSArray *todoListItems,NSError *error);
    NSThread *sourceThread;
    NSError *error;
    NSMutableData *requestData;
    NSInteger statusCode;
    BOOL finished;
    BOOL authenticationAttempted;
}
@property(nonatomic, assign) BOOL deleted;
@property(nonatomic,retain) NSMutableArray *todoListItems;
@property(nonatomic,retain) NSNumber *todoListID;    
@property(nonatomic,retain) NSNumber *completedCount;
@property(nonatomic,retain) NSString *description;
@property(nonatomic,retain) NSNumber *milestoneID;
@property(nonatomic,retain) NSString *name;
@property(nonatomic,retain) NSNumber *tracked;
@property(nonatomic,retain) NSNumber *projectID;
@property(nonatomic,retain) NSNumber *uncompletedCount;
@property(nonatomic,retain) NSNumber *complete;
@property(nonatomic,retain) NSNumber *isPrivate;
@property(nonatomic,retain) NSNumber *position;
@property(nonatomic,retain) BCProject *project;
@property(nonatomic,retain) BCAccount *account;

+(void)loadTodoListsForAccount:(BCAccount *)_account withCompletionHandler:(void (^)(NSArray *todoLists,NSError *error)) completionHandler;
+(void)loadTodoListsForProject:(BCProject *)_project withCompletionHandler:(void (^)(NSArray *todoLists,NSError *error)) completionHandler;
+(void)loadTodoList:(NSInteger)_todoListID forAccount:(BCAccount *)_account withCompletionHandler:(void (^)(BCTodoList *todoList,NSError *error)) _completionHandler;

-(void)reorderTodoListItems:(NSArray *)_todoListItems withCompletionHandler:(void (^)(NSArray *todoListItems,NSError *error)) _completionHandler;
-(id)initWithProject:(BCProject *) _project;
-(id)initWithAccount:(BCAccount *) _account;
-(void)saveWithCompletionHandler:(void (^)(BCTodoList *todoList,NSError *error)) _completionHandler;
-(void)createWithCompletionHandler:(void (^)(BCTodoList *todoList,NSError *error)) _completionHandler;
-(void)destroyWithCompletionHandler:(void (^)(BCTodoList *todoList,NSError *error)) _completionHandler;
//Don't Call these methods -- Private
-(NSData *)itemOrderXML:(NSArray *)_todoListItems;
-(NSData *)toRequestXML;
@end
