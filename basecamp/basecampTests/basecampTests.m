//
//  basecampTests.m
//  basecampTests
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "basecampTests.h"


@implementation basecampTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in basecampTests");
}

@end
