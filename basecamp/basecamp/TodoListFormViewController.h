//
//  TodoListFormViewController.h
//  basecamp
//
//  Created by David Estes on 4/22/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Project,TodoList;
@class TodoListFormViewController,RWMilestoneSelector,Milestone;
@protocol TodoListFormDelegate <NSObject>
-(void)todoListFormViewController:(TodoListFormViewController *)_todoListFormViewController didSaveTodoList:(TodoList *)_todoList;
-(void)didCancelTodoListFormViewController:(TodoListFormViewController *)_todoListFormViewController;
@end
@interface TodoListFormViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate> {
    UITextField *nameTextField;
    UITableViewCell *nameCellView;
    UITableViewCell *descriptionCellView;
    UILabel *milestoneLabel;
    NSNumber *milestoneID;
    UITableViewCell *milestoneTableViewCell;
    UITableViewCell *privateTableViewCell;
    UISwitch *privateSwitch;
    IBOutlet UITableView *tableView;
    NSString *description;
    Project *project;
    TodoList *todoList;
    RWMilestoneSelector *rwMilestoneSelector;
    id <TodoListFormDelegate> delegate;
    Milestone *milestone;
}
@property(nonatomic, retain) RWMilestoneSelector *rwMilestoneSelector;
@property(nonatomic, assign) id delegate;
@property(nonatomic, retain) Milestone *milestone;
@property(nonatomic, retain) UITableView *tableView;
@property(nonatomic, retain) NSString *description;
@property(nonatomic, retain) Project *project;
@property(nonatomic, retain) TodoList *todoList;
@property(nonatomic, retain) UITextField *nameTextField;
@property(nonatomic, retain) UITableViewCell *nameCellView;
@property(nonatomic, retain) UITableViewCell *descriptionCellView;
@property(nonatomic, retain) UILabel *milestoneLabel;
@property(nonatomic, retain) NSNumber *milestoneID;
@property(nonatomic, retain) UITableViewCell *milestoneTableViewCell;
@property(nonatomic, retain) UITableViewCell *privateTableViewCell;
@property(nonatomic, retain) UISwitch *privateSwitch;

-(IBAction)cancelButtonPressed:(id)sender;
-(IBAction)saveButtonPressed:(id)sender;
@end
