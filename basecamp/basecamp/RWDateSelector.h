//
//  RWDateSelector.h
//  basecamp
//
//  Created by David Estes on 3/26/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RWDateSelector;
@protocol DateSelectorDelegate <NSObject>
-(void)rwDateSelector:(RWDateSelector *)_rwDateSelector didSelectDate:(NSDate*)_dueDate;
@end
@interface RWDateSelector : UIViewController {
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIView *selectionView;
    UIView *parentView;
    NSDate *dueDate;
    id <DateSelectorDelegate> delegate;
}
@property(nonatomic, retain) UIView *parentView;
@property(nonatomic, assign) id delegate;
@property(nonatomic,retain)  UIDatePicker *datePicker;
@property(nonatomic,retain)  UIView *selectionView;
@property(nonatomic,retain)  NSDate *dueDate;
-(IBAction)doneButtonPressed:(id)sender;
-(IBAction)anytimeButtonPressed:(id)sender;
-(void)slideIn;
-(void)slideOut;
-(IBAction)dateValueChanged:(id)sender;
@end
