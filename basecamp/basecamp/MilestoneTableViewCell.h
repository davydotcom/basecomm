//
//  MilestoneTableViewCell.h
//  basecamp
//
//  Created by David Estes on 4/8/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MilestoneTableViewCell : UITableViewCell {
    UIButton *checkButton;
    UILabel *titleLabel;
    UILabel *assignDueLabel;
}

@property(nonatomic,retain) UIButton *checkButton;
@property(nonatomic,retain) UILabel *titleLabel;
@property(nonatomic,retain) UILabel *assignDueLabel;
@end
