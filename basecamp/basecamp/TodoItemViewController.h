//
//  TodoItemViewController.h
//  basecamp
//
//  Created by David Estes on 3/20/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
@class TodoList,NSFetchedResultsController;
@interface TodoItemViewController : UIViewController <EGORefreshTableHeaderDelegate,NSFetchedResultsControllerDelegate,UITableViewDelegate,UITableViewDataSource> {
    IBOutlet UITableView *tableView;
    IBOutlet UILabel *projectTitleLabel;
    IBOutlet UILabel *todoListTitleLabel;
    TodoList *todoList;
    NSFetchedResultsController *fetchedResultsController;
    BOOL _reloading;
    EGORefreshTableHeaderView *_refreshHeaderView;
}
@property(nonatomic,retain) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, retain) TodoList *todoList;
@property(nonatomic, retain) UITableView *tableView;
@property(nonatomic, retain) UILabel *projectTitleLabel;
@property(nonatomic, retain) UILabel *todoListTitleLabel;
-(void)registerForNotifications;
-(void)updateTodoListItems:(NSNotification *)notification;
-(void)unregisterForNotifications;
-(void)doneLoadingTableViewData;
@end
