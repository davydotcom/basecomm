//
//  TodoItemCompleteTableViewCell.m
//  basecamp
//
//  Created by David Estes on 3/21/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "TodoItemCompleteTableViewCell.h"


@implementation TodoItemCompleteTableViewCell
@synthesize titleLabel,checkButton;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(35.0f, 10.0f, 265.0f, 25.0f)];
        _titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
        _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _titleLabel.numberOfLines = 1;
        _titleLabel.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1];
        self.titleLabel = _titleLabel;
        [_titleLabel release];

        UIButton *_checkButton = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 10.0, 25.0f, 25.0f)];
        [_checkButton setImage:[UIImage imageNamed:@"button-checked-small.png"] forState:UIControlStateNormal];
//        [_checkButton setImage:[UIImage imageNamed:@"button-unchecked-small.png"] forState:UIControlStateHighlighted];
        self.checkButton = _checkButton;
        [_checkButton release];
        [self.contentView addSubview:checkButton];
        [self.contentView addSubview:titleLabel];

        
    }
    return self;
}

- (void)dealloc
{
    [checkButton release];
    [titleLabel release];
    [super dealloc];
}

@end
