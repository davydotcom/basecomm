//
//  Milestone.m
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import "Milestone.h"
#import "Project.h"
#import "TodoList.h"
#import "BCMilestone.h"
#import "BCAccount.h"
#import "BAccount.h"
#import "Comment.h"
#import "Message.h"
@implementation Milestone
@dynamic milestoneID;
@dynamic title;
@dynamic deadline;
@dynamic completed;
@dynamic projectID;
@dynamic createdOn;
@dynamic creatorID;
@dynamic mark;
@dynamic creatorName;
@dynamic responsiblePartyID;
@dynamic responsiblePartyType;
@dynamic responsiblePartyName;
@dynamic commentsCount;
@dynamic completedOn;
@dynamic completerID;
@dynamic completerName;
@dynamic project;
@dynamic todoLists;
@dynamic synced;
@dynamic deleted;
@dynamic notify;
@dynamic comments;
@dynamic messages;
@synthesize finished;
+(NSArray *)unsyncedMilestonesWithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Milestone" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"synced == 0"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"milestoneID" ascending:YES];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return returnArray;
}
+(NSArray *)allMilestonesForProjectID:(NSInteger)_projectID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Milestone" inManagedObjectContext:_managedObjectContext];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"milestoneID" ascending:YES];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"project.projectID == %d",_projectID];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return returnArray;
}
+(Milestone *)findByMilestoneID:(NSInteger)_milestoneID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Milestone" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"milestoneID == %d",_milestoneID];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"milestoneID" ascending:YES];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *results = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    results = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    Milestone *_milestone = nil;
    if(results != nil && [results count] > 0)
    {
        _milestone = (Milestone *)[results objectAtIndex:0];
    }
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return _milestone;
}
#pragma mark - Submit New Objects

-(void)syncMilestone
{
    self.finished = NO;
    BCMilestone *_bcMilestone = [self BCMilestoneFromMilestone];
    if(![self.deleted boolValue] && ![self.synced boolValue] && ![self.mark boolValue])
    {
    
    
        [_bcMilestone saveWithCompletionHandler:^(BCMilestone *_milestone, NSError *error) {
            if(error == nil)
            {
                self.milestoneID = _milestone.milestoneID;
                self.projectID = _milestone.projectID;
                //TODO: Add to proper project if exists
                self.project = [Project findByProjectID:[self.projectID integerValue] withManagedObjectContext:[self managedObjectContext]];
                self.title = _milestone.title;
                self.deadline = _milestone.deadline;
                self.createdOn = _milestone.createdOn;
                self.creatorID = _milestone.creatorID;
                self.creatorName = _milestone.creatorName;
                self.responsiblePartyID = _milestone.responsiblePartyID;
                self.responsiblePartyName = _milestone.responsiblePartyName;
                self.responsiblePartyType = _milestone.responsiblePartyType;
                self.completed = _milestone.completed;
                self.completedOn = _milestone.completedOn;
                self.completerID = _milestone.completerID;
                self.completerName = _milestone.completerName;
                self.commentsCount = _milestone.commentsCount;
                
                self.synced = [NSNumber numberWithBool:YES];
                [[self managedObjectContext] save:nil];
            }
            finished = YES;
        } notify:[self.notify boolValue]];
    }
    else if(![self.deleted boolValue] && ![self.synced boolValue] && [self.mark boolValue])
    {
        [_bcMilestone saveWithCompletionHandler:^(BCMilestone *_milestone, NSError *error) {
            if(error == nil)
            {
                self.milestoneID = _milestone.milestoneID;
                self.projectID = _milestone.projectID;
                //TODO: Add to proper project if exists
                self.project = [Project findByProjectID:[self.projectID integerValue] withManagedObjectContext:[self managedObjectContext]];
                self.title = _milestone.title;
                self.deadline = _milestone.deadline;
                self.createdOn = _milestone.createdOn;
                self.creatorID = _milestone.creatorID;
                self.creatorName = _milestone.creatorName;
                self.responsiblePartyID = _milestone.responsiblePartyID;
                self.responsiblePartyName = _milestone.responsiblePartyName;
                self.responsiblePartyType = _milestone.responsiblePartyType;
                self.completed = _milestone.completed;
                self.completedOn = _milestone.completedOn;
                self.completerID = _milestone.completerID;
                self.completerName = _milestone.completerName;
                self.commentsCount = _milestone.commentsCount;
                
                self.synced = [NSNumber numberWithBool:YES];
                [[self managedObjectContext] save:nil];
                if([self.completed boolValue])
                {
                    [_milestone markCompleteWithCompletionHandler:^(BCMilestone *__milestone, NSError *_error) {
                        if(_error == nil)
                        {
                            self.mark = [NSNumber numberWithBool:NO];
                            [[self managedObjectContext] save:nil];
                        }
                        finished = YES;
                    }];
                }
                else
                {
                    [_milestone markIncompleteWithCompletionHandler:^(BCMilestone *__milestone, NSError *_error) {
                        if(_error == nil)
                        {
                            self.mark = [NSNumber numberWithBool:NO];
                            [[self managedObjectContext] save:nil];
                        }
                        finished = YES;
                    }];
                }
            }
            
            else
            {
                finished = YES;
            }
        } notify:[self.notify boolValue]];
    }
    else if(![self.deleted boolValue] && [self.synced boolValue] && [self.mark boolValue])
    {
        if([self.completed boolValue])
        {
            [_bcMilestone markCompleteWithCompletionHandler:^(BCMilestone *__milestone, NSError *_error) {
                if(_error == nil)
                {
                    self.mark = [NSNumber numberWithBool:NO];
                    [[self managedObjectContext] save:nil];
                }
                finished = YES;
            }];
        }
        else
        {
            [_bcMilestone markIncompleteWithCompletionHandler:^(BCMilestone *__milestone, NSError *_error) {
                if(_error == nil)
                {
                    self.mark = [NSNumber numberWithBool:NO];
                    [[self managedObjectContext] save:nil];
                }
                finished = YES;
            }];
        }
    }
    else
    {
        [_bcMilestone destroyWithCompletionHandler:^(BCMilestone *milestone, NSError *error) {
            if(error == nil)
            {
                [[self managedObjectContext] deleteObject:self];
                [[self managedObjectContext] save:nil];
            }
            finished = YES;
        }];
    }
    
}

-(BCMilestone *) BCMilestoneFromMilestone
{
    BCAccount *_account = [self.project.account basecampAccount];
    BCMilestone *_bcMilestone = [[BCMilestone alloc] initWithAccount:_account];
    _bcMilestone.projectID = self.projectID;
    if (self.projectID == nil && self.project != nil) {
        _bcMilestone.projectID = self.project.projectID;
    }
    _bcMilestone.milestoneID = self.milestoneID;
    _bcMilestone.deadline = self.deadline;
    _bcMilestone.responsiblePartyType = self.responsiblePartyType;
    _bcMilestone.responsiblePartyName = self.responsiblePartyName;
    _bcMilestone.responsiblePartyID = self.responsiblePartyID;
    _bcMilestone.title = self.title;
    _bcMilestone.completed = self.completed;
    _bcMilestone.completedOn = self.completedOn;
    _bcMilestone.completerID = self.completerID;
    _bcMilestone.completerName = self.completerName;
    _bcMilestone.createdOn = self.createdOn;
    _bcMilestone.creatorID = self.creatorID;
    _bcMilestone.creatorName = self.creatorName;
    _bcMilestone.commentsCount = self.commentsCount;
    return _bcMilestone;
}
#pragma mark - CoreData RelationShip Methods
- (void)addTodoListsObject:(TodoList *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"todoLists"] addObject:value];
    [self didChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)removeTodoListsObject:(TodoList *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"todoLists"] removeObject:value];
    [self didChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addTodoLists:(NSSet *)value {    
    [self willChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"todoLists"] unionSet:value];
    [self didChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeTodoLists:(NSSet *)value {
    [self willChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"todoLists"] minusSet:value];
    [self didChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


@end
