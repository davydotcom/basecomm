//
//  CategorySyncController.m
//  basecamp
//
//  Created by David Estes on 6/11/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "CategorySyncController.h"
#import <CoreData/CoreData.h>
#import "Project.h"
#import "BCCategory.h"
#import "BCategory.h"
#import "BAccount.h"
#import "BCProject.h"
#import "SyncController.h"
#import "basecampAppDelegate.h"
@implementation CategorySyncController
@synthesize account,project;
@synthesize managedObjectContext,parentObjectContext,sourceThread;
@synthesize finished;
#pragma mark - Sync Methods

-(id)initWithProject:(Project* )_project
{
    self = [super init];
    if(self)
    {
        self.project = _project;
    }
    return self;
}
-(void)startSyncing
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [self performSelector:@selector(runSyncThread) onThread:appDelegate.syncController.syncThread withObject:nil waitUntilDone:NO];
}
-(void) fetchAccountWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"BAccount" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.account];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.account = [array objectAtIndex:0];
    }
    
}
-(void) fetchProjectWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.project];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.project = [array objectAtIndex:0];
    }
    
}
-(void)runSyncThread
{
    [self retain];
    
    finished = NO;
    BOOL createdOwnContext = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(self.managedObjectContext == nil)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        self.managedObjectContext = [appDelegate.syncController managedObjectContext];
        createdOwnContext = YES;
    }
    if(self.account != nil)
    {
        [self fetchAccountWithNewManagedObjectContext:managedObjectContext];
    }
    if(self.project != nil)
    {
        [self fetchProjectWithNewManagedObjectContext:managedObjectContext];
    }
    
    
    //Get Array of all companies from CoreData

    NSArray *categories = [BCategory allCategoriesForProject:self.project WithManagedObjectContext:managedObjectContext];

    
    //Then We Fetch all Projects from server to sync with
    if(self.project != nil)
    {
        BCProject *_bcProject = [[BCProject alloc] initWithAccount:[project.account basecampAccount]];
        _bcProject.projectID = self.project.projectID;

        [BCCategory loadCategoriesForProject:_bcProject withCompletionHandler:^(NSArray *_bccategories, NSError *error) {
            if(error == nil)
            {
                //First Check to see if any categories have been removed!
                for(BCategory *_dbCategory in categories)
                {
                    BOOL found=NO;
                    for(BCCategory *_bcCategory in _bccategories)
                    {
                        if ([_bcCategory.categoryID integerValue] == [_dbCategory.categoryID integerValue]) {
                            found = YES;
                        }
                    }
                    if(!found)
                    {
                        [managedObjectContext deleteObject:_dbCategory];
                    }
                }

                //Add New Projects and Update Existing
                for(BCCategory *_bcCategory in _bccategories)
                {
                    BOOL found = NO;
                    for(BCategory *_dbCategory in categories)
                    {
                        if ([_bcCategory.categoryID integerValue] == [_dbCategory.categoryID integerValue]) {
                            //Update record
                            _dbCategory.name = _bcCategory.name;
                            _dbCategory.type = _bcCategory.type;
                            found = YES;
                        }
                    }
                    if(!found)
                    {
                        BCategory *_dbCategory = (BCategory *)[NSEntityDescription insertNewObjectForEntityForName:@"BCategory" inManagedObjectContext:managedObjectContext];
                        _dbCategory.categoryID = _bcCategory.categoryID;
                        _dbCategory.name = _bcCategory.name;
                        _dbCategory.project = self.project;
                        _dbCategory.type = _bcCategory.type;
                    }
                }
            }
            finished = YES;
        }];
    }
       while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];  
    }
    // NSLog(@"Saving Changes!");
    NSError *error=nil;
    if(![managedObjectContext save:&error])
    {
        NSLog(@"Error Saving to CoreData After Category Sync");
    }
    [self performSelectorOnMainThread:@selector(didFinishSyncingCategoriesNotification) withObject:nil waitUntilDone:NO];
    if(createdOwnContext)
    {
        self.managedObjectContext = nil;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];   
    }
    [autoreleasepool release];
    [self release];
}

-(void)registerForManagedObjectNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mergeChanges:) name:NSManagedObjectContextDidSaveNotification object:self.managedObjectContext];
    
}
-(void)mergeChanges:(NSNotification *)notification
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *mainContext = [appDelegate managedObjectContext];
    [mainContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:) withObject:notification waitUntilDone:YES];        
}
-(void)didRemoveCategoryNotification:(BCategory *)_category
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:DID_REMOVE_CATEGORY_NOTIFICATION object:self userInfo:[NSDictionary dictionaryWithObject:_category forKey:@"category"]];
}
-(void)didFinishSyncingCategoriesNotification
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:CATEGORIES_DID_SYNC_NOTIFICATION object:nil];
}

-(void)dealloc
{
    [account release];
    [project release];
    [sourceThread release];
    [parentObjectContext release];
    [managedObjectContext release];
    [super dealloc];
}
@end
