//
//  NewAccountViewController.h
//  basecamp
//
//  Created by David Estes on 3/8/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RWTableViewController.h"
@class NSManagedObjectModel;
@class BAccount;
@interface NewAccountViewController : RWTableViewController {
    UITextField *urlField;
    UITextField *usernameField;
    UITextField *passwordField;
    UITableViewCell *urlViewCell;
    UITableViewCell *usernameViewCell;
    UITableViewCell *passwordViewCell;
    BAccount *account;
}
@property(nonatomic, retain) BAccount *account;
@property(nonatomic, retain) UITextField *urlField;
@property(nonatomic, retain) UITextField *usernameField;
@property(nonatomic, retain) UITextField *passwordField;
@property(nonatomic, retain) UITableViewCell *urlViewCell;
@property(nonatomic, retain) UITableViewCell *usernameViewCell;
@property(nonatomic, retain) UITableViewCell *passwordViewCell;
-(IBAction)saveAccountButtonPressed:(id)sender;
@end
