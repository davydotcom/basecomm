//
//  TodoItemViewController.m
//  basecamp
//
//  Created by David Estes on 3/20/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "TodoItemViewController.h"
#import "basecampAppDelegate.h"
#import <CoreData/CoreData.h>
#import "Project.h"
#import "BAccount.h"
#import "TodoList.h"
#import "TodoListItem.h"
#import "BCTodoListItem.h"
#import "BCTodoList.h"
#import "SyncController.h"
#import "TodoItemTableViewCell.h"
#import "TodoItemCompleteTableViewCell.h"
#import "TodoListItemSyncController.h"
#import "TodoListItemFormViewController.h"
#import "TodoItemShowViewController.h"
@implementation TodoItemViewController
@synthesize todoList,tableView,todoListTitleLabel,projectTitleLabel, fetchedResultsController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"To-Do Items";

        UIBarButtonItem *editAccountsButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButtonPressed:)];
        self.navigationItem.rightBarButtonItem = editAccountsButton;
        [editAccountsButton release];
    }
    return self;
}

- (void)dealloc
{
    [fetchedResultsController release];
    [todoListTitleLabel release];
    [projectTitleLabel release];
    [todoList release];
    [tableView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    self.tableView.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0f];

    [super viewDidLoad];
    if(_refreshHeaderView == nil)
    {
        EGORefreshTableHeaderView *refreshView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height) arrowImageName:@"grayArrow.png" textColor:[UIColor grayColor]];
        refreshView.delegate = self;
        _refreshHeaderView = refreshView;
        [self.tableView addSubview:refreshView];
        [refreshView release];
        
    }
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(appDelegate.syncController.finished == YES)
    {
        if(self.todoList.todoListItemSyncController == nil || self.todoList.todoListItemSyncController.finished)
        {
            TodoListItemSyncController *todoListItemSyncController = [[TodoListItemSyncController alloc] initWithTodoList:self.todoList];
            self.todoList.todoListItemSyncController = todoListItemSyncController;
            [todoListItemSyncController startSyncing];
            [todoListItemSyncController release];
        }
        
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    [self registerForNotifications];
    if(self.todoList != nil)
    {
        self.projectTitleLabel.text = self.todoList.project.name;
        self.todoListTitleLabel.text = self.todoList.name;
        self.fetchedResultsController = nil;
        [[self fetchedResultsController] performFetch:nil];
        [tableView reloadData];

    }
    [super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self unregisterForNotifications];
}
-(void)loadingEnabled
{
//    [_refreshHeaderView setState:EGOOPullRefreshLoading];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark - Table view data source
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [[self.fetchedResultsController sections] count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
    if(section == 0)
    {
        return @"Incomplete";
    }
    
    return @"Completed";
    
}
- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TodoListItem *_todoListItem = (TodoListItem *)[self.fetchedResultsController objectAtIndexPath:indexPath];

    if(![_todoListItem.completed boolValue])
    {
        static NSString *CellIdentifier = @"ProjectTodoListItemsCell";
        TodoItemTableViewCell *cell = (TodoItemTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[TodoItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            [cell.checkButton addTarget:self action:@selector(checkButtonPressed:) forControlEvents:UIControlEventTouchUpInside];

        }
        
        // Configure the cell...
        cell.titleLabel.numberOfLines = 0;
        cell.titleLabel.lineBreakMode = UILineBreakModeWordWrap;

        cell.titleLabel.text = _todoListItem.content;
        CGSize fitSize = [cell.titleLabel sizeThatFits:CGSizeMake(220.0f, 999.0f)];
        cell.titleLabel.frame = CGRectMake(cell.titleLabel.frame.origin.x, cell.titleLabel.frame.origin.y, 220.0f, fitSize.height);
        cell.checkButton.tag = (NSInteger)_todoListItem;
        NSString *assignmentText = nil;
        if(_todoListItem.responsiblePartyName != nil && ![_todoListItem.responsiblePartyName isEqualToString:@""])
        {

            assignmentText = [[NSString alloc] initWithFormat:@"Assigned To: %@",_todoListItem.responsiblePartyName];
            cell.assignDueLabel.text = assignmentText;
            [cell.assignDueLabel sizeToFit];

            
        }
        else
        {
            cell.assignDueLabel.text = @"";
        }
                    cell.assignDueLabel.frame = CGRectMake(cell.assignDueLabel.frame.origin.x, cell.titleLabel.frame.origin.y + cell.titleLabel.frame.size.height + 5.0f, cell.assignDueLabel.frame.size.width, cell.assignDueLabel.frame.size.height);
        if(_todoListItem.dueAt != nil)
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MM/dd"];
            cell.dueLabel.text = [dateFormatter stringFromDate:_todoListItem.dueAt];
//            cell.assignDueLabel.text = ;
            [dateFormatter release];
        }
        else
        {
            cell.dueLabel.text = @"";
        }
        
        [assignmentText release];
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"ProjectTodoListItemsCompleteCell";
        TodoItemCompleteTableViewCell *cell = (TodoItemCompleteTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[TodoItemCompleteTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            [cell.checkButton addTarget:self action:@selector(uncheckButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        // Configure the cell...
        cell.checkButton.tag = (NSInteger)_todoListItem;
        cell.titleLabel.text = _todoListItem.content;

        return cell;
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    TodoListItem *_todoListItem = (TodoListItem *)[self.fetchedResultsController objectAtIndexPath:indexPath];

    if([_todoListItem.completed boolValue])
    {
        [cell setBackgroundColor:[UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1]];
    }
    else
    {
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TodoListItem *_todoListItem = (TodoListItem *)[self.fetchedResultsController objectAtIndexPath:indexPath];

    if(![_todoListItem.completed boolValue])
    {   
        CGFloat height = [_todoListItem.content sizeWithFont:[UIFont boldSystemFontOfSize:13.0f] constrainedToSize:CGSizeMake(220.0f, 999.0f) lineBreakMode:UILineBreakModeWordWrap].height;
        height +=40.0f;
        if(height < 80.0f)
        {
            return 80.0f;
        }
        return height;

    }
    return 45;
}
/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */
- (BOOL)tableView:(UITableView *)_tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    TodoListItem *_todoListItem = (TodoListItem *)[fetchedResultsController objectAtIndexPath:indexPath];
    if(![_todoListItem.completed boolValue])
    {
        return YES;
    }
    return NO;
}
-(NSIndexPath *) tableView:(UITableView *)_tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    TodoListItem *_todoListItem = (TodoListItem *)[fetchedResultsController objectAtIndexPath:proposedDestinationIndexPath];
    if([_todoListItem.completed boolValue] == YES)
    {
        return sourceIndexPath;
    }
    return proposedDestinationIndexPath;
}
-(void)tableView:(UITableView *)_tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
        fetchedResultsController.delegate = nil;
    TodoListItem *_todoListItem = (TodoListItem *)[fetchedResultsController objectAtIndexPath:sourceIndexPath];

    if([sourceIndexPath row] < [destinationIndexPath row])
    {
        for(NSUInteger currentRow = [sourceIndexPath row] + 1;currentRow < [destinationIndexPath row];currentRow++)
        {
            NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:currentRow inSection:0];


            TodoListItem *_currentTodoListItem = (TodoListItem *)[fetchedResultsController objectAtIndexPath:currentIndexPath];
            NSNumber *position = [[NSNumber alloc] initWithInt:currentRow-1];
            _currentTodoListItem.position = position;
            [position release];
        }
    }
    else
    {
        for(NSUInteger currentRow = [sourceIndexPath row] - 1;currentRow > [destinationIndexPath row];currentRow--)
        {
            NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:currentRow inSection:0];
            TodoListItem *_currentTodoListItem = (TodoListItem *)[fetchedResultsController objectAtIndexPath:currentIndexPath];
            NSNumber *position = [[NSNumber alloc] initWithInt:currentRow+1];
            _currentTodoListItem.position = position;
            [position release];
        }
    }
        
    NSNumber *finalPosition = [[NSNumber alloc] initWithInt:[destinationIndexPath row]];

    _todoListItem.position = finalPosition;


    [finalPosition release];
    [[_todoListItem managedObjectContext] save:nil];
    NSMutableArray *bcTodoItems = [[NSMutableArray alloc] initWithCapacity:10];
    NSArray *todoItems = [fetchedResultsController fetchedObjects];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"completed" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    NSArray *sorted = [todoItems sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor,sortDescriptor2, nil]];
    [sortDescriptor release];
    [sortDescriptor2 release];
    for(TodoListItem *item in sorted)
    {
        if([item.completed boolValue])
        {
            break;
        }
        BCTodoListItem *bcItem = [[BCTodoListItem alloc] initWithAccount:[todoList.account basecampAccount]];
        bcItem.todoListID = self.todoList.todoListID;
        bcItem.todoListItemID = item.todoListItemID;
        bcItem.position = item.position;
        [bcTodoItems addObject:bcItem];
        [bcItem release];
    }

    BCTodoList *_bcTodoList = [[BCTodoList alloc] initWithAccount:[todoList.account basecampAccount]];
    _bcTodoList.projectID = todoList.project.projectID;
    _bcTodoList.todoListID = todoList.todoListID;

    [_bcTodoList reorderTodoListItems:bcTodoItems withCompletionHandler:^(NSArray *todoListItems, NSError *error) {
        [todoListItems release];
        fetchedResultsController.delegate = self;
    }];
    [_bcTodoList release];
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)_tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        TodoListItem *_todoListItem = (TodoListItem *)[fetchedResultsController objectAtIndexPath:indexPath];
        NSNumber *trueNum = [[NSNumber alloc] initWithBool:YES];
        NSNumber *falseNum = [[NSNumber alloc] initWithBool:NO];
        _todoListItem.deleted = trueNum;
        _todoListItem.synced = falseNum;
        [[_todoListItem managedObjectContext] save:nil];
        [_todoListItem syncTodoListItem];
        [trueNum release];
        [falseNum release];
        [fetchedResultsController performFetch:nil];
        [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

-(IBAction)editButtonPressed:(id)sender
{
    if(!self.tableView.editing)
    {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editButtonPressed:)];
        self.navigationItem.rightBarButtonItem = editButton;
        [editButton release];
    }
    else
    {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButtonPressed:)];
        self.navigationItem.rightBarButtonItem = editButton;
        [editButton release];
    }
    [self.tableView setEditing:!self.tableView.editing animated:YES];

}
-(IBAction)checkButtonPressed:(id)sender
{
    fetchedResultsController.delegate = nil;

    UIButton *_checkButton = (UIButton *)sender;
    TodoListItem *_todoListItem = (TodoListItem* )_checkButton.tag;
    NSIndexPath *indexPath = [fetchedResultsController indexPathForObject:_todoListItem];
    [fetchedResultsController performFetch:nil];

    NSNumber *trueNum = [[NSNumber alloc] initWithBool:YES];
    _todoListItem.completed = trueNum;
    _todoListItem.mark = trueNum;
    [[_todoListItem managedObjectContext] save:nil];
    [_todoListItem syncTodoListItem];
    [trueNum release];
    [fetchedResultsController performFetch:nil];    
    [tableView beginUpdates];
    [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[fetchedResultsController indexPathForObject:_todoListItem]] withRowAnimation:YES];
    
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];    
    [tableView endUpdates];
    fetchedResultsController.delegate = self;
//    [tableView reloadData];

}
-(IBAction)uncheckButtonPressed:(id)sender
{
    fetchedResultsController.delegate = nil;
    UIButton *_checkButton = (UIButton *)sender;
    TodoListItem *_todoListItem = (TodoListItem* )_checkButton.tag;
    NSIndexPath *indexPath = [fetchedResultsController indexPathForObject:_todoListItem];
    NSNumber *trueNum = [[NSNumber alloc] initWithBool:YES];
    NSNumber *falseNum = [[NSNumber alloc] initWithBool:NO];
    _todoListItem.completed = falseNum;
    _todoListItem.mark = trueNum;
    [[_todoListItem managedObjectContext] save:nil];
    [_todoListItem syncTodoListItem];
    [trueNum release];
    [falseNum release];
    
    [fetchedResultsController performFetch:nil];
    [tableView beginUpdates];
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[fetchedResultsController indexPathForObject:_todoListItem]] withRowAnimation:YES];

        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];    
    [tableView endUpdates];
        fetchedResultsController.delegate = self;
    
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    TodoListItem *_todoListItem = (TodoListItem *)[self.fetchedResultsController objectAtIndexPath:indexPath];

    TodoItemShowViewController *todoItemShowViewController = [[TodoItemShowViewController alloc] initWithStyle:UITableViewStylePlain];
    todoItemShowViewController.todoListItem = _todoListItem;
    [self.navigationController pushViewController:todoItemShowViewController animated:YES];
    [todoItemShowViewController release];
}
#pragma mark - NSNotificationCenter Methods
-(void)registerForNotifications
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(updateTodoListItems:) name:TODOLISTITEMS_DID_SYNC_NOTIFICATION object:nil];
}
-(void)unregisterForNotifications
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc removeObserver:self name:TODOLISTITEMS_DID_SYNC_NOTIFICATION object:nil];
}
-(void)updateTodoListItems:(NSNotification *)notification
{
    [self doneLoadingTableViewData];

}
-(IBAction)addButtonPressed:(id)sender
{
    TodoListItemFormViewController *_todoListItemFormViewController = [[TodoListItemFormViewController alloc] initWithNibName:@"TodoItemFormView" bundle:nil];
    _todoListItemFormViewController.delegate = self;
    _todoListItemFormViewController.todoList = self.todoList;
    [self presentModalViewController:_todoListItemFormViewController animated:YES];
    [_todoListItemFormViewController release];
}
#pragma mark - TodoListItemFormDelegate Methods
-(void)todoListItemFormViewController:(TodoListItemFormViewController *)_todoListItemFormViewController didSaveTodoListItem:(TodoListItem *)_todoListItem
{
    [self dismissModalViewControllerAnimated:YES];
}
-(void)didCancelTodoListItemFormViewController:(TodoListItemFormViewController *)_todoListItemFormViewController
{
    [self dismissModalViewControllerAnimated:YES];    
}
#pragma mark -
#pragma mark CoreData methods
-(NSFetchedResultsController *)fetchedResultsController {
    if(fetchedResultsController != nil)
    {
        return fetchedResultsController;
    }
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TodoListItem" inManagedObjectContext:[appDelegate managedObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"todoList == %@ && deleted != 1",todoList];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"completed" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];    
    
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor,sortDescriptor2,nil]];
    
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[appDelegate managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
    _fetchedResultsController.delegate = self;
    self.fetchedResultsController = _fetchedResultsController;
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [sortDescriptor2 release];
    [fetchRequest release];
    return fetchedResultsController;
}
-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [tableView beginUpdates];
}
-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [tableView endUpdates];
}
-(void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
             break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
             
    };
}
-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch (type) {
        case NSFetchedResultsChangeMove:
//            NSLog(@"OBject moved from section:%d, to section: %d",[indexPath section],[newIndexPath section]);

            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    };

}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
    
    if(self.todoList != nil)
    {
        self.projectTitleLabel.text = self.todoList.project.name;
        self.todoListTitleLabel.text = self.todoList.name;
        self.fetchedResultsController = nil;
        [[self fetchedResultsController] performFetch:nil];
        [tableView reloadData];
        basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        if(appDelegate.syncController.finished == YES)
        {
            if(self.todoList.todoListItemSyncController == nil || self.todoList.todoListItemSyncController.finished)
            {
                TodoListItemSyncController *todoListItemSyncController = [[TodoListItemSyncController alloc] initWithTodoList:self.todoList];
                self.todoList.todoListItemSyncController = todoListItemSyncController;
                [todoListItemSyncController startSyncing];
                [todoListItemSyncController release];
            }
        }
    }
	_reloading = YES;
    
}

- (void)doneLoadingTableViewData{
    
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{	
    
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
	[self reloadTableViewDataSource];
//	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
    
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
	return _reloading; // should return if data source model is reloading
    
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
    
	return [NSDate date]; // should return date data source was last changed
    
}
@end
