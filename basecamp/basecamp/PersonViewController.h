//
//  PersonViewController.h
//  basecamp
//
//  Created by David Estes on 4/28/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@class Person;
@interface PersonViewController : UITableViewController <MFMailComposeViewControllerDelegate> {
    Person *person;
    
}
@property(nonatomic, retain) Person *person;
@end
