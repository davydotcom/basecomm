//
//  RWDateSelector.m
//  basecamp
//
//  Created by David Estes on 3/26/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "RWDateSelector.h"

@implementation RWDateSelector
@synthesize datePicker,selectionView,dueDate,delegate,parentView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [dueDate release];
    [datePicker release];
    [selectionView release];
    [parentView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)slideIn
{
    if(parentView == nil)
    {
        return;
    }
    [self viewWillAppear:YES];
    self.view.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f];
    [self.view addSubview:selectionView];
    self.selectionView.frame = CGRectMake(0.0f, self.view.frame.size.height, 320.0f, self.selectionView.frame.size.height);
    [self.parentView addSubview:self.view];
    self.view.frame = CGRectMake(0.0f, 0.0f, self.parentView.frame.size.width, self.parentView.frame.size.height);
    
    [UIView animateWithDuration:0.5f animations:^(void) {
        self.view.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f];
        self.selectionView.frame = CGRectMake(0.0f, self.view.frame.size.height - self.selectionView.frame.size.height, 320.0f, self.selectionView.frame.size.height);
    }];
    
}
-(void)slideOut
{
    [self viewWillDisappear:YES];
    [UIView animateWithDuration:0.5f animations:^(void) {
        self.view.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f];
        self.selectionView.frame = CGRectMake(0.0f, self.view.frame.size.height, 320.0f, self.selectionView.frame.size.height);
    } completion:^(BOOL finished) {
        [self.selectionView removeFromSuperview];
        [self.view removeFromSuperview];
        [self viewDidDisappear:YES];
    }];
}
-(IBAction)doneButtonPressed:(id)sender
{
    self.dueDate = self.datePicker.date;
    if(delegate != nil && [delegate respondsToSelector:@selector(rwDateSelector:didSelectDate:)])
    {
        [delegate rwDateSelector:self didSelectDate:self.dueDate];
    }   
    [self slideOut];
}
-(IBAction)anytimeButtonPressed:(id)sender
{
    self.dueDate = nil;
    if(delegate != nil && [delegate respondsToSelector:@selector(rwDateSelector:didSelectDate:)])
    {
        [delegate rwDateSelector:self didSelectDate:self.dueDate];
    }
    [self slideOut];
}
-(IBAction)dateValueChanged:(id)sender
{
    self.dueDate = self.datePicker.date;
}
#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated
{
    if(self.dueDate != nil)
    {
        [self.datePicker setDate:dueDate animated:NO];
    }
    else
    {
        NSDate *now = [[NSDate alloc] init];
        [self.datePicker setDate:now animated:NO];
        [now release];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
