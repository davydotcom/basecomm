//
//  MilestoneSyncController.h
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Project;
@class Milestone;
@class NSManagedObjectContext;
@interface MilestoneSyncController : NSObject {
    Project *project;
    NSManagedObjectContext *managedObjectContext;
    NSManagedObjectContext *parentObjectContext;
    NSThread *sourceThread;
    BOOL finished;
}
@property(nonatomic,retain) NSManagedObjectContext *parentObjectContext;
@property(nonatomic, retain) NSThread *sourceThread;
@property(nonatomic,retain) Project *project;
@property(nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property(nonatomic,assign) BOOL finished;
-(id)initWithProject:(Project* )_project;
-(void)didRemoveMilestoneNotification:(Milestone *)_milestone;
-(void)didFinishSyncingMilestonesNotification;
-(void)runSyncThread;
-(void)startSyncing;
-(void) fetchProjectWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext;
-(void)registerForManagedObjectNotifications;
-(void)mergeChanges:(NSNotification *)notification;
@end
