//
//  MessageSyncController.h
//  basecamp
//
//  Created by David Estes on 6/11/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BAccount,Project,NSManagedObjectContext,Message;
@interface MessageSyncController : NSObject {
    BAccount *account;
    Project *project;
    NSManagedObjectContext *managedObjectContext;
    NSManagedObjectContext *parentObjectContext;
    NSThread *sourceThread;
    BOOL finished;   
}
@property(nonatomic,retain) BAccount *account;
@property(nonatomic,retain) Project *project;
@property(nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property(nonatomic,retain) NSManagedObjectContext *parentObjectContext;
@property(nonatomic, retain) NSThread *sourceThread;
@property(nonatomic,assign) BOOL finished;
-(id)initWithProject:(Project* )_project;
-(void)didRemoveMessageNotification:(Message *)_message;
-(void)didFinishSyncingMessagesNotification;
-(void)runSyncThread;
-(void)startSyncing;
-(void) fetchAccountWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext;
-(void) fetchProjectWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext;
-(void)registerForManagedObjectNotifications;
-(void)mergeChanges:(NSNotification *)notification;
@end
