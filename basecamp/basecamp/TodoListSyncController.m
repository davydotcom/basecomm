//
//  TodoListSyncController.m
//  basecamp
//
//  Created by David Estes on 3/18/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "TodoListSyncController.h"
#import "basecampAppDelegate.h"
#import <CoreData/CoreData.h>
#import "TodoList.h"
#import "BAccount.h"
#import "Project.h"
#import "TodoList.h"
#import "BCProject.h"
#import "BCAccount.h"
#import "BCTodoList.h"
#import "BCTodoListItem.h"
#import "TodoListItem.h"
#import "Milestone.h"
#import "SyncController.h"
@implementation TodoListSyncController

@synthesize project,managedObjectContext,finished,parentObjectContext,sourceThread,account;

#pragma mark - Sync Methods
-(id)initWithProject:(Project* )_project
{
    self = [super init];
    if(self)
    {
        self.project = _project;
    }
    return self;
}
-(id)initWithAccount:(BAccount* )_account
{
    self = [super init];
    if(self)
    {
        self.account = _account;
    }
    return self;
}

-(void)startSyncing
{
    finished = NO;
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [self performSelector:@selector(runSyncThread) onThread:appDelegate.syncController.syncThread withObject:nil waitUntilDone:NO];
}
-(void) fetchProjectWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.project];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.project = [array objectAtIndex:0];
    }
    
}
-(void) fetchAccountWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"BAccount" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.account];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.account = [array objectAtIndex:0];
    }
    
}
-(void)runSyncThread
{
    [self retain];
    BOOL createdOwnContext = NO;
    finished = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(self.managedObjectContext == nil)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

        self.managedObjectContext = [appDelegate.syncController managedObjectContext];
        createdOwnContext = YES;
        
    }
    

    
    
    //First We sync all todoLists that have not been synced yet
    NSArray *todoLists = nil;

    //Then We Fetch all TodoLists from server to sync with
    BCProject *_bcProject = nil;
    if(self.project != nil)
    {
        [self fetchProjectWithNewManagedObjectContext:managedObjectContext];
        _bcProject = [[BCProject alloc] initWithAccount:[project.account basecampAccount]];
        _bcProject.projectID = self.project.projectID;
        //Get Array of all todoLists from CoreData
         todoLists =[TodoList allTodoListsForProjectID:[self.project.projectID integerValue] withManagedObjectContext:managedObjectContext];
    }
    else
    {
        [self fetchAccountWithNewManagedObjectContext:managedObjectContext];
        todoLists = [TodoList allTodoListsForAccount:self.account withManagedObjectContext:managedObjectContext];
    }
    void (^completionHandler)(NSArray *objects,NSError *error) = ^(NSArray *_bctodoLists, NSError *error) {

        if(error == nil)
        {

            //First Check to see if any todoLists have been removed!
            for(TodoList *_dbTodoList in todoLists)
            {
                BOOL found=NO;
                for(BCTodoList *_bcTodoList in _bctodoLists)
                {
                    if ([_bcTodoList.todoListID integerValue] == [_dbTodoList.todoListID integerValue]) {
                        found = YES;
                    }
                }
                if(!found)
                {
                    [self performSelectorOnMainThread:@selector(didRemoveTodoListNotification:) withObject:_dbTodoList waitUntilDone:YES];
                    [managedObjectContext deleteObject:_dbTodoList];
                }
            }
            //Add New TodoLists and Update Existing
            for(BCTodoList *_bcTodoList in _bctodoLists)
            {
                BOOL found = NO;
                for(TodoList *_dbTodoList in todoLists)
                {
                    if ([_bcTodoList.todoListID integerValue] == [_dbTodoList.todoListID integerValue]) {
                        //Update record
                        
                        _dbTodoList.name = _bcTodoList.name;
                        _dbTodoList.account = _dbTodoList.project.account;
                        _dbTodoList.complete = _bcTodoList.complete;
                        _dbTodoList.desc = _bcTodoList.description;
                        _dbTodoList.position = _bcTodoList.position;
                        _dbTodoList.completedCount = _bcTodoList.completedCount;
                        _dbTodoList.uncompletedCount = _bcTodoList.uncompletedCount;
                        _dbTodoList.tracked = _bcTodoList.tracked;
                        _dbTodoList.isPrivate = _bcTodoList.isPrivate;
                        if(_bcTodoList.milestoneID != nil)
                        {
                            Milestone *_milestone = [Milestone findByMilestoneID:[_bcTodoList.milestoneID integerValue] withManagedObjectContext:managedObjectContext];
                            _dbTodoList.milestone = _milestone;
                        }
                        else
                        {
                            _dbTodoList.milestone = nil;
                        }
                        
                        if(_bcTodoList.todoListItems != nil && [_bcTodoList.todoListItems count] > 0)
                        {
                            
                            for(BCTodoListItem *_bcTodoListItem in _bcTodoList.todoListItems)
                            {
                                BOOL found = NO;
                                for(TodoListItem *_dbTodoListItem in _dbTodoList.todoListItems)
                                {
                                    if([_dbTodoListItem.todoListItemID integerValue] == [_bcTodoListItem.todoListItemID integerValue])
                                    {
                                        _dbTodoListItem.content = _bcTodoListItem.content;
                                        _dbTodoListItem.createdAt = _bcTodoListItem.createdAt;
                                        _dbTodoListItem.creatorID = _bcTodoListItem.creatorID;
                                        _dbTodoListItem.creatorName = _bcTodoListItem.creatorName;
                                        _dbTodoListItem.completed = _bcTodoListItem.completed;
                                        _dbTodoListItem.commentsCount = _bcTodoListItem.commentsCount;
                                        _dbTodoListItem.dueAt = _bcTodoListItem.dueAt;
                                        _dbTodoListItem.position = _bcTodoListItem.position;
                                        _dbTodoListItem.responsiblePartyID = _bcTodoListItem.responsiblePartyID;
                                        
                                        _dbTodoListItem.responsiblePartyName = _bcTodoListItem.responsiblePartyName;
                                        _dbTodoListItem.responsiblePartyType = _bcTodoListItem.responsiblePartyType;
                                        _dbTodoListItem.synced = [NSNumber numberWithBool:YES];
                                        found = YES;
                                    }
                                }
                                if(!found){
                                    TodoListItem *_dbTodoListItem = [NSEntityDescription insertNewObjectForEntityForName:@"TodoListItem" inManagedObjectContext:managedObjectContext];
                                    _dbTodoListItem.todoListItemID = _bcTodoListItem.todoListItemID;    
                                    _dbTodoListItem.todoList = _dbTodoList;

                                    
                                    _dbTodoListItem.content = _bcTodoListItem.content;
                                    _dbTodoListItem.createdAt = _bcTodoListItem.createdAt;
                                    _dbTodoListItem.creatorID = _bcTodoListItem.creatorID;
                                    _dbTodoListItem.creatorName = _bcTodoListItem.creatorName;
                                    _dbTodoListItem.completed = _bcTodoListItem.completed;
                                    _dbTodoListItem.commentsCount = _bcTodoListItem.commentsCount;
                                    _dbTodoListItem.dueAt = _bcTodoListItem.dueAt;
                                    _dbTodoListItem.position = _bcTodoListItem.position;
                                    _dbTodoListItem.responsiblePartyID = _bcTodoListItem.responsiblePartyID;
                                    
                                    _dbTodoListItem.responsiblePartyName = _bcTodoListItem.responsiblePartyName;
                                    _dbTodoListItem.responsiblePartyType = _bcTodoListItem.responsiblePartyType;
                                    _dbTodoListItem.synced = [NSNumber numberWithBool:YES];
                                }
                            }
                                
                        }
                        
                        _dbTodoList.synced = [NSNumber numberWithBool:YES];
                        found = YES;
                    }
                }
                if(!found)
                {
                    //Time to create a new todoList file
                    TodoList *_dbTodoList = [NSEntityDescription insertNewObjectForEntityForName:@"TodoList" inManagedObjectContext:managedObjectContext];
                    _dbTodoList.todoListID = _bcTodoList.todoListID;
                    if(self.project == nil)
                    {
                        Project *_project = [Project findByProjectID:[_bcTodoList.projectID integerValue] withManagedObjectContext:managedObjectContext];
                        _dbTodoList.account = self.account;
                        _dbTodoList.project = _project;
                    }
                    else
                    {
                        _dbTodoList.project = self.project;
                        
                        _dbTodoList.account = self.project.account;
                    }
                    _dbTodoList.name = _bcTodoList.name;
                    _dbTodoList.complete = _bcTodoList.complete;
                    _dbTodoList.desc = _bcTodoList.description;
                    _dbTodoList.position = _bcTodoList.position;
                    _dbTodoList.completedCount = _bcTodoList.completedCount;
                    _dbTodoList.uncompletedCount = _bcTodoList.uncompletedCount;
                    _dbTodoList.tracked = _bcTodoList.tracked;
                    _dbTodoList.isPrivate = _bcTodoList.isPrivate;
                    if(_bcTodoList.milestoneID != nil)
                    {
                        Milestone *_milestone = [Milestone findByMilestoneID:[_bcTodoList.milestoneID integerValue] withManagedObjectContext:managedObjectContext];
                        _dbTodoList.milestone = _milestone;
                    }
                    else
                    {
                        _dbTodoList.milestone = nil;
                    }
                    if(_bcTodoList.todoListItems != nil && [_bcTodoList.todoListItems count] > 0)
                    {
                        for(BCTodoListItem *_bcTodoListItem in _bcTodoList.todoListItems)
                        {
                            
                            TodoListItem *_dbTodoListItem = [NSEntityDescription insertNewObjectForEntityForName:@"TodoListItem" inManagedObjectContext:managedObjectContext];
                            _dbTodoListItem.todoListItemID = _bcTodoListItem.todoListItemID;    
                            _dbTodoListItem.todoList = _dbTodoList;
                            
                            _dbTodoListItem.content = _bcTodoListItem.content;
                            _dbTodoListItem.createdAt = _bcTodoListItem.createdAt;
                            _dbTodoListItem.creatorID = _bcTodoListItem.creatorID;
                            _dbTodoListItem.creatorName = _bcTodoListItem.creatorName;
                            _dbTodoListItem.completed = _bcTodoListItem.completed;
                            _dbTodoListItem.commentsCount = _bcTodoListItem.commentsCount;
                            _dbTodoListItem.dueAt = _bcTodoListItem.dueAt;
                            _dbTodoListItem.position = _bcTodoListItem.position;
                            _dbTodoListItem.responsiblePartyID = _bcTodoListItem.responsiblePartyID;
                            
                            _dbTodoListItem.responsiblePartyName = _bcTodoListItem.responsiblePartyName;
                            _dbTodoListItem.responsiblePartyType = _bcTodoListItem.responsiblePartyType;
                            _dbTodoListItem.synced = [NSNumber numberWithBool:YES];
                        
                        }
                        
                    }
                    _dbTodoList.synced = [NSNumber numberWithBool:YES];
                    //                    NSLog(@"Creating TodoList: %@, for Project: %d",_dbTodoList.title,[_dbTodoList.projectID integerValue]);
                }
            }
        }
        finished = YES;
    };
    if(self.project != nil)
    {
        [BCTodoList loadTodoListsForProject:_bcProject withCompletionHandler:completionHandler];
    }
    else
    {
        [BCTodoList loadTodoListsForAccount:[self.account basecampAccount] withCompletionHandler:completionHandler];
    }
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];  
    }
    //    NSLog(@"Saving TodoList Changes!");
    NSError *error=nil;
    if(![managedObjectContext save:&error])
    {
        NSLog(@"Error Saving to CoreData After TodoList Sync");
    }
    [self performSelectorOnMainThread:@selector(didFinishSyncingTodoListsNotification) withObject:nil waitUntilDone:NO];
    if(createdOwnContext)
    {
        self.managedObjectContext = nil;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
    [_bcProject release];
    [autoreleasepool release];
    [self release];
}

-(void)registerForManagedObjectNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mergeChanges:) name:NSManagedObjectContextDidSaveNotification object:self.managedObjectContext];
    
}
-(void)mergeChanges:(NSNotification *)notification
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    

    NSManagedObjectContext *mainContext = [appDelegate managedObjectContext];
    [mainContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:) withObject:notification waitUntilDone:YES];        
}
-(void)didRemoveTodoListNotification:(TodoList *)_todoList
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:DID_REMOVE_TODOLIST_NOTIFICATION object:self userInfo:[NSDictionary dictionaryWithObject:_todoList forKey:@"todoList"]];
}
-(void)didFinishSyncingTodoListsNotification
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:TODOLISTS_DID_SYNC_NOTIFICATION object:nil];
}
-(void)dealloc
{
    [project release];
    [sourceThread release];
    [parentObjectContext release];
    [managedObjectContext release];
    [account release];
    [super dealloc];
}
@end
