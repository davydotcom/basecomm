//
//  TodoItemTableViewCell.h
//  basecamp
//
//  Created by David Estes on 3/20/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TodoItemTableViewCell : UITableViewCell {
    UIButton *checkButton;
    UILabel *titleLabel;
    UILabel *assignDueLabel;
    UILabel *dueLabel;
}
@property(nonatomic, retain) UIButton *checkButton;
@property(nonatomic, retain) UILabel *titleLabel;
@property(nonatomic, retain) UILabel *assignDueLabel;
@property(nonatomic, retain) UILabel *dueLabel;
@end
