//
//  CompanySyncController.m
//  basecamp
//
//  Created by David Estes on 3/26/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "CompanySyncController.h"
#import "basecampAppDelegate.h"
#import <CoreData/CoreData.h>
#import "Company.h"
#import "BAccount.h"
#import "BCAccount.h"
#import "BCCompany.h"
#import "Project.h"
#import "BCProject.h"
#import "SyncController.h"

@implementation CompanySyncController
@synthesize account,project;
@synthesize managedObjectContext,parentObjectContext,sourceThread;
@synthesize finished;
#pragma mark - Sync Methods
-(id)initWithAccount:(BAccount* )_account
{
    self = [super init];
    if(self)
    {
        self.account = _account;
    }
    return self;
}
-(id)initWithProject:(Project* )_project
{
    self = [super init];
    if(self)
    {
        self.project = _project;
    }
    return self;
}
-(void)startSyncing
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [self performSelector:@selector(runSyncThread) onThread:appDelegate.syncController.syncThread withObject:nil waitUntilDone:NO];
}
-(void) fetchAccountWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"BAccount" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.account];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.account = [array objectAtIndex:0];
    }
    
}
-(void) fetchProjectWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.project];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.project = [array objectAtIndex:0];
    }
    
}
-(void)runSyncThread
{
    [self retain];
    
    finished = NO;
    BOOL createdOwnContext = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(self.managedObjectContext == nil)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        self.managedObjectContext = [appDelegate.syncController managedObjectContext];
        createdOwnContext = YES;
    }
    if(self.account != nil)
    {
        [self fetchAccountWithNewManagedObjectContext:managedObjectContext];
    }
    if(self.project != nil)
    {
        [self fetchProjectWithNewManagedObjectContext:managedObjectContext];
    }
    
    
    //Get Array of all companies from CoreData
    NSArray *companies = nil;
    if(self.project != nil)
    {
        companies = [Company allCompaniesForProject:self.project WithManagedObjectContext:managedObjectContext];
    }
    else
    {
        companies = [Company allCompaniesForAccount:account WithManagedObjectContext:managedObjectContext];
    }
    
    //Then We Fetch all Projects from server to sync with
    if(self.project != nil)
    {
        BCProject *_bcProject = [[BCProject alloc] initWithAccount:[project.account basecampAccount]];
        _bcProject.projectID = self.project.projectID;
        [BCCompany loadCompaniesForProject:_bcProject withCompletionHandler:^(NSArray *_bccompanies, NSError *error) {
            if(error == nil)
            {
                //First Check to see if any companies have been removed!
                for(Company *_dbCompany in companies)
                {
                    BOOL found=NO;
                    for(BCCompany *_bcCompany in _bccompanies)
                    {
                        if ([_bcCompany.companyID integerValue] == [_dbCompany.companyID integerValue]) {
                            found = YES;
                        }
                    }
                    if(!found)
                    {
                        [_dbCompany removeProjectsObject:self.project];
                    }
                }
                //     NSLog(@"Looking for new and updating companies!");
                //Add New Projects and Update Existing
                for(BCCompany *_bcCompany in _bccompanies)
                {
                    BOOL found = NO;
                    for(Company *_dbCompany in companies)
                    {
                        if ([_bcCompany.companyID integerValue] == [_dbCompany.companyID integerValue]) {
                            //Update record
                            _dbCompany.name = _bcCompany.name;
                            _dbCompany.addressOne = _bcCompany.addressOne;
                            _dbCompany.addressTwo = _bcCompany.addressTwo;
                            _dbCompany.city = _bcCompany.city;
                            _dbCompany.state = _bcCompany.state;
                            _dbCompany.zip = _bcCompany.zip;
                            _dbCompany.country = _bcCompany.country;
                            _dbCompany.canSeePrivate = _bcCompany.canSeePrivate;
                            _dbCompany.phoneNumberFax = _bcCompany.phoneNumberFax;
                            _dbCompany.phoneNumberOffice = _bcCompany.phoneNumberOffice;
                            _dbCompany.timeZoneID = _bcCompany.timeZoneID;
                            _dbCompany.webAddress = _bcCompany.webAddress;
                            _dbCompany.companyID = _bcCompany.companyID;
                            
                            found = YES;
                        }
                    }
                    if(!found)
                    {
                        //Time to create a new company file
                        Company *_dbCompany = [Company findCompanyByID:[_bcCompany.companyID integerValue] withManagedObjectContext:managedObjectContext];
                        if(_dbCompany == nil)
                        {
                            _dbCompany = (Company *)[NSEntityDescription insertNewObjectForEntityForName:@"Company" inManagedObjectContext:managedObjectContext];
                        }
                        _dbCompany.name = _bcCompany.name;
                        _dbCompany.addressOne = _bcCompany.addressOne;
                        _dbCompany.addressTwo = _bcCompany.addressTwo;
                        _dbCompany.city = _bcCompany.city;
                        _dbCompany.state = _bcCompany.state;
                        _dbCompany.zip = _bcCompany.zip;
                        _dbCompany.country = _bcCompany.country;
                        _dbCompany.canSeePrivate = _bcCompany.canSeePrivate;
                        _dbCompany.phoneNumberFax = _bcCompany.phoneNumberFax;
                        _dbCompany.phoneNumberOffice = _bcCompany.phoneNumberOffice;
                        _dbCompany.timeZoneID = _bcCompany.timeZoneID;
                        _dbCompany.webAddress = _bcCompany.webAddress;
                        _dbCompany.companyID = _bcCompany.companyID;
                        _dbCompany.account = self.project.account;
                        [_dbCompany addProjectsObject:self.project];
                    }
                }
            }
            finished = YES;
        }];
    }
    else
    {
        [BCCompany loadCompaniesForAccount:[account basecampAccount] withCompletionHandler:^(NSArray *_bccompanies, NSError *error) {
            if(error == nil)
            {
                //First Check to see if any companies have been removed!
                for(Company *_dbCompany in companies)
                {
                    BOOL found=NO;
                    for(BCCompany *_bcCompany in _bccompanies)
                    {
                        if ([_bcCompany.companyID integerValue] == [_dbCompany.companyID integerValue]) {
                            found = YES;
                        }
                    }
                    if(!found)
                    {
                        [self performSelectorOnMainThread:@selector(didRemoveCompanyNotification:) withObject:_dbCompany waitUntilDone:YES];
                        [managedObjectContext deleteObject:_dbCompany];
                    }
                }
                //     NSLog(@"Looking for new and updating companies!");
                //Add New Projects and Update Existing
                for(BCCompany *_bcCompany in _bccompanies)
                {
                    BOOL found = NO;
                    for(Company *_dbCompany in companies)
                    {
                        if ([_bcCompany.companyID integerValue] == [_dbCompany.companyID integerValue]) {
                            //Update record
                            _dbCompany.name = _bcCompany.name;
                            _dbCompany.addressOne = _bcCompany.addressOne;
                            _dbCompany.addressTwo = _bcCompany.addressTwo;
                            _dbCompany.city = _bcCompany.city;
                            _dbCompany.state = _bcCompany.state;
                            _dbCompany.zip = _bcCompany.zip;
                            _dbCompany.country = _bcCompany.country;
                            _dbCompany.canSeePrivate = _bcCompany.canSeePrivate;
                            _dbCompany.phoneNumberFax = _bcCompany.phoneNumberFax;
                            _dbCompany.phoneNumberOffice = _bcCompany.phoneNumberOffice;
                            _dbCompany.timeZoneID = _bcCompany.timeZoneID;
                            _dbCompany.webAddress = _bcCompany.webAddress;
                            _dbCompany.companyID = _bcCompany.companyID;
                            
                            found = YES;
                        }
                    }
                    if(!found)
                    {
                        //Time to create a new company file
                        Company *_dbCompany = (Company *)[NSEntityDescription insertNewObjectForEntityForName:@"Company" inManagedObjectContext:managedObjectContext];
                        _dbCompany.name = _bcCompany.name;
                        _dbCompany.addressOne = _bcCompany.addressOne;
                        _dbCompany.addressTwo = _bcCompany.addressTwo;
                        _dbCompany.city = _bcCompany.city;
                        _dbCompany.state = _bcCompany.state;
                        _dbCompany.zip = _bcCompany.zip;
                        _dbCompany.country = _bcCompany.country;
                        _dbCompany.canSeePrivate = _bcCompany.canSeePrivate;
                        _dbCompany.phoneNumberFax = _bcCompany.phoneNumberFax;
                        _dbCompany.phoneNumberOffice = _bcCompany.phoneNumberOffice;
                        _dbCompany.timeZoneID = _bcCompany.timeZoneID;
                        _dbCompany.webAddress = _bcCompany.webAddress;
                        _dbCompany.companyID = _bcCompany.companyID;
                        _dbCompany.account = self.account;
                    }
                }
            }
            finished = YES;
        }];
    }
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];  
    }
    // NSLog(@"Saving Changes!");
    NSError *error=nil;
    if(![managedObjectContext save:&error])
    {
        NSLog(@"Error Saving to CoreData After Project Sync");
    }
    NSLog(@"Notifying company sync completion!");
    [self performSelectorOnMainThread:@selector(didFinishSyncingCompaniesNotification) withObject:nil waitUntilDone:NO];
    if(createdOwnContext)
    {
        self.managedObjectContext = nil;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];   
    }
    [autoreleasepool release];
    [self release];
}

-(void)registerForManagedObjectNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mergeChanges:) name:NSManagedObjectContextDidSaveNotification object:self.managedObjectContext];
    
}
-(void)mergeChanges:(NSNotification *)notification
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *mainContext = [appDelegate managedObjectContext];
    [mainContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:) withObject:notification waitUntilDone:YES];        
}
-(void)didRemoveCompanyNotification:(Company *)_company
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:DID_REMOVE_COMPANY_NOTIFICATION object:self userInfo:[NSDictionary dictionaryWithObject:_company forKey:@"company"]];
}
-(void)didFinishSyncingCompaniesNotification
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:COMPANIES_DID_SYNC_NOTIFICATION object:nil];
}

-(void)dealloc
{
    [account release];
    [project release];
    [sourceThread release];
    [parentObjectContext release];
    [managedObjectContext release];
    [super dealloc];
}
@end
