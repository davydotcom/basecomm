//
//  AccountSyncController.h
//  basecamp
//
//  Created by David Estes on 3/18/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ProjectSyncController;
@class MilestoneSyncController;
@class BAccount;
@interface AccountSyncController : NSObject {
    BOOL finished;
    NSInteger status;
    BAccount *account;
}
@property(nonatomic,retain) BAccount *account;
@property(nonatomic,assign) BOOL finished;
@property(nonatomic,assign) NSInteger status;
-(id)initWithAccount:(BAccount* )_account;
-(void)startSyncing;
-(void)runSyncThread;
-(void)runSyncUnsyncedObjectsThread;
-(void)syncUnsyncedObjectsWithManagedObjectContext:(NSManagedObjectContext*)managedObjectContext;
@end
