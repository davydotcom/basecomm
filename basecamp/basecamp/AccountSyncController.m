//
//  AccountSyncController.m
//  basecamp
//
//  Created by David Estes on 3/18/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "AccountSyncController.h"
#import "BAccount.h"
#import "Project.h"
#import "ProjectSyncController.h"
#import "PeopleSyncController.h"
#import "CompanySyncController.h"
#import "MilestoneSyncController.h"
#import "basecampAppDelegate.h"
#import "TodoListSyncController.h"
#import "TodoList.h"
#import "TodoListItemSyncController.h"
#import "SyncController.h"
#import "Project.h"
#import "Milestone.h"
#import "TodoList.h"
#import "TodoListItem.h"
#import <CoreData/CoreData.h>
@implementation AccountSyncController
@synthesize status;
@synthesize finished;
@synthesize account;
-(id)initWithAccount:(BAccount* )_account
{
    self = [super init];
    if(self)
    {
        self.account = _account;
        finished = YES;
    }
    return self;
}
-(void)startSyncing
{
    if(!finished)
    {
        return;
    }
    finished = NO;
    status = 0;
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [self performSelector:@selector(runSyncThread) onThread:appDelegate.syncController.syncThread withObject:nil waitUntilDone:NO];
}

-(void)syncUnsyncedObjectsWithManagedObjectContext:(NSManagedObjectContext*)managedObjectContext
{
    //First We sync all projects that have not been synced yet
    NSArray *projects = [Project unsyncedProjectsWithManagedObjectContext:managedObjectContext];
    for(Project *_project in projects)
    {
        [_project syncProject];
        while(_project.finished == NO)
        {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
              
    }
    
    NSArray *milestones = [Milestone unsyncedMilestonesWithManagedObjectContext:managedObjectContext];
    for(Milestone *_milestone in milestones)
    {
        [_milestone syncMilestone];
        while(_milestone.finished == NO)
        {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
        
    }
    
    NSArray *todoLists = [TodoList unsyncedTodoListsWithManagedObjectContext:managedObjectContext];
    for(TodoList *_todoList in todoLists)
    {
        [_todoList syncTodoList];
        while(_todoList.finished == NO)
        {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }   
    }
    
    NSArray *todoListItems = [TodoListItem unsyncedTodoListItemsWithManagedObjectContext:managedObjectContext];
    for(TodoListItem *_todoListItem in todoListItems)
    {
        [_todoListItem syncTodoListItem];
        while(_todoListItem.finished == NO)
        {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }   
    }
}
-(void)runSyncUnsyncedObjectsThread
{
    [self retain];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    finished = NO;
    status = 0;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *importContext = [appDelegate.syncController managedObjectContext];
    
    [self syncUnsyncedObjectsWithManagedObjectContext:importContext];
    
    status++;
    
    [autoreleasepool release];
    finished = YES;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self release];
}
-(void)runSyncThread
{
    [self retain];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    finished = NO;
    status = 0;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *importContext = [appDelegate.syncController managedObjectContext];

    [self syncUnsyncedObjectsWithManagedObjectContext:importContext];
    
    CompanySyncController *_companySyncController = [[CompanySyncController alloc] initWithAccount:self.account];
    _companySyncController.managedObjectContext = importContext;
    [_companySyncController runSyncThread];
    [_companySyncController release];
    PeopleSyncController *_peopleSyncController = [[PeopleSyncController alloc] initWithAccount:self.account];
    _peopleSyncController.managedObjectContext = importContext;
    [_peopleSyncController runSyncThread];
    [_peopleSyncController release];
    ProjectSyncController *_projectSyncController = [[ProjectSyncController alloc] initWithAccount:self.account];
    _projectSyncController.managedObjectContext = importContext;
    [_projectSyncController runSyncThread];

    [_projectSyncController release];
    
    status = 1;
//    NSLog(@"Fetching all projects!");
    NSArray *_projects = [Project allProjectsForAccount:account WithManagedObjectContext:importContext];
    for(Project *_project in _projects)
    {
        MilestoneSyncController *_milestoneSyncController = [[MilestoneSyncController alloc] initWithProject:_project];
        _milestoneSyncController.managedObjectContext = importContext;
        [_milestoneSyncController runSyncThread];
        [_milestoneSyncController release];

        
//        TodoListSyncController *_todoListSyncController = [[TodoListSyncController alloc] initWithProject:_project];
//        _todoListSyncController.managedObjectContext = importContext;
//        [_todoListSyncController runSyncThread];
//        [_todoListSyncController release];

    }
    NSLog(@"Syncing Assigned Todo Items!");
    TodoListSyncController *_todoListSyncController = [[TodoListSyncController alloc] initWithAccount:self.account];
    _todoListSyncController.managedObjectContext = importContext;
    [_todoListSyncController runSyncThread];
    [_todoListSyncController release];
    status++;

    [autoreleasepool release];
    finished = YES;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self release];
}



@end
