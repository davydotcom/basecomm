//
//  PeopleSyncController.h
//  basecamp
//
//  Created by David Estes on 3/25/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BAccount,Person,Project;
@interface PeopleSyncController : NSObject {
    BAccount *account;
    Project *project;
    NSManagedObjectContext *managedObjectContext;
    NSManagedObjectContext *parentObjectContext;
    NSThread *sourceThread;
    BOOL finished;
}
@property(nonatomic,retain) BAccount *account;
@property(nonatomic,retain) Project *project;
@property(nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property(nonatomic,retain) NSManagedObjectContext *parentObjectContext;
@property(nonatomic, retain) NSThread *sourceThread;
@property(nonatomic,assign) BOOL finished;
-(id)initWithAccount:(BAccount* )_account;
-(id)initWithProject:(Project* )_project;
-(void)didRemovePersonNotification:(Person *)_person;
-(void)didFinishSyncingPeopleNotification;
-(void)runSyncThread;
-(void)startSyncing;
-(void) fetchAccountWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext;
-(void) fetchProjectWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext;
-(void)registerForManagedObjectNotifications;
-(void)mergeChanges:(NSNotification *)notification;
@end
