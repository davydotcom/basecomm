//
//  RWTableViewController.h
//  basecamp
//
//  Created by David Estes on 3/8/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "EGORefreshTableHeaderView.h"
@interface RWTableViewController : UITableViewController <EGORefreshTableHeaderDelegate> {
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
}
@property(nonatomic, retain) EGORefreshTableHeaderView *_refreshHeaderView;
- (void)doneLoadingTableViewData;
@end
