//
//  AccountsViewController.m
//  basecamp
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "AccountsViewController.h"
#import "NewAccountViewController.h"
#import <CoreData/CoreData.h>
#import "basecampAppDelegate.h"
#import "BAccount.h"
#import "SyncController.h"
#import "AccountDashboardViewController.h"
@implementation AccountsViewController
@synthesize rootNavigationController,newAccountViewController;
@synthesize fetchedResultsController;


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *addAccountButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addAccountButtonPressed:)];
    self.navigationItem.rightBarButtonItem = addAccountButton;
    [addAccountButton release];
    
    UIBarButtonItem *editAccountsButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAccountsButtonPressed:)];
    self.navigationItem.leftBarButtonItem = editAccountsButton;
    [editAccountsButton release];
    self.title = @"Accounts";
    
    NewAccountViewController *_newAccountViewController = [[NewAccountViewController alloc] initWithStyle:UITableViewStyleGrouped];
    self.newAccountViewController = _newAccountViewController;
    [_newAccountViewController release];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.fetchedResultsController = nil;
    
    [[self fetchedResultsController] performFetch:nil];
    [self.tableView reloadData];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark UITableView DataSource Methods
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.fetchedResultsController fetchedObjects] count];
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *accountsTableIdentifier = @"AccountsTableIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:accountsTableIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:accountsTableIdentifier];
        cell.editingAccessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    NSUInteger row = [indexPath row];
    BAccount *_account = (BAccount *)[[self.fetchedResultsController fetchedObjects] objectAtIndex:row];
    if(_account.companyName != nil && ![_account.companyName isEqualToString:@""])
    {
        cell.textLabel.text = _account.companyName;
    }
    else
    {
        cell.textLabel.text = _account.url;
    }

    return cell;
}
-(void)tableView:(UITableView *)_tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSUInteger row = [indexPath row];
        BAccount *currentAccount = (BAccount *)[[fetchedResultsController fetchedObjects] objectAtIndex:row];
        basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        [appDelegate.syncController removeSyncControllerForAccount:currentAccount];
        [[appDelegate managedObjectContext] deleteObject:currentAccount];
        [fetchedResultsController performFetch:nil];
        [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}
#pragma mark -
#pragma mark UITableViewDelegate Methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
    BAccount *currentAccount = (BAccount *)[[fetchedResultsController fetchedObjects] objectAtIndex:row];
    AccountDashboardViewController *_accountDashboardViewController = [[AccountDashboardViewController alloc] initWithStyle:UITableViewStylePlain];
    _accountDashboardViewController.account = currentAccount;
    [rootNavigationController pushViewController:_accountDashboardViewController.accountTabBarController animated:YES];
    [_accountDashboardViewController release];
}
-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
    BAccount *currentAccount = (BAccount *)[[fetchedResultsController fetchedObjects] objectAtIndex:row];
    if(self.tableView.editing)
    {
        newAccountViewController.account = currentAccount;
        [rootNavigationController pushViewController:newAccountViewController animated:YES];
    }
}
#pragma mark -
#pragma mark CoreData methods
-(NSFetchedResultsController *)fetchedResultsController {
    if(fetchedResultsController != nil)
    {
        return fetchedResultsController;
    }
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"BAccount" inManagedObjectContext:[appDelegate managedObjectContext]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"companyName" ascending:YES];
    
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[appDelegate managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
    
    self.fetchedResultsController = _fetchedResultsController;
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return fetchedResultsController;
}
#pragma mark -
#pragma mark IBAction Methods
-(IBAction)addAccountButtonPressed:(id)sender
{
    newAccountViewController.account = nil;
    [rootNavigationController pushViewController:newAccountViewController animated:YES];
}
-(IBAction)editAccountsButtonPressed:(id)sender
{
    if(!self.tableView.editing)
    {
        UIBarButtonItem *editAccountsButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editAccountsButtonPressed:)];
        self.navigationItem.leftBarButtonItem = editAccountsButton;
        [editAccountsButton release];
    }
    else
    {
        UIBarButtonItem *editAccountsButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAccountsButtonPressed:)];
        self.navigationItem.leftBarButtonItem = editAccountsButton;
        [editAccountsButton release];
    }
    [self.tableView setEditing:!self.tableView.editing animated:YES];
}

#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    [rootNavigationController release];
    [newAccountViewController release];
    [super dealloc];
}
@end
