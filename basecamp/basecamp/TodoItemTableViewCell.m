//
//  TodoItemTableViewCell.m
//  basecamp
//
//  Created by David Estes on 3/20/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "TodoItemTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation TodoItemTableViewCell
@synthesize checkButton,titleLabel,assignDueLabel,dueLabel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0f, 15.0f, 220.0f, 40.0f)];
        _titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
        _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _titleLabel.numberOfLines = 2;
        self.titleLabel = _titleLabel;
        [_titleLabel release];
        UILabel *_assignLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0f, 50.0f, 240.0f, 15.0f)];
        _assignLabel.font = [UIFont systemFontOfSize:12.0f];
        _assignLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _assignLabel.textColor = [UIColor lightGrayColor];
        _assignLabel.textAlignment = UITextAlignmentLeft;

        self.assignDueLabel = _assignLabel;
        [_assignLabel release];
        UIButton *_checkButton = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 10.0, 50.0f, 50.0f)];
        [_checkButton setImage:[UIImage imageNamed:@"button-unchecked.png"] forState:UIControlStateNormal];
//        [_checkButton setImage:[UIImage imageNamed:@"button-checked.png"] forState:UIControlStateHighlighted];
        self.checkButton = _checkButton;
        [_checkButton release];
        UILabel *_dueLabel = [[UILabel alloc] initWithFrame:CGRectMake(285.0f, 5.0f, 35.0f, 15.0f)];
        _dueLabel.font = [UIFont systemFontOfSize:12.0f];
        _dueLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        _dueLabel.textColor = [UIColor lightGrayColor];
        _dueLabel.textAlignment = UITextAlignmentRight;
        
        self.dueLabel = _dueLabel;
        [_dueLabel release];
        [self.contentView addSubview:checkButton];
        [self.contentView addSubview:titleLabel];
        [self.contentView addSubview:assignDueLabel];
        [self.contentView addSubview:dueLabel];
        
    }
    return self;
}

- (void)dealloc
{
    [checkButton release];
    [assignDueLabel release];
    [titleLabel release];
    [dueLabel release];
    [super dealloc];
}





@end
