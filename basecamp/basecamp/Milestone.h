//
//  Milestone.h
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#define DID_REMOVE_MILESTONE_NOTIFICATION @"DID_REMOVE_MILESTONE_NOTIFICATION"
#define MILESTONES_DID_SYNC_NOTIFICATION @"MILESTONES_DID_SYNC_NOTIFICATION"

@class Project, TodoList, BCMilestone,Comment,Message;

@interface Milestone : NSManagedObject {
        BOOL finished;
@private
}
@property (nonatomic, assign) BOOL finished;
@property (nonatomic, retain) NSNumber * milestoneID;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSDate * deadline;
@property (nonatomic, retain) NSNumber * completed;
@property (nonatomic, retain) NSNumber * projectID;
@property (nonatomic, retain) NSDate * createdOn;
@property (nonatomic, retain) NSNumber * creatorID;
@property (nonatomic, retain) NSString * creatorName;
@property (nonatomic, retain) NSNumber * responsiblePartyID;
@property (nonatomic, retain) NSString * responsiblePartyType;
@property (nonatomic, retain) NSString * responsiblePartyName;
@property (nonatomic, retain) NSNumber * commentsCount;
@property (nonatomic, retain) NSDate * completedOn;
@property (nonatomic, retain) NSNumber * completerID;
@property (nonatomic, retain) NSString * completerName;
@property (nonatomic, retain) Project * project;
@property (nonatomic, retain) NSSet* todoLists;
@property (nonatomic, retain) NSNumber * mark;
@property (nonatomic, retain) NSNumber * synced;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, assign) NSNumber * notify;
@property (nonatomic, retain) NSSet* comments;
@property (nonatomic, retain) NSSet* messages;
+(NSArray *)unsyncedMilestonesWithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(NSArray *)allMilestonesForProjectID:(NSInteger)_projectID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(Milestone *)findByMilestoneID:(NSInteger)_milestoneID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
-(BCMilestone *) BCMilestoneFromMilestone;

-(void)syncMilestone;
@end

@interface Milestone (CoreDataGeneratedAccessors)
- (void)addCommentsObject:(Comment *)value;
- (void)removeCommentsObject:(Comment *)value;
- (void)addComments:(NSSet *)value;
- (void)removeComments:(NSSet *)value;

- (void)addMessagesObject:(Message *)value;
- (void)removeMessagesObject:(Message *)value;
- (void)addMessages:(NSSet *)value;
- (void)removeMessages:(NSSet *)value;
@end