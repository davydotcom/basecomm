//
//  TodoListItemSyncContrlller.h
//  basecamp
//
//  Created by David Estes on 3/19/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TodoList,Project,NSManagedObjectContext,TodoListItem;
@interface TodoListItemSyncController : NSObject {
    Project *project;
    TodoList *todoList;
    NSManagedObjectContext *managedObjectContext;
    BOOL finished;  
}
@property(nonatomic,retain) Project *project;
@property(nonatomic,retain) TodoList *todoList;
@property(nonatomic,retain) NSManagedObjectContext *managedObjectContext;
@property(nonatomic,assign) BOOL finished;
-(id)initWithTodoList:(TodoList* )_todoList;
-(id)initWithProject:(Project* )_project;
-(void)didRemoveTodoListItemNotification:(TodoListItem *)_todoListItem;
-(void)didFinishSyncingTodoListItemsNotification;
-(void)startSyncing;
-(void) fetchTodoListWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext;
-(void)registerForManagedObjectNotifications;
-(void)mergeChanges:(NSNotification *)notification;
@end
