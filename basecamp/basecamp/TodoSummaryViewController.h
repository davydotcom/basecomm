//
//  TodoListViewController.h
//  basecamp
//
//  Created by David Estes on 3/12/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BAccount;
@class NSFetchedResultsController;
@interface TodoSummaryViewController : UITableViewController {
    BAccount *account;
    NSArray *todoListItems;
    NSFetchedResultsController *fetchedResultsController;

}
@property(nonatomic,retain) BAccount *account;
@property(nonatomic,retain) NSArray *todoListItems;
@property(nonatomic, retain) NSFetchedResultsController *fetchedResultsController;

-(void)updateTodoLists:(NSNotification *)notification;
-(void)unregisterForNotifications;
-(void)registerForNotifications;
@end
