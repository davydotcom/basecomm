//
//  TodoListFormViewController.m
//  basecamp
//
//  Created by David Estes on 4/22/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "TodoListFormViewController.h"
#import "Project.h"
#import "TodoList.h"
#import "RWMilestoneSelector.h"
#import "Milestone.h"
#import "basecampAppDelegate.h"
#import "UIDescriptionViewController.h"
@implementation TodoListFormViewController
@synthesize milestoneID,milestoneLabel,milestoneTableViewCell,privateSwitch,privateTableViewCell,nameCellView,nameTextField,descriptionCellView;
@synthesize project,todoList,delegate,description,rwMilestoneSelector,milestone,tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [milestoneID release];
    [milestoneLabel release];
    [milestoneTableViewCell release];
    [privateSwitch release];
    [privateTableViewCell release];
    [nameCellView release];
    [nameTextField release];
    [descriptionCellView release];
    [description release];
    [project release];
    [rwMilestoneSelector release];
    [milestone release];
    [tableView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    self.title = @"New";
    UITableViewCell *_nameViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"urlViewCellIdentifier"];
    _nameViewCell.textLabel.text = @"Name";
    _nameViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITextField *_nameField = [[UITextField alloc] initWithFrame:CGRectMake(85,14,200,25)];
    _nameField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _nameField.font = [UIFont systemFontOfSize:12.0f];
    _nameField.delegate = self;
    [_nameViewCell.contentView addSubview:_nameField];
    _nameViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    _nameViewCell.textLabel.textColor = [UIColor lightGrayColor];
    _nameViewCell.textLabel.font = [UIFont systemFontOfSize:12.0f];
    _nameField.placeholder = @"Give your To-Do list a name.";
    self.nameTextField = _nameField;
    [_nameField release];
    self.nameCellView = _nameViewCell;
    [_nameViewCell release];
    
    UITableViewCell *_milestoneViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"urlViewCellIdentifier"];
    _milestoneViewCell.textLabel.text = @"Milestone";
    UILabel *_milestoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(100,10,200,25)];
    _milestoneLabel.font = [UIFont systemFontOfSize:12.0f];
    _milestoneLabel.text = @"None";
    _milestoneViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    _milestoneLabel.textColor = [UIColor colorWithRed:0.212f green:0.522f blue:0.776f alpha:1.0f];
    _milestoneViewCell.textLabel.textColor = [UIColor lightGrayColor];
    _milestoneViewCell.textLabel.font = [UIFont systemFontOfSize:12.0f];
    [_milestoneViewCell addSubview:_milestoneLabel];
    self.milestoneLabel = _milestoneLabel;
    [_milestoneLabel release];
    self.milestoneTableViewCell = _milestoneViewCell;
    [_milestoneViewCell release];
    
    
    UITableViewCell *_privateViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"urlViewCellIdentifier"];
    _privateViewCell.textLabel.text = @"Private";
    UISwitch *_privateSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(200, 10, 50, 45)];
    _privateViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    _privateViewCell.textLabel.textColor = [UIColor lightGrayColor];
    _privateViewCell.textLabel.font = [UIFont systemFontOfSize:12.0f];
    [_privateViewCell addSubview:_privateSwitch];
    self.privateSwitch = _privateSwitch;
    self.privateTableViewCell = _privateViewCell;
    [_privateSwitch release];
    [_privateViewCell release];
    
    UITableViewCell *_descriptionViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"urlViewCellIdentifier"];
    _descriptionViewCell.textLabel.numberOfLines = 3;
    _descriptionViewCell.textLabel.font = [UIFont systemFontOfSize:12.0f];
    _descriptionViewCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.descriptionCellView = _descriptionViewCell;
    [_descriptionViewCell release];
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.nameTextField becomeFirstResponder];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    if(self.todoList != nil)
    {
        self.nameTextField.text = todoList.name;
        self.description = todoList.desc;
        if([todoList.isPrivate boolValue] == YES)
        {
            self.privateSwitch.on = YES;
        }
        else
        {
            self.privateSwitch.on = NO;
        }
        self.milestone = todoList.milestone;
        if(self.milestone != nil)
        {
            self.milestoneLabel.text = self.milestone.title;
        }
        else
        {
            self.milestoneLabel.text = @"None";
        }
        if(self.description == nil)
        {
            self.descriptionCellView.textLabel.text = nil;
        }
        else
        {
            self.descriptionCellView.textLabel.text = self.description;
        }
    }
    [tableView reloadData];
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0)
        return 3;
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section] == 0)
    {
        switch ([indexPath row]) {
            case 0:
                return self.nameCellView;
            case 1:
                return self.milestoneTableViewCell;
            case 2:
                return self.privateTableViewCell;
            default:
                return nil;
                break;
        };
    }
    else
    {
        return self.descriptionCellView;
    }
        

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 1)
    {
        return @"Description";
    }
    return nil;
}
#pragma mark - Table view delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section] == 1) {
        return 100.0f;
    }
    else
    {
        return 45.0f;
    }
}
- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section] == 0)
    {

        if([indexPath row] == 1)
        {
            basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            [self.nameTextField resignFirstResponder];
            RWMilestoneSelector *_rwMilestoneSelector = [[RWMilestoneSelector alloc] init];
            self.rwMilestoneSelector = _rwMilestoneSelector;
            self.rwMilestoneSelector.parentView = appDelegate.window;
            self.rwMilestoneSelector.delegate = self;
            [_rwMilestoneSelector release];
            self.rwMilestoneSelector.project = self.project;
            self.rwMilestoneSelector.milestone = self.milestone;
            
            [rwMilestoneSelector slideIn];
        }
    }
    else
    {
        [self resignFirstResponder];
        UIDescriptionViewController *_uiDescriptionViewController = [[UIDescriptionViewController alloc] initWithNibName:@"UIDescriptionViewController" bundle:nil];
        _uiDescriptionViewController.description = self.description;
        _uiDescriptionViewController.delegate = self;
        [self presentModalViewController:_uiDescriptionViewController animated:YES];
        [_uiDescriptionViewController release];
    }
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(NSIndexPath *)tableView:(UITableView *)_tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        if([indexPath section] == 0)
        {
            if ([indexPath row] == 0 || [indexPath row] == 2) {
                return nil;
            }

        }

    return indexPath;
}
#pragma mark - UITextField Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 216, 0);
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);    
}
-(void)rwMilestoneSelector:(RWMilestoneSelector *)_rwMilestoneSelector didSelectMilestone:(Milestone *)_milestone
{
    self.milestone = _milestone;
    if(self.milestone != nil)
    {
        self.milestoneLabel.text = self.milestone.title;
    }
    else
    {
        self.milestoneLabel.text = @"None";
    }
}
#pragma mark - UIDescriptionViewControllerDelegate Methods
-(void)UIDescriptionViewController:(UIDescriptionViewController *)_uiDescriptionViewController didSave:(NSString *)_description
{
    self.description = _description;
    self.descriptionCellView.textLabel.text = self.description;
    [self dismissModalViewControllerAnimated:YES];
    [self resignFirstResponder];
}
-(void)didCancelUIDescriptionViewController:(UIDescriptionViewController *)_uiDescriptionViewController
{

    [self dismissModalViewControllerAnimated:YES];    
}
#pragma mark - IBAction Methods
-(IBAction)cancelButtonPressed:(id)sender
{
    if(delegate != nil && [delegate respondsToSelector:@selector(didCancelTodoListFormViewController:)])
    {
        self.todoList = nil;
        [delegate didCancelTodoListFormViewController:self];
    }
}
-(IBAction)saveButtonPressed:(id)sender
{
    if(self.nameTextField.text == nil || [self.nameTextField.text isEqualToString:@""])
    {
        //Alert that text view cant be blank!
        return;
    }
    if(self.todoList == nil)
    {
       TodoList *_todoList = [NSEntityDescription insertNewObjectForEntityForName:@"TodoList" inManagedObjectContext:[self.project managedObjectContext]];
        self.todoList = _todoList;
    }
        //Create new Todo List Item !

     
    todoList.project = self.project;
    todoList.account = self.project.account;
    todoList.name = self.nameTextField.text;
    todoList.desc = self.description;


    NSNumber *yesNum = [[NSNumber alloc] initWithBool:YES];
    NSNumber *noNum = [[NSNumber alloc] initWithBool:NO];
    if(self.privateSwitch.on)
    {
        todoList.isPrivate = yesNum;
    }
    else
    {
        todoList.isPrivate = noNum;
    }
    
    todoList.milestone = self.milestone;
    
    todoList.synced = noNum;
    [noNum release];
    [yesNum release];
    [todoList syncTodoList];
   [[todoList managedObjectContext] save:nil];

    if(delegate != nil && [delegate respondsToSelector:@selector(todoListFormViewController:didSaveTodoList:)])
    {
        [delegate todoListFormViewController:self didSaveTodoList:self.todoList];
        self.milestone = nil;
        self.todoList = nil;
        self.project = nil;
    }
}

@end
