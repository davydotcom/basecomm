//
//  Company.h
//  basecamp
//
//  Created by David Estes on 3/26/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#define DID_REMOVE_COMPANY_NOTIFICATION @"DID_REMOVE_COMPANY_NOTIFICATION"
#define COMPANIES_DID_SYNC_NOTIFICATION @"COMPANIES_DID_SYNC_NOTIFICATION"
@class BAccount, Project;

@interface Company : NSManagedObject {
@private
}
@property (nonatomic, retain) NSNumber * companyID;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * addressOne;
@property (nonatomic, retain) NSString * addressTwo;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * zip;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * webAddress;
@property (nonatomic, retain) NSString * phoneNumberOffice;
@property (nonatomic, retain) NSString * phoneNumberFax;
@property (nonatomic, retain) NSString * timeZoneID;
@property (nonatomic, retain) NSNumber * canSeePrivate;
@property (nonatomic, retain) NSString * urlName;
@property (nonatomic, retain) BAccount * account;
@property (nonatomic, retain) NSSet* projects;
@property (nonatomic, retain) NSSet* people;
+(NSArray *)allCompaniesForProject:(Project *)_project WithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(Company *)findCompanyByID:(NSInteger)_companyID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(NSArray *)allCompaniesForAccount:(BAccount *)_account WithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
- (void)addProjectsObject:(Project *)value;
- (void)removeProjectsObject:(Project *)value;
@end
