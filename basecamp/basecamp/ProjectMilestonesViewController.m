//
//  ProjectMilestonesViewController.m
//  basecamp
//
//  Created by David Estes on 3/26/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "ProjectMilestonesViewController.h"

#import <CoreData/CoreData.h>
#import "basecampAppDelegate.h"
#import "MilestoneSyncController.h"
#import "Milestone.h"
#import "BAccount.h"
#import "Project.h"
#import "TodoItemCompleteTableViewCell.h"
#import "MilestoneSyncController.h"
#import "TodoItemTableViewCell.h"
#import "MilestoneFormViewController.h"
@implementation ProjectMilestonesViewController
@synthesize project,projectTitleLabel,companyTitleLabel,fetchedResultsController,tableView,keys,milestones;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Milestones";
        [self.tabBarItem setImage:[UIImage imageNamed:@"icon-milestone.png"]];
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [keys release];
    [milestones release];
    [tableView release];
    [companyTitleLabel release];
    [projectTitleLabel release];
    [fetchedResultsController release];
    [project release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    NSArray *_keys = [[NSArray alloc] initWithObjects:@"Upcoming",@"Past Due",@"Completed", nil];
    self.keys = _keys;
    [_keys release];
    [super viewDidLoad];
    if(_refreshHeaderView == nil)
    {
        EGORefreshTableHeaderView *refreshView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height) arrowImageName:@"grayArrow.png" textColor:[UIColor grayColor]];
        refreshView.delegate = self;
        _refreshHeaderView = refreshView;
        [self.tableView addSubview:refreshView];
        [refreshView release];
        
    }
    if(self.project != nil)
    {
        self.projectTitleLabel.text = self.project.name;
        self.companyTitleLabel.text = self.project.account.companyName;
        [self reloadData];
        [tableView reloadData];
        MilestoneSyncController *milestoneSyncController = [[MilestoneSyncController alloc] initWithProject:self.project];
        [milestoneSyncController startSyncing];
        [milestoneSyncController release];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)viewWillAppear:(BOOL)animated
{
    [self registerForNotifications];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self unregisterForNotifications];
    [super viewWillDisappear:animated];
}
-(void)reloadData
{
    NSMutableDictionary *_milestones = [[NSMutableDictionary alloc] initWithCapacity:3];
    self.milestones = _milestones;
    [_milestones release];
    //    @"Upcoming","Late","Past Due"
    NSMutableArray *upcoming = [[NSMutableArray alloc] initWithCapacity:1];
    NSMutableArray *completed = [[NSMutableArray alloc] initWithCapacity:1];
    NSMutableArray *pastDue = [[NSMutableArray alloc] initWithCapacity:1];
    [self.milestones setValue:upcoming forKey:@"Upcoming"];
    [upcoming release];
    [self.milestones setValue:completed forKey:@"Completed"];
    [self.milestones setValue:pastDue forKey:@"Past Due"];
    [completed release];
    [pastDue release];
    [[self fetchedResultsController] performFetch:nil];
    NSDate *now = [[NSDate alloc] init];
    for(Milestone *milestone in [fetchedResultsController fetchedObjects])
    {
        if([milestone.completed boolValue])
        {
            [completed addObject:milestone];
        }
        else if([milestone.deadline compare:now] == NSOrderedAscending)
        {
            [pastDue addObject:milestone];
        }
        else
        {
            [upcoming addObject:milestone];
        }
    }
}
#pragma mark - UITableViewDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 3;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [keys objectAtIndex:section];
}
- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    return [[milestones valueForKey:[keys objectAtIndex:section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Milestone *milestone = [[milestones valueForKey:[keys objectAtIndex:[indexPath section]]] objectAtIndex:[indexPath row]];

    if([indexPath section] == 0)
    {
        static NSString *CellIdentifier = @"MilestoneCell";
        TodoItemTableViewCell *cell = (TodoItemTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[TodoItemTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.titleLabel.textColor = [UIColor colorWithRed:0.8f green:0.6f blue:0.267f alpha:1.0f];
            [cell.checkButton addTarget:self action:@selector(checkButtonPressed:) forControlEvents:UIControlEventTouchUpInside];

        }
        
        // Configure the cell...
        
        cell.titleLabel.text = milestone.title;
        NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"EdMMM,y" options:0
                                                                  locale:[NSLocale currentLocale]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:formatString];

        NSString *responsibility = [[NSString alloc] initWithFormat:@"Due On: %@, %@",[dateFormatter stringFromDate:milestone.deadline],milestone.responsiblePartyName];
                cell.checkButton.tag = (NSInteger)milestone;
        cell.assignDueLabel.text = responsibility;
        [dateFormatter release];
        [responsibility release];
        return cell;
    }
    else if([indexPath section] == 1)
    {
        static NSString *CellIdentifier = @"MilestonePastDueCell";
        TodoItemTableViewCell *cell = (TodoItemTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[TodoItemTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.titleLabel.textColor = [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:1.0f];
            [cell.checkButton addTarget:self action:@selector(checkButtonPressed:) forControlEvents:UIControlEventTouchUpInside];

        }
        
        // Configure the cell...        
        cell.titleLabel.text = milestone.title;
        cell.checkButton.tag = (NSInteger)milestone;
        NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"EdMMM,y" options:0
                                                                  locale:[NSLocale currentLocale]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:formatString];
        
        NSString *responsibility = [[NSString alloc] initWithFormat:@"Due On: %@, %@",[dateFormatter stringFromDate:milestone.deadline],milestone.responsiblePartyName];
        
        cell.assignDueLabel.text = responsibility;
        [dateFormatter release];
        [responsibility release];
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"MilestoneCompletedCell";
        TodoItemCompleteTableViewCell *cell = (TodoItemCompleteTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[TodoItemCompleteTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.titleLabel.textColor = [UIColor colorWithRed:0.0f green:0.6f blue:0.0f alpha:1.0f];
            [cell.checkButton addTarget:self action:@selector(uncheckButtonPressed:) forControlEvents:UIControlEventTouchUpInside];

        }
                cell.checkButton.tag = (NSInteger)milestone;
        // Configure the cell...
        cell.titleLabel.text = milestone.title;
        return cell; 
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    Milestone *milestone = [[milestones valueForKey:[keys objectAtIndex:[indexPath section]]] objectAtIndex:[indexPath row]];
    if([milestone.completed boolValue])
    {
        [cell setBackgroundColor:[UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1]];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section] <= 1)
    {
        return 75;
    }
    return 45;
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)_tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSLog(@"Firing Delete Method!");
        Milestone *milestone = [[milestones valueForKey:[keys objectAtIndex:[indexPath section]]] objectAtIndex:[indexPath row]];
        NSNumber *trueNum = [[NSNumber alloc] initWithBool:YES];
        NSNumber *falseNum = [[NSNumber alloc] initWithBool:NO];
        milestone.deleted = trueNum;
        milestone.synced = falseNum;

        [[milestone managedObjectContext] save:nil];
        [milestone syncMilestone];
        [trueNum release];
        [falseNum release];
        [self reloadData];
        [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

-(IBAction)checkButtonPressed:(id)sender
{
    UIButton *_checkButton = (UIButton *)sender;
    Milestone *milestone = (Milestone* )_checkButton.tag;
    NSInteger section=0;
    NSInteger row = NSNotFound;
    for(section=0;section < [keys count];section++)
    {
        NSArray *array = [milestones valueForKey:[keys objectAtIndex:section]];
        if([array indexOfObject:milestone] != NSNotFound)
        {
            row = [array indexOfObject:milestone];
            break;
        }
            
    }
    if(row == NSNotFound)
    {
        return;
    }
    [fetchedResultsController performFetch:nil];
    
    NSNumber *trueNum = [[NSNumber alloc] initWithBool:YES];
    milestone.completed = trueNum;
    milestone.mark = trueNum;
    [[milestone managedObjectContext] save:nil];
    [milestone syncMilestone];
    [trueNum release];

    [tableView beginUpdates];
    //[self reloadData];
    [[milestones objectForKey:[keys objectAtIndex:section]] removeObject:milestone];
    [[milestones objectForKey:[keys objectAtIndex:2]] insertObject:milestone atIndex:0];
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:YES];    

    [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:2]] withRowAnimation:YES];

    [tableView endUpdates];
    
    
}
-(IBAction)uncheckButtonPressed:(id)sender
{
    UIButton *_checkButton = (UIButton *)sender;
    Milestone *milestone = (Milestone* )_checkButton.tag;
    NSInteger section=0;
    NSInteger row = NSNotFound;
    for(section=0;section < [keys count];section++)
    {
        NSArray *array = [milestones valueForKey:[keys objectAtIndex:section]];
        if([array indexOfObject:milestone] != NSNotFound)
        {
            row = [array indexOfObject:milestone];
            break;
        }
        
    }
    if(row == NSNotFound)
    {
        return;
    }
    [fetchedResultsController performFetch:nil];
    
    NSNumber *trueNum = [[NSNumber alloc] initWithBool:YES];
    NSNumber *falseNum = [[NSNumber alloc] initWithBool:NO];
    milestone.completed = falseNum;
    milestone.mark = trueNum;
    [[milestone managedObjectContext] save:nil];
    [milestone syncMilestone];
    [trueNum release];
    [falseNum release];
    [tableView beginUpdates];
    [self reloadData];
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:YES];    
    for(section=0;section < [keys count];section++)
    {
        NSArray *array = [milestones valueForKey:[keys objectAtIndex:section]];
        if([array indexOfObject:milestone] != NSNotFound)
        {
            row = [array indexOfObject:milestone];
            break;
        }
        
    }
    if(row != NSNotFound)
    {
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:YES];
    }
    
    [tableView endUpdates];
    
    
}
-(IBAction)editButtonPressed:(id)sender
{
    if(!self.tableView.editing)
    {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editButtonPressed:)];
        self.navigationItem.rightBarButtonItem = editButton;
        [editButton release];
    }
    else
    {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButtonPressed:)];
        self.navigationItem.rightBarButtonItem = editButton;
        [editButton release];
    }
    [self.tableView setEditing:!self.tableView.editing animated:YES];
    
}
#pragma mark - IBAction Methods
-(IBAction)addButtonPressed:(id)sender
{
    MilestoneFormViewController *_milestoneFormViewController = [[MilestoneFormViewController alloc] init];
    _milestoneFormViewController.delegate = self;
    _milestoneFormViewController.project = self.project;
    [self presentModalViewController:_milestoneFormViewController animated:YES];
    [_milestoneFormViewController release];
}
#pragma mark - TodoListItemFormDelegate Methods
-(void)milestoneFormViewController:(MilestoneFormViewController *)_milestoneFormViewController didSaveMilestone:(Milestone *)_milestone
{
    [self dismissModalViewControllerAnimated:YES];
}
-(void)didCancelMilestoneFormViewController:(MilestoneFormViewController *)_milestoneFormViewController
{
    [self dismissModalViewControllerAnimated:YES];    
}
#pragma mark - NSNotificationCenter Methods
-(void)updateMilestones:(NSNotification *)notification
{
    [self reloadData];
    [tableView reloadData];
    [self doneLoadingTableViewData];
}
-(void)registerForNotifications
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(updateMilestones:) name:MILESTONES_DID_SYNC_NOTIFICATION object:nil];

    
}
-(void)unregisterForNotifications
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc removeObserver:self name:MILESTONES_DID_SYNC_NOTIFICATION object:nil];
    

}
#pragma mark -
#pragma mark CoreData methods
-(NSFetchedResultsController *)fetchedResultsController {
    if(fetchedResultsController != nil)
    {
        return fetchedResultsController;
    }
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Milestone" inManagedObjectContext:[appDelegate managedObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"project == %@ && deleted != 1",project];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"deadline" ascending:YES];    
    
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor,nil]];
    
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[appDelegate managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
    
    self.fetchedResultsController = _fetchedResultsController;
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return fetchedResultsController;
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
    if(self.project != nil)
    {
        self.projectTitleLabel.text = self.project.name;
        self.companyTitleLabel.text = self.project.account.companyName;
        [self reloadData];
        [tableView reloadData];
        MilestoneSyncController *milestoneSyncController = [[MilestoneSyncController alloc] initWithProject:self.project];
        [milestoneSyncController startSyncing];
        [milestoneSyncController release];
    }
	_reloading = YES;
}

- (void)doneLoadingTableViewData{
    
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{	
    
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
	[self reloadTableViewDataSource];
    //	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
    
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
	return _reloading; // should return if data source model is reloading
    
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
    
	return [NSDate date]; // should return date data source was last changed
    
}

@end
