//
//  MilestoneFormViewController.h
//  basecamp
//
//  Created by David Estes on 4/11/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MilestoneFormViewController,RWAssignee,RWDateSelector,Milestone,Project;
@protocol MilestoneFormDelegate <NSObject>
-(void)milestoneFormViewController:(MilestoneFormViewController *)_milestoneFormViewController didSaveMilestone:(Milestone *)_milestone;
-(void)didCancelMilestoneFormViewController:(MilestoneFormViewController *)_milestoneFormViewController;
@end
@interface MilestoneFormViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITextField *titleTextField;
    UILabel *deadlineLabel;
    UILabel *responsibleLabel;
    IBOutlet UITableView *tableView;
    UITableViewCell *titleViewCell;
    UITableViewCell *deadlineViewCell;
    UITableViewCell *responsibleViewCell;
    
    NSDate *deadlineDate;
    NSString *assigneeType;
    NSNumber *assigneeID;
    NSString *assigneeName;
    BOOL notify;
    Project *project;
    Milestone *milestone;
    RWAssignee *rwAssignee;
    RWDateSelector *rwDateSelector;
    id <MilestoneFormDelegate> delegate;

}
@property(nonatomic, retain) UITextField *titleTextField;
@property(nonatomic, retain) UITableView *tableView;
@property(nonatomic, retain) UITableViewCell *titleViewCell;
@property(nonatomic, retain) UITableViewCell *deadlineViewCell;
@property(nonatomic, retain) UITableViewCell *responsibleViewCell;
@property(nonatomic, retain) UILabel *deadlineLabel;
@property(nonatomic, retain) UILabel *responsibleLabel;
@property(nonatomic, retain) NSDate *deadlineDate;
@property(nonatomic, retain) NSString *assigneeType;
@property(nonatomic, retain) NSNumber *assigneeID;
@property(nonatomic, retain) NSString *assigneeName;
@property(nonatomic, assign) BOOL notify;
@property(nonatomic, retain) Project *project;
@property(nonatomic, retain) Milestone *milestone;
@property(nonatomic, retain) RWAssignee *rwAssignee;
@property(nonatomic, retain) RWDateSelector *rwDateSelector;
@property(nonatomic, assign) id delegate;

-(IBAction)saveButtonPressed:(id)sender;
-(IBAction)cancelButtonPressed:(id)sender;

@end
