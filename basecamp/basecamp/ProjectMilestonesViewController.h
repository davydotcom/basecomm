//
//  ProjectMilestonesViewController.h
//  basecamp
//
//  Created by David Estes on 3/26/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
@class Project,NSFetchedResultsController;
@interface ProjectMilestonesViewController : UIViewController  <UITableViewDelegate,UITableViewDataSource,EGORefreshTableHeaderDelegate>{
    IBOutlet UITableView *tableView;
    IBOutlet UILabel *projectTitleLabel;
    IBOutlet UILabel *companyTitleLabel;
    
    Project *project;
    NSFetchedResultsController *fetchedResultsController;
    NSArray *keys;
    NSMutableDictionary *milestones;
    BOOL _reloading;
    EGORefreshTableHeaderView *_refreshHeaderView;
}
@property(nonatomic, retain)  NSArray *keys;
@property(nonatomic, retain)  NSDictionary *milestones;
@property(nonatomic, retain)  UITableView *tableView;
@property(nonatomic, retain)  UILabel *projectTitleLabel;
@property(nonatomic, retain)  UILabel *companyTitleLabel;

@property(nonatomic, retain) Project *project;
@property(nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
-(IBAction)addButtonPressed:(id)sender;
-(IBAction)uncheckButtonPressed:(id)sender;
-(IBAction)checkButtonPressed:(id)sender;
-(void)registerForNotifications;
-(void)unregisterForNotifications;
-(void)updateMilestones:(NSNotification *)notification;
-(void)reloadData;
- (void)doneLoadingTableViewData;
@end
