//
//  ProjectDashboardViewController.m
//  basecamp
//
//  Created by David Estes on 3/26/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "ProjectDashboardViewController.h"
#import "Project.h"
#import "ProjectTodoListViewController.h"
#import "ProjectMilestonesViewController.h"
#import "ProjectPeopleViewController.h"
#import "CompanySyncController.h"
#import "PeopleSyncController.h"
#import "CategorySyncController.h"
#import "ProjectMessagesViewController.h"
#import "basecampAppDelegate.h"
@implementation ProjectDashboardViewController
@synthesize project,projectTabBarController,projectTodoListViewController,projectMilestoneViewController,projectPeopleViewController,projectMessagesViewController;
@synthesize dashboardNavigationController,todoListNavigationController,milestoneNavigationController,messageNavigationController,peopleNavigationController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        
        self.title = @"Dashboard";
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Projects" style:UIBarButtonItemStyleBordered target:self action:@selector(backButtonPressed:)];
        self.navigationItem.leftBarButtonItem = backButton;
        [backButton release];
        UINavigationController *_dashboardNavigationController = [[UINavigationController alloc] initWithRootViewController:self];
        self.dashboardNavigationController = _dashboardNavigationController;
        self.dashboardNavigationController.navigationBar.tintColor = appDelegate.mainNavigationController.navigationBar.tintColor;
        self.dashboardNavigationController.tabBarItem.image = [UIImage imageNamed:@"icon-dashboard.png"];
        [_dashboardNavigationController release];
        
        ProjectTodoListViewController *_projectTodoListViewController = [[ProjectTodoListViewController alloc] initWithNibName:@"ProjectTodoListView" bundle:nil];
        self.projectTodoListViewController = _projectTodoListViewController;
        UINavigationController *_todoListNavigationController = [[UINavigationController alloc] initWithRootViewController:self.projectTodoListViewController];
        self.todoListNavigationController = _todoListNavigationController;
        [_todoListNavigationController release];
        self.todoListNavigationController.navigationBar.tintColor = appDelegate.mainNavigationController.navigationBar.tintColor;
        todoListNavigationController.tabBarItem.image = [UIImage imageNamed:@"icon-todo.png"];
        projectTodoListViewController.navigationItem.leftBarButtonItem = self.navigationItem.leftBarButtonItem;
        [_projectTodoListViewController release];
        
        ProjectMilestonesViewController *_projectMilestonesViewController = [[ProjectMilestonesViewController alloc] initWithNibName:@"ProjectMilestonesViewController" bundle:nil];
        self.projectMilestoneViewController = _projectMilestonesViewController;
        [_projectMilestonesViewController release];
        UINavigationController *_milestoneNavigationController = [[UINavigationController alloc] initWithRootViewController:self.projectMilestoneViewController];
        self.milestoneNavigationController = _milestoneNavigationController;
        [_milestoneNavigationController release];
        self.milestoneNavigationController.navigationBar.tintColor = appDelegate.mainNavigationController.navigationBar.tintColor;
        self.milestoneNavigationController.tabBarItem.image = [UIImage imageNamed:@"icon-milestone.png"];
        self.projectMilestoneViewController.navigationItem.leftBarButtonItem = self.navigationItem.leftBarButtonItem;
        
        ProjectPeopleViewController *_projectPeopleViewController = [[ProjectPeopleViewController alloc] initWithNibName:@"ProjectPeopleViewController" bundle:nil];
        self.projectPeopleViewController = _projectPeopleViewController;
        [_projectPeopleViewController release];
        UINavigationController *_peopleNavigationController = [[UINavigationController alloc] initWithRootViewController:self.projectPeopleViewController];
        self.peopleNavigationController = _peopleNavigationController;
        [_peopleNavigationController release];
            self.peopleNavigationController.navigationBar.tintColor = appDelegate.mainNavigationController.navigationBar.tintColor;
        self.peopleNavigationController.tabBarItem.image = [UIImage imageNamed:@"icon-people.png"];
        self.projectPeopleViewController.navigationItem.leftBarButtonItem = self.navigationItem.leftBarButtonItem;
        
        ProjectMessagesViewController *_projectMessagesViewController = [[ProjectMessagesViewController alloc] initWithNibName:@"ProjectMessagesViewController" bundle:nil];
        self.projectMessagesViewController = _projectMessagesViewController;
        [_projectMessagesViewController release];
        UINavigationController *_messageNavigationController = [[UINavigationController alloc] initWithRootViewController:self.projectMessagesViewController];
        self.messageNavigationController = _messageNavigationController;
        [_messageNavigationController release];
        self.messageNavigationController.navigationBar.tintColor = appDelegate.mainNavigationController.navigationBar.tintColor;
        self.messageNavigationController.tabBarItem.image = [UIImage imageNamed:@"icon-messages.png"];
        self.projectMessagesViewController.navigationItem.leftBarButtonItem = self.navigationItem.leftBarButtonItem;

        UITabBarController *_projectTabBarController = [[UITabBarController alloc] init];
        
        [_projectTabBarController setViewControllers:[NSArray arrayWithObjects:self.dashboardNavigationController,self.todoListNavigationController,self.milestoneNavigationController,self.messageNavigationController,self.peopleNavigationController, nil]];
        

        
        
        self.projectTabBarController = _projectTabBarController;
        self.projectTabBarController.delegate = self;
        self.projectTabBarController.title = @"Dashboard";
        [self.tabBarItem setImage:[UIImage imageNamed:@"icon-dashboard.png"]];
        
        [_projectTabBarController release];
    }
    return self;
}

- (void)dealloc
{
    [project release];
    [projectTabBarController release];
    [projectTodoListViewController release];
    [projectMilestoneViewController release];
    [projectPeopleViewController release];
    [dashboardNavigationController release];
    [messageNavigationController release];
    [todoListNavigationController release];
    [milestoneNavigationController release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)setProject:(Project *)_project
{
    [project release];
    project = [_project retain];
    self.projectMilestoneViewController.project = project;
    self.projectTodoListViewController.project = project;
    self.projectPeopleViewController.project = project;
    self.projectMessagesViewController.project = project;
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{

    
        if(self.project != nil && projectPeopleViewController.companySyncController == nil && projectPeopleViewController.peopleSyncController == nil)
        {
            CompanySyncController *_companySyncController = [[CompanySyncController alloc] initWithProject:self.project];
            projectPeopleViewController.companySyncController = _companySyncController;
            [_companySyncController startSyncing];
            [_companySyncController release];
            PeopleSyncController *_peopleSyncController = [[PeopleSyncController alloc] initWithProject:self.project];
            projectPeopleViewController.peopleSyncController = _peopleSyncController;
            [_peopleSyncController startSyncing];
            [_peopleSyncController release];
            CategorySyncController *_categorySyncController = [[CategorySyncController alloc] initWithProject:self.project];
            [_categorySyncController startSyncing];
            [_categorySyncController release];
        }
//    [self.tabBarController.navigationController setNavigationBarHidden:YES animated:NO];
    [super viewWillAppear:animated];
}
-(void) backButtonPressed:(id)sender
{
    [self.tabBarController.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
