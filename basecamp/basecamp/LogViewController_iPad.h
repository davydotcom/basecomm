//
//  LogViewController_iPad.h
//  basecamp
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AccountsViewController;
@interface LogViewController_iPad : UIViewController <UIPopoverControllerDelegate, UISplitViewControllerDelegate> {
    
}
@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;
@property (nonatomic, retain) IBOutlet UILabel *detailDescriptionLabel;
@property (nonatomic, retain) NSManagedObject *detailItem;
@property (nonatomic, assign) IBOutlet AccountsViewController *rootViewController;
@end
