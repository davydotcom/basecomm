//
//  PeopleSyncController.m
//  basecamp
//
//  Created by David Estes on 3/25/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "PeopleSyncController.h"
#import <CoreData/CoreData.h>
#import "basecampAppDelegate.h"
#import "BCAccount.h"
#import "BAccount.h"
#import "Project.h"
#import "BCProject.h"
#import "Person.h"
#import "Company.h"
#import "BCPerson.h"
#import "SyncController.h"

@implementation PeopleSyncController
@synthesize account,project;
@synthesize managedObjectContext,parentObjectContext,sourceThread;
@synthesize finished;
#pragma mark - Sync Methods
-(id)initWithAccount:(BAccount* )_account
{
    self = [super init];
    if(self)
    {
        self.account = _account;
    }
    return self;
}
-(id)initWithProject:(Project* )_project
{
    self = [super init];
    if(self)
    {
        self.project = _project;
    }
    return self;
}
-(void)startSyncing
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [self performSelector:@selector(runSyncThread) onThread:appDelegate.syncController.syncThread withObject:nil waitUntilDone:NO];
}
-(void) fetchAccountWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"BAccount" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.account];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.account = [array objectAtIndex:0];
    }
    
}
-(void) fetchProjectWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.project];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.project = [array objectAtIndex:0];
    }
    
}
-(void)runSyncThread
{
    [self retain];

    finished = NO;
    BOOL createdOwnContext = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(self.managedObjectContext == nil)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        self.managedObjectContext = [appDelegate.syncController managedObjectContext];
        createdOwnContext = YES;
    }
    if(self.account != nil)
    {
        [self fetchAccountWithNewManagedObjectContext:managedObjectContext];
    }
    if(self.project != nil)
    {
        [self fetchProjectWithNewManagedObjectContext:managedObjectContext];
    }
    

    //Get Array of all people from CoreData
    NSArray *people = nil;
    if(self.project != nil)
    {
        people = [Person allPeopleForProject:self.project WithManagedObjectContext:managedObjectContext];
    }
    else
    {
        people = [Person allPeopleForAccount:account WithManagedObjectContext:managedObjectContext];
    }
        
    //Then We Fetch all Projects from server to sync with
    if(self.project != nil)
    {
        BCProject *_bcProject = [[BCProject alloc] initWithAccount:[project.account basecampAccount]];
        _bcProject.projectID = self.project.projectID;
        [BCPerson loadPeopleForProject:_bcProject withCompletionHandler:^(NSArray *_bcpeople, NSError *error) {
            if(error == nil)
            {
                //First Check to see if any people have been removed!
                for(Person *_dbPerson in people)
                {
                    BOOL found=NO;
                    for(BCPerson *_bcPerson in _bcpeople)
                    {
                        if ([_bcPerson.personID integerValue] == [_dbPerson.personID integerValue]) {
                            found = YES;
                        }
                    }
                    if(!found)
                    {
                        [self performSelectorOnMainThread:@selector(didRemovePersonNotification:) withObject:_dbPerson waitUntilDone:YES];
                        [_dbPerson removeProjectsObject:self.project];
                    }
                }
                //     NSLog(@"Looking for new and updating people!");
                //Add New Projects and Update Existing
                for(BCPerson *_bcPerson in _bcpeople)
                {
                    BOOL found = NO;
                    for(Person *_dbPerson in people)
                    {
                        if ([_bcPerson.personID integerValue] == [_dbPerson.personID integerValue]) {
                            //Update record
                            _dbPerson.title = _bcPerson.title;
                            _dbPerson.lastLogin = _bcPerson.lastLogin;
                            _dbPerson.phoneNumberFax = _bcPerson.phoneNumberFax;
                            _dbPerson.phoneNumberHome = _bcPerson.phoneNumberHome;
                            _dbPerson.phoneNumberMobile = _bcPerson.phoneNumberMobile;
                            _dbPerson.phoneNumberOffice = _bcPerson.phoneNumberOffice;
                            _dbPerson.phoneNumberOfficeExt = _bcPerson.phoneNumberOfficeExt;
                            _dbPerson.clientID = _bcPerson.clientID;
                            _dbPerson.administrator = _bcPerson.administrator;
                            _dbPerson.imService = _bcPerson.imService;
                            _dbPerson.imHandle = _bcPerson.imHandle;
                            _dbPerson.emailAddress = _bcPerson.emailAddress;
                            _dbPerson.avatarURL = _bcPerson.avatarURL;
                            _dbPerson.firstName = _bcPerson.firstName;
                            _dbPerson.lastName = _bcPerson.lastName;
                            _dbPerson.userName = _bcPerson.userName;
                            _dbPerson.hasAccessToNewProjects = _bcPerson.hasAccessToNewProjects;
                            _dbPerson.companyID = _bcPerson.companyID;
                            if(_dbPerson.company == nil)
                            {
                                _dbPerson.company = [Company findCompanyByID:[_bcPerson.companyID integerValue] withManagedObjectContext:managedObjectContext];
                            }
                            found = YES;
                        }
                    }
                    if(!found)
                    {
                        //Time to create a new person file
                        Person *_dbPerson = [Person findPersonByID:[_bcPerson.personID integerValue] withManagedObjectContext:managedObjectContext];
                        if(_dbPerson == nil)
                        {
                            NSLog(@"Adding Person in project Sync!");
                            _dbPerson = (Person *)[NSEntityDescription insertNewObjectForEntityForName:@"Person" inManagedObjectContext:managedObjectContext];
                        }
                        _dbPerson.personID = _bcPerson.personID;
                        _dbPerson.account = self.project.account;
                        _dbPerson.title = _bcPerson.title;
                        _dbPerson.lastLogin = _bcPerson.lastLogin;
                        _dbPerson.phoneNumberFax = _bcPerson.phoneNumberFax;
                        _dbPerson.phoneNumberHome = _bcPerson.phoneNumberHome;
                        _dbPerson.phoneNumberMobile = _bcPerson.phoneNumberMobile;
                        _dbPerson.phoneNumberOffice = _bcPerson.phoneNumberOffice;
                        _dbPerson.phoneNumberOfficeExt = _bcPerson.phoneNumberOfficeExt;
                        _dbPerson.clientID = _bcPerson.clientID;
                        _dbPerson.administrator = _bcPerson.administrator;
                        _dbPerson.imService = _bcPerson.imService;
                        _dbPerson.imHandle = _bcPerson.imHandle;
                        _dbPerson.emailAddress = _bcPerson.emailAddress;
                        _dbPerson.avatarURL = _bcPerson.avatarURL;
                        _dbPerson.firstName = _bcPerson.firstName;
                        _dbPerson.lastName = _bcPerson.lastName;
                        _dbPerson.userName = _bcPerson.userName;
                        _dbPerson.hasAccessToNewProjects = _bcPerson.hasAccessToNewProjects;
                        _dbPerson.companyID = _bcPerson.companyID;
                        if(_dbPerson.company == nil)
                        {
                            _dbPerson.company = [Company findCompanyByID:[_bcPerson.companyID integerValue] withManagedObjectContext:managedObjectContext];
                        }
                        [_dbPerson addProjectsObject:self.project];
                    }
                }
            }
            finished = YES;
        }];
    }
    else
    {
        [BCPerson loadPeopleForAccount:[account basecampAccount] withCompletionHandler:^(NSArray *_bcpeople, NSError *error) {
            if(error == nil)
            {
                //First Check to see if any people have been removed!
                for(Person *_dbPerson in people)
                {
                    BOOL found=NO;
                    for(BCPerson *_bcPerson in _bcpeople)
                    {
                        if ([_bcPerson.personID integerValue] == [_dbPerson.personID integerValue]) {
                            found = YES;
                        }
                    }
                    if(!found)
                    {
                        [self performSelectorOnMainThread:@selector(didRemovePersonNotification:) withObject:_dbPerson waitUntilDone:YES];
                        [managedObjectContext deleteObject:_dbPerson];
                    }
                }
                //     NSLog(@"Looking for new and updating people!");
                //Add New Projects and Update Existing
                for(BCPerson *_bcPerson in _bcpeople)
                {
                    BOOL found = NO;
                    for(Person *_dbPerson in people)
                    {
                        if ([_bcPerson.personID integerValue] == [_dbPerson.personID integerValue]) {
                            //Update record
                            _dbPerson.title = _bcPerson.title;
                            _dbPerson.lastLogin = _bcPerson.lastLogin;
                            _dbPerson.phoneNumberFax = _bcPerson.phoneNumberFax;
                            _dbPerson.phoneNumberHome = _bcPerson.phoneNumberHome;
                            _dbPerson.phoneNumberMobile = _bcPerson.phoneNumberMobile;
                            _dbPerson.phoneNumberOffice = _bcPerson.phoneNumberOffice;
                            _dbPerson.phoneNumberOfficeExt = _bcPerson.phoneNumberOfficeExt;
                            _dbPerson.clientID = _bcPerson.clientID;
                            _dbPerson.administrator = _bcPerson.administrator;
                            _dbPerson.imService = _bcPerson.imService;
                            _dbPerson.imHandle = _bcPerson.imHandle;
                            _dbPerson.emailAddress = _bcPerson.emailAddress;
                            _dbPerson.avatarURL = _bcPerson.avatarURL;
                            _dbPerson.firstName = _bcPerson.firstName;
                            _dbPerson.lastName = _bcPerson.lastName;
                            _dbPerson.userName = _bcPerson.userName;
                            _dbPerson.hasAccessToNewProjects = _bcPerson.hasAccessToNewProjects;
                            
                            _dbPerson.companyID = _bcPerson.companyID;
                            if(_dbPerson.company == nil)
                            {
                                _dbPerson.company = [Company findCompanyByID:[_bcPerson.companyID integerValue] withManagedObjectContext:managedObjectContext];
                            }
                            found = YES;
                        }
                    }
                    if(!found)
                    {
                        //Time to create a new person file
                        NSLog(@"Adding Person for non project!");
                        Person *_dbPerson = (Person *)[NSEntityDescription insertNewObjectForEntityForName:@"Person" inManagedObjectContext:managedObjectContext];
                        _dbPerson.personID = _bcPerson.personID;
                        _dbPerson.account = self.account;
                        _dbPerson.title = _bcPerson.title;
                        _dbPerson.lastLogin = _bcPerson.lastLogin;
                        _dbPerson.phoneNumberFax = _bcPerson.phoneNumberFax;
                        _dbPerson.phoneNumberHome = _bcPerson.phoneNumberHome;
                        _dbPerson.phoneNumberMobile = _bcPerson.phoneNumberMobile;
                        _dbPerson.phoneNumberOffice = _bcPerson.phoneNumberOffice;
                        _dbPerson.phoneNumberOfficeExt = _bcPerson.phoneNumberOfficeExt;
                        _dbPerson.clientID = _bcPerson.clientID;
                        _dbPerson.administrator = _bcPerson.administrator;
                        _dbPerson.imService = _bcPerson.imService;
                        _dbPerson.imHandle = _bcPerson.imHandle;
                        _dbPerson.emailAddress = _bcPerson.emailAddress;
                        _dbPerson.avatarURL = _bcPerson.avatarURL;
                        _dbPerson.firstName = _bcPerson.firstName;
                        _dbPerson.lastName = _bcPerson.lastName;
                        _dbPerson.userName = _bcPerson.userName;
                        _dbPerson.hasAccessToNewProjects = _bcPerson.hasAccessToNewProjects;
                        _dbPerson.companyID = _bcPerson.companyID;
                        _dbPerson.company = [Company findCompanyByID:[_bcPerson.companyID integerValue] withManagedObjectContext:managedObjectContext];
                    }
                }
            }
            finished = YES;
        }];
    }
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];  
    }
    // NSLog(@"Saving Changes!");
    NSError *error=nil;
    if(![managedObjectContext save:&error])
    {
        NSLog(@"Error Saving to CoreData After Project Sync");
    }
    NSLog(@"Notifying person sync completion!");
    [self performSelectorOnMainThread:@selector(didFinishSyncingPeopleNotification) withObject:nil waitUntilDone:NO];
    if(createdOwnContext)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

    }
    [autoreleasepool release];
    [self release];
}

-(void)registerForManagedObjectNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mergeChanges:) name:NSManagedObjectContextDidSaveNotification object:self.managedObjectContext];
    
}
-(void)mergeChanges:(NSNotification *)notification
{
    NSLog(@"Merging person Changes!");
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *mainContext = [appDelegate managedObjectContext];
    [mainContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:) withObject:notification waitUntilDone:YES];        
}
-(void)didRemovePersonNotification:(Person *)_person
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:DID_REMOVE_PERSON_NOTIFICATION object:self userInfo:[NSDictionary dictionaryWithObject:_person forKey:@"person"]];
}
-(void)didFinishSyncingPeopleNotification
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:PEOPLE_DID_SYNC_NOTIFICATION object:nil];
}

-(void)dealloc
{
    [account release];
    [project release];
    [sourceThread release];
    [parentObjectContext release];
    [managedObjectContext release];
    [super dealloc];
}
@end
