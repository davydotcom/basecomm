//
//  TodoList.h
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#define DID_REMOVE_TODOLIST_NOTIFICATION @"DID_REMOVE_TODOLIST_NOTIFICATION"
#define TODOLISTS_DID_SYNC_NOTIFICATION @"TODOLISTS_DID_SYNC_NOTIFICATION"
@class BAccount, Project,BCTodoList,Milestone,TodoListItemSyncController;

@interface TodoList : NSManagedObject {
    BOOL finished;
    TodoListItemSyncController *todoListItemSyncController;
@private
}
@property (nonatomic, assign) BOOL finished;
@property (nonatomic, retain) NSNumber * position;
@property (nonatomic, retain) NSNumber * completedCount;
@property (nonatomic, retain) NSNumber * uncompletedCount;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * todoListID;
@property (nonatomic, retain) NSNumber * tracked;
@property (nonatomic, retain) NSNumber * complete;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSNumber * isPrivate;
@property (nonatomic, retain) NSNumber * synced;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) BAccount * account;
@property (nonatomic, retain) Project * project;
@property (nonatomic, retain) Milestone * milestone;
@property (nonatomic, retain) NSSet* todoListItems;
@property (nonatomic, retain) TodoListItemSyncController *todoListItemSyncController;

+(NSArray *)allTodoListsForProjectID:(NSInteger)_projectID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(NSArray *)allTodoListsForAccount:(BAccount *)_account withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(NSArray *)unsyncedTodoListsWithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(TodoList *)findByTodoListID:(NSInteger)_todoListID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
-(void)syncTodoList;
-(BCTodoList *) BCTodoListFromTodoList;

@end
