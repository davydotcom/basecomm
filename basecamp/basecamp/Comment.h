//
//  Comment.h
//  basecamp
//
//  Created by David Estes on 6/11/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#define DID_REMOVE_COMMENT_NOTIFICATION @"DID_REMOVE_COMMENT_NOTIFICATION"
#define COMMENTS_DID_SYNC_NOTIFICATION @"COMMENTS_DID_SYNC_NOTIFICATION"
@class Message, Milestone, Person, TodoListItem;

@interface Comment : NSManagedObject {
@private
}
@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSString * authorName;
@property (nonatomic, retain) NSString * emailedFrom;
@property (nonatomic, retain) NSNumber * commentID;
@property (nonatomic, retain) Person *person;
@property (nonatomic, retain) TodoListItem *todoListItem;
@property (nonatomic, retain) Milestone *milestone;
@property (nonatomic, retain) Message *message;
@property (nonatomic, retain) NSNumber * synced;
@property (nonatomic, retain) NSNumber * deleted;
@end
