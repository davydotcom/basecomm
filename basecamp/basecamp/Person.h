//
//  Person.h
//  basecamp
//
//  Created by David Estes on 3/25/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#define DID_REMOVE_PERSON_NOTIFICATION @"DID_REMOVE_PERSON_NOTIFICATION"
#define PEOPLE_DID_SYNC_NOTIFICATION @"PEOPLE_DID_SYNC_NOTIFICATION"
#define AVATAR_IMAGE_UPDATED_NOTIFICATION @"AVATAR_IMAGE_UPDATED_NOTIFICATION"
@class BAccount, Project,Company,Message,Comment;

@interface Person : NSManagedObject {
    UIImage *cachedAvatar;
    BOOL loadingImage;
@private

}
@property(nonatomic, assign)  BOOL loadingImage;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSDate * lastLogin;
@property (nonatomic, retain) NSString * phoneNumberMobile;
@property (nonatomic, retain) NSNumber * clientID;
@property (nonatomic, retain) NSString * imService;
@property (nonatomic, retain) NSNumber * administrator;
@property (nonatomic, retain) NSNumber * personID;
@property (nonatomic, retain) NSString * emailAddress;
@property (nonatomic, retain) NSString * phoneNumberOfficeExt;
@property (nonatomic, retain) NSString * phoneNumberHome;
@property (nonatomic, retain) NSString * avatarURL;
@property (nonatomic, retain) NSString * imHandle;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * phoneNumberOffice;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * phoneNumberFax;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSNumber * hasAccessToNewProjects;
@property (nonatomic, retain) NSNumber * companyID;
@property (nonatomic, retain) BAccount * account;
@property (nonatomic, retain) Company * company;
@property (nonatomic, retain) NSSet* projects;
@property (nonatomic, retain) NSSet* comments;
@property (nonatomic, retain) NSSet* messages;
@property (nonatomic, retain) UIImage *cachedAvatar;
+(Person *)findPersonByID:(NSInteger)_personID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(NSArray *)allPeopleForAccount:(BAccount *)_account WithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(NSArray *)allPeopleForProject:(Project *)_project WithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
- (void)addProjectsObject:(Project *)value;
- (void)removeProjectsObject:(Project *)value;
-(void)loadCachedImage;
@end
@interface Person (CoreDataGeneratedAccessors)
- (void)addCommentsObject:(Comment *)value;
- (void)removeCommentsObject:(Comment *)value;
- (void)addComments:(NSSet *)value;
- (void)removeComments:(NSSet *)value;

- (void)addMessagesObject:(Message *)value;
- (void)removeMessagesObject:(Message *)value;
- (void)addMessages:(NSSet *)value;
- (void)removeMessages:(NSSet *)value;
@end
