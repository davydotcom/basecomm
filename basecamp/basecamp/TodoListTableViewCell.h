//
//  TodoListTableViewCell.h
//  basecamp
//
//  Created by David Estes on 3/19/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TodoListTableViewCell : UITableViewCell {
    UILabel *title;
    UILabel *todoItem1;
    UILabel *todoItem2;
    UILabel *todoItem3;
    UILabel *more;
}
@property(nonatomic, retain) UILabel *title;
@property(nonatomic, retain) UILabel *todoItem1;
@property(nonatomic, retain) UILabel *todoItem2;
@property(nonatomic, retain) UILabel *todoItem3;
@property(nonatomic, retain) UILabel *more;
@end
