//
//  ProjectDashboardViewController.h
//  basecamp
//
//  Created by David Estes on 3/26/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Project,ProjectTodoListViewController,ProjectMilestonesViewController,ProjectPeopleViewController,ProjectMessagesViewController;
@interface ProjectDashboardViewController : UIViewController <UITabBarControllerDelegate> {
    UITabBarController *projectTabBarController;
    ProjectTodoListViewController *projectTodoListViewController;
    ProjectMilestonesViewController *projectMilestoneViewController;
    ProjectPeopleViewController *projectPeopleViewController;
    ProjectMessagesViewController *projectMessagesViewController;
//  NavigationController for sub tab items
    UINavigationController *dashboardNavigationController;
    UINavigationController *todoListNavigationController;
    UINavigationController *milestoneNavigationController;
    UINavigationController *messageNavigationController;
    UINavigationController *peopleNavigationController;
    Project *project;
}
@property(nonatomic,retain) UITabBarController *projectTabBarController;
@property(nonatomic,retain) Project *project;
@property(nonatomic, retain) ProjectTodoListViewController *projectTodoListViewController;
@property(nonatomic, retain) ProjectMilestonesViewController *projectMilestoneViewController;
@property(nonatomic, retain) ProjectPeopleViewController *projectPeopleViewController;
@property(nonatomic, retain) ProjectMessagesViewController *projectMessagesViewController;

@property(nonatomic,retain) UINavigationController *dashboardNavigationController;
@property(nonatomic,retain) UINavigationController *todoListNavigationController;
@property(nonatomic,retain) UINavigationController *milestoneNavigationController;
@property(nonatomic,retain) UINavigationController *messageNavigationController;
@property(nonatomic,retain) UINavigationController *peopleNavigationController;
@end
