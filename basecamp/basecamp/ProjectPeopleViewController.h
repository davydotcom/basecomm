//
//  ProjectPeopleViewController.h
//  basecamp
//
//  Created by David Estes on 4/28/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
@class Project,NSFetchedResultsController, PeopleSyncController,CompanySyncController;
@interface ProjectPeopleViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,EGORefreshTableHeaderDelegate,NSFetchedResultsControllerDelegate> {
    IBOutlet UITableView *tableView;
    IBOutlet UILabel *projectTitleLabel;
    IBOutlet UILabel *companyTitleLabel;
    PeopleSyncController *peopleSyncController;
    CompanySyncController *companySyncController;
    Project *project;
    NSFetchedResultsController *fetchedResultsController;   
    BOOL _reloading;
    EGORefreshTableHeaderView *_refreshHeaderView;
}
@property(nonatomic, retain) UITableView *tableView;
@property(nonatomic, retain) UILabel *projectTitleLabel;
@property(nonatomic, retain) UILabel *companyTitleLabel;
@property(nonatomic, retain) PeopleSyncController *peopleSyncController;
@property(nonatomic, retain) CompanySyncController *companySyncController;
@property(nonatomic, retain) Project *project;
@property(nonatomic, retain) NSFetchedResultsController *fetchedResultsController;


-(void)registerForNotifications;
-(void)unregisterForNotifications;
-(void)AvatarLoadedNotification:(NSNotification *)notification;
-(void)doneLoadingTableViewData;
@end
