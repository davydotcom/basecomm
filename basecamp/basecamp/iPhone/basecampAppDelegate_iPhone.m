//
//  basecampAppDelegate_iPhone.m
//  basecamp
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "basecampAppDelegate_iPhone.h"

@implementation basecampAppDelegate_iPhone
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.window.rootViewController = self.mainNavigationController;
//    [self.window addSubview:mainNavigationController.view];
    [super application:application didFinishLaunchingWithOptions:launchOptions];
    return YES;
}

- (void)dealloc
{
	[super dealloc];
}

@end
