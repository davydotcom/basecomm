//
//  SyncController.m
//  basecamp
//
//  Created by David Estes on 3/18/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "SyncController.h"
#import "AccountSyncController.h"
#import "BAccount.h"
#import "basecampAppDelegate.h"
#import <CoreData/CoreData.h>
@implementation SyncController
@synthesize finished,status,syncControllers,managedObjectContext,syncThread;
-(id)init
{
    self = [super init];
    if(self)
    {
        NSMutableArray *_syncControllers = [[NSMutableArray alloc] initWithCapacity:1];
        self.syncControllers = _syncControllers;
        [_syncControllers release];
        finished = YES;
    }
    return self;
}
-(void)startSyncing
{
    if(!finished)
    {
        return;
    }
    finished = NO;
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reverseMergeChanges:) name:NSManagedObjectContextDidSaveNotification object:[appDelegate managedObjectContext]];

    [NSThread detachNewThreadSelector:@selector(runSyncThread) toTarget:self withObject:nil];
    
}
-(void)startupSyncThread
{
    NSThread *_thread = [[NSThread alloc] initWithTarget:self selector:@selector(syncLoop) object:nil];
    self.syncThread = _thread;
    [_thread release];
    [self.syncThread start];
}
-(void)syncLoop
{
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *importContext = [[NSManagedObjectContext alloc] init];
    [importContext setPersistentStoreCoordinator:[appDelegate persistentStoreCoordinator]];
    [importContext setUndoManager:nil];
    self.managedObjectContext = importContext;
    [importContext release];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mergeChanges:) name:NSManagedObjectContextDidSaveNotification object:importContext];
    while(![[NSThread currentThread] isCancelled])
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextDidSaveNotification object:importContext];
    [autoreleasepool release];
}
-(void)addSyncControllerForAccount:(BAccount *)_account
{
    BOOL found = NO;
    for(AccountSyncController *accountSyncController in self.syncControllers)
    {
        if([accountSyncController.account.accountID integerValue] == [_account.accountID integerValue])
        {
            found = YES;
            break;
        }
    }
    AccountSyncController *_accountSyncContorller = [[AccountSyncController alloc] initWithAccount:_account];
    [self.syncControllers addObject:_accountSyncContorller];
    [_accountSyncContorller release];
    if(self.finished)
    {
        [self startSyncing];
    }
    
}
-(void)removeSyncControllerForAccount:(BAccount *)_account
{
    for(AccountSyncController *accountSyncController in self.syncControllers)
    {
        if([accountSyncController.account.accountID integerValue] == [_account.accountID integerValue])
        {
            [self.syncControllers removeObject:accountSyncController];
            break;
        }
    }
}
-(void)runSyncThread
{
    [self retain];
    finished = NO;
    status = 0;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];

    for(AccountSyncController *_accountSyncController in syncControllers)
    {
        NSLog(@"Syncing Account: %@",_accountSyncController.account.companyName);
        [_accountSyncController performSelector:@selector(runSyncThread) onThread:self.syncThread withObject:nil waitUntilDone:YES];
        status++;
    }
    
    [autoreleasepool release];
    finished = YES;
    [self release];
}
-(void)runSyncUnsyncedObjectsThread
{
    [self retain];
    finished = NO;
    status = 0;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    
    for(AccountSyncController *_accountSyncController in syncControllers)
    {
        NSLog(@"Syncing Account: %@",_accountSyncController.account.companyName);
        [_accountSyncController performSelector:@selector(runSyncUnsyncedObjectsThread) onThread:self.syncThread withObject:nil waitUntilDone:YES];
        status++;
    }
    
    [autoreleasepool release];
    finished = YES;
    [self release];
}
-(void)mergeChanges:(NSNotification *)notification
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [[appDelegate managedObjectContext] performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:) withObject:notification waitUntilDone:YES];            
}
-(void)reverseMergeChanges:(NSNotification *)notification
{
    [self.managedObjectContext performSelector:@selector(mergeChangesFromContextDidSaveNotification:) onThread:self.syncThread withObject:notification waitUntilDone:YES];
}
-(void)dealloc
{
    [managedObjectContext release];
    [syncThread release];
    [syncControllers release];
    [super dealloc];
}
@end
