//
//  ProjectMessagesViewController.m
//  basecamp
//
//  Created by David Estes on 4/27/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "ProjectMessagesViewController.h"


@implementation ProjectMessagesViewController
@synthesize tableView,companyTitleLabel,projectTitleLabel,project,categories,fetchedResultsController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Messages";
        self.tabBarItem.image = [UIImage imageNamed:@"icon-messages.png"];
    }
    return self;
}

- (void)dealloc
{
    [projectTitleLabel release];
    [companyTitleLabel release];
    [tableView release];
    [project release];
    [categories release];
    [fetchedResultsController release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
