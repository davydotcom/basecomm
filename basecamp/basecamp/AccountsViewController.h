//
//  AccountsViewController.h
//  basecamp
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWTableViewController.h"
@class NewAccountViewController;
@class NSFetchedResultsController;
@interface AccountsViewController : RWTableViewController {
    IBOutlet UINavigationController *rootNavigationController;
    NewAccountViewController *newAccountViewController;
    NSFetchedResultsController *fetchedResultsController;
}
@property(nonatomic, retain) UINavigationController *rootNavigationController;
@property(nonatomic, retain) NewAccountViewController *newAccountViewController;
@property(nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
-(IBAction)addAccountButtonPressed:(id)sender;
-(IBAction)editAccountsButtonPressed:(id)sender;
@end
