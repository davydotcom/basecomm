//
//  RWMilestoneSelector.h
//  basecamp
//
//  Created by David Estes on 4/11/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RWMilestoneSelector;
@class Project,BAccount,Milestone,NSFetchedResultsController;
@protocol RWMilestoneSelectorDelegate <NSObject>
-(void)rwMilestoneSelector:(RWMilestoneSelector *)_rwMilestoneSelector didSelectMilestone:(Milestone *)_milestone;
@end
@interface RWMilestoneSelector : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource> {
    IBOutlet UIPickerView *pickerView;
    IBOutlet UIView *selectionView;
    UIView *parentView;
    NSFetchedResultsController *fetchedResultsController;
    Project *project;
    BAccount *account;
    Milestone *milestone;
    id <RWMilestoneSelectorDelegate> delegate;
}
@property(nonatomic,assign) id delegate;
@property(nonatomic,retain) UIPickerView *pickerView;
@property(nonatomic,retain) UIView *selectionView;
@property(nonatomic,retain) UIView *parentView;
@property(nonatomic,retain) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic,retain) Project *project;
@property(nonatomic,retain) BAccount *account;
@property(nonatomic,retain) Milestone *milestone;

-(IBAction)doneButtonPressed:(id)sender;
-(IBAction)cancelButtonPressed:(id)sender;
-(void)slideIn;
-(void)slideOut;
@end
