//
//  UIDescriptionViewController.m
//  basecamp
//
//  Created by David Estes on 4/27/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "UIDescriptionViewController.h"


@implementation UIDescriptionViewController
@synthesize delegate,textView,description;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [description release];
    [textView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.textView.text = self.description;
    if(self.description == nil || [self.description isEqualToString:@""])
    {
        [self.textView becomeFirstResponder];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardDidShowNotification object:nil];
    [super viewWillAppear:animated];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [super viewDidDisappear:animated];
}
-(IBAction)cancelButtonPressed:(id)sender
{
    if(delegate != nil && [delegate respondsToSelector:@selector(didCancelUIDescriptionViewController:)])
    {
        [delegate didCancelUIDescriptionViewController:self];
    }
}
-(IBAction)saveButtonPressed:(id)sender
{
    self.description = self.textView.text;
    if(delegate != nil && [delegate respondsToSelector:@selector(UIDescriptionViewController:didSave:)])
    {
        [delegate UIDescriptionViewController:self didSave:self.description];
    }
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue].size;
	UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);		
    
    
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
	{
		switch(self.interfaceOrientation)
		{
			case UIInterfaceOrientationPortrait:
			case UIInterfaceOrientationPortraitUpsideDown:
				contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);		
				break;
			default:
				contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.width, 0.0);		
		};
        
	}
	self.textView.contentInset = contentInsets;
    
    self.textView.scrollIndicatorInsets = contentInsets;
	

}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.textView.contentInset = contentInsets;
    self.textView.scrollIndicatorInsets = contentInsets;
}


@end
