//
//  ProjectMessagesViewController.h
//  basecamp
//
//  Created by David Estes on 4/27/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Project,NSFetchedResultsController;
@interface ProjectMessagesViewController : UIViewController {
    IBOutlet UITableView *tableView;
    IBOutlet UILabel *projectTitleLabel;
    IBOutlet UILabel *companyTitleLabel;
    NSMutableArray *categories;
    Project *project;
    NSFetchedResultsController *fetchedResultsController;
}
@property(nonatomic, retain) UITableView *tableView;
@property(nonatomic, retain) UILabel *projectTitleLabel;
@property(nonatomic, retain) UILabel *companyTitleLabel;
@property(nonatomic, retain) NSMutableArray *categories;
@property(nonatomic, retain) Project *project;
@property(nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@end
