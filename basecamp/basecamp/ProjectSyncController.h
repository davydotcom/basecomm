//
//  ProjectSyncController.h
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
@class NSManagedObjectContext;
@class BAccount;
@class Project;
@interface ProjectSyncController : NSObject {
    BAccount *account;
    NSManagedObjectContext *managedObjectContext;
   NSManagedObjectContext *parentObjectContext;
    NSThread *sourceThread;
    BOOL finished;
}
@property(nonatomic,retain) BAccount *account;
@property(nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property(nonatomic,retain) NSManagedObjectContext *parentObjectContext;
@property(nonatomic, retain) NSThread *sourceThread;
@property(nonatomic,assign) BOOL finished;
-(id)initWithAccount:(BAccount* )_account;
-(void)didRemoveProjectNotification:(Project *)_project;
-(void)didFinishSyncingProjectsNotification;
-(void)runSyncThread;
-(void)startSyncing;
-(void) fetchAccountWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext;
-(void)registerForManagedObjectNotifications;
-(void)mergeChanges:(NSNotification *)notification;
@end
