//
//  TodoList.m
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import "TodoList.h"
#import "BAccount.h"
#import "Project.h"
#import "BCTodoList.h"
#import "Milestone.h"
#import "TodoListItemSyncController.h"

@implementation TodoList
@dynamic position;
@dynamic completedCount;
@dynamic uncompletedCount;
@dynamic name;
@dynamic todoListID;
@dynamic tracked;
@dynamic complete;
@dynamic desc;
@dynamic isPrivate;
@dynamic account;
@dynamic project;
@dynamic milestone;
@dynamic todoListItems;
@dynamic synced;
@dynamic deleted;
@synthesize finished;
@synthesize todoListItemSyncController;
+(NSArray *)unsyncedTodoListsWithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TodoList" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"synced == 0 || synced == nil"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"todoListID" ascending:YES];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return returnArray;
}
+(NSArray *)allTodoListsForProjectID:(NSInteger)_projectID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TodoList" inManagedObjectContext:_managedObjectContext];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"todoListID" ascending:YES];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"project.projectID == %d",_projectID];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return returnArray;
}
+(NSArray *)allTodoListsForAccount:(BAccount *)_account withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext
{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TodoList" inManagedObjectContext:_managedObjectContext];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"todoListID" ascending:YES];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"account == %@",_account];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return returnArray;
}
+(TodoList *)findByTodoListID:(NSInteger)_todoListID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TodoList" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"todoListID == %d",_todoListID];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"todoListID" ascending:YES];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *results = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    results = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    TodoList *_todoList = nil;
    if(results != nil && [results count] > 0)
    {
        _todoList = (TodoList *)[results objectAtIndex:0];
    }
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return _todoList;
}
#pragma mark - Submit New Objects

-(void)syncTodoList
{
    self.finished = NO;
    BCTodoList *_bcTodoList = [self BCTodoListFromTodoList];
    if(![self.deleted boolValue])
    {
        
        
        [_bcTodoList saveWithCompletionHandler:^(BCTodoList *_todoList, NSError *error) {
            if(error == nil)
            {

                self.todoListID = _todoList.todoListID;
                //TODO: Add to proper project if exists
                self.project = [Project findByProjectID:[_todoList.projectID integerValue] withManagedObjectContext:[self managedObjectContext]];
                self.name = _todoList.name;
                self.desc = _todoList.description;
                self.complete = _todoList.complete;
                self.completedCount = _todoList.completedCount;
                self.uncompletedCount = _todoList.uncompletedCount;
                self.position = _todoList.position;
                self.tracked = _todoList.tracked;
                self.isPrivate = _todoList.isPrivate;
                if(_todoList.milestoneID != nil)
                {
                    self.milestone = [Milestone findByMilestoneID:[_todoList.milestoneID integerValue] withManagedObjectContext:[self managedObjectContext]];
                }
                self.synced = [NSNumber numberWithBool:YES];
                
            }
            [[self managedObjectContext] save:nil];
            finished = YES;
        }];
    }
    else
    {
        [_bcTodoList destroyWithCompletionHandler:^(BCTodoList *todoList, NSError *error) {
            if(error == nil)
            {
                [[self managedObjectContext] deleteObject:self];
                [[self managedObjectContext] save:nil];
            }
            finished = YES;
        }];
    }
    
}

-(BCTodoList *) BCTodoListFromTodoList
{
    BCAccount *_account = [self.project.account basecampAccount];
    BCTodoList *_bcTodoList = [[BCTodoList alloc] initWithAccount:_account];
    _bcTodoList.projectID = self.project.projectID;
    _bcTodoList.todoListID = self.todoListID;
    _bcTodoList.tracked = self.tracked;
    _bcTodoList.complete = self.complete;
    _bcTodoList.completedCount = self.completedCount;
    _bcTodoList.uncompletedCount = self.uncompletedCount;
    _bcTodoList.name = self.name;
    _bcTodoList.description = self.desc;
    _bcTodoList.position = self.position;
    _bcTodoList.isPrivate = self.isPrivate;
    if(self.milestone != nil)
    {
        _bcTodoList.milestoneID = self.milestone.milestoneID;
    }
    return _bcTodoList;
}
-(void)dealloc
{
    [todoListItemSyncController release];
    [super dealloc];
}
#pragma mark - CoreData RelationShip Methods
- (void)addTodoListItemsObject:(NSManagedObject *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"todoListItems" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"todoListItems"] addObject:value];
    [self didChangeValueForKey:@"todoListItems" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)removeTodoListItemsObject:(NSManagedObject *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"todoListItems" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"todoListItems"] removeObject:value];
    [self didChangeValueForKey:@"todoListItems" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addTodoListItems:(NSSet *)value {    
    [self willChangeValueForKey:@"todoListItems" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"todoListItems"] unionSet:value];
    [self didChangeValueForKey:@"todoListItems" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeTodoListItems:(NSSet *)value {
    [self willChangeValueForKey:@"todoListItems" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"todoListItems"] minusSet:value];
    [self didChangeValueForKey:@"todoListItems" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


@end
