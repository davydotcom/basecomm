//
//  TodoListItem.h
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#define DID_REMOVE_TODOLISTITEM_NOTIFICATION @"DID_REMOVE_TODOLISTITEM_NOTIFICATION"
#define TODOLISTITEMS_DID_SYNC_NOTIFICATION @"TODOLISTITEMS_DID_SYNC_NOTIFICATION"
@class TodoList,BCTodoListItem,Comment;

@interface TodoListItem : NSManagedObject {
    BOOL finished;
@private
}
@property (nonatomic, assign) BOOL finished;
@property (nonatomic, retain) NSNumber * completed;
@property (nonatomic, retain) NSNumber * commentsCount;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSDate * dueAt;
@property (nonatomic, retain) NSNumber * todoListItemID;
@property (nonatomic, retain) NSNumber * creatorID;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSNumber * position;
@property (nonatomic, retain) NSNumber * responsiblePartyID;
@property (nonatomic, retain) NSString * responsiblePartyName;
@property (nonatomic, retain) NSString * responsiblePartyType;
@property (nonatomic, retain) NSString * creatorName;
@property (nonatomic, retain) TodoList * todoList;
@property (nonatomic, retain) NSNumber * synced;
@property (nonatomic, retain) NSNumber * mark;
@property (nonatomic, retain) NSNumber * notify;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSSet *comments;
+(NSArray *)unsyncedTodoListItemsWithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(NSArray *)allTodoListItemsForTodoListID:(NSInteger)_todoListID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
-(BCTodoListItem *) BCTodoListItemFromTodoListItem;
-(void)syncTodoListItem;
@end
@interface TodoListItem (CoreDataGeneratedAccessors)
- (void)addCommentsObject:(Comment *)value;
- (void)removeCommentsObject:(Comment *)value;
- (void)addComments:(NSSet *)value;
- (void)removeComments:(NSSet *)value;
@end