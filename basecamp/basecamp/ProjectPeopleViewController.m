//
//  ProjectPeopleViewController.m
//  basecamp
//
//  Created by David Estes on 4/28/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "ProjectPeopleViewController.h"
#import <CoreData/CoreData.h>
#import "basecampAppDelegate.h"
#import "Project.h"
#import "Company.h"
#import "BAccount.h"
#import "Person.h"
#import "CompanySyncController.h"
#import "PeopleSyncController.h"
#import "CompanySyncController.h"
#import "PersonViewController.h"

@implementation ProjectPeopleViewController
@synthesize tableView,companyTitleLabel,projectTitleLabel,project,fetchedResultsController,peopleSyncController,companySyncController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"People";
        self.tabBarItem.image = [UIImage imageNamed:@"icon-people.png"];
    }
    return self;
}

- (void)dealloc
{
    [projectTitleLabel release];
    [companyTitleLabel release];
    [tableView release];
    [project release];
    [fetchedResultsController release];
    [peopleSyncController release];
    [companySyncController release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(_refreshHeaderView == nil)
    {
        EGORefreshTableHeaderView *refreshView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height) arrowImageName:@"grayArrow.png" textColor:[UIColor grayColor]];
        refreshView.delegate = self;
        _refreshHeaderView = refreshView;
        [self.tableView addSubview:refreshView];
        [refreshView release];
        
    }
    if(self.project != nil)
    {
        self.projectTitleLabel.text = self.project.name;
        self.companyTitleLabel.text = self.project.account.companyName;
        [[self fetchedResultsController] performFetch:nil];
        if((companySyncController == nil && peopleSyncController == nil) || (companySyncController != nil && companySyncController.finished == YES && peopleSyncController != nil && peopleSyncController.finished == YES))
        {
            CompanySyncController *_companySyncController = [[CompanySyncController alloc] initWithProject:self.project];
            self.companySyncController = _companySyncController;
            [_companySyncController startSyncing];
            [_companySyncController release];
            PeopleSyncController *_peopleSyncController = [[PeopleSyncController alloc] initWithProject:self.project];
            self.peopleSyncController = _peopleSyncController;
            [_peopleSyncController startSyncing];
            [_peopleSyncController release];
            
        }
        [tableView reloadData];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self registerForNotifications];

    [self registerForNotifications];
    [super viewWillAppear:animated];
}



-(void)viewWillDisappear:(BOOL)animated
{
    [self unregisterForNotifications];
    [super viewWillDisappear:animated];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [[self.fetchedResultsController sections] count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[[self.fetchedResultsController sections] objectAtIndex:section] name];
}
- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{


        static NSString *CellIdentifier = @"PersonCell";
        UITableViewCell *cell = (UITableViewCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            cell.textLabel.font = [UIFont boldSystemFontOfSize:12.0f];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        // Configure the cell...
    Person *_person = (Person *)[self.fetchedResultsController objectAtIndexPath:indexPath];

    NSString *fullName = [[NSString alloc] initWithFormat:@"%@ %@",_person.firstName,_person.lastName];
    cell.textLabel.text = fullName;
    [_person loadCachedImage];
    cell.imageView.image = _person.cachedAvatar;
    [fullName release];
        return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Person *_person = [fetchedResultsController objectAtIndexPath:indexPath];
    PersonViewController *_personViewController = [[PersonViewController alloc] initWithStyle:UITableViewStyleGrouped];
    _personViewController.person = _person;
    [self.navigationController pushViewController:_personViewController animated:YES];
    [_personViewController release];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
#pragma mark - NSNotificationCenter Methods
-(void)registerForNotifications
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(AvatarLoadedNotification:) name:AVATAR_IMAGE_UPDATED_NOTIFICATION object:nil];
    [nc addObserver:self selector:@selector(updatePersons:) name:PEOPLE_DID_SYNC_NOTIFICATION object:nil];
}
-(void)updatePersons:(NSNotification *)notification
{
    [self doneLoadingTableViewData];
}
-(void)unregisterForNotifications
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc removeObserver:self name:AVATAR_IMAGE_UPDATED_NOTIFICATION object:nil];
    [nc removeObserver:self name:PEOPLE_DID_SYNC_NOTIFICATION object:nil];
}
-(void)AvatarLoadedNotification:(NSNotification *)notification
{
    Person *_person = (Person *)[[notification userInfo] valueForKey:@"person"];
    NSIndexPath *indexPath = [fetchedResultsController indexPathForObject:_person];
    if(indexPath != nil)
    {
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}
#pragma mark -
#pragma mark CoreData methods
-(NSFetchedResultsController *)fetchedResultsController {
    if(fetchedResultsController != nil)
    {
        return fetchedResultsController;
    }
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:[appDelegate managedObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY projects == %@",project];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company.name" ascending:YES];    
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:YES];        
    NSSortDescriptor *sortDescriptor3 = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];        
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor,sortDescriptor2,sortDescriptor3,nil]];
    
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[appDelegate managedObjectContext] sectionNameKeyPath:@"company.name" cacheName:nil];
    
    self.fetchedResultsController = _fetchedResultsController;
    self.fetchedResultsController.delegate = self;
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [sortDescriptor2 release];
    [sortDescriptor3 release];
    [fetchRequest release];
    return fetchedResultsController;
}
-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [tableView beginUpdates];
}
-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [tableView endUpdates];
}
-(void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
    };
}
-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch (type) {
        case NSFetchedResultsChangeMove:
            //            NSLog(@"OBject moved from section:%d, to section: %d",[indexPath section],[newIndexPath section]);
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    };
    
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
    [super viewDidLoad];
    if(self.project != nil)
    {
        self.projectTitleLabel.text = self.project.name;
        self.companyTitleLabel.text = self.project.account.companyName;
        [[self fetchedResultsController] performFetch:nil];
        if((companySyncController == nil && peopleSyncController == nil) || (companySyncController != nil && companySyncController.finished == YES && peopleSyncController != nil && peopleSyncController.finished == YES))
        {
            CompanySyncController *_companySyncController = [[CompanySyncController alloc] initWithProject:self.project];
            self.companySyncController = _companySyncController;
            [_companySyncController startSyncing];
            [_companySyncController release];
            PeopleSyncController *_peopleSyncController = [[PeopleSyncController alloc] initWithProject:self.project];
            self.peopleSyncController = _peopleSyncController;
            [_peopleSyncController startSyncing];
            [_peopleSyncController release];
            
        }
    }
	_reloading = YES;
}

- (void)doneLoadingTableViewData{
    
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{	
    
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
	[self reloadTableViewDataSource];
    //	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
    
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
	return _reloading; // should return if data source model is reloading
    
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
    
	return [NSDate date]; // should return date data source was last changed
    
}

@end
