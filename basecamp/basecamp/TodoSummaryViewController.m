//
//  TodoListViewController.m
//  basecamp
//
//  Created by David Estes on 3/12/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "TodoSummaryViewController.h"
#import "BCAccount.h"
#import "BAccount.h"
#import "BCTodoList.h"
#import "TodoList.h"
#import "TodoListItem.h"
#import "TodoItemTableViewCell.h"
#import "TodoListSyncController.h"
#import "basecampAppDelegate.h"
#import <CoreData/CoreData.h>
@implementation TodoSummaryViewController
@synthesize account,todoListItems,fetchedResultsController;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"To-Dos";
        [self.tabBarItem setImage:[UIImage imageNamed:@"icon-todo.png"]];
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [todoListItems release];
    [fetchedResultsController release];
    [account release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"To-Dos";
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    if(self.account != nil)
    {

        self.fetchedResultsController = nil;
        [[self fetchedResultsController] performFetch:nil];
//        [BCTodoList loadTodoListsForAccount:account.basecampAccount withCompletionHandler:^(NSArray *_todoLists, NSError *error) {
//            self.todoLists = _todoLists;
//            
//            
//        }];

    }
    [self registerForNotifications];

    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [[self.fetchedResultsController sections] count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    TodoListItem *_todoListItem = (TodoListItem *)[[[[self.fetchedResultsController sections] objectAtIndex:section] objects] objectAtIndex:0];

    return _todoListItem.todoList.name;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TodoListsCell";
    
    TodoItemTableViewCell *cell = (TodoItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[TodoItemTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    // Configure the cell...
    TodoListItem *_todoListItem = (TodoListItem *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.titleLabel.text = _todoListItem.content;
    
    NSString *assignmentText = nil;
    if(_todoListItem.responsiblePartyName != nil && ![_todoListItem.responsiblePartyName isEqualToString:@""])
    {
        assignmentText = [[NSString alloc] initWithFormat:@"Assigned To: %@",_todoListItem.responsiblePartyName];
        cell.assignDueLabel.text = assignmentText;
        [cell.assignDueLabel sizeToFit];
    }
    else
    {
        cell.assignDueLabel.alpha = 0.0f;
    }
    
    [assignmentText release];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75.0f;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}
#pragma mark - NSNotificationCenter Methods
-(void)registerForNotifications
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(updateTodoLists:) name:TODOLISTITEMS_DID_SYNC_NOTIFICATION object:nil];
}
-(void)unregisterForNotifications
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc removeObserver:self name:TODOLISTITEMS_DID_SYNC_NOTIFICATION object:nil];
}
-(void)updateTodoLists:(NSNotification *)notification
{
    [[self fetchedResultsController] performFetch:nil];
    [self.tableView reloadData];
}
#pragma mark -
#pragma mark CoreData methods
-(NSFetchedResultsController *)fetchedResultsController {
    if(fetchedResultsController != nil)
    {
        return fetchedResultsController;
    }
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TodoListItem" inManagedObjectContext:[appDelegate managedObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"todoList.account == %@ && responsiblePartyType == \"Person\" && responsiblePartyID == %d && (completed == nil || completed == 0)",account,[account.meID integerValue]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"todoList.todoListID" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];    
    NSSortDescriptor *sortDescriptor3 = [[NSSortDescriptor alloc] initWithKey:@"completed" ascending:YES];    
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor,sortDescriptor2,sortDescriptor3,nil]];
    

    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[appDelegate managedObjectContext] sectionNameKeyPath:@"todoList.todoListID" cacheName:nil];
    
    self.fetchedResultsController = _fetchedResultsController;
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [sortDescriptor2 release];
    [sortDescriptor3 release];
    [fetchRequest release];
    return fetchedResultsController;
}
@end
