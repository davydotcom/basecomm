//
//  RWMilestoneSelector.m
//  basecamp
//
//  Created by David Estes on 4/11/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "RWMilestoneSelector.h"
#import "basecampAppDelegate.h"
#import <CoreData/CoreData.h>
#import "BAccount.h"
#import "Project.h"
#import "Milestone.h"
#import "MilestoneSyncController.h"
@implementation RWMilestoneSelector
@synthesize pickerView,project,account,milestone,parentView,fetchedResultsController,delegate,selectionView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [parentView release];
    [selectionView release];
    [milestone release];
    [account release];
    [project release];
    [fetchedResultsController release];
    [pickerView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)slideIn
{
    if(parentView == nil)
    {
        return;
    }
    [self viewWillAppear:YES];
    self.view.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f];
    [self.view addSubview:selectionView];
    self.selectionView.frame = CGRectMake(0.0f, self.view.frame.size.height, 320.0f, self.selectionView.frame.size.height);
    [self.parentView addSubview:self.view];
    self.view.frame = CGRectMake(0.0f, 0.0f, self.parentView.frame.size.width, self.parentView.frame.size.height);
    
    [UIView animateWithDuration:0.5f animations:^(void) {
        self.view.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f];
        self.selectionView.frame = CGRectMake(0.0f, self.view.frame.size.height - self.selectionView.frame.size.height, 320.0f, self.selectionView.frame.size.height);
    }];
    
}
-(void)slideOut
{
    [self viewWillDisappear:YES];
    [UIView animateWithDuration:0.5f animations:^(void) {
        self.view.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f];
        self.selectionView.frame = CGRectMake(0.0f, self.view.frame.size.height, 320.0f, self.selectionView.frame.size.height);
    } completion:^(BOOL finished) {
        [self.selectionView removeFromSuperview];
        [self.view removeFromSuperview];
        [self viewDidDisappear:YES];
    }];
}
-(IBAction)cancelButtonPressed:(id)sender
{
    self.milestone = nil;
    if(delegate != nil && [delegate respondsToSelector:@selector(rwMilestoneSelector:didSelectMilestone:)])
    {
        [delegate rwMilestoneSelector:self didSelectMilestone:self.milestone];
    }
    [self slideOut];    
}
-(IBAction)doneButtonPressed:(id)sender
{
    NSInteger row = [pickerView selectedRowInComponent:0];
    self.milestone = (Milestone *)[[[self fetchedResultsController] fetchedObjects] objectAtIndex:row];
    
    if(delegate != nil && [delegate respondsToSelector:@selector(rwMilestoneSelector:didSelectMilestone:)])
    {
            [delegate rwMilestoneSelector:self didSelectMilestone:self.milestone];
    }
    [self slideOut];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [[self fetchedResultsController] performFetch:nil];
    
    [self.pickerView reloadComponent:0];
    if(self.milestone == nil && [self.pickerView numberOfRowsInComponent:0] > 0)
    {
        [self.pickerView selectRow:0 inComponent:0 animated:NO];
    }
    else
    {
        NSUInteger row = [[fetchedResultsController fetchedObjects] indexOfObject:self.milestone];
        if(row != NSNotFound)
        {
            [self.pickerView selectRow:row inComponent:0 animated:NO];            
        }
                          
    }
    [super viewWillAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark -
#pragma mark CoreData methods
-(NSFetchedResultsController *)fetchedResultsController {
    if(fetchedResultsController != nil)
    {
        return fetchedResultsController;
    }
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Milestone" inManagedObjectContext:[appDelegate managedObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"completed != 1 && project == %@",self.project];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    
    
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor,nil]];
    
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[appDelegate managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
    
    self.fetchedResultsController = _fetchedResultsController;
    [_fetchedResultsController release];   
    [sortDescriptor release];
    
    [fetchRequest release];
    return fetchedResultsController;
}
#pragma mark - UIPickerViewDelegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    Milestone *_milestone = (Milestone *)[[[self fetchedResultsController] fetchedObjects] objectAtIndex:row];
    return _milestone.title;
}


#pragma mark - UIPickerView DataSource Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[[self fetchedResultsController] fetchedObjects] count];
}
@end
