//
//  ProjectTodoListViewController.m
//  basecamp
//
//  Created by David Estes on 3/19/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "ProjectTodoListViewController.h"
#import <CoreData/CoreData.h>
#import "basecampAppDelegate.h"
#import "Project.h"
#import "BAccount.h"
#import "TodoList.h"
#import "TodoListItem.h"
#import "MilestoneSyncController.h"
#import "TodoListSyncController.h"
#import "TodoListItemSyncController.h"
#import "SyncController.h"
#import "TodoListTableViewCell.h"
#import "TodoListFormViewController.h"
#import "TodoItemViewController.h"
#import "SyncController.h"

@implementation ProjectTodoListViewController
@synthesize tableView,project,fetchedResultsController,companyTitleLabel,projectTitleLabel;
@synthesize todoItemViewController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"To-Do Lists";
        UIBarButtonItem *editAccountsButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButtonPressed:)];
        self.navigationItem.rightBarButtonItem = editAccountsButton;
        [editAccountsButton release];
        [self.tabBarItem setImage:[UIImage imageNamed:@"icon-todo.png"]];

    }
    return self;
}

- (void)dealloc
{
    [project release];
    [fetchedResultsController release];
    [tableView release];
    [todoItemViewController release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)viewDidLoad
{
    TodoItemViewController *_todoItemViewController = [[TodoItemViewController alloc] initWithNibName:@"TodoItemView" bundle:nil];
    self.todoItemViewController = _todoItemViewController;
    [_todoItemViewController release];
    if(_refreshHeaderView == nil)
    {
        EGORefreshTableHeaderView *refreshView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height) arrowImageName:@"grayArrow.png" textColor:[UIColor grayColor]];
        refreshView.delegate = self;
        _refreshHeaderView = refreshView;
        [self.tableView addSubview:refreshView];
        [refreshView release];
        
    }
    
    if(self.project != nil)
    {
        self.projectTitleLabel.text = self.project.name;
        self.companyTitleLabel.text = self.project.account.companyName;
        [self fetchedResultsController];
        [[self fetchedResultsController] performFetch:nil];
        basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        if(appDelegate.syncController.finished == YES)
        {
            MilestoneSyncController *milestoneSyncController = [[MilestoneSyncController alloc] initWithProject:self.project];
            [milestoneSyncController startSyncing];
            [milestoneSyncController release];
            TodoListSyncController *todoListSyncController = [[TodoListSyncController alloc] initWithProject:self.project];
            [todoListSyncController startSyncing];
            [todoListSyncController release];
        }
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [self registerForNotifications];
    
    [super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self unregisterForNotifications];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [[self.fetchedResultsController sections] count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
    if(section == 0)
    {
        return @"Incomplete";
    }
    
    return @"Completed";
        
}
- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section] == 0)
    {
    static NSString *CellIdentifier = @"ProjectTodoListsCell";
    TodoListTableViewCell *cell = (TodoListTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[TodoListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.editingAccessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }
    
    // Configure the cell...
    TodoList *_todoList = (TodoList *)[self.fetchedResultsController objectAtIndexPath:indexPath];
        
    cell.title.text = _todoList.name;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"completed" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    NSArray *todoitems = [_todoList.todoListItems sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor,sortDescriptor2, nil]];
    [sortDescriptor release];
    [sortDescriptor2 release];
    NSInteger indexCounter = 0;
        cell.todoItem1.text = nil;
        cell.todoItem2.text = nil;
        cell.todoItem3.text = nil;
        cell.more.text = nil;
        for(indexCounter = 0;indexCounter < [todoitems count];indexCounter++)
        {
            TodoListItem *_todoListItem = (TodoListItem *)[todoitems objectAtIndex:indexCounter];
            if([_todoListItem.completed boolValue])
            {
                break;
            }
            
            if(indexCounter == 0)
            {
                cell.todoItem1.text = _todoListItem.content;
            }
            if(indexCounter == 1)
            {
                cell.todoItem2.text = _todoListItem.content;
            }
            if(indexCounter == 2)
            {
                cell.todoItem3.text = _todoListItem.content;;
            }   
            if(indexCounter >= 3)
            {
                if([_todoList.uncompletedCount integerValue] - 3 > 0)
                {
                    NSString *moreText = [[NSString alloc] initWithFormat:@"... and %d more.",[_todoList.uncompletedCount integerValue] - 3];
                    cell.more.text = moreText;
                    [moreText release];
                }
                break;
            }
        }

    

    return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"ProjectTodoListsCompletedCell";
        UITableViewCell *cell = (UITableViewCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        }
        
        // Configure the cell...
        TodoList *_todoList = (TodoList *)[self.fetchedResultsController objectAtIndexPath:indexPath];

        cell.textLabel.text = _todoList.name;
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if([indexPath section] == 1)
    {
        [cell setBackgroundColor:[UIColor colorWithRed:.8 green:.8 blue:.8 alpha:1]];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section] == 0)
    {
        return 100;
    }
    return 45;
}
/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


 // Override to support editing the table view.
 - (void)tableView:(UITableView *)_tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
     TodoList *_todoList = (TodoList *)[fetchedResultsController objectAtIndexPath:indexPath];
     NSNumber *trueNum = [[NSNumber alloc] initWithBool:YES];
     NSNumber *falseNum = [[NSNumber alloc] initWithBool:NO];
     _todoList.deleted = trueNum;
     _todoList.synced = falseNum;
     [[_todoList managedObjectContext] save:nil];
     [_todoList syncTodoList];
     [trueNum release];
     [falseNum release];
     [fetchedResultsController performFetch:nil];

     [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];

 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    TodoList *_todoList = (TodoList *)[fetchedResultsController objectAtIndexPath:indexPath];

    TodoListFormViewController *_todoListFormViewController = [[TodoListFormViewController alloc] initWithNibName:@"TodoListFormViewController" bundle:nil];
    _todoListFormViewController.project = self.project;
    _todoListFormViewController.delegate = self;
    _todoListFormViewController.todoList = _todoList;
    [self presentModalViewController:_todoListFormViewController animated:YES];
    [_todoListFormViewController release];
}
-(IBAction)editButtonPressed:(id)sender
{
    if(!self.tableView.editing)
    {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editButtonPressed:)];
        self.navigationItem.rightBarButtonItem = editButton;
        self.tabBarController.navigationItem.rightBarButtonItem = editButton;
        [editButton release];
    }
    else
    {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButtonPressed:)];
        self.navigationItem.rightBarButtonItem = editButton;
        self.tabBarController.navigationItem.rightBarButtonItem = editButton;
        [editButton release];
    }
    [self.tableView setEditing:!self.tableView.editing animated:YES];
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TodoList *todoList = (TodoList *)[fetchedResultsController objectAtIndexPath:indexPath];
    self.todoItemViewController.todoList = todoList;
    [self.navigationController pushViewController:self.todoItemViewController animated:YES];
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark - NSNotificationCenter Methods
-(void)registerForNotifications
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(updateTodoLists:) name:TODOLISTS_DID_SYNC_NOTIFICATION object:nil];
    [nc addObserver:self selector:@selector(updateTodoListItems:) name:TODOLISTITEMS_DID_SYNC_NOTIFICATION object:nil];

}
-(void)unregisterForNotifications
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc removeObserver:self name:TODOLISTS_DID_SYNC_NOTIFICATION object:nil];

    [nc removeObserver:self name:TODOLISTITEMS_DID_SYNC_NOTIFICATION object:nil];
}
-(void)backgroundLoadTodoItems:(NSArray *)todoLists
{
    for(TodoList *_todoList in todoLists)
    {
        if( [_todoList.complete integerValue] != 1 && (_todoList.todoListItemSyncController == nil || _todoList.todoListItemSyncController.finished == YES))
        {
            NSLog(@"Sending Todo Item Sync for TodoList ID: %d",[_todoList.todoListID integerValue]);
             TodoListItemSyncController *_todoListItemSyncControler = [[TodoListItemSyncController alloc] initWithTodoList:_todoList];  
            _todoList.todoListItemSyncController = _todoListItemSyncControler;
            [_todoListItemSyncControler release];
            [_todoList.todoListItemSyncController startSyncing];

        }
    }
}
-(void)updateTodoLists:(NSNotification *)notification
{
//    [[self fetchedResultsController] performFetch:nil];
    NSArray *todoLists = [fetchedResultsController fetchedObjects];
    [NSThread detachNewThreadSelector:@selector(backgroundLoadTodoItems:) toTarget:self withObject:todoLists];
//    [self.tableView reloadData];
    [self doneLoadingTableViewData];
}
-(void)updateTodoListItems:(NSNotification *)notification
{
//    [[self fetchedResultsController] performFetch:nil];
//    [self.tableView reloadData];   
}
-(IBAction)addButtonPressed:(id)sender
{
    TodoListFormViewController *_todoListFormViewController = [[TodoListFormViewController alloc] initWithNibName:@"TodoListFormViewController" bundle:nil];
    _todoListFormViewController.project = self.project;
    _todoListFormViewController.delegate = self;
    [self presentModalViewController:_todoListFormViewController animated:YES];
    [_todoListFormViewController release];
}
-(void)todoListFormViewController:(TodoListFormViewController *)_todoListFormViewController didSaveTodoList:(TodoList *)_todoList
{
    [self dismissModalViewControllerAnimated:YES];
}
-(void)didCancelTodoListFormViewController:(TodoListFormViewController *)_todoListFormViewController
{
    [self dismissModalViewControllerAnimated:YES];    
}
#pragma mark -
#pragma mark CoreData methods
-(NSFetchedResultsController *)fetchedResultsController {
    if(fetchedResultsController != nil)
    {
        return fetchedResultsController;
    }
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TodoList" inManagedObjectContext:[appDelegate managedObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"project == %@ && deleted != 1",project];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"complete" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];    

    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor,sortDescriptor2,nil]];
    
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[appDelegate managedObjectContext] sectionNameKeyPath:@"complete" cacheName:nil];
    _fetchedResultsController.delegate = self;
    self.fetchedResultsController = _fetchedResultsController;
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [sortDescriptor2 release];
    [fetchRequest release];
    return fetchedResultsController;
}
-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [tableView beginUpdates];
}
-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [tableView endUpdates];
}
-(void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
    };
}
-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch (type) {
        case NSFetchedResultsChangeMove:
            //            NSLog(@"OBject moved from section:%d, to section: %d",[indexPath section],[newIndexPath section]);
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    };
    
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
    
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(appDelegate.syncController.finished == YES)
    {
        MilestoneSyncController *milestoneSyncController = [[MilestoneSyncController alloc] initWithProject:self.project];
        [milestoneSyncController startSyncing];
        [milestoneSyncController release];
        TodoListSyncController *todoListSyncController = [[TodoListSyncController alloc] initWithProject:self.project];
        [todoListSyncController startSyncing];
        [todoListSyncController release];
    }
	_reloading = YES;
    
}

- (void)doneLoadingTableViewData{
    
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{	
    
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
	[self reloadTableViewDataSource];
    //	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
    
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
	return _reloading; // should return if data source model is reloading
    
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
    
	return [NSDate date]; // should return date data source was last changed
    
}

@end
