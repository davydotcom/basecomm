//
//  Project.m
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import "Project.h"
#import "BAccount.h"
#import "TodoList.h"
#import "Person.h"
#import "basecampAppDelegate.h"
#import "BCProject.h"
#import "BCAccount.h"
#import "Message.h"
@implementation Project
@dynamic status;
@dynamic createdOn;
@dynamic lastChangedOn;
@dynamic showWriteboards;
@dynamic showAnnouncement;
@dynamic announcement;
@dynamic synced;
@dynamic startPage;
@dynamic name;
@dynamic projectID;
@dynamic todoLists;
@dynamic account;
@dynamic milestones;
@dynamic people;
@dynamic companies;
@dynamic messages;
@synthesize finished;


+(NSArray *)unsyncedProjectsWithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"synced == 0"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"projectID" ascending:YES];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return returnArray;
}
+(NSArray *)allProjectsForAccount:(BAccount *)_account WithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"account == %@",_account];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"projectID" ascending:YES];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return returnArray;
}
+(Project *)findByProjectID:(NSInteger)_projectID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"projectID == %d",_projectID];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"projectID" ascending:YES];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *results = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    results = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    Project *_project = nil;
    if(results != nil && [results count] > 0)
    {
        _project = (Project *)[results objectAtIndex:0];
    }
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return _project;
}
#pragma mark - Submit New Objects

-(void)syncProject
{
    self.finished = NO;
    BCProject *_bcProject = [self BCProjectFromProject];
    [_bcProject saveWithCompletionHandler:^(BCProject *_project, NSError *error) {
        if(error == nil)
        {
            self.projectID = _project.projectID;
            self.createdOn = _project.createdOn;
            self.lastChangedOn = _project.lastChangedOn;
            self.name = _project.name;
            self.status = _project.status;
            self.announcement = _project.announcement;
            self.showAnnouncement = _project.showAnnouncement;
            self.showWriteboards = _project.showWriteboards;
            self.startPage = _project.startPage;
            self.synced = [NSNumber numberWithBool:YES];
            [[self managedObjectContext] save:nil];
        }
        finished = YES;
    }];
}

-(BCProject *) BCProjectFromProject
{
    BCAccount *_account = [self.account basecampAccount];
    BCProject *_bcProject = [[[BCProject alloc] initWithAccount:_account] autorelease];
    _bcProject.projectID = self.projectID;
    _bcProject.name = self.name;
    _bcProject.createdOn = self.createdOn;
    _bcProject.status = self.status;
    _bcProject.announcement = self.announcement;
    _bcProject.showWriteboards = self.showWriteboards;
    _bcProject.showAnnouncement = self.showAnnouncement;
    _bcProject.startPage = self.startPage;
    _bcProject.lastChangedOn = self.lastChangedOn;
    
    return _bcProject;
}
#pragma mark -
#pragma mark Relationship Methods
- (void)addTodoListsObject:(TodoList *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"todoLists"] addObject:value];
    [self didChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)removeTodoListsObject:(TodoList *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"todoLists"] removeObject:value];
    [self didChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addTodoLists:(NSSet *)value {    
    [self willChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"todoLists"] unionSet:value];
    [self didChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeTodoLists:(NSSet *)value {
    [self willChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"todoLists"] minusSet:value];
    [self didChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}



- (void)addMilestonesObject:(NSManagedObject *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"milestones" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"milestones"] addObject:value];
    [self didChangeValueForKey:@"milestones" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)removeMilestonesObject:(NSManagedObject *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"milestones" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"milestones"] removeObject:value];
    [self didChangeValueForKey:@"milestones" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addMilestones:(NSSet *)value {    
    [self willChangeValueForKey:@"milestones" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"milestones"] unionSet:value];
    [self didChangeValueForKey:@"milestones" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeMilestones:(NSSet *)value {
    [self willChangeValueForKey:@"milestones" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"milestones"] minusSet:value];
    [self didChangeValueForKey:@"milestones" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (void)addPeopleObject:(NSManagedObject *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"people" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"people"] addObject:value];
    [self didChangeValueForKey:@"people" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)removePeopleObject:(NSManagedObject *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"people" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"people"] removeObject:value];
    [self didChangeValueForKey:@"people" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addPeople:(NSSet *)value {    
    [self willChangeValueForKey:@"people" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"people"] unionSet:value];
    [self didChangeValueForKey:@"people" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removePeople:(NSSet *)value {
    [self willChangeValueForKey:@"people" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"people"] minusSet:value];
    [self didChangeValueForKey:@"people" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}
- (void)addCompaniesObject:(NSManagedObject *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"companies" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"companies"] addObject:value];
    [self didChangeValueForKey:@"companies" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)removeCompaniesObject:(NSManagedObject *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"companies" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"companies"] removeObject:value];
    [self didChangeValueForKey:@"companies" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addCompanies:(NSSet *)value {    
    [self willChangeValueForKey:@"companies" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"companies"] unionSet:value];
    [self didChangeValueForKey:@"companies" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeCompanies:(NSSet *)value {
    [self willChangeValueForKey:@"companies" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"companies"] minusSet:value];
    [self didChangeValueForKey:@"companies" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}
@end
