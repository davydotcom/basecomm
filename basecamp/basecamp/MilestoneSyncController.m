//
//  MilestoneSyncController.m
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "MilestoneSyncController.h"
#import "basecampAppDelegate.h"
#import <CoreData/CoreData.h>
#import "Milestone.h"
#import "BAccount.h"
#import "Project.h"
#import "BCProject.h"
#import "BCAccount.h"
#import "BCMilestone.h"
#import "SyncController.h"
@implementation MilestoneSyncController
@synthesize project,managedObjectContext,finished,parentObjectContext,sourceThread;
#pragma mark - Sync Methods
-(id)initWithProject:(Project* )_project
{
    self = [super init];
    if(self)
    {
        self.project = _project;
    }
    return self;
}
-(void)startSyncing
{
    finished = NO;
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [self performSelector:@selector(runSyncThread) onThread:appDelegate.syncController.syncThread withObject:nil waitUntilDone:NO];
}
-(void) fetchProjectWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.project];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.project = [array objectAtIndex:0];
    }
    
}
-(void)runSyncThread
{
    [self retain];
    finished = NO;
    BOOL createdOwnContext = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(self.managedObjectContext == nil)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

        self.managedObjectContext = [appDelegate.syncController managedObjectContext];
        createdOwnContext = YES;
    }

    [self fetchProjectWithNewManagedObjectContext:managedObjectContext];
    
    //First We sync all milestones that have not been synced yet
//    NSArray *milestones = [Milestone unsyncedMilestonesWithManagedObjectContext:managedObjectContext];
//    for(Milestone *_milestone in milestones)
//    {
//        [_milestone syncMilestone];
//        while(_milestone.finished == NO)
//        {
//            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
//        }
//        
//    }
    //Get Array of all milestones from CoreData
    NSArray *milestones =[Milestone allMilestonesForProjectID:[self.project.projectID integerValue] withManagedObjectContext:managedObjectContext];
    //Then We Fetch all Milestones from server to sync with
    BCProject *_bcProject = [[BCProject alloc] initWithAccount:[project.account basecampAccount]];
    _bcProject.projectID = self.project.projectID;
    [BCMilestone loadMilestonesForProject:_bcProject withCompletionHandler:^(NSArray *_bcmilestones, NSError *error) {
        if(error == nil)
        {
        
            //First Check to see if any milestones have been removed!
            for(Milestone *_dbMilestone in milestones)
            {
                BOOL found=NO;
                for(BCMilestone *_bcMilestone in _bcmilestones)
                {
                    if ([_bcMilestone.milestoneID integerValue] == [_dbMilestone.milestoneID integerValue]) {
                        found = YES;
                    }
                }
                if(!found)
                {
                    [self performSelectorOnMainThread:@selector(didRemoveMilestoneNotification:) withObject:_dbMilestone waitUntilDone:YES];
                    [managedObjectContext deleteObject:_dbMilestone];
                }
            }
            //Add New Milestones and Update Existing
            for(BCMilestone *_bcMilestone in _bcmilestones)
            {
                BOOL found = NO;
                for(Milestone *_dbMilestone in milestones)
                {
                    if ([_bcMilestone.milestoneID integerValue] == [_dbMilestone.milestoneID integerValue]) {
                        //Update record
                        _dbMilestone.title = _bcMilestone.title;
                        _dbMilestone.createdOn = _bcMilestone.createdOn;
                        _dbMilestone.responsiblePartyID = _bcMilestone.responsiblePartyID;
                        _dbMilestone.responsiblePartyName = _bcMilestone.responsiblePartyName;
                        _dbMilestone.responsiblePartyType = _bcMilestone.responsiblePartyType;
                        _dbMilestone.creatorID = _bcMilestone.creatorID;
                        _dbMilestone.creatorName = _bcMilestone.creatorName;

                        _dbMilestone.completed = _bcMilestone.completed;
                        _dbMilestone.completedOn = _bcMilestone.completedOn;
                        _dbMilestone.completerID = _bcMilestone.completerID;
                        _dbMilestone.completerName = _bcMilestone.completerName;
                        _dbMilestone.commentsCount = _bcMilestone.commentsCount;
                        _dbMilestone.deadline = _bcMilestone.deadline;
                        _dbMilestone.projectID = _bcMilestone.projectID;
                        _dbMilestone.synced = [NSNumber numberWithBool:YES];
                        found = YES;
                    }
                }
                if(!found)
                {
                    //Time to create a new milestone file
                    Milestone *_dbMilestone = [NSEntityDescription insertNewObjectForEntityForName:@"Milestone" inManagedObjectContext:managedObjectContext];

                    _dbMilestone.milestoneID = _bcMilestone.milestoneID;
                    _dbMilestone.project = self.project;
                    _dbMilestone.projectID = self.project.projectID;
                    _dbMilestone.title = _bcMilestone.title;
                    _dbMilestone.createdOn = _bcMilestone.createdOn;
                    _dbMilestone.responsiblePartyID = _bcMilestone.responsiblePartyID;
                    _dbMilestone.responsiblePartyName = _bcMilestone.responsiblePartyName;
                    _dbMilestone.responsiblePartyType = _bcMilestone.responsiblePartyType;
                    _dbMilestone.creatorID = _bcMilestone.creatorID;
                    _dbMilestone.creatorName = _bcMilestone.creatorName;
                    _dbMilestone.completed = _bcMilestone.completed;
                    _dbMilestone.completedOn = _bcMilestone.completedOn;
                    _dbMilestone.completerID = _bcMilestone.completerID;
                    _dbMilestone.completerName = _bcMilestone.completerName;
                    _dbMilestone.commentsCount = _bcMilestone.commentsCount;
                    _dbMilestone.deadline = _bcMilestone.deadline;
                    _dbMilestone.projectID = _bcMilestone.projectID;
                    _dbMilestone.synced = [NSNumber numberWithBool:YES];
//                    NSLog(@"Creating Milestone: %@, for Project: %d",_dbMilestone.title,[_dbMilestone.projectID integerValue]);
                }
            }
        }
        finished = YES;
    }];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];  
    }
//    NSLog(@"Saving Milestone Changes!");
    NSError *error=nil;
    if(![managedObjectContext save:&error])
    {
        
        NSLog(@"Error Saving to CoreData After Milestone Sync");
        NSLog(@"Error: %@",[error localizedDescription]);
    }
    [self performSelectorOnMainThread:@selector(didFinishSyncingMilestonesNotification) withObject:nil waitUntilDone:NO];
    if(createdOwnContext)
    {
        self.managedObjectContext = nil;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
    [_bcProject release];
    [autoreleasepool release];
    [self release];
}

-(void)registerForManagedObjectNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mergeChanges:) name:NSManagedObjectContextDidSaveNotification object:self.managedObjectContext];
    
}
-(void)mergeChanges:(NSNotification *)notification
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *mainContext = [appDelegate managedObjectContext];
    [mainContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:) withObject:notification waitUntilDone:YES];        
}
-(void)didRemoveMilestoneNotification:(Milestone *)_milestone
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:DID_REMOVE_MILESTONE_NOTIFICATION object:self userInfo:[NSDictionary dictionaryWithObject:_milestone forKey:@"milestone"]];
}
-(void)didFinishSyncingMilestonesNotification
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:MILESTONES_DID_SYNC_NOTIFICATION object:nil];
}
-(void)dealloc
{
    [project release];
    [parentObjectContext release];
    [sourceThread release];
    [super dealloc];
}
@end
