//
//  TodoListItemFormViewController.h
//  basecamp
//
//  Created by David Estes on 3/25/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TodoListItem;
@class TodoList;
@class TodoListItemFormViewController;
@class RWDateSelector,RWAssignee;
@protocol TodoListItemFormDelegate <NSObject>
-(void)todoListItemFormViewController:(TodoListItemFormViewController *)_todoListItemFormViewController didSaveTodoListItem:(TodoListItem *)_todoListItem;
-(void)didCancelTodoListItemFormViewController:(TodoListItemFormViewController *)_todoListItemFormViewController;
@end
@interface TodoListItemFormViewController : UIViewController {
    IBOutlet UITextView *contentTextView;
    IBOutlet UIButton *assignButton;
    IBOutlet UIButton *dueButton;
    NSDate *dueDate;
    NSString *assigneeType;
    NSNumber *assigneeID;
    NSString *assigneeName;
    BOOL notify;
    TodoList *todoList;
    TodoListItem *todoListItem;
    RWDateSelector *rwDateSelector;
    RWAssignee *rwAssigneeSelector;
    id <TodoListItemFormDelegate> delegate;
}
@property(nonatomic, assign) BOOL notify;
@property(nonatomic, retain) RWAssignee *rwAssigneeSelector;
@property(nonatomic, retain) NSString *assigneeType;
@property(nonatomic, retain) NSNumber *assigneeID;
@property(nonatomic, retain) NSString *assigneeName;
@property(nonatomic, retain) RWDateSelector *rwDateSelector;
@property(nonatomic, retain) NSDate *dueDate;
@property(nonatomic, assign) id delegate;
@property(nonatomic, retain) UITextView *contentTextView;
@property(nonatomic, retain) UIButton *assignButton;
@property(nonatomic, retain) UIButton *dueButton;
@property(nonatomic, retain) TodoList *todoList;
@property(nonatomic, retain) TodoListItem *todoListItem;
-(IBAction)cancelButtonPressed:(id)sender;
-(IBAction)saveButtonPressed:(id)sender;
-(IBAction)dueButtonPressed:(id)sender;
@end
