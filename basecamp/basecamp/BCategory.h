//
//  Category.h
//  basecamp
//
//  Created by David Estes on 6/11/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#define DID_REMOVE_CATEGORY_NOTIFICATION @"DID_REMOVE_CATEGORY_NOTIFICATION"
#define CATEGORIES_DID_SYNC_NOTIFICATION @"CATEGORIES_DID_SYNC_NOTIFICATION"
@class Message, Project,BCategory;

@interface BCategory : NSManagedObject {
@private
}
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * categoryID;
@property (nonatomic, retain) Project *project;
@property (nonatomic, retain) NSSet *messages;
@property (nonatomic, retain) NSNumber * synced;
@property (nonatomic, retain) NSNumber * deleted;
+(NSArray *)allCategoriesForProject:(Project *)_project WithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(BCategory *)findCategoryByID:(NSInteger)_categoryID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;

@end

@interface BCategory (CoreDataGeneratedAccessors)
- (void)addMessagesObject:(Message *)value;
- (void)removeMessagesObject:(Message *)value;
- (void)addMessages:(NSSet *)value;
- (void)removeMessages:(NSSet *)value;

@end
