//
//  TodoListItemItemSyncContrlller.m
//  basecamp
//
//  Created by David Estes on 3/19/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "TodoListItemSyncController.h"
#import "basecampAppDelegate.h"
#import <CoreData/CoreData.h>
#import "TodoList.h"
#import "BAccount.h"
#import "Project.h"
#import "BCProject.h"
#import "BCAccount.h"
#import "BCTodoList.h"
#import "BCTodoListItem.h"
#import "TodoListItem.h"
#import "SyncController.h"
@implementation TodoListItemSyncController
@synthesize project,managedObjectContext,finished,todoList;

#pragma mark - Sync Methods
-(id)initWithProject:(Project* )_project
{
    self = [super init];
    if(self)
    {
        self.project = _project;
    }
    return self;
}
-(id)initWithTodoList:(TodoList* )_todoList
{
    self = [super init];
    if(self)
    {
        self.todoList = _todoList;
    }
    return self;
}
-(void)startSyncing
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [self performSelector:@selector(sync) onThread:appDelegate.syncController.syncThread withObject:nil waitUntilDone:NO];
}
-(void) fetchTodoListWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"TodoList" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.todoList];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.todoList = [array objectAtIndex:0];
    }
    
}
-(void)sync
{
    [self retain];
    finished = NO;
    BOOL createdOwnContext = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(self.managedObjectContext == nil)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        self.managedObjectContext = [appDelegate.syncController managedObjectContext];
        createdOwnContext = YES;
    }
    [self fetchTodoListWithNewManagedObjectContext:managedObjectContext];
    
    //First We sync all todoListItems that have not been synced yet
//    NSArray *todoListItems = [TodoListItem unsyncedTodoListItemsWithManagedObjectContext:managedObjectContext];
//    
//    for(TodoListItem *_todoListItem in todoListItems)
//    {
//        [_todoListItem syncTodoListItem];
//        while(_todoListItem.finished == NO)
//        {
//            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
//        }
//        
//    }
    //Get Array of all todoListItems from CoreData
    NSArray *todoListItems =[TodoListItem allTodoListItemsForTodoListID:[self.todoList.todoListID integerValue] withManagedObjectContext:managedObjectContext];
    //Then We Fetch all TodoListItems from server to sync with

    BCTodoList *_bcTodoList = [[BCTodoList alloc] initWithAccount:[todoList.account basecampAccount]];
    _bcTodoList.todoListID = todoList.todoListID;
    _bcTodoList.projectID = todoList.project.projectID;
    NSLog(@"Requesting Todo List Items with request URL: %@",[_bcTodoList.account requestURL]);
    [BCTodoListItem loadTodoListItemsForTodoList:_bcTodoList withCompletionHandler:^(NSArray *_bctodoListItems, NSError *error) {
        if(error == nil)
        {
//            NSLog(@"Found todoList Items: %d",[_bctodoListItems count]);
            //First Check to see if any todoListItems have been removed!
            for(TodoListItem *_dbTodoListItem in todoListItems)
            {
                BOOL found=NO;
                for(BCTodoListItem *_bcTodoListItem in _bctodoListItems)
                {
                    if ([_bcTodoListItem.todoListItemID integerValue] == [_dbTodoListItem.todoListItemID integerValue]) {
                        found = YES;
                    }
                }
                if(!found)
                {
                    [self performSelectorOnMainThread:@selector(didRemoveTodoListItemNotification:) withObject:_dbTodoListItem waitUntilDone:YES];
                    [managedObjectContext deleteObject:_dbTodoListItem];
                }
            }
            //Add New TodoListItems and Update Existing
            for(BCTodoListItem *_bcTodoListItem in _bctodoListItems)
            {
                BOOL found = NO;
                for(TodoListItem *_dbTodoListItem in todoListItems)
                {
                    if ([_bcTodoListItem.todoListItemID integerValue] == [_dbTodoListItem.todoListItemID integerValue]) {
                        //Update record
                        
                        _dbTodoListItem.content = _bcTodoListItem.content;
                        _dbTodoListItem.createdAt = _bcTodoListItem.createdAt;
                        _dbTodoListItem.creatorID = _bcTodoListItem.creatorID;
                        _dbTodoListItem.creatorName = _bcTodoListItem.creatorName;
                        _dbTodoListItem.completed = _bcTodoListItem.completed;
                        _dbTodoListItem.commentsCount = _bcTodoListItem.commentsCount;
                        _dbTodoListItem.dueAt = _bcTodoListItem.dueAt;
                        _dbTodoListItem.position = _bcTodoListItem.position;
                        _dbTodoListItem.responsiblePartyID = _bcTodoListItem.responsiblePartyID;
                        
                        _dbTodoListItem.responsiblePartyName = _bcTodoListItem.responsiblePartyName;
                        _dbTodoListItem.responsiblePartyType = _bcTodoListItem.responsiblePartyType;
                        _dbTodoListItem.synced = [NSNumber numberWithBool:YES];
                        found = YES;
                    }
                }
                if(!found)
                {
                    //Time to create a new todoListItem file
                    TodoListItem *_dbTodoListItem = [NSEntityDescription insertNewObjectForEntityForName:@"TodoListItem" inManagedObjectContext:managedObjectContext];
                    _dbTodoListItem.todoListItemID = _bcTodoListItem.todoListItemID;
                    if(self.todoList != nil)
                    {
                        _dbTodoListItem.todoList = self.todoList;
                     
                    }
                    else
                    {
                        _dbTodoListItem.todoList = [TodoList findByTodoListID:[_bcTodoListItem.todoListID integerValue] withManagedObjectContext:managedObjectContext];
                    }
                    _dbTodoListItem.content = _bcTodoListItem.content;
                    _dbTodoListItem.createdAt = _bcTodoListItem.createdAt;
                    _dbTodoListItem.creatorID = _bcTodoListItem.creatorID;
                    _dbTodoListItem.creatorName = _bcTodoListItem.creatorName;
                    _dbTodoListItem.completed = _bcTodoListItem.completed;
                    _dbTodoListItem.commentsCount = _bcTodoListItem.commentsCount;
                    _dbTodoListItem.dueAt = _bcTodoListItem.dueAt;
                    _dbTodoListItem.position = _bcTodoListItem.position;
                    _dbTodoListItem.responsiblePartyID = _bcTodoListItem.responsiblePartyID;
                    _dbTodoListItem.responsiblePartyName = _bcTodoListItem.responsiblePartyName;
                    _dbTodoListItem.responsiblePartyType = _bcTodoListItem.responsiblePartyType;
                    _dbTodoListItem.synced = [NSNumber numberWithBool:YES];
                }
            }
        }
        else
        {
            NSLog(@"Error Getting Items: %d",[error code]);
        }
        finished = YES;
    }];
    
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];  
    }
    //    NSLog(@"Saving TodoListItem Changes!");
    NSError *error=nil;
    if(![managedObjectContext save:&error])
    {
        NSLog(@"Error Saving to CoreData After TodoListItem Sync");
    }
    [self performSelectorOnMainThread:@selector(didFinishSyncingTodoListItemsNotification) withObject:nil waitUntilDone:NO];
    if(createdOwnContext)
    {
        self.managedObjectContext = nil;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

    }
    [_bcTodoList release];
    [autoreleasepool release];
    [self release];
}

-(void)registerForManagedObjectNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mergeChanges:) name:NSManagedObjectContextDidSaveNotification object:self.managedObjectContext];
    
}
-(void)mergeChanges:(NSNotification *)notification
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *mainContext = [appDelegate managedObjectContext];
    [mainContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:) withObject:notification waitUntilDone:YES];
}
-(void)didRemoveTodoListItemNotification:(TodoListItem *)_todoListItem
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:DID_REMOVE_TODOLISTITEM_NOTIFICATION object:self userInfo:[NSDictionary dictionaryWithObject:_todoListItem forKey:@"todoListItem"]];
}
-(void)didFinishSyncingTodoListItemsNotification
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:TODOLISTITEMS_DID_SYNC_NOTIFICATION object:nil];
}
-(void)dealloc
{
    [project release];
    [super dealloc];
}

@end
