//
//  TodoItemShowViewController.m
//  basecamp
//
//  Created by David Estes on 6/11/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "TodoItemShowViewController.h"
#import "TodoListItem.h"
#import "TodoItemTableViewCell.h"
#import "TodoListItemFormViewController.h"
@implementation TodoItemShowViewController
@synthesize todoListItem,comments;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"To-Do Item";
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    [todoListItem release];
    [comments release];
    [super dealloc];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    if(section == 0)
    {
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section] == 0)
    {
            static NSString *CellIdentifier = @"ProjectTodoListItemsCell";
            TodoItemTableViewCell *cell = (TodoItemTableViewCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[[TodoItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//                [cell.checkButton addTarget:self action:@selector(checkButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
            }
            
            // Configure the cell...
            cell.titleLabel.numberOfLines = 0;
            cell.titleLabel.lineBreakMode = UILineBreakModeWordWrap;
            
            cell.titleLabel.text = todoListItem.content;
            CGSize fitSize = [cell.titleLabel sizeThatFits:CGSizeMake(220.0f, 999.0f)];
            cell.titleLabel.frame = CGRectMake(cell.titleLabel.frame.origin.x, cell.titleLabel.frame.origin.y, 220.0f, fitSize.height);
            cell.checkButton.tag = (NSInteger)todoListItem;
            NSString *assignmentText = nil;
            if(todoListItem.responsiblePartyName != nil && ![todoListItem.responsiblePartyName isEqualToString:@""])
            {
                
                assignmentText = [[NSString alloc] initWithFormat:@"Assigned To: %@",todoListItem.responsiblePartyName];
                cell.assignDueLabel.text = assignmentText;
            }
            else
            {
                cell.assignDueLabel.text = @"";
            }
            cell.assignDueLabel.frame = CGRectMake(cell.assignDueLabel.frame.origin.x, cell.titleLabel.frame.origin.y + cell.titleLabel.frame.size.height + 5.0f, cell.assignDueLabel.frame.size.width, cell.assignDueLabel.frame.size.height);
            if(todoListItem.dueAt != nil)
            {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"MM/dd"];
                cell.dueLabel.text = [dateFormatter stringFromDate:todoListItem.dueAt];
                //            cell.assignDueLabel.text = ;
                [dateFormatter release];
            }
            else
            {
                cell.dueLabel.text = @"";
            }
            
            [assignmentText release];
            return cell;
    
    }
    else
    {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        
        // Configure the cell...
        
        return cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if([indexPath section] == 0)
    {   
        CGFloat height = [todoListItem.content sizeWithFont:[UIFont boldSystemFontOfSize:13.0f] constrainedToSize:CGSizeMake(220.0f, 999.0f) lineBreakMode:UILineBreakModeWordWrap].height;
        height +=40.0f;
        if(height < 80.0f)
        {
            return 80.0f;
        }
        return height;
        
    }
    return 45;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section] == 0)
    {
        TodoListItemFormViewController *todoListItemFormViewController = [[TodoListItemFormViewController alloc] initWithNibName:@"TodoItemFormView" bundle:nil];
        todoListItemFormViewController.todoListItem = self.todoListItem;
        todoListItemFormViewController.delegate = self;
        [self presentModalViewController:todoListItemFormViewController animated:YES];
        [todoListItemFormViewController release];
    }
}
#pragma mark - TodoListItemFormDelegate Methods
-(void)todoListItemFormViewController:(TodoListItemFormViewController *)_todoListItemFormViewController didSaveTodoListItem:(TodoListItem *)_todoListItem
{
    [self dismissModalViewControllerAnimated:YES];
}
-(void)didCancelTodoListItemFormViewController:(TodoListItemFormViewController *)_todoListItemFormViewController
{
    [self dismissModalViewControllerAnimated:YES];    
}
@end
