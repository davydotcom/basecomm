//
//  Comment.m
//  basecamp
//
//  Created by David Estes on 6/11/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import "Comment.h"
#import "Message.h"
#import "Milestone.h"
#import "Person.h"
#import "TodoListItem.h"


@implementation Comment
@dynamic body;
@dynamic createdAt;
@dynamic authorName;
@dynamic emailedFrom;
@dynamic commentID;
@dynamic person;
@dynamic todoListItem;
@dynamic milestone;
@dynamic message;
@dynamic deleted;
@dynamic synced;
@end
