//
//  TodoItemShowViewController.h
//  basecamp
//
//  Created by David Estes on 6/11/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWTableViewController.h"
@class TodoListItem;
@interface TodoItemShowViewController : RWTableViewController<UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate> {
    TodoListItem *todoListItem;
    NSMutableArray *comments;
}

@property(nonatomic, retain) TodoListItem *todoListItem;
@property(nonatomic, retain) NSMutableArray *comments;
@end
