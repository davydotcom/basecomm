//
//  MessageSyncController.m
//  basecamp
//
//  Created by David Estes on 6/11/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "MessageSyncController.h"
#import <CoreData/CoreData.h>
#import "basecampAppDelegate.h"
#import "SyncController.h"
#import "BCProject.h"
#import "Message.h"
#import "BCMessage.h"
#import "BCAccount.h"
#import "BAccount.h"
#import "Milestone.h"
#import "Project.h"
#import "BCategory.h"
#import "Person.h"

@implementation MessageSyncController
@synthesize account,project;
@synthesize managedObjectContext,parentObjectContext,sourceThread;
@synthesize finished;
#pragma mark - Sync Methods

-(id)initWithProject:(Project* )_project
{
    self = [super init];
    if(self)
    {
        self.project = _project;
    }
    return self;
}
-(void)startSyncing
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [self performSelector:@selector(runSyncThread) onThread:appDelegate.syncController.syncThread withObject:nil waitUntilDone:NO];
}
-(void) fetchAccountWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"BAccount" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.account];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.account = [array objectAtIndex:0];
    }
    
}
-(void) fetchProjectWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.project];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.project = [array objectAtIndex:0];
    }
    
}
-(void)runSyncThread
{
    [self retain];
    
    finished = NO;
    BOOL createdOwnContext = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(self.managedObjectContext == nil)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        self.managedObjectContext = [appDelegate.syncController managedObjectContext];
        createdOwnContext = YES;
    }
    if(self.account != nil)
    {
        [self fetchAccountWithNewManagedObjectContext:managedObjectContext];
    }
    if(self.project != nil)
    {
        [self fetchProjectWithNewManagedObjectContext:managedObjectContext];
    }
    
    
    //Get Array of all companies from CoreData
    
    NSArray *messages = [Message allMessagesForProject:self.project WithManagedObjectContext:managedObjectContext];
    
    //Then We Fetch all Projects from server to sync with
    if(self.project != nil)
    {
        BCProject *_bcProject = [[BCProject alloc] initWithAccount:[project.account basecampAccount]];
        _bcProject.projectID = self.project.projectID;
        
        [BCMessage loadMessagesForProject:_bcProject withCompletionHandler:^(NSArray *_bcmessages, NSError *error) {
            if(error == nil)
            {
                //First Check to see if any messages have been removed!
                for(Message *_dbMessage in messages)
                {
                    BOOL found=NO;
                    for(Message *_bcMessage in _bcmessages)
                    {
                        if ([_bcMessage.messageID integerValue] == [_dbMessage.messageID integerValue]) {
                            found = YES;
                        }
                    }
                    if(!found)
                    {
                        [managedObjectContext deleteObject:_dbMessage];
                    }
                }
                
                //Add New Projects and Update Existing
                for(BCMessage *_bcMessage in _bcmessages)
                {
                    BOOL found = NO;
                    for(Message *_dbMessage in messages)
                    {
                        if ([_bcMessage.messageID integerValue] == [_dbMessage.messageID integerValue]) {
                            //Update record
                            _dbMessage.title = _bcMessage.body;
                            _dbMessage.body = _bcMessage.body;
                            _dbMessage.postedOn = _bcMessage.postedOn;
                            _dbMessage.commentedAt = _bcMessage.commentedAt;
                            _dbMessage.authorName = _bcMessage.authorName;
                            _dbMessage.project = self.project;
                            _dbMessage.isPrivate = _bcMessage.isPrivate;
                            _dbMessage.useTextile = _bcMessage.useTextile;
                            _dbMessage.author = [Person findPersonByID:[_bcMessage.authorID integerValue] withManagedObjectContext:managedObjectContext];
                            if([_bcMessage.milestoneID integerValue] > 0)
                            {
                                _dbMessage.milestone = [Milestone findByMilestoneID:[_bcMessage.milestoneID integerValue] withManagedObjectContext:managedObjectContext];
                            }
                            if([_bcMessage.categoryID integerValue] > 0)
                            {
                                _dbMessage.category = [BCategory findCategoryByID:[_bcMessage.categoryID integerValue] withManagedObjectContext:managedObjectContext];
                            }
                            _dbMessage.synced = [NSNumber numberWithBool:YES];
                            found = YES;
                        }
                    }
                    if(!found)
                    {
                        Message *_dbMessage = (Message *)[NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:managedObjectContext];
                        _dbMessage.messageID = _bcMessage.messageID;
                        _dbMessage.title = _bcMessage.body;
                        _dbMessage.body = _bcMessage.body;
                        _dbMessage.postedOn = _bcMessage.postedOn;
                        _dbMessage.commentedAt = _bcMessage.commentedAt;
                        _dbMessage.authorName = _bcMessage.authorName;
                        _dbMessage.project = self.project;
                        _dbMessage.isPrivate = _bcMessage.isPrivate;
                        _dbMessage.useTextile = _bcMessage.useTextile;
                        _dbMessage.author = [Person findPersonByID:[_bcMessage.authorID integerValue] withManagedObjectContext:managedObjectContext];
                        if([_bcMessage.milestoneID integerValue] > 0)
                        {
                            _dbMessage.milestone = [Milestone findByMilestoneID:[_bcMessage.milestoneID integerValue] withManagedObjectContext:managedObjectContext];
                        }
                        if([_bcMessage.categoryID integerValue] > 0)
                        {
                            _dbMessage.category = [BCategory findCategoryByID:[_bcMessage.categoryID integerValue] withManagedObjectContext:managedObjectContext];
                        }
                        _dbMessage.synced = [NSNumber numberWithBool:YES];
                    }
                }
            }
            finished = YES;
        }];
    }
    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];  
    }
    // NSLog(@"Saving Changes!");
    NSError *error=nil;
    if(![managedObjectContext save:&error])
    {
        NSLog(@"Error Saving to CoreData After Category Sync");
    }
    [self performSelectorOnMainThread:@selector(didFinishSyncingCategoriesNotification) withObject:nil waitUntilDone:NO];
    if(createdOwnContext)
    {
        self.managedObjectContext = nil;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];   
    }
    [autoreleasepool release];
    [self release];
}

-(void)registerForManagedObjectNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mergeChanges:) name:NSManagedObjectContextDidSaveNotification object:self.managedObjectContext];
    
}
-(void)mergeChanges:(NSNotification *)notification
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *mainContext = [appDelegate managedObjectContext];
    [mainContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:) withObject:notification waitUntilDone:YES];        
}
-(void)didRemoveCategoryNotification:(Message *)_message
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:DID_REMOVE_MESSAGE_NOTIFICATION object:self userInfo:[NSDictionary dictionaryWithObject:_message forKey:@"message"]];
}
-(void)didFinishSyncingCategoriesNotification
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:MESSAGES_DID_SYNC_NOTIFICATION object:nil];
}

-(void)dealloc
{
    [account release];
    [project release];
    [sourceThread release];
    [parentObjectContext release];
    [managedObjectContext release];
    [super dealloc];
}

@end
