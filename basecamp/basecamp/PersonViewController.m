//
//  PersonViewController.m
//  basecamp
//
//  Created by David Estes on 4/28/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "PersonViewController.h"
#import "Person.h"
#import "Company.h"
#import <QuartzCore/QuartzCore.h>
@implementation PersonViewController
@synthesize person;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title = @"Info";
    }
    return self;
}

- (void)dealloc
{
    [person release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    if(person.emailAddress != nil)
    {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    NSInteger count = 0;
    if (section == 0) {
        if(person.phoneNumberHome != nil && ![person.phoneNumberHome isEqualToString:@""])
        {
            count++;
        }
        
        if(person.phoneNumberMobile != nil && ![person.phoneNumberMobile isEqualToString:@""])
        {
            count++;
        }     
        if(person.phoneNumberOffice != nil && ![person.phoneNumberOffice isEqualToString:@""])
        {
            count++;
        }     
        if(person.phoneNumberFax != nil && ![person.phoneNumberFax isEqualToString:@""])
        {
            count++;
        }     
    }

    if(section == 1)
    {
        if(person.emailAddress != nil )
        {
            count++;
        }
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:CellIdentifier] autorelease];
    }
    NSUInteger section = [indexPath section];
    if(section == 0)
    {
        NSInteger count = 0;
        if(person.phoneNumberHome != nil && ![person.phoneNumberHome isEqualToString:@""])
        {
            count++;
            if(count == [indexPath row] + 1)
            {
                cell.textLabel.text = @"Home";
                cell.detailTextLabel.text = person.phoneNumberHome;
            }
        }
        if(person.phoneNumberMobile != nil && ![person.phoneNumberMobile isEqualToString:@""])
        {
            count++;
            if(count == [indexPath row] + 1)
            {
                cell.textLabel.text = @"Mobile";
                cell.detailTextLabel.text = person.phoneNumberMobile;
            }
        }
        if(person.phoneNumberOffice != nil && ![person.phoneNumberOffice isEqualToString:@""])
        {
            count++;
            if(count == [indexPath row] + 1)
            {
                cell.textLabel.text = @"Office";
                if(person.phoneNumberOfficeExt != nil && ![person.phoneNumberOfficeExt isEqualToString:@""])
                {
                    NSString *officePhone = [[NSString alloc] initWithFormat:@"%@ ext. %@",person.phoneNumberOffice,person.phoneNumberOfficeExt];
                    cell.detailTextLabel.text = officePhone;
                    [officePhone release];
                }
                else
                {
                    cell.detailTextLabel.text = person.phoneNumberOffice;
                }
            }
        }
        if(person.phoneNumberFax != nil && ![person.phoneNumberFax isEqualToString:@""])
        {
            count++;
            if(count == [indexPath row] + 1)
            {
                cell.textLabel.text = @"Fax";
                cell.detailTextLabel.text = person.phoneNumberFax;
            }
        }

    }
    else if(section == 1)
    {
        cell.textLabel.text = @"Email";
        cell.detailTextLabel.text = person.emailAddress;
    }
    // Configure the cell...
    
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        UIView *_headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320.0, 150.0)] autorelease];
        
        UIImageView *avatarImage = [[UIImageView alloc] initWithFrame:CGRectMake(10.0f, 10.0f, 55.0f, 55.0f)];
        avatarImage.backgroundColor = [UIColor whiteColor];
        [[avatarImage layer] setBorderWidth:1.0f];
        avatarImage.layer.borderColor = [[UIColor blackColor] CGColor];
        avatarImage.layer.masksToBounds = YES;
        avatarImage.layer.cornerRadius = 5.0f;
        
        if(person != nil)
        {
            avatarImage.image = person.cachedAvatar;
        }
        [_headerView addSubview:avatarImage];
        [avatarImage release];
        
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(75.0f, 20.0f, 215.0f, 25.0f)];
        nameLabel.textColor = [UIColor blackColor];
        nameLabel.shadowColor = [UIColor whiteColor];
        nameLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        nameLabel.opaque = NO;
        nameLabel.backgroundColor = [UIColor clearColor];
        NSString *name = [[NSString alloc] initWithFormat:@"%@ %@",person.firstName,person.lastName];
        
        nameLabel.text = name;
        [name release];
        [_headerView addSubview:nameLabel];
        [nameLabel release];
        
        
        UILabel *companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(75.0f, 45.0f, 215.0f, 25.0f)];
        companyLabel.textColor = [UIColor blackColor];
        companyLabel.shadowColor = [UIColor whiteColor];
        companyLabel.font = [UIFont systemFontOfSize:12.0f];
        companyLabel.opaque = NO;
        companyLabel.backgroundColor = [UIColor clearColor];
        if(person.company != nil)
        {
        companyLabel.text = person.company.name;
        }
        [_headerView addSubview:companyLabel];
        [companyLabel release];
        
        return _headerView;
    }
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 75.0f;
    }
    return 10.0f;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section] == 1)
    {
        //Compose Email
        if([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mfMailComposeViewController = [[MFMailComposeViewController alloc] init];
            mfMailComposeViewController.mailComposeDelegate = self;
            [mfMailComposeViewController setToRecipients:[NSArray arrayWithObject:person.emailAddress]];
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                [mfMailComposeViewController setModalPresentationStyle:2];
            }
            [self presentModalViewController:mfMailComposeViewController animated:YES];
        }
    }
    else if([indexPath section] == 0)
    {
        NSInteger count = 0;
        if(person.phoneNumberHome != nil && ![person.phoneNumberHome isEqualToString:@""])
        {
            count++;
            if(count == [indexPath row] + 1)
            {
                NSString *officeLink = [[NSString alloc] initWithFormat:@"tel:%@",person.phoneNumberHome];
                
                NSURL *url = [NSURL URLWithString:officeLink];
                [officeLink release];
                if ([[UIApplication sharedApplication] canOpenURL:url])
                {
                    [[UIApplication sharedApplication] openURL:url];
                }
            }
        }
        if(person.phoneNumberMobile != nil && ![person.phoneNumberMobile isEqualToString:@""])
        {
            count++;
            if(count == [indexPath row] + 1)
            {
                NSString *officeLink = [[NSString alloc] initWithFormat:@"tel:%@",person.phoneNumberMobile];
                
                NSURL *url = [NSURL URLWithString:officeLink];
                [officeLink release];
                if ([[UIApplication sharedApplication] canOpenURL:url])
                {
                    [[UIApplication sharedApplication] openURL:url];
                }
            }
        }
        if(person.phoneNumberOffice != nil && ![person.phoneNumberOffice isEqualToString:@""])
        {
            count++;
            if(count == [indexPath row] + 1)
            {
                NSString *officeLink = [[NSString alloc] initWithFormat:@"tel:%@",person.phoneNumberOffice];
                
                NSURL *url = [NSURL URLWithString:officeLink];
                [officeLink release];
                if ([[UIApplication sharedApplication] canOpenURL:url])
                {
                    [[UIApplication sharedApplication] openURL:url];
                }
            }
        }
        if(person.phoneNumberFax != nil && ![person.phoneNumberFax isEqualToString:@""])
        {
            count++;
            if(count == [indexPath row] + 1)
            {
//                Fax Service?
            }
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

@end
