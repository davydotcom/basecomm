//
//  BAccount.m
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import "BAccount.h"
#import "Project.h"
#import "TodoList.h"
#import "Person.h"
#import "BCAccount.h"
#import "BCCompany.h"
#import "BCProject.h"
#import "BCPerson.h"
#import "basecampAppDelegate.h"

@implementation BAccount
@dynamic avatarURL;
@dynamic password;
@dynamic accountID;
@dynamic username;
@dynamic meID;
@dynamic companyName;
@dynamic url;
@dynamic todoLists;
@dynamic projects;
@dynamic accountHolderID;
@dynamic people;
@dynamic companies;
@synthesize syncing;
@synthesize error;
@synthesize basecampAccount;
@synthesize basecampCompany;

+(NSArray *)allAccountsWithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"BAccount" inManagedObjectContext:_managedObjectContext];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"accountID" ascending:YES];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return returnArray;
}
-(void)updateAccountDataFromServer
{
    BCAccount *_account = [[BCAccount alloc] initWithAccountURL:self.url username:self.username password:self.password];
    self.syncing = YES;
    [_account loadAccountDetailsWithCompletionHandler:^(BCAccount *__account,NSError *_error) {
        if(error == nil)
        {
            self.accountID = __account.accountID;
            self.accountHolderID = __account.accountHolderID;
            [BCCompany loadCompany:[__account.primaryCompanyID integerValue] forAccount:__account withCompletionHandler:^(BCCompany *company,NSError *__error){
                if(__error == nil)
                {
                    NSLog(@"Company Found: %@",company.name);
                    self.companyName = company.name;
                    NSLog(@"Loading Person!");
                    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                    [[appDelegate managedObjectContext] save:nil];
                    [BCPerson loadCurrentPersonforAccount:company.account withCompletionHandler:^(BCPerson *_bcPerson, NSError *___error) {
                        NSLog(@"Person responding!");
                        if(___error == nil)
                        {
                            self.meID = _bcPerson.personID;
                            NSLog(@"Saving!");
                            [[self managedObjectContext] save:nil];
                            NSLog(@"Person Saved!");
                        }
                        else
                        {
                            self.error = ___error; 
                        }
                        self.syncing = NO;
                    }];
                }
                else
                {
                    self.error = __error;
                    self.syncing = NO;
                }

            }];
            
        }
        else
        {
            self.error = _error;
            self.syncing = NO;
        }
        [__account release];
    }];
}


-(BCAccount *)basecampAccount
{
    
    if(basecampAccount != nil && [basecampAccount.accountURL isEqualToString:self.url] && [basecampAccount.username isEqualToString:self.username] && [basecampAccount.password isEqualToString:self.password])
    {
        return basecampAccount;
    }
    
    BCAccount *_account = [[BCAccount alloc] initWithAccountURL:self.url username:self.username password:self.password];
    self.basecampAccount = _account;
    [_account release];

//    [basecampAccount loadAccountDetailsWithCompletionHandler:nil];
    return basecampAccount;
}
- (void)addTodoListsObject:(TodoList *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"todoLists"] addObject:value];
    [self didChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)removeTodoListsObject:(TodoList *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"todoLists"] removeObject:value];
    [self didChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addTodoLists:(NSSet *)value {    
    [self willChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"todoLists"] unionSet:value];
    [self didChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeTodoLists:(NSSet *)value {
    [self willChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"todoLists"] minusSet:value];
    [self didChangeValueForKey:@"todoLists" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (void)addProjectsObject:(Project *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"projects" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"projects"] addObject:value];
    [self didChangeValueForKey:@"projects" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)removeProjectsObject:(Project *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"projects" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"projects"] removeObject:value];
    [self didChangeValueForKey:@"projects" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addProjects:(NSSet *)value {    
    [self willChangeValueForKey:@"projects" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"projects"] unionSet:value];
    [self didChangeValueForKey:@"projects" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeProjects:(NSSet *)value {
    [self willChangeValueForKey:@"projects" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"projects"] minusSet:value];
    [self didChangeValueForKey:@"projects" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}
- (void)addPeopleObject:(NSManagedObject *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"people" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"people"] addObject:value];
    [self didChangeValueForKey:@"people" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)removePeopleObject:(NSManagedObject *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"people" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"people"] removeObject:value];
    [self didChangeValueForKey:@"people" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addPeople:(NSSet *)value {    
    [self willChangeValueForKey:@"people" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"people"] unionSet:value];
    [self didChangeValueForKey:@"people" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removePeople:(NSSet *)value {
    [self willChangeValueForKey:@"people" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"people"] minusSet:value];
    [self didChangeValueForKey:@"people" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (void)addCompaniesObject:(NSManagedObject *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"companies" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"companies"] addObject:value];
    [self didChangeValueForKey:@"companies" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)removeCompaniesObject:(NSManagedObject *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"companies" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"companies"] removeObject:value];
    [self didChangeValueForKey:@"companies" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addCompanies:(NSSet *)value {    
    [self willChangeValueForKey:@"companies" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"companies"] unionSet:value];
    [self didChangeValueForKey:@"companies" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeCompanies:(NSSet *)value {
    [self willChangeValueForKey:@"companies" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"companies"] minusSet:value];
    [self didChangeValueForKey:@"companies" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}
-(void)dealloc
{
    [super dealloc];
    [basecampAccount release];
    [basecampCompany release];
}
@end
