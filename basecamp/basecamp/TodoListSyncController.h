//
//  TodoListSyncController.h
//  basecamp
//
//  Created by David Estes on 3/18/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Project;
@class NSManagedObjectContext;
@class TodoList,BAccount;
@interface TodoListSyncController : NSObject {
    Project *project;
    BAccount *account;
    NSManagedObjectContext *managedObjectContext;
    NSManagedObjectContext *parentObjectContext;
    NSThread *sourceThread;
    BOOL finished;
}
@property(nonatomic,retain) Project *project;
@property(nonatomic,retain) BAccount *account;
@property(nonatomic,retain) NSManagedObjectContext *managedObjectContext;
@property(nonatomic,retain) NSManagedObjectContext *parentObjectContext;
@property(nonatomic,retain) NSThread *sourceThread;
@property(nonatomic,assign) BOOL finished;
-(id)initWithProject:(Project* )_project;
-(id)initWithAccount:(BAccount* )_account;
-(void)didRemoveTodoListNotification:(TodoList *)_todoList;
-(void)didFinishSyncingTodoListsNotification;
-(void)runSyncThread;
-(void)startSyncing;
-(void) fetchProjectWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext;
-(void)registerForManagedObjectNotifications;
-(void)mergeChanges:(NSNotification *)notification;
@end
