//
//  basecampAppDelegate_iPad.m
//  basecamp
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "basecampAppDelegate_-iPad.h"

@implementation basecampAppDelegate_iPad
@synthesize splitViewController;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.window.rootViewController = splitViewController;

    [super application:application didFinishLaunchingWithOptions:launchOptions];
    return YES;
}
- (void)dealloc
{
    [splitViewController release];
	[super dealloc];
}

@end
