//
//  RWAssignee.m
//  basecamp
//
//  Created by David Estes on 3/26/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "RWAssignee.h"
#import "basecampAppDelegate.h"
#import <CoreData/CoreData.h>
#import "Project.h"
#import "Person.h"
#import "BAccount.h"
#import "Company.h"
@implementation RWAssignee
@synthesize pickerView,project,account,delegate,parentView,selectionView;
@synthesize fetchedResultsController,assignees;
@synthesize assigneeType,hideAnyone;
@synthesize assigneeID,noNotify,notifyLabel,notifySwitch;
- (id)init
{
    return [self initWithNibName:@"RWAssignee" bundle:nil];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [assigneeID release];
    [assigneeType release];
    [parentView release];
    [selectionView release];
    [account release];
    [project release];
    [assignees release];
    [pickerView release];
    [fetchedResultsController release];
    [notifySwitch release];
    [notifyLabel release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)slideIn
{
    if(parentView == nil)
    {
        return;
    }
    [self viewWillAppear:YES];
    self.view.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f];
    [self.view addSubview:selectionView];
    self.selectionView.frame = CGRectMake(0.0f, self.view.frame.size.height, 320.0f, self.selectionView.frame.size.height);
    [self.parentView addSubview:self.view];
    self.view.frame = CGRectMake(0.0f, 0.0f, self.parentView.frame.size.width, self.parentView.frame.size.height);
    
    [UIView animateWithDuration:0.5f animations:^(void) {
        self.view.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f];
        self.selectionView.frame = CGRectMake(0.0f, self.view.frame.size.height - self.selectionView.frame.size.height, 320.0f, self.selectionView.frame.size.height);
    }];
    
}
-(void)slideOut
{
    [self viewWillDisappear:YES];
    [UIView animateWithDuration:0.5f animations:^(void) {
        self.view.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f];
        self.selectionView.frame = CGRectMake(0.0f, self.view.frame.size.height, 320.0f, self.selectionView.frame.size.height);
    } completion:^(BOOL finished) {
        [self.selectionView removeFromSuperview];
        [self.view removeFromSuperview];
        [self viewDidDisappear:YES];
    }];
}
-(IBAction)doneButtonPressed:(id)sender
{
    NSInteger row = [pickerView selectedRowInComponent:0];
    NSDictionary *currentDictionary = (NSDictionary *)[self.assignees objectAtIndex:row];
    
    if(delegate != nil && [delegate respondsToSelector:@selector(rwAssignee:didSelect:ofType:withName:notify:)])
    {
        if([[currentDictionary valueForKey:@"assigneeID"] intValue] == 0)
        {
            NSString *anyoneName = [[NSString alloc] initWithFormat:@"Anyone"];
            [delegate rwAssignee:self didSelect:nil ofType:nil withName:anyoneName notify:NO];
            [anyoneName release];
        }
        else
        {
            [delegate rwAssignee:self didSelect:[currentDictionary valueForKey:@"assigneeID"] ofType:[currentDictionary valueForKey:@"type"] withName:[currentDictionary valueForKey:@"name"] notify:notifySwitch.on];
        }
    }
    [self slideOut];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.notifySwitch.alpha = 0.0f;
    self.notifyLabel.alpha = 0.0f;
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [[self fetchedResultsController] performFetch:nil];
    NSMutableArray *_assignees = [[NSMutableArray alloc] initWithCapacity:2];
    self.assignees = _assignees;
    [_assignees release];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    Person *me = [Person findPersonByID:[self.project.account.meID integerValue] withManagedObjectContext:[appDelegate managedObjectContext]];
    NSString *personType = [[NSString alloc] initWithFormat:@"Person"];
    NSString *companyType = [[NSString alloc] initWithFormat:@"Company"];
    NSString *myName = [[NSString alloc] initWithFormat:@"Me (%@ %@)",me.firstName,me.lastName];
    NSDictionary *anyoneDict = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:0],@"assigneeID",@"Anyone",@"name",personType,@"type", nil];
    NSDictionary *separatorDict = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:0],@"assigneeID",@"-----------------",@"name",personType,@"type", nil];
    NSDictionary *meDict = [[NSDictionary alloc] initWithObjectsAndKeys:me.personID,@"assigneeID",myName,@"name",personType,@"type", nil];
    [myName release];
    if(!hideAnyone)
    {
        [self.assignees addObject:anyoneDict];
    }
    [self.assignees addObject:meDict];
    [meDict release];
    [anyoneDict release];

    for(Company *company in [fetchedResultsController fetchedObjects])
    {
        [self.assignees addObject:separatorDict];
        NSDictionary *companyDict = [[NSDictionary alloc] initWithObjectsAndKeys:company.companyID,@"assigneeID",company.name,@"name",companyType,@"type", nil];
        [self.assignees addObject:companyDict];
        [companyDict release];
        for(Person *person in [company.people allObjects])
        {
            NSString *personName = [[NSString alloc] initWithFormat:@"%@ %@",person.firstName,person.lastName];

            NSDictionary *personDict = [[NSDictionary alloc] initWithObjectsAndKeys:person.personID,@"assigneeID",personName,@"name",personType,@"type", nil];
            [self.assignees addObject:personDict];
            [personDict release];
            [personName release];
        }
    }

    [separatorDict release];
    [personType release];
    [companyType release];
    [pickerView reloadAllComponents];
    if(assigneeID != nil && assigneeType != nil)
    {
        NSInteger row = 0;
        BOOL found=NO;
        for(row = 0;row < [assignees count];row++)
        {
            NSDictionary *currentDictionary = (NSDictionary *)[assignees objectAtIndex:row];
            if([[currentDictionary valueForKey:@"type"] isEqualToString:assigneeType] && [[currentDictionary valueForKey:@"assigneeID"] integerValue] == [assigneeID integerValue])
            {
                found = YES;
                if ([[currentDictionary valueForKey:@"type"] isEqualToString:@"Person"] && [[currentDictionary valueForKey:@"assigneeID"] integerValue] != 0 && [[currentDictionary valueForKey:@"assigneeID"] integerValue] != [self.project.account.meID integerValue] ) {
                    self.notifyLabel.alpha = 1.0f;
                    self.notifySwitch.alpha = 1.0f;
                    //        self.notifySwitch.enabled = YES;
                }
                else
                {
                    self.notifyLabel.alpha = 0.0f;
                    self.notifySwitch.alpha = 0.0f;
                    //        self.notifySwitch.enabled = NO;        
                }
                break;
            }
        }
        if(found)
        {
            [pickerView selectRow:row inComponent:0 animated:NO];
        }
        else
        {
            self.notifyLabel.alpha = 0.0f;
            self.notifySwitch.alpha = 0.0f;
            //        self.notifySwitch.enabled = NO;        
        }
    }
    [super viewWillAppear:animated];
}
#pragma mark - UIPickerViewDelegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSDictionary *currentDict = (NSDictionary *)[self.assignees objectAtIndex:row];
    return [currentDict valueForKey:@"name"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSDictionary *currentDict = (NSDictionary *)[self.assignees objectAtIndex:row];
    if ([[currentDict valueForKey:@"type"] isEqualToString:@"Person"] && [[currentDict valueForKey:@"assigneeID"] integerValue] != 0 && [[currentDict valueForKey:@"assigneeID"] integerValue] != [self.project.account.meID integerValue] ) {
        self.notifyLabel.alpha = 1.0f;
        self.notifySwitch.alpha = 1.0f;
//        self.notifySwitch.enabled = YES;
    }
    else
    {
        self.notifyLabel.alpha = 0.0f;
        self.notifySwitch.alpha = 0.0f;
//        self.notifySwitch.enabled = NO;        
    }
}
#pragma mark - UIPickerView DataSource Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.assignees count];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark -
#pragma mark CoreData methods
-(NSFetchedResultsController *)fetchedResultsController {
    if(fetchedResultsController != nil)
    {
        return fetchedResultsController;
    }
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Company" inManagedObjectContext:[appDelegate managedObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY projects == %@",self.project];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];

    
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor,nil]];
    
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[appDelegate managedObjectContext] sectionNameKeyPath:nil cacheName:nil];

    self.fetchedResultsController = _fetchedResultsController;
    [_fetchedResultsController release];   
    [sortDescriptor release];

    [fetchRequest release];
    return fetchedResultsController;
}
@end
