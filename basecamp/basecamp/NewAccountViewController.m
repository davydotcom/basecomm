
//
//  NewAccountViewController.m
//  basecamp
//
//  Created by David Estes on 3/8/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "NewAccountViewController.h"
#import "basecampAppDelegate.h"
#import <CoreData/CoreData.h>
#import "BAccount.h"
#import "SyncController.h"
@implementation NewAccountViewController
@synthesize urlField,urlViewCell,usernameField,usernameViewCell,passwordField,passwordViewCell,account;
-(void)viewDidLoad
{
    UIBarButtonItem *saveAccountButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveAccountButtonPressed:)];
    self.navigationItem.rightBarButtonItem = saveAccountButton;
    [saveAccountButton release];
    [super viewDidLoad];
    self.title = @"New Account";
    
    UITableViewCell *_urlViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"urlViewCellIdentifier"];
    _urlViewCell.textLabel.text = @"Url";
    _urlViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITextField *_urlField = [[UITextField alloc] initWithFrame:CGRectMake(85,14,200,25)];
    _urlField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _urlField.font = [UIFont systemFontOfSize:12.0f];
    [_urlViewCell.contentView addSubview:_urlField];
    _urlField.placeholder = @"yourcompany.basecamphq.com";
    [_urlField setKeyboardType:UIKeyboardTypeURL];
    [_urlField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_urlField setAutocorrectionType:UITextAutocorrectionTypeNo];
    self.urlField = _urlField;
    [_urlField release];
    self.urlViewCell = _urlViewCell;
    [_urlViewCell release];
    
    UITableViewCell *_usernameViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"urlViewCellIdentifier"];
    _usernameViewCell.textLabel.text = @"Username";
    _usernameViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITextField *_usernameField = [[UITextField alloc] initWithFrame:CGRectMake(85,14,200,25)];
    _usernameField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _usernameField.font = [UIFont systemFontOfSize:12.0f];
    [_usernameField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_usernameField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_usernameViewCell.contentView addSubview:_usernameField];
    _usernameField.placeholder = @"john.doe";
    self.usernameField = _usernameField;

    [_usernameField release];
    self.usernameViewCell = _usernameViewCell;
    [_usernameViewCell release];
    
    UITableViewCell *_passwordViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"urlViewCellIdentifier"];
    _passwordViewCell.textLabel.text = @"Password";
    _passwordViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITextField *_passwordField = [[UITextField alloc] initWithFrame:CGRectMake(85,14,200,25)];
    _passwordField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _passwordField.font = [UIFont systemFontOfSize:12.0f];
    [_passwordField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_passwordField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_passwordViewCell.contentView addSubview:_passwordField];
    _passwordField.secureTextEntry = YES;
    self.passwordField = _passwordField;
    [_passwordField release];
    self.passwordViewCell = _passwordViewCell;
    [_passwordViewCell release];
}
-(void)viewWillAppear:(BOOL)animated
{
    if(account == nil)
    {
        self.passwordField.text = nil;
        self.urlField.text = nil;
        self.usernameField.text = nil;
    }
    else
    {
        self.passwordField.text = account.password;
        self.urlField.text = account.url;
        self.usernameField.text = account.username;
    }
    [self.urlField becomeFirstResponder];
    [super viewWillAppear:animated];
}
#pragma mark -
#pragma mark UITableViewDataSource Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == 0)
    {
        return self.urlViewCell;
    }
    if([indexPath row] == 1)
    {
        return self.usernameViewCell;
    }
    if([indexPath row] == 2)
    {
        return self.passwordViewCell;
    }
    return nil;
}
-(NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"A Basecamp account can be created by visiting http://basecamphq.com.";
}
-(void)monitorAuthentication
{
        while(account.syncing)
        {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
        if(account.error == nil && [account.accountID integerValue] != 0)
        {
            [self performSelectorOnMainThread:@selector(authenticationSuccessful) withObject:nil waitUntilDone:NO];
        }
        else
        {
            [self performSelectorOnMainThread:@selector(authenticationFailed:) withObject:account.error waitUntilDone:NO];

        }
        
}
-(void)authenticationSuccessful
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate.syncController addSyncControllerForAccount:self.account];
    [appDelegate.mainNavigationController popToRootViewControllerAnimated:YES];
}
-(void)authenticationFailed:(NSError *)error
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [[appDelegate managedObjectContext] deleteObject:self.account];
    [[appDelegate managedObjectContext] save:nil];
    self.account = nil;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Error" message:@"There was an error verifying your account. Please verify the information and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
    
}
#pragma mark -
#pragma mark IBAction Methods
-(IBAction)saveAccountButtonPressed:(id)sender
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(account == nil && ![urlField.text isEqualToString:@""] && ![usernameField.text isEqualToString:@""] && ![passwordField.text isEqualToString:@""])
    {
        NSLog(@"Creating new account record!");
        BAccount *_account = (BAccount *)[NSEntityDescription insertNewObjectForEntityForName:@"BAccount" inManagedObjectContext:[appDelegate managedObjectContext]];
        _account.url = [[self.urlField.text lowercaseString] stringByReplacingOccurrencesOfString:@".basecamphq.com" withString:@""];
        _account.username = self.usernameField.text;
        _account.password = self.passwordField.text;
        self.account = _account;
        [[appDelegate managedObjectContext] save:nil];
        [_account updateAccountDataFromServer];
        [NSThread detachNewThreadSelector:@selector(monitorAuthentication) toTarget:self withObject:nil];
    }
    else if(![urlField.text isEqualToString:@""] && ![usernameField.text isEqualToString:@""] && ![passwordField.text isEqualToString:@""])
    {
        BAccount *_account = (BAccount *)self.account;
        _account.url = [[self.urlField.text lowercaseString] stringByReplacingOccurrencesOfString:@".basecamphq.com" withString:@""];
        _account.username = self.usernameField.text;
        _account.password = self.passwordField.text;

        [[appDelegate managedObjectContext] save:nil];
        [_account updateAccountDataFromServer];
        [NSThread detachNewThreadSelector:@selector(monitorAuthentication) toTarget:self withObject:nil];
    }
    
}
                             
#pragma mark -
#pragma mark Memory Management Methods
-(void)dealloc
{
    [urlViewCell release];
    [urlField release];
    [usernameViewCell release];
    [usernameField release];
    [passwordViewCell release];
    [passwordField release];
    [account release];
    [super dealloc];
}
@end
