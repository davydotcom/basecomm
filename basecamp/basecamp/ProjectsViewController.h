//
//  ProjectsViewController.h
//  basecamp
//
//  Created by David Estes on 3/10/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWTableViewController.h"
@class BAccount;
@class NSFetchedResultsController;
@interface ProjectsViewController : RWTableViewController {
    BAccount *account;
    NSArray *projects;
    BOOL showNavigationBar;
}
@property(nonatomic,retain) BAccount *account;
@property(nonatomic,retain) NSArray *projects;
@property(nonatomic,retain) NSFetchedResultsController *fetchedResultsController;
-(void)registerForNotifications;
-(void)unregisterForNotifications;
-(void)updateProjects:(NSNotification *)notification;
@end
