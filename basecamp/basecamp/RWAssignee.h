//
//  RWAssignee.h
//  basecamp
//
//  Created by David Estes on 3/26/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NSFetchedResultsController;
@class RWAssignee,Project,BAccount;
@protocol RWAssigneeDelegate <NSObject>
-(void)rwAssignee:(RWAssignee *)_rwAssignee didSelect:(NSNumber *)_id ofType:(NSString*)_type withName:(NSString *)_name notify:(BOOL)notify;
@end
@interface RWAssignee : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource> {
    IBOutlet UIPickerView *pickerView;
    IBOutlet UIView *selectionView;
    UIView *parentView;
    NSFetchedResultsController *fetchedResultsController;
    Project *project;
    BAccount *account;
    BOOL hideAnyone;
    BOOL noNotify;
    IBOutlet UILabel *notifyLabel;
    IBOutlet UISwitch *notifySwitch;
    NSMutableArray *assignees;
    NSNumber *assigneeID;
    NSString *assigneeType;
    id <RWAssigneeDelegate> delegate;
}
@property(nonatomic, assign) BOOL noNotify;
@property(nonatomic, retain) UILabel *notifyLabel;
@property(nonatomic, retain) UISwitch *notifySwitch;
@property(nonatomic, assign) BOOL hideAnyone;
@property(nonatomic,retain) NSNumber *assigneeID;
@property(nonatomic,retain) NSString *assigneeType;
@property(nonatomic, retain) NSMutableArray *assignees;

@property(nonatomic,retain) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, retain) UIPickerView *pickerView;
@property(nonatomic, retain) UIView *selectionView;
@property(nonatomic, retain) UIView *parentView;

@property(nonatomic, retain) Project *project;
@property(nonatomic, retain) BAccount *account;
@property(nonatomic, assign) id delegate;

-(IBAction)doneButtonPressed:(id)sender;
- (id)init;
-(void)slideOut;
-(void)slideIn;
@end
