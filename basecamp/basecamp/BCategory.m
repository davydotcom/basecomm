//
//  Category.m
//  basecamp
//
//  Created by David Estes on 6/11/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import "BCategory.h"
#import "Message.h"
#import "Project.h"


@implementation BCategory
@dynamic name;
@dynamic type;
@dynamic categoryID;
@dynamic project;
@dynamic messages;
@dynamic synced;
@dynamic deleted;
+(NSArray *)allCategoriesForProject:(Project *)_project WithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"BCategory" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"project == %@",_project];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"categoryID" ascending:YES];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return returnArray;
}
+(BCategory *)findCategoryByID:(NSInteger)_categoryID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"BCategory" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"categoryID == %d",_categoryID];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"categoryID" ascending:YES];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *results = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    results = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    BCategory *_category = nil;
    if(results != nil && [results count] > 0)
    {
        _category = (BCategory *)[results objectAtIndex:0];
    }
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return _category;
}
@end
