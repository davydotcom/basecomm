//
//  ProjectTodoListViewController.h
//  basecamp
//
//  Created by David Estes on 3/19/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
@class NSFetchedResultsController,Project;
@class TodoItemViewController;
@interface ProjectTodoListViewController : UIViewController <NSFetchedResultsControllerDelegate,UITableViewDelegate,UITableViewDataSource,EGORefreshTableHeaderDelegate> {
    IBOutlet UITableView *tableView;
    IBOutlet UILabel *projectTitleLabel;
    IBOutlet UILabel *companyTitleLabel;
    TodoItemViewController *todoItemViewController;
    Project *project;
    NSFetchedResultsController *fetchedResultsController;
    BOOL _reloading;
    EGORefreshTableHeaderView *_refreshHeaderView;
}
@property(nonatomic, retain) TodoItemViewController *todoItemViewController;
@property(nonatomic, retain) UILabel *projectTitleLabel;
@property(nonatomic, retain) UILabel *companyTitleLabel;
@property(nonatomic, retain) Project *project;
@property(nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic,retain)UITableView *tableView;
-(IBAction)addButtonPressed:(id)sender;
-(void)registerForNotifications;
-(void)unregisterForNotifications;

- (void)doneLoadingTableViewData;
@end
