//
//  BAccount.h
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Project, TodoList, BCAccount,BCCompany,Person;

@interface BAccount : NSManagedObject {
    NSError *error;
    BOOL syncing;
    BCAccount *basecampAccount;
    BCCompany *basecampCompany;
@private

}
@property (nonatomic, retain) NSError *error;
@property (nonatomic, assign) BOOL syncing;
@property (nonatomic, retain) BCAccount *basecampAccount;
@property (nonatomic, retain) BCCompany *basecampCompany;
@property (nonatomic, retain) NSString * avatarURL;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSNumber * accountID;
@property (nonatomic, retain) NSNumber * accountHolderID;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * companyName;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber *meID;
@property (nonatomic, retain) NSSet* todoLists;
@property (nonatomic, retain) NSSet* projects;
@property (nonatomic, retain) NSSet* people;
@property (nonatomic, retain) NSSet* companies;
-(void)updateAccountDataFromServer;
+(NSArray *)allAccountsWithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
@end
