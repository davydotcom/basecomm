//
//  basecampAppDelegate.h
//  basecamp
//
//  Created by David Estes on 3/5/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SyncController;
@interface basecampAppDelegate : NSObject <UIApplicationDelegate> {
    IBOutlet UINavigationController *mainNavigationController;
    SyncController *syncController;
}
@property (nonatomic, retain) SyncController *syncController;
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *mainNavigationController;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)setupSyncController;
@end
