//
//  TodoListTableViewCell.m
//  basecamp
//
//  Created by David Estes on 3/19/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "TodoListTableViewCell.h"


@implementation TodoListTableViewCell
@synthesize todoItem1,todoItem2,todoItem3,more,title;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 10.0f, 300.0f, 20.0f)];
        _titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.title = _titleLabel;
        [_titleLabel release];
        UILabel *_todoItem1 = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 30.0f, 280.0f, 15.0f)];
        _todoItem1.font = [UIFont systemFontOfSize:12.0f];
        _todoItem1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _todoItem1.textColor = [UIColor darkGrayColor];
        self.todoItem1 = _todoItem1;
        [_todoItem1 release];
        UILabel *_todoItem2 = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 45.0f, 280.0f, 15.0f)];
        _todoItem2.font = [UIFont systemFontOfSize:12.0f];
        _todoItem2.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _todoItem2.textColor = [UIColor darkGrayColor];
        self.todoItem2 = _todoItem2;
        [_todoItem2 release];
        UILabel *_todoItem3 = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 60.0f, 280.0f, 15.0f)];
        _todoItem3.font = [UIFont systemFontOfSize:12.0f];
        _todoItem3.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _todoItem3.textColor = [UIColor darkGrayColor];
        self.todoItem3 = _todoItem3;
        [_todoItem3 release];
        UILabel *_more = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 75.0f, 280.0f, 15.0f)];
        _more.font = [UIFont systemFontOfSize:12.0f];
        _more.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _more.textColor = [UIColor darkGrayColor];
        self.more = _more;
        [_more release];
        
        [self.contentView addSubview:title];
        [self.contentView addSubview:todoItem1];
        [self.contentView addSubview:todoItem2];
        [self.contentView addSubview:todoItem3];
        [self.contentView addSubview:more];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [title release];
    [todoItem1 release];
    [todoItem2 release];
    [todoItem3 release];
    [more release];
    [super dealloc];
}

@end
