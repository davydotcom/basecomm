//
//  Project.h
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#define DID_REMOVE_PROJECT_NOTIFICATION @"DID_REMOVE_PROJECT_NOTIFICATION"
#define PROJECTS_DID_SYNC_NOTIFICATION @"PROJECTS_DID_SYNC_NOTIFICATION"
@class BAccount, TodoList, BCProject,Message;

@interface Project : NSManagedObject {
    BOOL finished;
@private
}
@property(nonatomic,assign) BOOL finished;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSDate * createdOn;
@property (nonatomic, retain) NSDate * lastChangedOn;
@property (nonatomic, retain) NSNumber * showWriteboards;
@property (nonatomic, retain) NSNumber * showAnnouncement;
@property (nonatomic, retain) NSString * announcement;
@property (nonatomic, retain) NSNumber * synced;
@property (nonatomic, retain) NSString * startPage;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * projectID;
@property (nonatomic, retain) NSSet* todoLists;
@property (nonatomic, retain) BAccount * account;
@property (nonatomic, retain) NSSet* milestones;
@property (nonatomic, retain) NSSet* people;
@property (nonatomic, retain) NSSet* companies;
@property (nonatomic, retain) NSSet* messages;

+(NSArray *)unsyncedProjectsWithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(NSArray *)allProjectsForAccount:(BAccount *)_account WithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(Project *)findByProjectID:(NSInteger)_projectID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
-(BCProject *) BCProjectFromProject;

-(void)syncProject;
@end
@interface Project (CoreDataGeneratedAccessors)
- (void)addMessagesObject:(Message *)value;
- (void)removeMessagesObject:(Message *)value;
- (void)addMessages:(NSSet *)value;
- (void)removeMessages:(NSSet *)value;
@end
