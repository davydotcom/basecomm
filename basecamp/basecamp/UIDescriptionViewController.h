//
//  UIDescriptionViewController.h
//  basecamp
//
//  Created by David Estes on 4/27/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UIDescriptionViewController;
@protocol UIDescriptionViewControllerDelegate <NSObject>
-(void)UIDescriptionViewController:(UIDescriptionViewController *)_uiDescriptionViewController didSave:(NSString *)description;
-(void)didCancelUIDescriptionViewController:(UIDescriptionViewController *)_uiDescriptionViewController;
@end
@interface UIDescriptionViewController : UIViewController <UITextViewDelegate> {
    IBOutlet UITextView *textView;
    NSString *description;
    id <UIDescriptionViewControllerDelegate> delegate;
}
@property(nonatomic,retain) NSString *description;
@property(nonatomic,retain) UITextView *textView;
@property(nonatomic,assign) id delegate;

-(IBAction)cancelButtonPressed:(id)sender;
-(IBAction)saveButtonPressed:(id)sender;
@end
