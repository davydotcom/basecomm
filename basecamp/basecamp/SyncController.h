//
//  SyncController.h
//  basecamp
//
//  Created by David Estes on 3/18/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BAccount,NSManagedObjectContext;
@interface SyncController : NSObject {
    BOOL finished;
    NSInteger status;
    NSMutableArray *syncControllers;
    NSThread *syncThread;
    NSManagedObjectContext *managedObjectContext;
}
@property(nonatomic, retain) NSThread *syncThread;
@property(nonatomic,assign) BOOL finished;
@property(nonatomic,assign) NSInteger status;
@property(nonatomic,retain) NSMutableArray *syncControllers;
@property(nonatomic,retain) NSManagedObjectContext *managedObjectContext;
-(void)startSyncing;
-(void)runSyncThread;
-(void)runSyncUnsyncedObjectsThread;
-(void)addSyncControllerForAccount:(BAccount *)_account;
-(void)removeSyncControllerForAccount:(BAccount *)_account;
-(void)startupSyncThread;
@end
