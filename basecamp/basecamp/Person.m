//
//  Person.m
//  basecamp
//
//  Created by David Estes on 3/25/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import "Person.h"
#import "BAccount.h"
#import "Project.h"
#import "Company.h"
#import "Comment.h"
#import "Message.h"
@implementation Person
@dynamic title;
@dynamic lastLogin;
@dynamic phoneNumberMobile;
@dynamic clientID;
@dynamic imService;
@dynamic administrator;
@dynamic personID;
@dynamic emailAddress;
@dynamic phoneNumberOfficeExt;
@dynamic phoneNumberHome;
@dynamic avatarURL;
@dynamic imHandle;
@dynamic firstName;
@dynamic phoneNumberOffice;
@dynamic userName;
@dynamic phoneNumberFax;
@dynamic lastName;
@dynamic hasAccessToNewProjects;
@dynamic companyID;
@dynamic account;
@dynamic company;
@dynamic projects;
@dynamic comments;
@dynamic messages;
@synthesize cachedAvatar;
@synthesize loadingImage;
+(NSArray *)allPeopleForAccount:(BAccount *)_account WithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"account == %@",_account];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"personID" ascending:YES];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return returnArray;
}

+(NSArray *)allPeopleForProject:(Project *)_project WithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY projects == %@",_project];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"personID" ascending:YES];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return returnArray;
}
+(Person *)findPersonByID:(NSInteger)_personID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"personID == %d",_personID];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"personID" ascending:YES];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *results = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    results = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    Person *_person = nil;
    if(results != nil && [results count] > 0)
    {
        _person = (Person *)[results objectAtIndex:0];
    }
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return _person;
}

-(void)loadCachedImage
{
    if(self.cachedAvatar != nil || loadingImage)
    {
        return;
    }
    loadingImage = YES;
    [self retain];
    [NSThread detachNewThreadSelector:@selector(loadCachedImageThread) toTarget:self withObject:nil];
}
-(void)loadCachedImageThread
{
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    if(self.avatarURL != nil)
    {
        UIImage *img = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.avatarURL]]];
        self.cachedAvatar = img;
        [img release];
    }
    [autoreleasepool release];
    [self release];
    [self performSelectorOnMainThread:@selector(finishLoadingCachedImage) withObject:nil waitUntilDone:YES];
    loadingImage = NO;

}
-(void)finishLoadingCachedImage
{
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:self,@"person", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:AVATAR_IMAGE_UPDATED_NOTIFICATION object:nil userInfo:dict];
    [dict release];
}
- (void)addProjectsObject:(Project *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"projects" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"projects"] addObject:value];
    [self didChangeValueForKey:@"projects" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)removeProjectsObject:(Project *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"projects" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"projects"] removeObject:value];
    [self didChangeValueForKey:@"projects" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addProjects:(NSSet *)value {    
    [self willChangeValueForKey:@"projects" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"projects"] unionSet:value];
    [self didChangeValueForKey:@"projects" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeProjects:(NSSet *)value {
    [self willChangeValueForKey:@"projects" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"projects"] minusSet:value];
    [self didChangeValueForKey:@"projects" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


-(void)dealloc
{
    [cachedAvatar release];
    [super dealloc];
}
@end
