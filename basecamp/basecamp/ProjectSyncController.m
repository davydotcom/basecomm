//
//  ProjectSyncController.m
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "ProjectSyncController.h"
#import "basecampAppDelegate.h"
#import <CoreData/CoreData.h>
#import "Project.h"
#import "BAccount.h"
#import "BCAccount.h"
#import "BCProject.h"
#import "SyncController.h"
@implementation ProjectSyncController
@synthesize account;
@synthesize managedObjectContext,parentObjectContext,sourceThread;
@synthesize finished;
#pragma mark - Sync Methods
-(id)initWithAccount:(BAccount* )_account
{
    self = [super init];
    if(self)
    {
        self.account = _account;
    }
    return self;
}
-(void)startSyncing
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [self performSelector:@selector(runSyncThread) onThread:appDelegate.syncController.syncThread withObject:nil waitUntilDone:NO];
}
-(void) fetchAccountWithNewManagedObjectContext:(NSManagedObjectContext *)_managedObjectContext
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"BAccount" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate =  [NSPredicate predicateWithFormat:@"self == %@",self.account];
    [fetchRequest setPredicate:predicate];
    
    NSArray *array = [_managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if(array != nil)
    {
        self.account = [array objectAtIndex:0];
    }

}
-(void)runSyncThread
{
    [self retain];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    finished = NO;
    BOOL createdOwnContext = NO;
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(self.managedObjectContext == nil)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        self.managedObjectContext = [appDelegate.syncController managedObjectContext];
        createdOwnContext = YES;
    }

    [self fetchAccountWithNewManagedObjectContext:managedObjectContext];

//    //First We sync all projects that have not been synced yet
//    NSArray *projects = [Project unsyncedProjectsWithManagedObjectContext:managedObjectContext];
//    for(Project *_project in projects)
//    {
//        [_project syncProject];
//        while(_project.finished == NO)
//        {
//            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
//        }
//              
//    }
    //Get Array of all projects from CoreData
    NSArray *projects =[Project allProjectsForAccount:account WithManagedObjectContext:managedObjectContext];
    //Then We Fetch all Projects from server to sync with
    [BCProject loadProjectsForAccount:[account basecampAccount] withCompletionHandler:^(NSArray *_bcprojects, NSError *error) {
        if(error == nil)
        {
            //First Check to see if any projects have been removed!
            for(Project *_dbProject in projects)
            {
                BOOL found=NO;
                for(BCProject *_bcProject in _bcprojects)
                {
                    if ([_bcProject.projectID integerValue] == [_dbProject.projectID integerValue]) {
                        found = YES;
                    }
                }
                if(!found)
                {
                    [self performSelectorOnMainThread:@selector(didRemoveProjectNotification:) withObject:_dbProject waitUntilDone:YES];
                    [managedObjectContext deleteObject:_dbProject];
                }
            }
            //     NSLog(@"Looking for new and updating projects!");
            //Add New Projects and Update Existing
            for(BCProject *_bcProject in _bcprojects)
            {
                BOOL found = NO;
                for(Project *_dbProject in projects)
                {
                    if ([_bcProject.projectID integerValue] == [_dbProject.projectID integerValue]) {
                        //Update record
                        NSLog(@"Found Project!");
                        _dbProject.name = _bcProject.name;
                        _dbProject.status = _bcProject.status;
                        _dbProject.lastChangedOn = _bcProject.lastChangedOn;
                        _dbProject.createdOn = _bcProject.createdOn;
                        _dbProject.showWriteboards = _bcProject.showWriteboards;
                        _dbProject.showAnnouncement = _bcProject.showAnnouncement;
                        _dbProject.startPage = _bcProject.startPage;
                        _dbProject.announcement = _bcProject.announcement;
                        _dbProject.synced = [NSNumber numberWithBool:YES];
                        found = YES;
                    }
                }
                if(!found)
                {
                    //Time to create a new project file
                    NSLog(@"Adding Project To Database!");
                    Project *_newProject = [NSEntityDescription insertNewObjectForEntityForName:@"Project" inManagedObjectContext:managedObjectContext];
                    _newProject.account = account;
                    _newProject.projectID = _bcProject.projectID;
                    _newProject.name = _bcProject.name;
                    _newProject.status = _bcProject.status;
                    _newProject.lastChangedOn = _bcProject.lastChangedOn;
                    _newProject.createdOn = _bcProject.createdOn;
                    _newProject.showWriteboards = _bcProject.showWriteboards;
                    _newProject.showAnnouncement = _bcProject.showAnnouncement;
                    _newProject.startPage = _bcProject.startPage;
                    _newProject.announcement = _bcProject.announcement;
                    _newProject.synced = [NSNumber numberWithBool:YES];
                }
            }
        }
        finished = YES;
    }];

    while(!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];  
    }
    // NSLog(@"Saving Changes!");
    NSError *error=nil;
    if(![managedObjectContext save:&error])
    {
        NSLog(@"Error Saving to CoreData After Project Sync");
    }
      NSLog(@"Notifying project sync completion!");
    [self performSelectorOnMainThread:@selector(didFinishSyncingProjectsNotification) withObject:nil waitUntilDone:NO];
    if(createdOwnContext)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        self.managedObjectContext = nil;
    }
    [autoreleasepool release];
    [self release];

}

-(void)registerForManagedObjectNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mergeChanges:) name:NSManagedObjectContextDidSaveNotification object:self.managedObjectContext];
    
}
-(void)mergeChanges:(NSNotification *)notification
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
        NSManagedObjectContext *mainContext = [appDelegate managedObjectContext];
        [mainContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:) withObject:notification waitUntilDone:YES];        
}
-(void)didRemoveProjectNotification:(Project *)_project
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:DID_REMOVE_PROJECT_NOTIFICATION object:self userInfo:[NSDictionary dictionaryWithObject:_project forKey:@"project"]];
}
-(void)didFinishSyncingProjectsNotification
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:PROJECTS_DID_SYNC_NOTIFICATION object:nil];
}

-(void)dealloc
{
    [account release];
    [sourceThread release];
    [parentObjectContext release];
    [managedObjectContext release];
    [super dealloc];
}
@end
