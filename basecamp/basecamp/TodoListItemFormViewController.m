//
//  TodoListItemFormViewController.m
//  basecamp
//
//  Created by David Estes on 3/25/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "TodoListItemFormViewController.h"
#import "TodoList.h"
#import "TodoListItem.h"
#import <CoreData/CoreData.h>
#import "basecampAppDelegate.h"
#import "RWDateSelector.h"
#import "RWAssignee.h"
@implementation TodoListItemFormViewController
@synthesize contentTextView,assignButton,dueButton,notify;
@synthesize todoList,todoListItem,delegate,dueDate,rwDateSelector,assigneeID,assigneeName,assigneeType,rwAssigneeSelector;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [assigneeName release];
    [assigneeID release];
    [assigneeType release];
    [rwAssigneeSelector release];
    [contentTextView release];
    [assignButton release];
    [dueButton release];
    [dueDate release];
    [todoListItem release];
    [todoList release];
    self.rwDateSelector = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(IBAction)cancelButtonPressed:(id)sender
{
    if(delegate != nil && [delegate respondsToSelector:@selector(didCancelTodoListItemFormViewController:)])
    {
        self.todoListItem = nil;
        self.todoList = nil;
        [delegate didCancelTodoListItemFormViewController:self];
    }
}
-(IBAction)saveButtonPressed:(id)sender
{
    if(self.contentTextView.text == nil || self.contentTextView.text == @"")
    {
        //Alert that text view cant be blank!
        return;
    }
    if(self.todoListItem == nil)
    {
        //Create new Todo List Item !
        
        TodoListItem *_todoListItem = [NSEntityDescription insertNewObjectForEntityForName:@"TodoListItem" inManagedObjectContext:[self.todoList managedObjectContext]];
        _todoListItem.todoList = self.todoList;
        _todoListItem.content = self.contentTextView.text;
        _todoListItem.dueAt = self.dueDate;
        _todoListItem.responsiblePartyID = self.assigneeID;
        _todoListItem.responsiblePartyType = self.assigneeType;
        _todoListItem.responsiblePartyName = self.assigneeName;
        NSNumber *yesNum = [[NSNumber alloc] initWithBool:YES];
        NSNumber *noNum = [[NSNumber alloc] initWithBool:NO];
        if([self.assigneeID integerValue] > 0)
        {
            if(self.notify)
            {
                _todoListItem.notify = yesNum;
            }
            else
            {
                _todoListItem.notify = noNum;
            }
        }

        _todoListItem.synced = noNum;
        [noNum release];
        [yesNum release];
        self.todoListItem = _todoListItem;
        [[_todoListItem managedObjectContext] save:nil];
        [_todoListItem syncTodoListItem];
    }
    else
    {
        //Save Current
        todoListItem.content = self.contentTextView.text;
        todoListItem.dueAt = self.dueDate;
        todoListItem.responsiblePartyID = self.assigneeID;
        todoListItem.responsiblePartyType = self.assigneeType;
        todoListItem.responsiblePartyName = self.assigneeName;
        NSNumber *yesNum = [[NSNumber alloc] initWithBool:YES];
        NSNumber *noNum = [[NSNumber alloc] initWithBool:NO];
        if([self.assigneeID integerValue] > 0)
        {
            if(self.notify)
            {
                todoListItem.notify = yesNum;
            }
            else
            {
                todoListItem.notify = noNum;
            }
        }

        todoListItem.synced = noNum;
        [noNum release];
        [yesNum release];
        [[todoListItem managedObjectContext ] save:nil];
        [todoListItem syncTodoListItem];

    }
    if(delegate != nil && [delegate respondsToSelector:@selector(todoListItemFormViewController:didSaveTodoListItem:)])
    {
        self.todoListItem = nil;
        self.todoList = nil;
        [delegate todoListItemFormViewController:self didSaveTodoListItem:self.todoListItem];
    }
}

-(IBAction)dueButtonPressed:(id)sender
{
    [self.contentTextView resignFirstResponder];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    RWDateSelector *dateSelector = [[RWDateSelector alloc] initWithNibName:@"RWDateSelector" bundle:nil];
    self.rwDateSelector = dateSelector;
    dateSelector.parentView = appDelegate.window;
    dateSelector.delegate = self;
    [dateSelector slideIn];
    [dateSelector release];
}
-(IBAction)assignButtonPressed:(id)sender
{
    [self.contentTextView resignFirstResponder];
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    RWAssignee *_assigneeSelector = [[RWAssignee alloc] init];
    self.rwAssigneeSelector = _assigneeSelector;
    [_assigneeSelector release];
    self.rwAssigneeSelector.assigneeID = self.assigneeID;
    self.rwAssigneeSelector.assigneeType = self.assigneeType;
    self.rwAssigneeSelector.delegate = self;
    self.rwAssigneeSelector.parentView = appDelegate.window;
    self.rwAssigneeSelector.notifySwitch.on = self.notify;
    self.rwAssigneeSelector.project = self.todoList.project;
    [rwAssigneeSelector slideIn];
}
#pragma mark RWDateSelectorDelegate Methods
-(void)rwDateSelector:(RWDateSelector *)_rwDateSelector didSelectDate:(NSDate*)_dueDate
{
    self.dueDate = _dueDate;
    if(self.dueDate == nil)
    {
        [self.dueButton setTitle:@"Anytime" forState:UIControlStateNormal];
    }
    else
    {
        NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"EdMMM,y" options:0
                                                                  locale:[NSLocale currentLocale]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:formatString];
        [self.dueButton setTitle:[dateFormatter stringFromDate:self.dueDate] forState:UIControlStateNormal];
    }
    [self.contentTextView becomeFirstResponder];
}
-(void)rwAssignee:(RWAssignee *)_rwAssignee didSelect:(NSNumber *)_id ofType:(NSString*)_type withName:(NSString *)_name notify:(BOOL)_notify
{
    self.assigneeID = _id;
    self.assigneeType = _type;
    self.assigneeName = _name;
    self.notify = _notify;
    [self.assignButton setTitle:assigneeName forState:UIControlStateNormal];
    [self.contentTextView becomeFirstResponder];
}
#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/
-(void)viewWillAppear:(BOOL)animated
{
    if(self.todoListItem != nil)
    {
        self.contentTextView.text = todoListItem.content;
        self.dueDate = self.todoListItem.dueAt;
        self.assigneeID = self.todoListItem.responsiblePartyID;
        self.assigneeType = self.todoListItem.responsiblePartyType;
        self.assigneeName = self.todoListItem.responsiblePartyName;
        self.notify = [self.todoListItem.notify boolValue];
        [self.assignButton setTitle:assigneeName forState:UIControlStateNormal];
        if(self.todoList == nil)
        {
            self.todoList = self.todoListItem.todoList;
        }
    }
    
    if(self.dueDate == nil)
    {
        [self.dueButton setTitle:@"Anytime" forState:UIControlStateNormal];
    }
    else
    {
        NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"EdMMM,y" options:0
                                                                  locale:[NSLocale currentLocale]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:formatString];
        [self.dueButton setTitle:[dateFormatter stringFromDate:self.dueDate] forState:UIControlStateNormal];
    }
    [self.contentTextView becomeFirstResponder];
}
/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
