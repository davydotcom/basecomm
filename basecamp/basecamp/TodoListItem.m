//
//  TodoListItem.m
//  basecamp
//
//  Created by David Estes on 3/17/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import "TodoListItem.h"
#import "TodoList.h"
#import "BCTodoListItem.h"
#import "BCAccount.h"
#import "BAccount.h"
#import "Comment.h"
@implementation TodoListItem
@dynamic completed;
@dynamic commentsCount;
@dynamic createdAt;
@dynamic dueAt;
@dynamic todoListItemID;
@dynamic creatorID;
@dynamic content;
@dynamic position;
@dynamic responsiblePartyID;
@dynamic responsiblePartyName;
@dynamic responsiblePartyType;
@dynamic creatorName;
@dynamic todoList;
@dynamic synced;
@dynamic mark;
@dynamic notify;
@dynamic deleted;
@dynamic comments;
@synthesize finished;
+(NSArray *)unsyncedTodoListItemsWithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TodoListItem" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"synced == 0 || mark == 1"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"todoListItemID" ascending:YES];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return returnArray;
}
+(NSArray *)allTodoListItemsForTodoListID:(NSInteger)_todoListID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TodoListItem" inManagedObjectContext:_managedObjectContext];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"completed" ascending:YES];    
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"todoList.todoListID == %d",_todoListID];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor,sortDescriptor2,nil]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    NSArray *returnArray = nil;
    if(![_fetchedResultsController performFetch:nil])
    {
        NSLog(@"Error Performing Fetch Request!");
    }
    returnArray = [[[_fetchedResultsController fetchedObjects] retain] autorelease];
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [sortDescriptor2 release];
    [fetchRequest release];
    return returnArray;
}

#pragma mark - Submit New Objects

-(void)syncTodoListItem
{
    self.finished = NO;
    BCTodoListItem *_bcTodoListItem = [self BCTodoListItemFromTodoListItem];
    if(![self.deleted boolValue] && ![self.synced boolValue] && ![self.mark boolValue])
    {
        
        
        [_bcTodoListItem saveWithCompletionHandler:^(BCTodoListItem *_todoListItem, NSError *error) {
            if(error == nil)
            {
                
                self.todoListItemID = _todoListItem.todoListItemID;
                //TODO: Add to proper project if exists
                self.content = _todoListItem.content;
                self.creatorName = _todoListItem.creatorName;
                self.createdAt = _todoListItem.createdAt;
                self.responsiblePartyType = _todoListItem.responsiblePartyType;
                self.responsiblePartyName = _todoListItem.responsiblePartyName;
                self.responsiblePartyID = _todoListItem.responsiblePartyID;
                self.dueAt = _todoListItem.dueAt;
                self.position = _todoListItem.position;
                self.commentsCount = _todoListItem.commentsCount;
                self.completed = _todoListItem.completed;
                self.synced = [NSNumber numberWithBool:YES];
                [[self managedObjectContext] save:nil];
            }
            finished = YES;
        } notify:[self.notify boolValue]];
    }
    else if(![self.deleted boolValue] && ![self.synced boolValue] && [self.mark boolValue])
    {
        [_bcTodoListItem saveWithCompletionHandler:^(BCTodoListItem *_todoListItem, NSError *error) {
            if(error == nil)
            {
                
                self.todoListItemID = _todoListItem.todoListItemID;
                //TODO: Add to proper project if exists
                self.content = _todoListItem.content;
                self.creatorName = _todoListItem.creatorName;
                self.createdAt = _todoListItem.createdAt;
                self.responsiblePartyType = _todoListItem.responsiblePartyType;
                self.responsiblePartyName = _todoListItem.responsiblePartyName;
                self.responsiblePartyID = _todoListItem.responsiblePartyID;
                self.dueAt = _todoListItem.dueAt;
                self.position = _todoListItem.position;
                self.commentsCount = _todoListItem.commentsCount;
                self.synced = [NSNumber numberWithBool:YES];
                [[self managedObjectContext] save:nil];
                if([self.completed boolValue])
                {
                    [_todoListItem markCompleteWithCompletionHandler:^(BCTodoListItem *__todoListItem, NSError *_error) {
                        if(_error == nil)
                        {
                            self.mark = [NSNumber numberWithBool:NO];
                            [[self managedObjectContext] save:nil];
                        }
                        finished = YES;
                    }];
                }
                else
                {
                    [_todoListItem markIncompleteWithCompletionHandler:^(BCTodoListItem *__todoListItem, NSError *_error) {
                        if(_error == nil)
                        {
                            self.mark = [NSNumber numberWithBool:NO];
                            [[self managedObjectContext] save:nil];
                        }
                        finished = YES;
                    }];
                }
            }
            else
            {
                finished = YES;
            }
        } notify:[self.notify boolValue]];
    }
    else if(![self.deleted boolValue] && [self.synced boolValue] && [self.mark boolValue])
    {
        if([self.completed boolValue])
        {
            [_bcTodoListItem markCompleteWithCompletionHandler:^(BCTodoListItem *__todoListItem, NSError *_error) {
                if(_error == nil)
                {
                    self.mark = [NSNumber numberWithBool:NO];
                    [[self managedObjectContext] save:nil];
                }
                finished = YES;
            }];
        }
        else
        {
            [_bcTodoListItem markIncompleteWithCompletionHandler:^(BCTodoListItem *__todoListItem, NSError *_error) {
                if(_error == nil)
                {
                    self.mark = [NSNumber numberWithBool:NO];
                    [[self managedObjectContext] save:nil];
                }
                finished = YES;
            }];
        }
    }
    else
    {
        [_bcTodoListItem destroyWithCompletionHandler:^(BCTodoListItem *todoListItem, NSError *error) {
            if(error == nil)
            {
                [[self managedObjectContext] deleteObject:self];
                [[self managedObjectContext] save:nil];
            }
            finished = YES;
        }];
    }

    
}

-(BCTodoListItem *) BCTodoListItemFromTodoListItem
{
    BCAccount *_account = [self.todoList.account basecampAccount];
    BCTodoListItem *_bcTodoListItem = [[BCTodoListItem alloc] initWithAccount:_account];
    _bcTodoListItem.todoListID = self.todoList.todoListID;
    _bcTodoListItem.todoListItemID = self.todoListItemID;
    _bcTodoListItem.content = self.content;
    _bcTodoListItem.createdAt = self.createdAt;
    _bcTodoListItem.creatorID = self.creatorID;
    _bcTodoListItem.creatorName = self.creatorName;
    _bcTodoListItem.responsiblePartyID = self.responsiblePartyID;
    _bcTodoListItem.responsiblePartyName = self.responsiblePartyName;
    _bcTodoListItem.responsiblePartyType = self.responsiblePartyType;
    _bcTodoListItem.dueAt = self.dueAt;
    _bcTodoListItem.position = self.position;
    _bcTodoListItem.commentsCount = self.commentsCount;
    _bcTodoListItem.completed = self.completed;

    return _bcTodoListItem;
}
#pragma mark - CoreData RelationShip Methods
@end
