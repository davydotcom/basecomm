//
//  MilestoneFormViewController.m
//  basecamp
//
//  Created by David Estes on 4/11/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "MilestoneFormViewController.h"
#import "basecampAppDelegate.h"
#import <CoreData/CoreData.h>
#import "RWAssignee.h"
#import "RWDateSelector.h"
#import "Milestone.h"
#import "Project.h"

@implementation MilestoneFormViewController
@synthesize titleTextField,tableView,deadlineDate,assigneeID,assigneeName,assigneeType,notify,delegate,rwAssignee,rwDateSelector,milestone,project;
@synthesize titleViewCell,responsibleViewCell,deadlineViewCell,deadlineLabel,responsibleLabel;


- (void)dealloc
{
    [tableView release];
    [titleTextField release];
    [titleViewCell release];
    [responsibleLabel release];
    [responsibleViewCell release];
    [deadlineLabel release];
    [deadlineViewCell release];
    [assigneeName release];
    [assigneeID release];
    [assigneeType release];
    [deadlineDate release];
    [milestone release];
    [rwDateSelector release];
    [rwAssignee release];
    [project release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    UITableViewCell *_titleViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"urlViewCellIdentifier"];
    _titleViewCell.textLabel.text = @"Title";
    _titleViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITextField *_titleField = [[UITextField alloc] initWithFrame:CGRectMake(85,14,200,25)];
    _titleField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _titleField.font = [UIFont systemFontOfSize:12.0f];
    [_titleViewCell.contentView addSubview:_titleField];
    _titleViewCell.textLabel.textColor = [UIColor lightGrayColor];
    _titleViewCell.textLabel.font = [UIFont systemFontOfSize:12.0f];
    _titleField.placeholder = @"Give your milestone a name.";
    self.titleTextField = _titleField;
    [_titleField release];
    self.titleViewCell = _titleViewCell;
    [_titleViewCell release];
    
    UITableViewCell *_deadlineViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"urlViewCellIdentifier"];
    _deadlineViewCell.textLabel.text = @"Deadline";
    UILabel *_deadlineLabel = [[UILabel alloc] initWithFrame:CGRectMake(100,10,200,25)];
    _deadlineLabel.font = [UIFont systemFontOfSize:12.0f];
    _deadlineLabel.text = @"Today";
    _deadlineLabel.textColor = [UIColor colorWithRed:0.212f green:0.522f blue:0.776f alpha:1.0f];
    _deadlineViewCell.textLabel.textColor = [UIColor lightGrayColor];
    _deadlineViewCell.textLabel.font = [UIFont systemFontOfSize:12.0f];
    [_deadlineViewCell addSubview:_deadlineLabel];
    self.deadlineLabel = _deadlineLabel;
    [_deadlineLabel release];
    self.deadlineViewCell = _deadlineViewCell;
    [_deadlineViewCell release];
    
    UITableViewCell *_responsibleViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"urlViewCellIdentifier"];
    _responsibleViewCell.textLabel.text = @"Assignee";
    _responsibleViewCell.textLabel.textColor = [UIColor lightGrayColor];
    _responsibleViewCell.textLabel.font = [UIFont systemFontOfSize:12.0f];
    UILabel *_responsibleLabel = [[UILabel alloc] initWithFrame:CGRectMake(100,10,200,25)];
    _responsibleLabel.font = [UIFont systemFontOfSize:12.0f];
    _responsibleLabel.text = @"Me";
    _responsibleLabel.textColor = [UIColor colorWithRed:0.212f green:0.522f blue:0.776f alpha:1.0f];
    [_responsibleViewCell addSubview:_responsibleLabel];
    self.responsibleLabel = _responsibleLabel;
    [_responsibleLabel release];
    self.responsibleViewCell = _responsibleViewCell;
    [_responsibleViewCell release];
   
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.titleTextField becomeFirstResponder];
    if(milestone == nil)
    {
        self.titleTextField.text = @"";
        self.responsibleLabel.text = @"Me";
        self.deadlineLabel.text = @"Today";
        NSDate *now = [[NSDate alloc] init];
        self.deadlineDate = now;
        self.notify = NO;
        self.assigneeID = nil;
        self.assigneeName = nil;
        self.assigneeType = nil;
    }
    else
    {
        self.titleTextField.text = milestone.title;
        self.responsibleLabel.text = milestone.responsiblePartyName;
        self.assigneeID = milestone.responsiblePartyID;
        self.assigneeName = milestone.responsiblePartyName;
        self.assigneeType = milestone.responsiblePartyType;
        self.notify = [milestone.notify boolValue];
    }
    [super viewWillAppear:animated];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark - TableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];

    if([indexPath row] == 1)
    {
        [self.titleTextField resignFirstResponder];
        RWAssignee *_rwAssignee = [[RWAssignee alloc] init];
        self.rwAssignee = _rwAssignee;
        self.rwAssignee.hideAnyone = YES;
        self.rwAssignee.parentView = appDelegate.window;
        self.rwAssignee.delegate = self;
        [_rwAssignee release];
        rwAssignee.project = self.project;
        rwAssignee.assigneeID = self.assigneeID;
        rwAssignee.notifySwitch.on = self.notify;
        [rwAssignee slideIn];
    }
    else
    {        

        [self.titleTextField resignFirstResponder];
        basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        RWDateSelector *dateSelector = [[RWDateSelector alloc] initWithNibName:@"RWDateSelector" bundle:nil];
        self.rwDateSelector = dateSelector;
        self.rwDateSelector.dueDate = self.deadlineDate;
        dateSelector.parentView = appDelegate.window;
        dateSelector.delegate = self;
        [dateSelector slideIn];
        [dateSelector release];
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark - TableView DataSource Methods
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if([indexPath row] == 0)
        {
            return self.titleViewCell;
        }
        else if([indexPath row] == 1)
        {
            return self.responsibleViewCell;
        }
        else
        {
            return self.deadlineViewCell;   
        }
}
#pragma mark RWDateSelectorDelegate Methods
-(void)rwDateSelector:(RWDateSelector *)_rwDateSelector didSelectDate:(NSDate*)_dueDate
{
    self.deadlineDate = _dueDate;
    if(self.deadlineDate == nil)
    {
        NSDate *now = [[NSDate alloc] init];
        self.deadlineDate = now;
        [now release];
    }

    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"EdMMM,y" options:0
                                                              locale:[NSLocale currentLocale]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formatString];
    self.deadlineLabel.text = [dateFormatter stringFromDate:self.deadlineDate];
    [self.titleTextField becomeFirstResponder];
}
-(void)rwAssignee:(RWAssignee *)_rwAssignee didSelect:(NSNumber *)_id ofType:(NSString*)_type withName:(NSString *)_name notify:(BOOL)_notify
{   
    if([_id integerValue] == 0)
    {
        [self.titleTextField becomeFirstResponder];
        return;
    }
    self.assigneeID = _id;
    self.assigneeType = _type;
    self.assigneeName = _name;
    self.notify = _notify;
    self.responsibleLabel.text = assigneeName;
    [self.titleTextField becomeFirstResponder];
}
#pragma mark - IBAction Methods
-(IBAction)cancelButtonPressed:(id)sender
{
    if(delegate != nil && [delegate respondsToSelector:@selector(didCancelMilestoneFormViewController:)])
    {
        self.milestone = nil;
        [delegate didCancelMilestoneFormViewController:self];
    }
}
-(IBAction)saveButtonPressed:(id)sender
{
    if(self.titleTextField.text == nil || [self.titleTextField.text isEqualToString:@""])
    {
        //Alert that text view cant be blank!
        return;
    }
    if(self.milestone == nil)
    {
        //Create new Todo List Item !
        Milestone *_milestone = [NSEntityDescription insertNewObjectForEntityForName:@"Milestone" inManagedObjectContext:[self.project managedObjectContext]];

        _milestone.project = self.project;
        _milestone.title = self.titleTextField.text;
        _milestone.responsiblePartyType = assigneeType;
        _milestone.responsiblePartyName = assigneeName;
        _milestone.responsiblePartyID = assigneeID;
        _milestone.deadline = self.deadlineDate;
        NSNumber *yesNum = [[NSNumber alloc] initWithBool:YES];
        NSNumber *noNum = [[NSNumber alloc] initWithBool:NO];
        if([self.assigneeID integerValue] > 0)
        {
            if(self.notify)
            {
                _milestone.notify = yesNum;
            }
            else
            {
                _milestone.notify = noNum;
            }
        }
        
        _milestone.synced = noNum;
        [noNum release];
        [yesNum release];
        self.milestone = _milestone;
        [[_milestone managedObjectContext] save:nil];
        [milestone syncMilestone];
    }
    else
    {
        //Save Current
        milestone.title = self.titleTextField.text;
        milestone.deadline = self.deadlineDate;
        milestone.responsiblePartyType = assigneeType;
        milestone.responsiblePartyName = assigneeName;
        milestone.responsiblePartyID = assigneeID;
        NSNumber *yesNum = [[NSNumber alloc] initWithBool:YES];
        NSNumber *noNum = [[NSNumber alloc] initWithBool:NO];
        if([self.assigneeID integerValue] > 0)
        {
            if(self.notify)
            {
                milestone.notify = yesNum;
            }
            else
            {
                milestone.notify = noNum;
            }
        }
        
        milestone.synced = noNum;
        [noNum release];
        [yesNum release];
        [[milestone managedObjectContext ] save:nil];
        [milestone syncMilestone];
        
    }
    if(delegate != nil && [delegate respondsToSelector:@selector(milestoneFormViewController:didSaveMilestone:)])
    {
        [delegate milestoneFormViewController:self didSaveMilestone:self.milestone];
        self.milestone = nil;
        self.project = nil;
    }
}
@end
