//
//  TodoItemCompleteTableViewCell.h
//  basecamp
//
//  Created by David Estes on 3/21/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TodoItemCompleteTableViewCell : UITableViewCell {
    UIButton *checkButton;
    UILabel *titleLabel;   
}
@property(nonatomic, retain) UIButton *checkButton;
@property(nonatomic, retain) UILabel *titleLabel;
@end
