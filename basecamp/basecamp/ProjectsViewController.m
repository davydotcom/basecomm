//
//  ProjectsViewController.m
//  basecamp
//
//  Created by David Estes on 3/10/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import "ProjectsViewController.h"
#import "BCAccount.h"
#import "BCProject.h"
#import "ProjectSyncController.h"
#import "Project.h"
#import "BAccount.h"
#import "Person.h"
#import "basecampAppDelegate.h"
#import "SyncController.h"

#import "ProjectDashboardViewController.h"
#import <CoreData/CoreData.h>
@implementation ProjectsViewController
@synthesize projects,account,fetchedResultsController;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"Projects";
        [self.tabBarItem setImage:[UIImage imageNamed:@"icon-projects.png"]];
        // Custom initialization

    }
    return self;
}

- (void)dealloc
{
    [account release];
    [projects release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Projects";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (void)viewWillAppear:(BOOL)animated
{
    showNavigationBar = YES;
    [self registerForNotifications];
    if(self.account != nil)
    {
        [self fetchedResultsController];
        [[self fetchedResultsController] performFetch:nil];
        
    }
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [super viewWillAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if(!showNavigationBar)
    {
        [self.tabBarController.navigationController setNavigationBarHidden:YES animated:YES];
        showNavigationBar = YES;
    }
    [self unregisterForNotifications];
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ProjectsCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:13.0f];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    // Configure the cell...
    Project *_project = (Project *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = _project.name;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Project *_project = (Project *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    ProjectDashboardViewController *_projectDashboardViewController = [[ProjectDashboardViewController alloc] initWithNibName:@"ProjectDashboardViewController" bundle:nil];
    _projectDashboardViewController.project = _project;
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    showNavigationBar = NO;
    [appDelegate.mainNavigationController pushViewController:_projectDashboardViewController.projectTabBarController animated:YES];
    [_projectDashboardViewController release];
}

#pragma mark - NSNotificationCenter Methods
-(void)registerForNotifications
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(updateProjects:) name:PROJECTS_DID_SYNC_NOTIFICATION object:nil];
   [nc addObserver:self selector:@selector(updateProjects:) name:PEOPLE_DID_SYNC_NOTIFICATION object:nil];
}
-(void)unregisterForNotifications
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];

    [nc removeObserver:self name:PROJECTS_DID_SYNC_NOTIFICATION object:nil];
    [nc removeObserver:self name:PEOPLE_DID_SYNC_NOTIFICATION object:nil];
}
-(void)updateProjects:(NSNotification *)notification
{
    NSLog(@"----====UPDATING PROJECTS====----");
    [[self fetchedResultsController] performFetch:nil];
    [self doneLoadingTableViewData];
//    [self.tableView reloadData];


}
- (void)reloadTableViewDataSource{
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(appDelegate.syncController.finished == YES)
    {
        ProjectSyncController *_projectSyncController = [[ProjectSyncController alloc] initWithAccount:self.account];
        [_projectSyncController startSyncing];
        [_projectSyncController release];
    }
}
#pragma mark - Table view delegate


#pragma mark -
#pragma mark CoreData methods
-(NSFetchedResultsController *)fetchedResultsController {
    if(fetchedResultsController != nil)
    {
        return fetchedResultsController;
    }
    basecampAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:[appDelegate managedObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"account == %@",account];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"status" ascending:YES];
    
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[appDelegate managedObjectContext] sectionNameKeyPath:@"status" cacheName:nil];
    
    self.fetchedResultsController = _fetchedResultsController;
    [_fetchedResultsController release];   
    [sortDescriptor release];
    [fetchRequest release];
    return fetchedResultsController;
}
@end
