//
//  AccountDashboardViewController.h
//  basecamp
//
//  Created by David Estes on 3/12/11.
//  Copyright 2011 Redwind Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ProjectsViewController;
@class TodoSummaryViewController;
@class BAccount;
@interface AccountDashboardViewController : UITableViewController <UITabBarControllerDelegate>  {
    BAccount *account;
    UITabBarController *accountTabBarController;
    ProjectsViewController *projectsViewController;
    TodoSummaryViewController *todoSummaryViewController;
}
@property(nonatomic, retain) BAccount *account;
@property(nonatomic, retain) UITabBarController *accountTabBarController;
@property(nonatomic, retain) ProjectsViewController *projectsViewController;
@property(nonatomic, retain) TodoSummaryViewController *todoSummaryViewController;
@end
