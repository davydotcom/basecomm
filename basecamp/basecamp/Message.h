//
//  Message.h
//  basecamp
//
//  Created by David Estes on 6/11/11.
//  Copyright (c) 2011 Redwind Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#define DID_REMOVE_MESSAGE_NOTIFICATION @"DID_REMOVE_MESSAGE_NOTIFICATION"
#define MESSAGES_DID_SYNC_NOTIFICATION @"MESSAGES_DID_SYNC_NOTIFICATION"
@class Comment, Milestone, Person, Project;

@interface Message : NSManagedObject {
@private
}
@property (nonatomic, retain) NSNumber * messageID;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSString * displayBody;
@property (nonatomic, retain) NSDate * postedOn;
@property (nonatomic, retain) NSDate * commentedAt;
@property (nonatomic, retain) NSString * authorName;
@property (nonatomic, retain) NSNumber * commentsCount;
@property (nonatomic, retain) NSNumber * attachmentsCount;
@property (nonatomic, retain) NSNumber * useTextile;
@property (nonatomic, retain) NSNumber * isPrivate;
@property (nonatomic, retain) Milestone *milestone;
@property (nonatomic, retain) Project *project;
@property (nonatomic, retain) NSManagedObject *category;
@property (nonatomic, retain) Person *author;
@property (nonatomic, retain) NSNumber * synced;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSSet *comments;
+(NSArray *)allMessagesForProject:(Project *)_project WithManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;
+(Message *)findMessageByID:(NSInteger)_messageID withManagedObjectContext:(NSManagedObjectContext*)_managedObjectContext;


@end

@interface Message (CoreDataGeneratedAccessors)
- (void)addCommentsObject:(Comment *)value;
- (void)removeCommentsObject:(Comment *)value;
- (void)addComments:(NSSet *)value;
- (void)removeComments:(NSSet *)value;

@end
